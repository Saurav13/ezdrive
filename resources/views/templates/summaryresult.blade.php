<html>
<head>
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>


    <table class="table">
        <thead>
            <th>S.N.</th>
            <th>Roll No.</th>
            <th>Name</th>
            @foreach($results->groupBy('collection_id') as $r)
                <th>{{$r->first()->collection->name}}</th>
            @endforeach

        </thead>
        <tbody>
            @foreach($students as $student)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td> {{ $student->roll_no }}</td>
                    <td>{{$student->name}}</td>
                    @foreach($results as $r)
                        <?php $record = $student->records()->where('result_id',$r->id)->first() ?>
                        <td>{{ $record ? $record->grade : '-' }}</td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>