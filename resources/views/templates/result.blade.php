<html>
<head>
<title>Results of {{ $resultrecords->first() ? $resultrecords->first()->result->collection->name : '' }}</title>
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>

<div style="text-align:center;margin:25px">
	<h3>Results of {{ $resultrecords->first() ? $resultrecords->first()->result->collection->name : '' }}</h3>
</div>
<table class="table">
    <thead>
        <th>SN</th>
        <th>Roll No.</th>
        <th>Name</th>
        <th>Marks</th>

    </thead>
    <tbody>
        
        @foreach($resultrecords as $r)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $r->rollno }}</td>
            <td>{{$r->name}}</td>
            <td>{{$r->grade}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>