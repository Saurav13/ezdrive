@extends('layouts.admin')

@section('body')
       
    <div class="content-body">
        <p class="pull-right">Total Entry: {{$college_count}}</p>
        <form action="/admin/colleges/search" method="GET">
    		<input type="text" class="form-control" name="key" placeholder="Enter name"/>
    		<input type="submit" class="btn btn-primary" value="Search"/>
    	</form>
        <section id="addcolleges">  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title" ><a data-action="collapse"><h3>Colleges</h3><h6>Add, Edit or Remove College Names</h6>  </a></div>
                            <a class="heading-elements-toggle">
                                <i class="icon-ellipsis font-medium-3"></i>
                            </a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body"> <!-- // collapse -->
                            <div class="card-block">
                                <div class="row col-md-12">
                                    <div style="padding-bottom:20px;">
                                        <form class="form" method="POST" action="{{ route('colleges.store') }}">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="form-body">
                                                    <div class="form-group col-sm-3">
                                                        <input type="text" class="form-control" placeholder="Add New College" name="name" required>
                                                    </div>

                                                    <div class="form-group  col-sm-3">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="icon-check2">Add College</i> 
                                                        </button>
                                                    </div>  
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(count($colleges) == 0)
                                            <h6 style="text-align:  center;margin: 50px;">No college added yet.</h6>
                                        @else
                                            <div class="table-responsive ">
                                                
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 40%">College Name</th>
                                                            <th style="width: 20%">Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($colleges as $c)
                                                            <tr>
                                                                <td class="text-middle">
                                                                    <h4 id="collegeName{{ $c->id }}">{{$c->name}} </h4>
                                                                    
                                                                    <div id="ColupdateDiv{{$c->id}}" hidden>
                                                                        <form class="form" method="POST" action="{{ route('colleges.update',$c->id) }}">
                                                                            <div class="row form-body">
                                                                                <div class="form-group col-sm-6">
                                                                                    {{csrf_field()}}
                                                                                    <input hidden name="_method" value="PATCH">

                                                                                    <input type="text" class="form-control" placeholder="Edit Name" name="name" value="{{ $c->name }}" required> 
                                                                                </div>
                                                                                <div class="form-group col-sm-6">
                                                                                    <button type="submit" class="btn btn-blue"> 
                                                                                        <i class="icon-check2"></i> 
                                                                                    </button>

                                                                                    <button class="btn btn-red" type="button" id="Colcancel{{ $c->id }}">
                                                                                        <i class="icon-cross"></i>
                                                                                    </button>          
                                                                                </div>
                                                                            </div> 
                                                                        </form>
                                                                    </div>
                                                                </td>
                                                            
                                                                <td class="text-middle">
                                                                    <a href="javascript:void(0)"  class="btn btn-outline-warning" style="" id="Coledit{{ $c->id }}">Edit</a>
                                                                
                                                                    <form action="{{ route('colleges.destroy',$c->id) }}" method="POST" style="display:inline">
                                                                        {{ csrf_field() }}
                                                                        <input hidden name="_method" value="DELETE"/>
                                                                        <button type="button" id="deleteCol{{ $c->id }}" class="btn btn-outline-danger">Delete</button>
                                                                    </form>

                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        @endif
                                        <div class="text-xs-center mb-3">
                                            <nav aria-label="Page navigation">
                                                {{ $colleges->links() }}
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')

<script type="text/javascript">
    $(document).ready(function() {
        $("[id*='Coledit']").click(function(){
            var id = $(this).attr("id").slice(7);
            $('#collegeName'+id).attr("hidden","true");
            $("#ColupdateDiv"+id).removeAttr("hidden");
        });

        $("[id*='Colcancel']").click(function(){
            var id = $(this).attr("id").slice(9);
            $('#collegeName'+id).removeAttr("hidden");
            $("#ColupdateDiv"+id).attr("hidden","true");
        });
    });
</script>
@endsection