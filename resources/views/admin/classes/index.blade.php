@extends('layouts.admin')

@section('body')
       
    <div class="content-body">
        <section id="addclasses">  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title" ><a data-action="collapse"><h3>Classes</h3></a></div>
                            <a class="heading-elements-toggle">
                                <i class="icon-ellipsis font-medium-3"></i>
                            </a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="expand" title="Expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body"> <!-- // collapse -->
                            <div class="card-block">
                                <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-md-6">
                                        <form method="GET" action="{{ route('classes.searchByName') }}">
                                            <input type="text" class="form-control" name="key" placeholder="Search By Name" required/>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form method="GET" action="{{ route('classes.searchByCode') }}">
                                            <input type="text" class="form-control" name="key" placeholder="Search By Code" required/>
                                        </form>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        @if(count($classes) == 0)
                                            <h6 style="text-align:  center;margin: 50px;">No classes added yet.</h6>
                                        @else
                                            <div class="table-responsive ">
                                                
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Class Name</th>
                                                            <th>Owner</th>
                                                            <th>Details</th>
                                                            <th style="width: 18%;">Class Max Space</th>
                                                            <th style="width: 15%;">Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($classes as $c)
                                                            <tr>
                                                                <td class="text-middle">{{ $c->name }}</td>
                                                                <td class="text-middle">{{ $c->owner->data()->name }}</td>
                                                                <td class="text-middle">
                                                                    <b>Category:</b> Engineering<br>
                                                                    <b>College:</b> {{ $c->college }}<br>
                                                                    <b>Type:</b> {{ $c->type }}<br>
                                                                    <b>Info:</b> {{ $c->detail }}
                                                                </td>
                                                                <td class="text-middle">
                                                                    <p id="classSpace{{ $c->id }}">{{ $c->max_space/(1024*1024*1024) }} GB</p> 
                                                                    
                                                                    <div id="classupdateDiv{{$c->id}}" hidden>
                                                                        <form class="form" method="POST" id="classupdateForm{{$c->id}}" action="{{ route('classes.update',$c->id) }}">
                                                                            <div class="form-body">
                                                                                <div class="form-group">
                                                                                    {{csrf_field()}}
                                                                                    <input hidden name="_method" value="PATCH">

                                                                                    <input type="text" id="editSpace{{$c->id}}" class="form-control" name="space" value="{{ $c->max_space/(1024*1024*1024) }}" style="width: 50px; display: inline;margin-right:  5px;" required>
                                                                                
                                                                                    <button type="submit" class="btn btn-sm btn-blue"> 
                                                                                        <i class="icon-check2"></i> 
                                                                                    </button>

                                                                                    <button class="btn btn-sm btn-red" type="button" id="classCancel{{ $c->id }}">
                                                                                        <i class="icon-cross"></i>
                                                                                    </button>          
                                                                                </div>
                                                                            </div> 
                                                                        </form>
                                                                    </div>
                                                                </td>
                                                            
                                                                <td class="text-middle">
                                                                    <a href="javascript:void(0)"  class="btn btn-outline-warning" style="" id="classEdit{{ $c->id }}"><i class="icon-edit"></i></a>
                                                                
                                                                    <form action="{{ route('classes.destroy',$c->id) }}" method="POST" style="display:inline">
                                                                        {{ csrf_field() }}
                                                                        <input hidden name="_method" value="DELETE"/>
                                                                        <button type="button" id="deleteClass{{ $c->id }}" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                                                    </form>

                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        @endif
                                        <div class="text-xs-center mb-3">
                                            <nav aria-label="Page navigation">
                                                {{ $classes->links() }}
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')

<script type="text/javascript">
    $(document).ready(function() {
        $("[id*='classEdit']").click(function(){
            var id = $(this).attr("id").slice(9);
            $('#classSpace'+id).attr("hidden","true");
            $("#classupdateDiv"+id).removeAttr("hidden");
        });

        $("[id*='classCancel']").click(function(){
            var id = $(this).attr("id").slice(11);
            $('#classSpace'+id).removeAttr("hidden");
            $("#classupdateDiv"+id).attr("hidden","true");
        });

        $("[id*='classupdateForm']").submit(function(e){
            e.preventDefault();

            var id = $(this).attr("id").slice(15);
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                beforeSend: function(){
                    $(this).find('button').each(function() {
                        $(this).attr('disabled','disabled');
                    });
                },
                success: function(response) {
                    $('#classSpace'+id).text($('#editSpace'+id).val()+ ' GB');
                    $('#classSpace'+id).removeAttr("hidden");
                    $("#classupdateDiv"+id).attr("hidden","true");
                    swal(
                        'Success',
                        'Class Updated Successfully',
                        'success'
                    );
                },
                error: function(xhr){
                    if(xhr.status == 422){
                        $('#editSpace'+id).val(1);
                        swal(
                            'Error',
                            xhr.responseJSON.errors.space[0],
                            'error'
                        );
                    }
                    else{
                        location.reload();
                    }
                },
                complete: function(){
                    $(this).find('button').each(function() {
                        $(this).removeAttr('disabled');
                    });
                }
            });
        })
    });
</script>
@endsection