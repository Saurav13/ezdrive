@extends('layouts.admin')

@section('body')
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    
    #img-upload{
        height:30%;
        width: 30%;
        text-align: center;
        padding-top:10px;
        }
</style>
<link href="{{ asset('admin-assets/select2.css') }}" rel="stylesheet" />

    <div class="content-body">
        <section>  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class=" {{ count($errors)>0 ? 'icon-minus4' : 'icon-plus4' }}" aria-hidden="true"></i> Add Box Ads</button></a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class=" {{ count($errors)>0 ? 'icon-minus4' : 'icon-plus4' }}"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                            
                        <div class="card-body collapse {{ count($errors)>0 ? 'in' : '' }}">
                            <div class="card-block ">
                                <form class="form" method="POST" action="{{route('boxAds.store')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="form-body ">
                                            <div class="form-group col-sm-12">
                                                <label>Target</label>
                                                <div class="input-group">
                                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                                        <input type="radio" name="target" value="Teacher" {{ old('target') ? (old('target') == 'Teacher' ? 'checked' : '') : 'checked' }} class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description ml-0">Teacher</span>
                                                    </label>
                                                    <label class="display-inline-block custom-control custom-radio">
                                                        <input type="radio" name="target" value="Student" {{ old('target') ? (old('target') == 'Student' ? 'checked' : '') : '' }} class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description ml-0">Student</span>
                                                    </label>
                                                    <label class="display-inline-block custom-control custom-radio">
                                                        <input type="radio" name="target" value="Both" {{ old('target') ? (old('target') == 'Both' ? 'checked' : '') : '' }} class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description ml-0">Both</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label for="selectcategories">Faculty</label>
                                                <select id="selectcategories" class="form-control" name="faculty[]" class="js-example-basic-multiple" multiple="multiple" width="100%">
                                                    @foreach($faculties as $c)
                                                        <option value="{{ $c->id }}"  {{ old('faculty') ? (in_array($c->id,old('faculty')) ? 'selected' : '') : '' }}>{{ $c->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-sm-12">
                                                <label for="selectcolleges">College</label>
                                                <select id="selectcolleges" class="form-control" name="college[]" class="js-example-basic-multiple" multiple="multiple" width="100%">
                                                    @foreach($colleges as $c)
                                                        <option value="{{ $c->id }}"  {{ old('college') ? (in_array($c->id,old('college')) ? 'selected' : '') : '' }}>{{ $c->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label>Year</label>
                                                <div class="input-group">
                                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                                        <input type="checkbox" name="year[]" value="Current" {{ old('year') ? (in_array('Current',old('year')) ? 'checked' : '') : 'checked' }} class="chk-remember">
                                                        <span class="custom-control-description ml-0">Current</span>
                                                    </label>
                                                    <label class="display-inline-block custom-control custom-radio">
                                                        <input type="checkbox" name="year[]" value="Pass Out" {{ old('year') ? (in_array('Pass Out',old('year')) ? 'checked' : '') : '' }} class="chk-remember">
                                                        <span class="custom-control-description ml-0">Pass Out</span>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label for="link">Link</label>
                                                <input type="text" id="link" class="form-control" placeholder="Link - http://www." name="link" value="{{old('link')}}" required>
                                            </div>
                                            
                                            <div class="form-group col-sm-12">
                                                <label for="timesheetinput3">Expiry Date</label>
                                                <div class="position-relative has-icon-left">
                                                    <input type="date" id="timesheetinput3" class="form-control" value="{{old('expiry_date')}}" name="expiry_date" required>
                                                    <div class="form-control-position">
                                                        <i class="icon-calendar5"></i>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="form-group col-sm-12">
                                                    
                                                <label for="projectinput8">Content</label>
                                                <textarea id="projectinput8" rows="5" class="form-control" name="content" placeholder="Ad content"></textarea>
                                            
                                            </div>
            
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-md btn-success" id="add">
                                                    <i class="icon-check2">Add</i> 
                                                </button>
                                            </div>  
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <div style="float:right">
            <a href="{{ route('boxAds.expiredAds') }}" class="btn btn-md btn-primary mb-1">See Expired Ads</a>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><a data-action="collapse">Box Ads</a></h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        @if(count($ads) == 0)
                            <h6 style="text-align:  center;margin: 50px;">No ads added yet.</h6>
                        @else
                            <div class="table-responsive">
                                <table class="table mb-0">
                                    <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Target</th>
                                            <th>Faculties</th>
                                            <th>Colleges</th>
                                            <th>Year</th>
                                            <th>Content</th>
                                            <th>Expiry Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ads as $ad)
                                            <tr>
                                                <td>{{ $loop->iteration * $ads->currentPage() }}</td>
                                                <td>{{ $ad->target }}</td>
                                                <td>
                                                    @foreach($ad->faculties as $c)
                                                        <div class="tag tag-info" style="margin:2px">{{ $c->name }}</div>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach($ad->colleges as $c)
                                                        <div class="tag tag-success" style="margin:2px">{{ $c->name }}</div>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @foreach(json_decode($ad->year) as $c)
                                                        <div class="tag tag-default" style="margin:2px">{{ $c }}</div>
                                                    @endforeach
                                                </td>
                                            <td>{{substr($ad->content,0,20).'...'}}</td>
                                                <td>{{ date('M j,Y',strtotime($ad->expiry_date)) }}</td>
                                                <td>
                                                    <a href="{{ route('boxAds.edit',$ad->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                                    <form action="{{ route('boxAds.destroy',$ad->id) }}" method="POST" style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE"/>
                                                        <button type="button" id="deleteAd{{$ad->id}}" class="btn btn-sm btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach   
                                    </tbody>
                                </table>
                            </div>
                        @endif
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $ads->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('admin-assets/select2.js') }}"></script>
   
    <script>
        $(document).ready( function() {
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });
    
            $('.btn-file :file').on('fileselect', function(event, label) {
                
                var input = $(this).parents('.input-group').find(':text'),
                    log = label;
                
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }
    
            $("#imgInp").change(function(){
                readURL(this);
            });

            $('#selectcategories').select2({
                placeholder: "Select Categories",
                width: '100%',
                height: '45px',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                
            });

            $('#selectcolleges').select2({
                placeholder: "Select College Names",
                width: '100%',
                height: '45px',
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                
            }); 	
        });
    </script>


@endsection