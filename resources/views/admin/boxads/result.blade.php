@extends('layouts.admin')

@section('body')

    <div class="content-body">
        <section>  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse">Results for Box Ads</a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a href="{{ route('boxAds.index') }}"><i class="icon-arrow-left"></i></a></li>
                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                            
                        <div class="card-body collapse in">                    
                            @if(count($ads) == 0)
                                <h6 style="text-align:  center;margin: 50px;">No expired box ads.</h6>
                            @else
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Target</th>
                                                <th>Categories</th>
                                                <th>Colleges</th>
                                                <th>Year</th>
                                                <th>Image</th>
                                                <th>Expiry Date</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ads as $ad)
                                                <tr>
                                                    <td>{{ $loop->iteration * $ads->currentPage() }}</td>
                                                    <td>{{ $ad->target }}</td>
                                                    <td>
                                                        @foreach($ad->categories as $c)
                                                            <div class="tag tag-info" style="margin:2px">{{ $c->name }}</div>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        @foreach($ad->colleges as $c)
                                                            <div class="tag tag-success" style="margin:2px">{{ $c->name }}</div>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        @foreach(json_decode($ad->year) as $c)
                                                            <div class="tag tag-default" style="margin:2px">{{ $c }}</div>
                                                        @endforeach
                                                    </td>
                                                    <td><a href="{{ $ad->link }}" target="_blank" title="{{ $ad->link }}"><img src="{{ asset('boxad_images/'.$ad->image) }}" width="50px" height="50px"/></a></td>
                                                    <td>{{ date('M j,Y',strtotime($ad->expiry_date)) }}</td>
                                                    <td>
                                                        <a href="{{ route('boxAds.edit',$ad->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                                        <form action="{{ route('boxAds.destroy',$ad->id) }}" method="POST" style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="_method" value="DELETE"/>
                                                            <button type="button" id="deleteAd{{$ad->id}}" class="btn btn-sm btn-danger">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                            <div class="text-xs-center mb-3">
                                <nav aria-label="Page navigation">
                                    {{ $ads->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection