@extends('layouts.admin')

@section('body')
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    
    #img-upload{
        height:30%;
        width: 30%;
        text-align: center;
        padding-top:10px;
        }
</style>

    <div class="content-body">
        <section>  
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" ><a data-action="collapse"><button  class="btn btn-md btn-primary"><i class="icon-minus4" aria-hidden="true"></i> Edit Banner Ads</button></a></h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a href="{{ route('bannerAds.index') }}"><i class="icon-arrow-left"></i></a></li>
                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                            
                        <div class="card-body collapse in">                    
                            <div class="card-block ">
                                <form class="form" method="POST" action="{{route('bannerAds.update',$ad->id)}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="PATCH"/>
                                    <div class="row">
                                        <div class="form-body">
                                            <div class="form-group col-sm-12">
                                                <label for="title">Title</label>
                                                <input type="text" id="title" class="form-control" placeholder="Title of ad" name="title" value="{{ $ad->title }}" required>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <label for="link">Link</label>
                                                <input type="text" id="link" class="form-control" placeholder="Link - http://www." name="link" value="{{ $ad->link }}" required>
                                            </div>
                                            
                                            <div class="form-group col-sm-12">
                                                <label for="timesheetinput3">Expiry Date</label>
                                                <div class="position-relative has-icon-left">
                                                    <input type="date" id="timesheetinput3" class="form-control" value="{{ $ad->expiry_date }}" name="expiry_date" required>
                                                    <div class="form-control-position">
                                                        <i class="icon-calendar5"></i>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row col-sm-12">
                                                <div class="form-group col-sm-6" style="margin-bottom:0px;">
                                                    <label>Upload Image (Optional)</label>
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse images… <input type="file" name="image" id="imgInp">
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" name="image" readonly>
                                                    </div>
                                                    <img style="padding-bottom: 15px;" id='img-upload'/>
                                                </div>
                                            </div>
            
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-md btn-success" id="add">
                                                    <i class="icon-check2">Save</i> 
                                                </button>
                                            </div>  
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready( function() {
            var max = $('#category').find(":selected").attr('data-count');
            $('#quantity').attr('max',max);

            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });
    
            $('.btn-file :file').on('fileselect', function(event, label) {
                
                var input = $(this).parents('.input-group').find(':text'),
                    log = label;
                
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }
                    
                    reader.readAsDataURL(input.files[0]);
                }
            }
    
            $("#imgInp").change(function(){
                readURL(this);
            }); 

            $('#category').on('change',function(){
                max = $(this).find(":selected").attr('data-count');
                $('#quantity').attr('max',max);
            });	
        });
    </script>


@endsection