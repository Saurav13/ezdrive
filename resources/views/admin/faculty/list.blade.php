@extends('layouts.admin')
@section('body')
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Sub Faculties</h2><br>
                <button type="button" class="btn btn-primary btn-min-width " style="float:right;" data-toggle="modal" data-target="#createform">
                        Create
                    </button>
               
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="table-responsive">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                                                    <th>name</th>
                                                                    <th>category</th>
                                                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($faculty as $no)
                                <tr>
                                    <th scope="row">{{ $loop->iteration + (($faculty->currentPage()-1)*$faculty->perPage()) }}</th>
                                    <td><text id="facultyname{{$no->id}}">{{$no->name}}</text></td>
                                    <td><text id="facultycategory_id{{$no->id}}">{{$no->category->name}}</text></td>
                                                                    
                                    <td>
                                    <button faculty_id="{{$no->id}}" class="btn btn-info" type="button" data-toggle="modal" id="faculty_edit{{$no->id}}" data-target="#editform">Edit</button>
                                    <form action="/admin/faculty/{{$no->id}}" method="post" style="display:  inline;">
                                        {{csrf_field()}}
                                        <input name="_method" id ="delete" type="hidden" value="DELETE">
                                        <button class="btn btn-danger"id="delete" type ="submit" value="delete">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $faculty->links() }}
                            </nav>
                        </div>
                
                    </div>
               
                </div>
            </div>
        </div>
    </div>
</div>
  <!-- Table head2 options end -->
    <div class="card-body collapse in">
		<div class="card-block">
			<div class="row my-2">
			    <div class="col-lg-4 col-md-6 col-sm-12">
					<div class="form-group">
                
                
<!-- notes ADDING  FORM Modal 2-->
                            <div class="modal fade text-xs-left" id="createform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <label class="modal-title text-text-bold-600" id="myModalLabel33">Add The Details</label>
                                        </div>
                                        <form class= "Form" action="/admin/faculty/" method = "POST"  files="true" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="modal-body">
                                                <label>Name</label>
                                                                                                                                                                    <label for="projectinput8">name</label>
                                                        <div class="form-group">
                                                            <input type="text"  class="form-control" placeholder="Enter name" name="name">
                                                        </div>
                                                                               <label>Category</label>                                                                                                                                             <label for="projectinput8">category_id</label>
                                                        <div class="form-group">
                                                            <select class="form-control" name="category_id">
                                                                <option selected disabled>Select Category</option>
                                                                @foreach($categories as $c)
                                                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                                                                                                                             
                                                <div class="modal-footer">
                                                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                                                    <input type="submit" class="btn btn-outline-primary btn-lg" value="submit">
                                                </div>
                                        </form>		
                                    </div>
                                </div>

                            </div>
                         </div>
                    </div>
                </div>
            </div>
       
        </div>
    </div>
    </div>
   <div class="card-body collapse in">
		<div class="card-block">
			<div class="row my-2">
			    <div class="col-lg-4 col-md-6 col-sm-12">
					<div class="form-group">
                
                

                
<!-- notes Modaledit the details 1_4 -->
                            <div class="modal fade text-xs-left" id="editform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit The subjects</label>
                                        </div>
                                        <form class= "Form"  id="editformmodal"action=""method = "post" files="true" enctype="multipart/form-data" >
                                            {{csrf_field()}}
                                           <input type="hidden" name="_method" value="PATCH">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    
								            	<input type="hidden"  id="facultyidmodaleditid" value="" class="form-control" placeholder="id" name="id">
                                                </div>
                                    
                                         
                                                                                                                                <div class="form-group">
                                                    <label for="formfacultynameedit">name</label>
                                                <input type="text" id="formfacultynameedit" class="form-control" placeholder="Enter name" name="name">
                                            </div>
                                                                                                                                                                              <div class="form-group">
                                                    <label for="formfacultycategory_idedit">category</label>
                                                    <select class="form-control" name="category_id">
                                                            <option selected disabled>Select Category</option>
                                                            @foreach($categories as $c)
                                                                <option value="{{$c->id}}">{{$c->name}}</option>
                                                            @endforeach
                                                        </select>
                                            </div>
                                                                                      							                
                                        <div class="modal-footer">
                                            <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                                            <input type="submit" class="btn btn-outline-primary btn-lg" value="submit">
                                        </div>
                                        </form>		
                                    </div>
                                </div>

                            </div>
                         </div>
                    </div>
                </div>
            </div>
       
        </div>
    </div>

                                           
    








</div>


@endsection
@section("js")
<script>
        
      $("[id*='delete']").click(function(e){
        var ele = this;
        e.preventDefault();
        
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this record!",
          type: "warning",

          showCancelButton: true,

        }).then(function(){
          ele.form.submit();
        }).catch(swal.noop);

      });
    
$('document').ready(function(){
    $('[id*= "faculty_edit"]').click(function(){
            var faculty_id = $(this).attr('faculty_id');
           // console.log(note_id);
                        var facultyname =$('#facultyname'+faculty_id).html();
                        var facultycategory_id =$('#facultycategory_id'+faculty_id).html();
                        $('#formfacultyidmodaleditid').val(faculty_id);
                        $('#formfacultynameedit').val(facultyname);
                        $('#formfacultycategory_idedit').val(facultycategory_id);
                        
            $('#editformmodal').attr("action","/admin/faculty/"+faculty_id);
           
    });
});
</script>
@endsection