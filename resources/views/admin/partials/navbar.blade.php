 <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
            <li class="nav-item"><a href="" class="navbar-brand nav-link"><img alt="branding logo" src="" class="brand-logo"></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
          </ul>
        </div>
        <div class="navbar-container content container-fluid">
          <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
            <ul class="nav navbar-nav">
              <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5">         </i></a></li>
              <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
            </ul>
            <ul class="nav navbar-nav float-xs-right">

              {{--  @if(Auth::user()->roles()->where('title', '=', 'Quotations')->exists())  --}} 
                {{-- <li class="dropdown dropdown-notification nav-item"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label" onclick="getUnseenQuote()"><i class="ficon icon-bell4"></i><span class="tag tag-pill tag-default tag-danger tag-default tag-up" id="qno1">{{ $mno }}</span></a>
                  <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                      <h6 class="dropdown-header m-0"><span class="grey darken-2">Quotations</span><span class="notification-tag tag tag-default tag-danger float-xs-right m-0" id="qno2">{{ $mno }} New</span></h6>
                    </li>
                    <li class="list-group scrollable-container" id="Qnotis">
                      
                    </li>
                    <li class="dropdown-menu-footer"><a href="" class="dropdown-item text-muted text-xs-center">Read all new quotations</a></li>
                  </ul>
                </li> --}}
              {{--  @endif
          
              @if(Auth::user()->roles()->where('title', '=', 'Contact Us Messages')->exists())  --}}
                {{-- <li class="dropdown dropdown-notification nav-item"><a href="#" onclick="getUnseenMsg()" data-toggle="dropdown" class="nav-link nav-link-label"><i class="ficon icon-mail6"></i><span class="tag tag-pill tag-default tag-info tag-default tag-up" id="mno1">{{ $mno }}</span></a>
                  <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                      <h6 class="dropdown-header m-0"><span class="grey darken-2">Contact Us Messages</span><span class="notification-tag tag tag-default tag-info float-xs-right m-0" id="mno2">{{ $mno }} New</span></h6>
                    </li>
                    <li class="list-group scrollable-container" id="Mnotis">
                      
                    </li>
                    <li class="dropdown-menu-footer"><a href="{{ route('contact-us-messages.unseen') }}" class="dropdown-item text-muted text-xs-center">Read all new messages</a></li>
                  </ul>
                </li> --}}
              
              
              <li class="dropdown dropdown-user nav-item"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class=""><i style="font-size:20px" class="icon-user-secret"></i></span><span class="user-name" id="adminName">{{ Auth::guard('admin_user')->user()->name }}</span></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="{{ route('profile') }}" class="dropdown-item"><i class="icon-head"></i> Edit Profile</a>
                  <div class="dropdown-divider"></div>
                  <a href="{{ url('/admin/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item">
                  <i class="icon-power3"></i> Logout</a>
                  <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <!-- main menu-->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      
      <!-- main menu content-->
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <br>
          <li id="dashboard" class="nav-item "><a href="{{ route('admin_dashboard') }}"><i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">Dashboard</span></a>
          </li>
      
          <li id="categories" class="nav-item "><a href="{{ route('categories.index') }}"><i class="icon-tags"></i><span data-i18n="nav.categories.main" class="menu-title">Faculties</span></a>
          </li>

          <li id="faculty" class="nav-item "><a href="{{ route('faculty.index') }}"><i class="icon-tags"></i><span data-i18n="nav.faculty.main" class="menu-title">Sub-Faculties</span></a>
          </li>
          <li id="colleges" class="nav-item "><a href="{{ route('colleges.index') }}"><i class="icon-graduation-cap"></i><span data-i18n="nav.colleges.main" class="menu-title">Colleges</span></a>
          </li>

          <li id="classes" class="nav-item "><a href="{{ route('classes.index') }}"><i class="icon-book2"></i><span data-i18n="nav.classes.main" class="menu-title">Classes</span></a>
          </li>
          
          <li class="nav-item"><a href="#"><i class="icon-buysellads"></i><span data-i18n="nav.ads.main" class="menu-title">Advertisements</span></a>
            <ul class="menu-content">
              <li id="bannerAds"><a href="{{ route('bannerAds.index') }}" data-i18n="nav.ads.main" class="menu-item">Banner Ads</a>
              </li>
              <li id="boxAds"><a href="{{ route('boxAds.index') }}" data-i18n="nav.ads.main" class="menu-item">Box Ads</a>
              </li>
            </ul>
          </li>
          {{-- <li id="categories" class="nav-item "><a href="{{ route('categories.index') }}"><i class="icon-android-contacts"></i><span data-i18n="nav.categories.main" class="menu-title">Categories</span></a>
          </li>

          <li id="colleges" class="nav-item "><a href="{{ route('colleges.index') }}"><i class="icon-image3"></i><span data-i18n="nav.colleges.main" class="menu-title">Colleges</span></a>
          </li>
          <li id="newsletter" class="nav-item "><a href="{{ route('newsletter') }}"><i class="icon-mail6"></i><span data-i18n="nav.newsletter.main" class="menu-title">Newsletter</span></a>
          </li>

          <li id="testimonials" class="nav-item "><a href="{{ route('testimonials.index') }}"><i class="icon-commenting"></i><span data-i18n="nav.testimonials.main" class="menu-title">Testimonials</span></a>
          </li>

          <li id="contact-us-messages" class="nav-item "><a href="{{ route('contact-us-messages.index') }}"><i class="icon-at2"></i><span data-i18n="nav.contact-us-messages.main" class="menu-title">Contact Messages</span></a>
          </li>

          <li id="settings" class="nav-item "><a href="{{ route('admin.settings') }}"><i class="icon-gear"></i><span data-i18n="nav.settings.main" class="menu-title">Settings</span></a>
          </li> --}}
        </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <!-- include includes/menu-footer-->
      <!-- main menu footer-->
    </div>
    <!-- / main menu-->
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
          {{--  This is lazy validation. but if needed put it in that specifuc blade only  --}}
          @if (count($errors)>0 )
            <div class="alert alert-dismissible fade in mb-2">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" style="color:black">&times;</span>
              </button>
              <ul class="list-group">
                @foreach ($errors->all() as $error)
                  <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
              </ul>
            </div>	
          @endif 