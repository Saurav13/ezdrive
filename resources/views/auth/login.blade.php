@extends('layouts.app')

@section('body')

<style>
.form-control{
    border-radius:3px;
}    

</style>

<div class="section" style="padding-top: 40px;padding-bottom: 0px;">
    <div class="container text-center">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-8">
                <h2 class="title">EzuClass Login</h2>
            </div>
        </div>
    </div>
</div>
<div class="section section-signup" style="background-image: url('assets/img/bg11.jpg'); background-size: cover; background-position: top center; min-height: 700px; padding-top: 22px;">
    <div class="container">
        <div class="row">
            <div class="card card-signup" style="background:#2385aa91">
                <form class="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                    <div class="header text-center">
                        <h4 class="title title-up">Login</h4>
                        <div class="social-line">
                            <a href="/signin/facebook" class="btn btn-neutral btn-facebook btn-icon btn-round" >
                                <i class="fab fa-facebook-f" style="margin-top: 0.6rem;"></i>
                            </a>
                           
                            <a href="/signin/google" class="btn btn-neutral btn-google btn-icon btn-round">
                                <i class="fab fa-google-plus-g" style="margin-top: 0.6rem;"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                            @if ($errors->has('email'))
                            <span class="help-block text-center">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </span>
                            <br>
                            
                            @endif 
                        <div class="input-group form-group-no-border{{ $errors->has('email') ? ' has-error' : '' }}">
                          
                                  
                            <input id="email" type="email" placeholder="Email..." class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                               
                        </div>
                        @if ($errors->has('password'))
                                
                            <span class="help-block text-center">
                                <strong>{!! $errors->first('password') !!}</strong>
                            </span>
                            <br>
                        @endif
                        <div class="input-group form-group-no-border{{ $errors->has('password') ? ' has-error' : '' }}">
                               
                                <input id="password" type="password" placeholder="Password..." class="form-control" name="password" required>

                               
                        </div>
                        
                      <div class="checkbox">
                          <input id="checkboxSignup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                              <label for="checkboxSignup">
                              Remember Me
                              </label>
                            </div> 
                    </div>

                    <div class="footer text-center">
                        <button type="submit" class="btn btn-neutral btn-round btn-lg hovergreen">Login</button>
                    </div>
                    <a class="btn btn-link" style="color:#c9ccd4;" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </form>
            </div>
        </div>
        <div class="col text-center">
            <a href="/" class="btn btn-simple btn-round btn-white btn-lg hovergreen" style="color: white;border-color: white;">Back to Home</a>
        </div>
    </div>
 </div>
{{--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('email') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">

                            <a class="btn btn-link" href="/signin/facebook">
                                Facebook
                            </a>
                            <a class="btn btn-link" href="/signin/google">
                                Google
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 --}}

@endsection
