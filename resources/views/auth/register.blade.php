@extends('layouts.app')

@section('body')

<style>
.form-control{
    border-radius:3px;
}    
</style>

<div class="section" style="padding-top: 40px;padding-bottom: 0px;">
    <div class="container text-center">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-8">
                <h2 class="title">Register</h2>
                </div>
        </div>
    </div>
</div>

<div class="section section-signup" style="background-image: url('assets/img/bg11.jpg'); background-size: cover; background-position: top center; min-height: 700px; padding-top: 22px;">
    <div class="container">
        <div class="row">
            <div class="card card-signup" style="background:#2385aa91">
                <form class="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                    <div class="header text-center">
                        <h4 class="title title-up">Sign Up</h4>
                        <div class="social-line">
                            <a href="/signin/facebook" class="btn btn-neutral btn-facebook btn-icon btn-round">
                                <i class="fab fa-facebook-f" style="margin-top: 0.6rem;"></i>
                            </a>
                            <a href="/signin/google" class="btn btn-neutral btn-google btn-icon btn-round">
                                <i class="fab fa-google-plus-g" style="margin-top: 0.6rem;"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        <div class="input-group form-group-no-border">
                            
                            <input id="name" type="text" class="form-control" placeholder="Your Name..." name="name" value="{{ old('name') }}" required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <span class="   help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <div class="input-group form-group-no-border">
                           
                            <input id="email" type="email" class="form-control" placeholder="Your Email..." name="email" value="{{ old('email') }}" required>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <div class="input-group form-group-no-border">
                            
                            <input id="password" type="password" placeholder="Password..." class="form-control" name="password" required>
                        </div>
                        <div class="input-group form-group-no-border">
                            <input id="password-confirm" placeholder="Confirm Password..." type="password" class="form-control" name="password_confirmation" required>
                        </div>
                       
                        
                       
                    </div>
                    <div class="footer text-center">
                        <button type="submit" class="btn btn-neutral btn-round btn-lg hovergreen">Get Started</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col text-center">
            <a href="/" class="btn btn-simple btn-round btn-white btn-lg hovergreen">Back to home</a>
        </div>
    </div>
</div>
{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
