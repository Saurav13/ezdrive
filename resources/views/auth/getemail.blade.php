@extends('layouts.app')

@section('body')

<style>
.form-control{
    border-radius:3px;
}    
</style>

<div class="section" style="padding-top: 40px;padding-bottom: 0px;">
    <div class="container text-center">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-8">
                <h2 class="title">Enter an Email</h2>
            </div>
        </div>
    </div>
</div>
<div class="section section-signup" style="background-image: url('/assets/img/bg11.jpg'); background-size: cover; background-position: top center; min-height: 700px; padding-top: 22px;">
    <div class="container">
        <div class="row">
            <div class="card card-signup" style="background:#2385aa; border-radius:21px; padding: 2rem; max-width:406px;">
            	<div class="photo-container" style="margin-bottom: 10px;">
            		<img src="{{ $user['photo'] }}" style="height:  123px;width:  123px;"/>
            	</div>
                <form class="form" method="POST" action="{{ route('set_email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ array_has($errors,'email') ? ' has-error' : '' }}">
                                @if (array_has($errors,'email'))
                                    <span class="help-block">
                                        <strong>{!! $errors['email'] !!}</strong>
                                    </span>
                                @endif
                                <input id="email" placeholder="enter email.." type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
    
                                    
                            </div>
                            
    
                            <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary"  style="background-color:#61adbd" >
                                        Submit
                                    </button>
                            </div> 
                   
                </form>
            </div>
        </div>
        <div class="col text-center">
            <a href="/" class="btn btn-simple btn-round btn-white btn-lg hovergreen" style="color: white;border-color: white;">Back to Home</a>
        </div>
    </div>
</div>
@endsection
