@extends('layouts.app')

@section('body')
<style>
.form-control{
    border-radius:3px;
}    
</style>

<div class="section" style="padding-top: 40px;padding-bottom: 0px;">
    <div class="container text-center">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-8">
                <h2 class="title">Reset password</h2>
            </div>
        </div>
    </div>
</div>
<div class="section section-signup" style="background-image: url('/assets/img/bg11.jpg'); background-size: cover; background-position: top center; min-height: 700px; padding-top: 22px;">
    <div class="container">
        <div class="row">
            <div class="card card-signup" style="background:#2385aa; border-radius:21px; padding: 0.8rem; max-width:406px;">
                <form class="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                    
                    <div class="card-body">
                        
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} justify-content-center">
                            <p style="font-size:13px; color:white; margin: 0.5rem 0rem; text-align:center">We will send reset link in your email</p>    
                            
                            <input id="email" placeholder="Enter your email..." type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>

                    <div class="footer text-center" style="padding: 13px 0px;">
                        <button type="submit" class="btn btn-success" style="background-color:#61adbd">
                            Send Password Reset Link
                        </button>
                    </div>
                    
                </form>
            </div>
        </div>
        <div class="col text-center">
            <a href="/" class="btn btn-simple btn-round btn-white btn-lg hovergreen" style="color: white;border-color: white;">Back to Home</a>
        </div>
    </div>
</div>

@endsection
