@extends('layouts.app')

@section('body')
    <style>
        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }
        .card-profile, .card-testimonial {
            margin-top: 30px;
            text-align: center;
        }
        .card {
            border: 0;
            border-radius: 0.25rem;
            display: inline-block;
            position: relative;
            width: 100%;
            margin-bottom: 30px;
            box-shadow: 0px 5px 25px 0px rgba(0, 0, 0, 0.2);
        }
        .card-profile .card-body .card-avatar, .card-testimonial .card-body .card-avatar {
            margin: 0 auto 30px;
        }
        .card-profile .card-avatar, .card-testimonial .card-avatar {
            max-width: 130px;
            max-height: 130px;
            margin: -60px auto 0;
        }
        .card-profile .card-avatar img, .card-testimonial .card-avatar img {
            border-radius: 50% !important;
            height:130px;
            width: 130px;
        }
        .pull-down-footer{
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
        .pull-down-info{
            position: absolute;
            bottom: 73px;
            left: 0;
            right: 0;
        }
        .pull-down-code{
            position: absolute;
            bottom: 105px;
            left: 0;
            right: 0;
        }
        .student_number{
            font-size: 11px;
            margin-right: 5px;
            margin-top: 3px;
            color: #aa2323;
        }
</style>
<div class="wrapper">
    <div class="page-header page-header-small" filter-color="blue">
        <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg5.jpg');">
        </div>
        <div class="container">
            <div class="content-center">
                <div class="photo-container">
                    @if(Auth::user()->data()->photo)
                        <img src="{{Auth::user()->data()->photo}}" alt="">
                    @else
                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="">
                    @endif
                </div>
                <br>
           
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="button-container">
                <a href="#button" class="btn btn-primary btn-round btn-lg">My Profile</a>
               
            </div>

            
            
                    
            
            <h4 class="title text-center">My Classes:</h4>
            <div class="nav-align-center">
                <ul class="nav nav-pills nav-pills-primary" role="tablist">
                   
                    <li class="nav-item">
                        <a style="cursor:pointer" class="nav-link active" rel="tooltip" data-placement="top"  title="Create new class" role="tablist" data-toggle="modal" data-target="#createDrive">
                            <i class="now-ui-icons ui-1_simple-add"></i>
                        </a>
                    </li>
                </ul>
            </div>
            
            <div class="row">        
                   
                @forelse($drives as $d)    
          
                        
                <div class="col-md-6 col-lg-3">
                    <div class="card card-profile" style="height:18rem">
                            <span rel="tooltip" data-placement="top" title="Number of Students" class="student_number pull-right">{{$d->followers()->count()>0?$d->followers()->count():''}}</span>
                            
                        <div class="card-body">
                            <h6 class="category text-gray">{{$d->college}}</h6>
                            <h4 class="card-title">{{$d->name}}</h4>
                            <p class="card-description pull-down-code" rel="tooltip" data-placement="top" title="Class Code"  style="font-weight:600;">
                                {{$d->code}}
                            </p>
                            <p class="card-description pull-down-info">
                                {{$d->detail}}
                            </p>
                            <div class="card-footer pull-down-footer">
                                <a href="{{URL::to('/user/mydrive'.'/'.$d->slug)}}" class="btn btn-primary btn-simple btn-round">Manage</a>
                            </div>
                        </div>
                    </div>
                </div> 
                @empty
                <div class="col-md-12 col-lg-12">
                    <h3 class="title">Create your first drive!!!</h3>
                    <h5 class="description" style="font-weight:600;">
                        For the teachers go to <a href="#" data-toggle="modal" data-target="#createDrive" class="btn btn-primary">Create Drive</a> to share your notes or give assigments.
                        If you are a student then scroll down and search for your teacher's drive.
                    </h5>
                </div>
                @endforelse 
            </div>
        </div>
    </div>
       <!-- Sart Modal -->
    <div class="modal fade" id="createDrive" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                    </button>
                    <h4 class="title">Class Info</h4>
                </div>
                <form action="{{URL::to('/user/createdrive')}}" method="POST">
                    
                    <div class="modal-body">
                            {{csrf_field()}}
                            <label class="title" style="margin-left:15px; margin-top:0px;">Faculty</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-info"></i>
                                </span>
                                <select class="form-control"  id="AddClassCategory">
                                   <option selected disabled>Select Category</option>
                                   @foreach($categories as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                   @endforeach
                                    
                                </select>
                            </div>
                            <label class="title" style="margin-left:15px; margin-top:0px;">Sub-Faculty</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-info"></i>
                                </span>
                                <select class="form-control" name="faculty_id" id="AddClassFaculty">
                                   <option selected disabled>Select Sub-Faculty</option>
                                   @foreach($faculties as $f)
                                    <option value="{{$f->id}}">{{$f->name}}</option>
                                   @endforeach
                                    
                                </select>
                            </div>
                            <label class="title" style="margin-left:15px; margin-top:0px;">Subject</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-bookmark-o"></i>
                                </span>
                                <input type="text" name="name" class="form-control" placeholder="Name of subject eg. Mathematics-I, Sociology">
                            </div>
                            <label class="title" style="margin-left:15px;  margin-top:0px;">College</label>
                                
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-university"></i>
                                </span>
                                <input type="text" name="college" class="form-control" placeholder="College eg. Pulchowk Campus, LACM..">
                            </div>
                            <label class="title" style="margin-left:15px;  margin-top:0px;">Info</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-info"></i>
                                </span>
                                <input type="text" name="detail" class="form-control" placeholder="eg. 1st year, 2nd sem...etc">
                            </div>
                            <label class="title" style="margin-left:15px;  margin-top:0px;">Type</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-info"></i>
                                </span>
                               <select class="form-control" name="type">
                                   <option class="form-control" value="semester">Semester</option>
                                   <option class="form-control" value="trimester">Trimester</option>
                                   <option class="form-control" value="annual">Annual</option>
                                   
                               </select>
                            </div>
                        
                    </div>
                    <div class="modal-footer justify-content-center">
                            <img name="createclassLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 3rem; width:3rem;"/>
                                       
                        <button type="submit" name="createclass" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--  End Modal -->
    @endsection


    @section('js')
    <script>
    $(document).ready(function(){
        $('[name="createclass"]').click(function(){
            $(this).attr('hidden','true');
            $('[name="createclassLoading"]').removeAttr('hidden');
        })
        var faculties=@json($faculties);
        $('#AddClassCategory').change(function(){
            $('#AddClassFaculty').empty();
            var cat_id=$('#AddClassCategory').val();
            for(var i=0 ;i<faculties.length;i++)
            {
                if(faculties[i].category_id==cat_id)
                {
                    var a='<option value="'+faculties[i].id+'">'+faculties[i].name+'</option>';
                    $('#AddClassFaculty').append(a);   
                }
            }
        })
    });
    </script>
    @endsection