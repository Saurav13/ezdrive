@extends('layouts.app')

@section('body')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<style>
    .landingmain{
        position: relative;
        top: 10%;
        color: black;
    }
    .landinglogo{
        margin-top:4rem;
    }
    .landing_signup{
        color: black;
        padding: 0px;
    }
    .signup_icons{
        background:transparent;
    }
    .card-signup{
        box-shadow: none;
        background: transparent;
        max-width:300px;
    }
    
    .card-signup input{
        border-radius: 5px;
        margin-bottom: 1rem;
        color:white;
    }
    .card-signup input:focus { 
        background-color: #fdfdfd40;
        color:white;
        border:1px solid white;
        
    }
    .classcodesearch input{
        border-radius: 5px;
        margin-bottom: 1rem;
        color:white;
    }
    .classcodesearch span{
        border-radius: 5px;
        background: transparent !important;
        color: #d8d5d0;
        cursor: pointer;
        margin-bottom: 0;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.25;
        text-align: center;
        transition: color 0.3s ease-in-out, border-color 0.3s ease-in-out, background-color 0.3s ease-in-out;
    }
    .classcodesearch input:focus { 
        background-color: transparent;
        color:white;
        border:1px solid white;
        
    }
    .classcodesearch input:focus + .classcodesearch span{ 
        background-color: #fdfdfd40 !important;
        color:white !important;
        border:1px solid white !important;
        
    }
    .signup_header{
        color: #ffffff;
        font-size: 22px;
        font-weight: 600;
        margin-bottom: 0rem;
    }
    .signup_social{
        border-radius: 5px;
        border: none;
        
        color: white;
    }
    .color_google{
        background: #ea4335c2;
    }
    .color_google:hover{
        background: #da1808c2;
    }
    .color_facebook{
        background: #3b5998c2;
    }
    .color_facebook:hover{
        background:#1e4494c2;
    }
    .signup_social i{
        margin-right:1rem;
    }
    .landing_infos i{
        margin-bottom: 1rem;
        font-size: 60px;
        color:#2385aa;
    }
    .landing_infos p{
        font-size: 18px;
        font-weight:500;
        margin-left: 3rem;
        margin-right: 3rem;
        color:#2385aa;
    }
    .info_div{
        padding: 2rem 1rem;
    }
    /* .rh{
        max-height:700px;
    } */
    @media(min-width:300px){
        .rh{
            height:1070px;
        }
    }
    @media(min-width:500px){
        .rh{
            height:1070px;
        }
    }
    @media(min-width:700px){
        .rh{
            height:680px;
        }
    }
    .form-control:focus+.input-group-addon,
    .form-control:focus~.input-group-addon {
        border: 1px solid white;
        border-left: none;
        background-color: transparent;
    }

    
</style>
<!-- End Navbar -->
    <div class="wrapper">
        <div class="page-header clear-filter rh" filter-color="#2385aa">
            <div class="page-header-image" data-parallax="true" style="background-image: url('./assets/img/drivebg2.jpg');">
            </div>
            <div class="container">
                
                <div class="landingmain brand">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="landinglogo text-center">
                                <img class="n-logo" src="./assets/img/now-logo.png" alt="">
                                <h1 class="h1-seo" style="font-weight:600; font-size: 36px;">Ezu-Class</h1>
                                <h3 style="font-size: 22px;">"Bringing learning to people instead of people to learning."</h3>
                                {{-- <input type="text" class="form-control classcodesearch" placeholder="Search by Class Code..."/> --}}
                                <form id="searchByCodeForm" action="{{URL::to('/searchbycode')}}">
                                    <div class="input-group classcodesearch">
                            
                                        <input type="text" name="code" class="form-control" id="searchByCode" placeholder="Enter class code here.">
                                        <span class="input-group-addon" id="searchByCodeIcon">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </form> 
                            </div>
                        </div>
                        <div class="col-md-7">

                            <div class="card card-signup pull-right" style="background-color:trasnparent; ">
                                <form class="form" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}
                                    <h4 class="signup_header">Sign Up</h4><br>

                                    <input type="text" placeholder="Your Name" class="form-control" name="name" value="{{ old('name') }}" required/>
                                    <input type="email"  name="email" value="{{ old('email') }}" required placeholder="Your Email" class="form-control"/>
                                    <input type="password" placeholder="Password" class="form-control" name="password" required/>
                                    <input type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required/>
                                    <button type="submit" class="btn btn-primary" style="background-color:#1476ad">Get Started</button>
                                    <hr>
                                    <a href="/signin/facebook" class="btn btn-primary form-control signup_social color_facebook"><i class="fab fa-facebook"></i>Sign up with Facebook</a>
                                    <a href="/signin/google" class="btn btn-primary form-control signup_social color_google"><i class="fab fa-google"></i>Sign up with Google</a>
                                    
                                </form>
                            </div>

                        </div>
                    </div>
                
                </div>
                
                <a href="javascript:void(0)" target="#infoSection" class="scrolldown"><i class="fa fa-angle-double-down" style="position:absolute; font-size:30px; bottom:1rem;color:white;"></i></a>
            </div>
        </div>
        <div class="main">
            <div class="section" id="infoSection">
                <div class="container text-center">
                    <div class="row justify-content-md-center">
                        <div class="col-md-12 col-lg-8">
                            <h3 class="title">  An organized platform to enhance teaching/learning experience.</h3>
                            
                          </div>
                    </div>
                    <br>
                    <div class="row justify-content-md-center landing_infos">
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fas fa-paper-plane"></i>
                                <p class="description info_description">Digitize the education process, go paperless.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fab fa-slideshare"></i>
                                <p class="description">Online platform for both teachers and students.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fas fa-dollar-sign"></i>
                                <p class="description">Free to use</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fas fa-plus"></i>
                                <p class="description info_description">Create class or find and join a class.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fab fa-hubspot"></i>
                                <p class="description">Manage all your classes from a single portal.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fab fa-searchengin"></i>
                                <p class="description">Unique class code for every class, for better search.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fab fa-creative-commons-share"></i>
                                
                                <p class="description info_description">Share teaching/learning materials with your class.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                    <i class="fab fa-audible"></i>
                                <p class="description">Handle all your assignments digitally.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="far fa-clipboard"></i>
                                <p class="description">Grade assignments and publish results</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fas fa-bullhorn"></i>
                                <p class="description">Send class notice</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fas fa-link"></i>
                                <p class="description"> Share extra materials and resources with your class</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info_div">
                                <i class="fa fa-train"></i>
                                <p class="description"> Manage your class on the go.</p>
                            </div>
                        </div>
                        
                    </div>
                    <p class="text-center">
                        <a href="/explore" class="btn btn-neutral btn-lg hovergreen" style="border: 1px solid #2385aa;">Explore EZU-Class</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
           
       
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('#searchByCodeIcon').click(function(){
           
            $('#searchByCodeForm').submit();
        })
        $('#searchByCode').keypress(function(e){
            
            if(e.which == 13){//Enter key pressed
               $('#searchByCodeForm').submit();
            }
        });
        $(".scrolldown").click(function() {
            var target=$(this).attr('target');
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 1000);
        });
        
    })
</script>
@endsection