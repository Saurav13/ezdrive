<style>
    .search-result img{
        height:130px;
        width: 130px;
    }
    .crossbtn{
        font-weight: 600;
        color: #4397b6;
        text-decoration: none;
        font-size: 19px;
        cursor:pointer;
        background: transparent;
        border:none;
        
    }
</style>
<div class="section section-team text-center" style="padding:10px 0px;">
    <div class="container" style="border:2px solid #e5e5e5;">
       
            <button id="searchcrossbtn" class="crossbtn pull-right">X</button>
        <h4 class="title" style="margin-left: 2rem;">Search Results</h4>
        <div class="team">
            <div class="row">
                @foreach($results as $r)
                    <div class="col-md-3">
                        <div class="search-result">
                            @if($r->photo)
                            <img src="{{$r->photo}}" alt="{{ $r->name }}" class="rounded-circle img-raised">
                            @else
                            <img src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $r->name }}" class="rounded-circle img-raised">
                            @endif
                            <h4 class="title"><a href="{{ route('publicprofile',$r->slug) }}" style="color:#212529">{{ $r->name }}</a></h4>
                            <p class="category text-primary" style="font-size: 12px;margin-bottom:  5px;">{{$r->designation}}</p>
                            <p class="category text-primary" style="font-size: 13px;"> {{$r->organization}}</p>
                            <p class="description">{{(strlen($r->about) < 85) ? $r->about : substr($r->about,0,85).'..'}}</p>
                            
                        </div>
                    </div>
                @endforeach
            </div>
            @if($results->count()>0)
                <a href="#!" id="viewmoreuser">view more</a>
            @else
            <h4>No results found...</h4>
            @endif
        </div>
  
    </div>
</div>
<script>
$('#searchcrossbtn').click(function(){
    $('#searchResults').attr('hidden','true');
});
</script>