<style>
        .search-result img{
            height:130px;
            width: 130px;
        }
        .crossbtn{
            font-weight: 600;
            color: #4397b6;
            text-decoration: none;
            font-size: 19px;
            cursor:pointer;
            background: transparent;
            border:none;
            
        }
    </style>
    <div class="section section-team text-center" style="padding:10px 0px;">
        <div class="container" style="border:2px solid #e5e5e5;">
           
                <button id="searchcrossbtn" class="crossbtn pull-right">X</button>
            <h4 class="title" style="margin-left: 2rem;">Search Results</h4>
            <div class="team">
               
                    <div class="card card-profile" style="height:20rem; width:18rem;">
                            <span rel="tooltip" data-placement="top" title="Number of Students" class="student_number pull-right">{{$drive->followers()->count()}}</span>
                            
                        <div class="card-body">
                            <h6 class="category text-gray" title="{{ $drive->college }}">{{strlen($drive->college)>32?substr($drive->college,0,32).'..':$drive->college}}</h6>
                            <h4 class="card-title"><a href="{{ route('class',$drive->slug) }}" target="_blank" style="color:#2c2c2c" title="{{$drive->name}}">{{strlen($drive->name)>24?substr($drive->name,0,20).'..':$drive->name}}</a></h4>
                            <p class="card-description" style="font-weight:600;"><a target="_blank" style="color:#9a9a9a" href="{{ route('publicprofile',$drive->owner->data()->slug) }}">
                                ({{$drive->owner->data()->name}})</a>
                            </p>
                            <p class="card-description pull-down-code" rel="tooltip" data-placement="top" title="Class Code"  style="font-weight:600;">
                                {{$drive->code}}
                            </p>
                            <p class="card-description pull-down-info" title="{{ $drive->detail }}">
                                {{strlen($drive->detail)>35?substr($drive->detail,0,35).'..':$drive->detail}}
                            </p>
                            <div class="card-footer pull-down-footer">
                                @if(Auth::check() && $drive->owner->id != Auth::user()->id)
                                    <button class="btn btn-primary btn-round" id="Drive{{ $drive->id }}" drive-id="{{ $drive->id }}" drive-name="{{ $drive->name }}">{{ Auth::user()->followed_drives()->where('drives.id',$drive->id)->exists() ? 'Leave':(Auth::user()->pending_drives()->where('drives.id',$drive->id)->exists() ? 'Requested':'Join') }}</button>
                                @else
                                    <a href="{{URL::to('/user/mydrive'.'/'.$drive->slug)}}" class="btn btn-primary btn-simple btn-round">Manage</a>
                                @endif
                            </div>
                        </div>
                    </div>
            </div>
            
      
        </div>
    </div>
    <script>
    $('#searchcrossbtn').click(function(){
        $('#searchResults').attr('hidden','true');
    });
    </script>
    <script>
    $(document).ready(function(){
        
        $("[id*='Drive']").click(function(e){

            var ele = $(this);
            
            var text = (ele.text() == 'Requested' ? 'Cancel request to join' : ele.text()) + ' ' + ele.attr('drive-name') + '?';
            $('#confirmText').html(text);
            $('#confirmAlert').modal('show');

            $('#confirmOk').unbind().click(function(){
                $('#confirmAlert').modal('hide');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::to('follow') }}/"+ele.attr('drive-id'),
                    dataType: 'json',
                    beforeSend: function(){
                        ele.attr('disabled','disabled');
                    },
                    success: function(response) {
                        ele.html(response.action);
                    },
                    error: function(){
                        $('#errorMessage').html('Something went wrong. Pleaase Try again');
                        $('#errorAlert').modal('show');
                    },
                    complete: function(){
                        ele.removeAttr('disabled');
                    }
                });
            });
        });

    });
   
</script>