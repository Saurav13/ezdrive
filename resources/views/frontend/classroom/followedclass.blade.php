@extends('layouts.app')

@section('body')
<link rel='stylesheet' href='/build/loading-bar.css' type='text/css' media='all' />
<style>

    .filename:hover{
        text-decoration-line: underline !important;
    }

    #loading-bar .bar {
        position: relative;
    }
    [ng\:cloak], [ng-cloak], .ng-cloak {
          display: none !important;
        }

    .badge-primary{
        background-color:#2385aa;
        color:white;
        border-color:#2385aa;
    }
    .description{
        font-weight:500;
    }
    .info {
        max-width: 360px;
        margin: 0 auto;
        padding: 70px 0 30px;
        text-align: center;
    }
    .icon.icon-primary {
        color: #f96332;
    }
    
    .info .icon {
        color: #888888;
        transition: transform .4s, box-shadow .4s;
    }
    .info.info-hover:hover .icon.icon-primary+.info-title {
        color: #f96332;
    }
    .info.info-hover:hover .icon {
        -webkit-transform: translate3d(0, -0.5rem, 0);
        -moz-transform: translate3d(0, -0.5rem, 0);
        -o-transform: translate3d(0, -0.5rem, 0);
        -ms-transform: translate3d(0, -0.5rem, 0);
        transform: translate3d(0, -0.5rem, 0);
    }
    .info.info-hover .info-title {
        transition: color .4s;
    }
    .info .info-title {
        margin: 25px 0 15px;
        padding: 0 15px;
        color: #2c2c2c;
    }
    .info p {
        color: #888888;
        padding: 0 15px;
        font-size: 1.1em;
    }
    .profile-page .page-header {
        min-height: 290px;
    }
    .nav-pills1 .nav-item .nav-link{
        padding: 10px;
        text-align: center;
        height: 100px;
        width: 100px;
        font-weight: 400;
        color: #9A9A9A;
        background-color: white;
        border-radius: 10px;
    }
    .nav-pills1 .nav-item .nav-link.active, .nav-pills .nav-item .nav-link.active:focus, .nav-pills .nav-item .nav-link.active:hover {
        background-color: #2385aa;
        color: #FFFFFF;
        box-shadow: 0px 5px 35px 0px rgba(0, 0, 0, 0.3);
    }
        
    .nav-pills1.nav-pills-primary .nav-item .nav-link .n1 .active,
    .nav-pills1.nav-pills-primary .nav-item .nav-link .n1 .active:focus,
    .nav-pills1.nav-pills-primary .nav-item .nav-link .n1 .active:hover {
        background-color:#2385aa ;
    }
    .nav-pills .nav-item .nav-link {
        padding: 0 15.5px;
        text-align: center;
        height: 100px;
        width: 100px;
        font-weight: 400;
        color: #9A9A9A;
        margin-right: 19px;
        background-color: rgba(222, 222, 222, 0.3);
        border-radius: 10px;
    }
    .folder{
        padding: 0px;
        margin:10px;
        margin-top:12px;
    }

    .folder img{
        height: 50px;
        width: 50px;
    }

   
    .folder-small{
        padding: 0px;
        margin:32px;
        margin-top:12px;
        cursor: pointer;
        /* text-decoration: none!important; */
    }
    .folder-small a:hover{
        /* text-decoration: underline!important; */
        cursor:pointer;
    }

    .folder-small img{
        height: 30px;
        width: 30px;
    }
    .folder-medium{
        padding: 0px;
        margin:32px;
        margin-top:12px;
        cursor: pointer;
        /* text-decoration: none!important; */
    }
    .folder-medium a:hover{
        text-decoration: underline!important;
        cursor:pointer;
    }

    .folder-medium img{
        height: 35px;
        width: 35px;
    }
    .uploadicon{
        font-size:25px; 
        margin-top:10px;
    }
            
    .uploadicon:hover{
        font-size:30px;
        margin-top:5px;
    }

    .card_post{
        border: 1px solid #2385aa;
        border-radius: 10px;
    }
    .notice_list_title{
        color: #2383aa;
    }
    .general_items{
        /* border: 1px solid #2385aa; */
        /* border-radius: 10px; */
        margin-top:2rem;
    }
    .general_icons{
        margin-top: 2rem;
        margin-left: 4rem;
        font-size: 48px;
        /* border: 1px solid #2385aa; */
        /* border-radius: 54px; */
        padding: 1rem;
    }
    .general_assignment{
        margin-bottom:3rem;
    }
    .general_assignment .container{
        padding-bottom: 1rem;
        border-bottom:2px solid #edecec;
    }
    .general_notice{
        margin-bottom:3rem;
    }
    .general_notice .container{
        padding-bottom: 1rem;
        border-bottom:2px solid #edecec;
    }
    .general_result{
        margin-bottom:3rem;
    }
    .general_result .container{
        padding-bottom: 1rem;
        border-bottom:2px solid #edecec;
    }
    .general_posted_date{
        text-align: left !important;
        margin-bottom: 0;
        font-size:12px !important;
    }
    .general_deadline{
        font-weight: 500;
        font-size: 1em;
    }
    .general_deadline span{
        margin-left: 1rem;
        color: red;
    }
    .general_extra{
        margin-bottom:3rem;
    }
    .general_extra .container{
        padding-bottom: 1rem;
        border-bottom:2px solid #edecec;
    }
    .general_extra .general_icons{
        margin-top: 0rem;
        margin-left: 4rem;
        font-size: 48px;
        /* border: 1px solid #2385aa; */
        /* border-radius: 54px; */
        padding: 1rem;
    }
</style>
<div class="modal fade" id="showSuccess" tabindex="-1" role="dialog" aria-labelledby="errorDriveLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center" style="background:white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                    </button>
                    <h4 class="title">Success</h4><br>
                </div>
                <div class="modal-body">
                <p class="description" id="successMessage"></p>
                        
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="errorDrive" tabindex="-1" role="dialog" aria-labelledby="errorDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center" style="background:white">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Something went wrong</h4><br>
            </div>
            <div class="modal-body">
            <p class="description" style="color:red;" id="errorMessage"></p>
            <p class="description" style="color:red;" id="errorDetails"></p>
                    
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="teacherProfile" tabindex="-1" role="dialog" aria-labelledby="myProfileLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center" style="border-radius: 4px 4px 0 0; background: none; padding-top: 24px; padding-right: 24px;padding-bottom: 0;padding-left: 24px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Teacher's Profile</h4>
            </div>
            <div class="modal-body justify-content-center">
                <div class="photo-container">
                    @if($drive->owner->data()->photo)
                        <img src="{{ $drive->owner->data()->photo}}" alt="{{ $drive->owner->data()->name }}">
                    @else
                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $drive->owner->data()->name }}">
                    @endif
                </div>
                <br>
                @if($drive->owner->data()->name)
                <p class="description" style="font-size:18px;">{{$drive->owner->data()->name}}</p>
                @endif
                @if($drive->owner->data()->designation)
                <p class="description" style="font-size:18px;">{{$drive->owner->data()->designation}}</p>
                @endif
                @if($drive->owner->data()->email)
                <p class="description" style="font-size:18px;"><b>Email:  </b>{{$drive->owner->data()->email}}</p>
                @endif
                @if($drive->owner->data()->phone)
                <p class="description" style="font-size:18px;"><b>Phone </b> {{$drive->owner->data()->phone}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{ asset('dist/angular-filemanager.min.css') }}">
<style>
    #context-menu ul,#context-menupost ul,#context-menutask ul, #context-general ul, #downloadlist{
        box-shadow: 0 1px 4px rgba(0,0,0,0.6)!important;
    }

    #context-menu ul li,#context-menupost ul li,#context-menutask ul li, #context-general ul li, #downloadlist ul li{
        display:flex;
        align-items:center;
        font-size:15px;
    }
    #context-menu ul li:hover, #context-menupost ul li:hover,#context-menutask ul li:hover, #context-general ul li:hover{
        background:rgba(100,100,100,0.12);
    }

    #context-menu ul li i,#context-menupost ul li i,#context-menutask ul li i,#context-general ul li i, #downloadlist ul li i {
        width:20px; 
    }

    #viewerpopup .modal-dialog {
        min-width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #context-menupost,#context-menutask, #context-general {
        position: absolute;
        display: none;
        z-index: 9999;
    }
    #viewerpopup .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
        background-color: #00000096;
    }
    .drive_breadcrumb{
        padding-left: 2rem;
        border-bottom: 2px solid #dcd9d980;
        color: #6cbaff;
        font-weight: 600;
        font-size: 14px;
        
    }
    .drive_breadcrumb_a{
       
        color: #6cbaff;
        
    }
    .drive_breadcrumb_seperator{
        font-size: 18px;
        padding-left: 5px;
        padding-right: 5px;
        color: #949fab;
    }
    .drive_breadcrumb_last{
        color: #1677ce;
    }
    .task_card{
        height:24rem;
        /* box-shadow: 0px 5px 25px 0px rgba(0, 0, 0, 0.07); */
        border: 2px solid #a0a0a082;
        border-radius: 8px;
    }
    .task_card .topbar{
        margin: 1rem 2rem;
    }
    .task_card .check{
        font-size: 25px;
        margin-top: 0.2rem;
    }
    .task_card p{
        margin-bottom: 0px;
    }
    .task_card .edited{
        margin-top: 0.5rem;
    }
    .tack_card h4{
        margin-top: 1rem;
        margin-bottom: 0.5rem;
    }
    .task_file{
        margin:1rem 0rem;
    }
    .task_deadline{
        font-weight: 600;
        font-size: 1em;
    }
    .task_deadline span{
        color:red;
    }
    .task_action{
        margin-top:1rem;
    }
    .task_posted_date{
        margin-top: 1rem;
        text-align: left !important;
        margin-bottom: 0;
        font-size:12px !important;

    }
    .extra_head{
        text-align:left !important;
    }
    .extra_head img{
        max-width: 3rem;
        max-height: 3rem;
        border-radius: 50%;
        overflow: hidden;
        margin: 0 auto;
        box-shadow: 0px 10px 25px 0px rgba(0, 0, 0, 0.3);
    }
    
    .pdname .name{

    }
    .shared_materials{
        box-shadow:none;
         }
    .shared_header p{
        padding-top: 0.4rem;
        margin-bottom: 0px;
        font-size: 16px;
        font-weight: 400;
    }
    .shared_materials ul{
        list-style-type: none;
        padding-left: 0px;
    }
    .shared_materials ul li{
        margin-bottom: 1rem;
    }
    .shared_materials .listfiles{
        margin-bottom: 0.7rem;
        padding-left: 1px;
    }
    .floating_card{
        top: 0.5rem;
        right: 0.5rem;
        position: absolute;
        margin-right: 1rem;
    }
    .pull-down-task-date{
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            margin-left: 1rem;
            margin-bottom: 1px;
        }
    .pull-down-task-actions{
        position: absolute;
        bottom: 2rem;
        left: 0;
        right: 0;
    }
    .pull-down-task-deadline{
        position: absolute;
        bottom: 6rem;
        left: 0;
        right: 0;
    }
    .container_adv{
        text-align: center !important;
        margin: 1rem 0rem;
    }
    .banner_adv{
        border: 2px solid #0039b1ad;
        margin: 0px 20%;
       
        border-radius: 6px;
    }
    .banner_inner_adv{
        padding: 1rem 0px;
    }
    .banner_inner_adv a{
        color: #888888;
        font-weight: 600;
        font-size: 16px;
    }
    .banner_inner_close{
        font-size: 11px;
        cursor: pointer;
        margin: 2px;
        margin-right: 4px;
    }
    .banner_inner_badge{
        margin: 3px;
        padding: 0px 6px;
        background: #efbf0a;
        border-radius: 4px;
        color: white;
        font-size: 10px;
    }
    .sponsor{
        position: absolute;
        bottom: 1px;
        right: 1px;
        color:white;
    }
    .sponsor a{
        color:white;
    }
</style>

<style>
   @media only screen and (min-width : 320px) and (max-width : 768px) {
        .nav-tabs {
            /* display: inline-block; */
            /* width: 100%; */
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        .sponsor{
            display:none;
        }
        
           
    }
    @media only screen and (min-width : 320px) and (max-width : 768px) {
        .general_icons  {
            display: none;
           
            
        }
        .extra_head .pdimg{
        padding-left: 1rem;
        padding-right: 1.5rem;
        }
        .extra_head .pdname{
           margin-left:2rem;
           padding:0px;

        }
      
        
    }

</style>

<div class="wrapper" id="ClassView">
    <div class="page-header page-header-small" style="height: 100vh; max-height: 300px;" filter-color="blue">
        @if($banner->image)
        <div class="page-header-image" data-parallax="true" style="background-image: url(/bannerad_images{{'/'.$banner->image}});">
        @else
        <div class="page-header-image" data-parallax="true" style="background-image: url(/assets/img/bg8.jpg)}});">
        
        @endif
        </div>
        <div class="container">
            <div class="content-center">
                    
                <h3 class="title" style="margin-bottom:30px;">{{ $drive->name }}</h3>
                
                <p class="category">{{ $drive->owner->data()->name }}</p>
                
            </div>
         @if($banner->link)   
            <p class="pull-right sponsor"><a  href="{{$banner->link}}" target="_blank">Sponsored by {{$banner->title}}.</a></p>
        @else
            <p class="pull-right sponsor"><a  href="http://www.incubeweb.com" target="_blank">Sponsored by Incube.</a></p>
        @endif
        </div>
        <div class="corner">
            
                
        </div>
    </div>
    <div class="section" ng-controller="GeneralController" ng-init="drive_id={{ $drive->id }};rollno='{{ $rollno }}';duplicate='{{ $duplicate }}';init()">
        <div class="container" >
            <div class="button-container" style="margin-top:-130px;">
                
            <a style="cursor:pointer" data-toggle="modal" data-target="#teacherProfile">
                @if($drive->owner->data()->photo)
                    <img class="photo-container" src="{{ $drive->owner->data()->photo}}" alt="{{ $drive->owner->data()->name }}">
                @else
                    <img class="photo-container" src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $drive->owner->data()->name }}">
                @endif
            </a>
           
            </div>
        </div>
        <br>
        <div class="section section-pills" style="padding-top:40px;">
            <div class="container">
                <p ng-show="duplicate" style="text-align:center;color: #e29e30;" ng-cloak>
                    <i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;Duplicate entry of roll no. (@{{ rollno }}) found in this class. Please confirm your roll no. Change roll no by editing any one task.
                </p>
                <div class="card">
                    <ul class="nav nav-tabs justify-content-center" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab"  href="#general_tab" id="generaltab" role="tab">
                                <i class="now-ui-icons shopping_shop"></i>General
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#board_tab" id="boardtab" role="tab">
                                <i class="fas fa-chalkboard"></i> Board
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#drive_tab" id="drivetab" role="tab">
                                <i class="far fa-hdd"></i> Drive
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  data-toggle="tab" id="tasktab" href="#task_tab" role="tab">
                                <i class="fa fa-edit"></i>Task
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#extra_tab" id="extratab" role="tab">
                                <i class="fas fa-puzzle-piece"></i> Extras
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#notice_tab" id="noticetab" role="tab">
                                <i class="far fa-bell"></i> Notices
                            </a>
                        </li>
                    </ul>
                    <div class="card-body" style="padding: 1.25rem 5px;">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="general_tab" role="tabpanel" >
                                <p class="text-center">
                                    <img id="GeneralMainLoading" class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                </p> 
                                <div class="container" id="GeneralMainBeforeLoading" hidden style="align:left">
                                    <div class="general_items">
                                        <div ng-repeat="g in generals | limitTo: g_pagn_count">
                                            <div class="general_assignment" ng-if="g.type=='ttask'" >
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p class="text-center">
                                                            <i  class="fa fa-edit general_icons"></i>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="container">
                                                            <div class="pull-right">
                                                                <span class="badge badge-primary" >Task</span>
                                                            </div>
                                                            <h5><span  style="color: #2385aa;font-size: 25px;" ng-if="g.mysubmission!=null && g.resultPublished=='unpublished'" ><i class="fa fa-check-circle"  data-placement="top" title="Submitted, Not Graded"></i></span>
                                                                <span  style="color: #0f9d58;font-size: 25px;" ng-if="g.mysubmission!=null && g.resultPublished=='published'" ><i class="fa fa-check-circle" data-placement="top" title="Submitted and Graded"></i></span>
                                                                <span  style="color: red;font-size: 25px;" ng-if="g.mysubmission==null && g.status=='closed'" ><i class="fas fa-ban" data-placement="top" title="Failed to Submit"></i></span>
                                                                
                                                                &nbsp;@{{g.name}}</h5>
                                                            <p class="description" style="text-align:left;">@{{g.collectiontask.description}}
                                                                
                                                            </p>
                                                            <div class="row" ng-show="g.collectiontask.file">
                                                                <div class="col-md-12" >
                                                                    <a class="folder-small general-list-item filename" style="font-size:13px;margin-left:15px; margin-right:15px;" ng-click="select(g.collectiontask,4)" ng-right-click="select(g.collectiontask,4)">
                                                                            
                                                                    <img ng-src="{{asset('/assets/img')}}/@{{getIcon(g.collectiontask.file.split('.').slice(-1)[0])}}" />
                                                                        @{{g.collectiontask.file.split('/').slice(-1)[0].split('.').slice(0,1)[0].substr(0,50)}}..
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div ng-switch="g.mysubmission">
                                                                <div ng-switch-when="null">
                                                                    <button ng-if="g.status == 'open'" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#uploadFile" style="margin-top: 15px;" ng-click="select(g,4)">Upload</button>
                                                                </div>
                                                                <div ng-switch-default>
                                                                    <button ng-if="g.status == 'open' && !g.mysubmission.grade" class="btn btn-warning btn-sm pull-right" data-toggle="modal" data-target="#editFile" style="margin-top: 15px;" ng-click="select(g.mysubmission,5)">Edit</button>
                                                                    <button class="btn btn-default btn-sm pull-right" style="margin-top: 15px;" ng-click="select(g.mysubmission,5);task_open();">View</button>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <br>
                                                            <p class="general_deadline" ng-show="g.deadline">
                                                                Deadline: <span>@{{g.deadline | date: 'MMM d, y'}}</span>
                                                            </p>
                                                            <p class="description general_posted_date">
                                                            <span><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="general_notice" ng-if="g.type=='notice'">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p class="text-center">
                                                            <i  class="far fa-bell general_icons"></i>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="container">
                                                            <div class="pull-right">
                                                                <span class="badge badge-primary" >Notice</span>
                                                            </div>
                                                        <h5>@{{g.title}}</h5>
                                                            <p ng-if="g.description.length<400" class="description" style="text-align:left;">@{{g.description}}</p>
                                                            <div ng-if="g.description.length>400">
                                                                <p class="description" style="text-align:left;">@{{g.description.substr(0,200)}}</p>
                                                                <a href="javascript:void(0);" class="viewmorelink" >View more..</a>
                                                            </div>
                                                            <div ng-if="g.description.length>400" hidden>
                                                                <p class="description" style="text-align:left;">@{{g.description}}</p>
                                                                <a href="javascript:void(0);" class="viewlesslink" >..hide</a>
                                                            </div>
                                                            
                                                            <p class="description general_posted_date">
                                                                <span><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="general_extra" ng-if="g.type=='extra'">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p class="text-center">
                                                            <i class="fas fa-puzzle-piece general_icons"></i>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="container">
                                                            <div class="pull-right">
                                                                <span class="badge badge-primary" >Extras</span>
                                                            </div>
                                                            <br>
                                                            <p ng-if="g.content.length<400" class="description" style="text-align:left;" ng-bind-html="g.content"></p>
                                                            <div ng-if="g.content.length>400">
                                                                <p class="description" style="text-align:justify;" ng-bind-html="g.content.substr(0,200)"></p>
                                                                <a href="javascript:void(0);" class="viewmorelink" >View more..</a>
                                                            </div>
                                                            <div ng-if="g.content.length>400" hidden>
                                                                <p class="description" style="text-align:justify;" ng-bind-html="g.content"></p>
                                                                <a href="javascript:void(0);" class="viewlesslink" >..hide</a>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3 " ng-repeat="f in g.postedfiles" >
                                                                    <a class="folder-small general-list-item filename" style="font-size:13px;margin-left:15px; margin-right:15px;" title="@{{f.file.split('/').slice(-1)[0]}}" ng-click="select(f,3)" ng-right-click="select(f,3)">
                                                                            
                                                                        <img ng-src="{{asset('/assets/img')}}/@{{getIcon(f.file.split('.').slice(-1)[0])}}"/>
                                                                        @{{f.file.split('/').slice(-1)[0].split('.').slice(0,1)[0].substr(0,10)}}..
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <p class="description general_posted_date">
                                                                <span><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="general_result" ng-if="g.type=='result'">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p class="text-center">
                                                            <i class="fas fa-graduation-cap general_icons"></i>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="container">
                                                            <div class="pull-right">
                                                                <span class="badge badge-primary" >Result</span>
                                                            </div>
                                                            <h5>@{{g.collection.name}}</h5>
                                                            <p class="description" style="text-align:left;">@{{g.message}}</p>
                                                            <p>
                                                                <a href="/class/@{{drive_id}}/@{{g.id}}/viewresult" target="_blank" class="btn btn-success btn-sm pull-right">View Result</a>
                                                            </p>
                                                            <br>  
                                                            <br> 
                                                            <p class="description general_posted_date">
                                                                <span><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="context-general" class=" clearfix animated fast fadeIn">
                                            <ul class="dropdown-menu dropdown-right-click" role="menu" aria-labelledby="dropdownMenu">
                                        
                                                <li>
                                                    <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                                        <i class="fa fa-eye"></i> Preview
                                                    </a>
                                                </li>
                                        
                                                <li>
                                                    <a style="color:#666666" href="#!" tabindex="-1" ng-click="download()">
                                                        <i class="fas fa-download"></i> Download
                                                    </a>
                                                </li>
                                        
                                            </ul>
                                        
                                        </div>
                                    </div>
                                    <p class="text-center" id="ViewMoreGeneralBeforeLoading">
                                        <a href="JavaScript:Void(0);" ng-hide="g_pagn_count>=generals.length" ng-click="viewMoreGenerals()">View more</a>
                                        <span ng-show="g_pagn_count>=generals.length && generals.length!=0">No old updates</span>
                                        <span ng-show="generals.length==0">No Updates</span>
                                    </p> 
                                    <p class="text-center">
                                        <img id="ViewMoreGeneralLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                                    </p>
                                </div>
                                    
                                <div class="modal fade modal-mini modal-primary" id="viewNotice" tabindex="-1" role="dialog" aria-labelledby="viewNoticeLabel" aria-hidden="true">
                                    <div class="modal-dialog" style="max-width:38rem">
                                        <div class="modal-content">
                                            <div >
                                                <div class="modal-header justify-content-center" style="background: #2385aa;">
                                                    
                                                    <div class="modal-profile" >
                                                    
                                                        <i class="fa fa-info"></i>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <p id="noticeDate" style="font-size:  12px;font-style:  italic;"></p>
                                                    <p id="noticeModalBody"></p>
                                                </div>
                                                <div class="modal-footer justify-content-center">
                                                    
                                                    <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal" style="box-shadow: none;">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="board_tab" role="tabpanel">
                                <p class="text-center">
                                    <img id="BoardMainLoading" class="justify-content-center" hidden src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                </p>
                                <div id="BoardMainBeforeLoading" hidden >
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="container" style="align:left">
                                                    <div class="" style="">
                                                        <div class="container">
                                                            <div class="card-body">
                                                                <div ng-if="board">
                                                                    <select class="form-control" ng-model="board.date" ng-change="changeBoard()">
                                                                        <option ng-repeat="d in board_dates" value="@{{ d }}" @{{ d == board.date ? 'selected':'' }}>@{{d | date: 'MMM d, y'}}</option>
                                                                    </select>
                                                                    <br>
                                                                    <div style="text-align:center">
                                                                        <img id="changeBoardLoading" hidden src="{{asset('drive/driveloading.gif')}}" style="height:3rem; width:3rem;"/>
                                                                    </div>                                                                    
                                                                    <div id="theBoard" class="form-control" ng-bind-html="board.content" style="min-height:150px;max-width: 100%;padding: 10px;resize: none;border: none;border-bottom: 1px solid #E3E3E3;border-radius: 0;line-height: 2;"></div>
                                                                </div>
                                                                <div ng-if="!board" style="text-align:center">
                                                                    <p>No Boards Yet</p>
                                                                </div>  
                                                            </div>
                                                        </div>
                                                    </div>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="drive_tab" role="tabpanel">
                                <div class="container">
                                    
                                    <h4 class="category text-gray text-center" >

                                        <a style="float:left" href="javascript:void(0)" ng-click="drive_back()" ng-hide="isRoot" ng-cloak>
                                            <i class="fa fa-arrow-left" title="Back" rel="tooltip" data-placement="top"></i>
                                        </a>
                                        <a style="float:left;padding-left:25px" href="javascript:void(0)" ng-show="selectType==1 || selectType==2" ng-click="download()">
                                            <i class="fa fa-download"></i>
                                        </a>
                                        Shared Materials
                                    </h4> 
                                    <div class="row drive_breadcrumb">
                                        <div class="col-md-8">
                                            <a style="cursor:pointer" class="drive_breadcrumb_a" ng-click="drive_openRoot()"><span>{{$drive->name}}</span></a>
                                            <span class="drive_breadcrumb_seperator">/</span>
                                            <span ng-repeat="b in breadcrumbs.slice(0,-1)">
                                                <a style="cursor:pointer" class="drive_breadcrumb_a" ng-click="drive_openFolder(b.id)"><span>@{{b.name}}</span></a>
                                                <span class="drive_breadcrumb_seperator">/</span>
                                            </span>
                                            <span class="drive_breadcrumb_last">@{{breadcrumbs.slice(-1)[0].name}}</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" style="float:right;max-width:220px" class="form-control" ng-model="driveSearchKey" placeholder="Search.." ng-keyup="$event.keyCode == 13 && drive_searchDrive(driveSearchKey)"/>
                                        </div>                                            
                                    </div> 
                                    
                                    <p class="text-center">
                                        <img id="DriveMainLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                    </p>    
                                    <div class="container"  style="padding-bottom:20px; min-height:400px; padding-right:5px; padding-left:5px;">
                                        <div id="DriveMainBeforeLoading">
                                            <ul class="nav nav-pills1 nav-pills-primary nav-pills-icons item-list"  id="navicons" role="tablist" style="clear:both">
                                                <li class="nav-item " ng-repeat="c in folders">
                                                    <a ng-cloak class="nav-link folder item-list-item" data-toggle="tab"  role="tablist" ng-click="select(c,1)" ng-dblclick="drive_openFolder(c.id)" ios-dblclick="drive_openFolder(c.id)" ng-right-click="select(c,1)" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" title="@{{c.name}}">
                                                        <img src="/assets/img/folder.png"/><br>
                                                        @{{c.name}} </a>
                                                </li>
                                                <li class="nav-item " ng-repeat="c in files">
                                                    <a ng-cloak class="nav-link folder item-list-item" data-toggle="tab"  role="tablist"  ng-click="select(c,2)" ng-dblclick="openFile(c.id)" ios-dblclick="openFile(c.id)" ng-right-click="select(c,2)" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" title="@{{c.name}}">
                                                        <img ng-src="{{asset('/assets/img')}}/@{{getIcon(c.extension)}}"/><br>
                                                        <span>@{{c.name}}</span></a>
                                                </li>
                                            </ul>
                                            
                                            <p class="description" ng-cloak ng-show="folders.length==0 && files.length==0"><br><br>No files or folders found.<p>  
                                        </div>
                                    </div>
                                </div>
                                <div id="context-menu" class=" clearfix animated fast fadeIn">
                                    <ul class="dropdown-menu dropdown-right-click" role="menu" aria-labelledby="dropdownMenu">
                                
                                        <li ng-show="selectType==1">
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                                <i class="fa fa-folder-open"></i> Open
                                            </a>
                                        </li>

                                        <li ng-show="selectType==2">
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                                <i class="fa fa-eye"></i> Preview
                                            </a>
                                        </li>
                                
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="download()">
                                                <i class="fas fa-download"></i> Download
                                            </a>
                                        </li>
                                
                                    </ul>
                                
                                </div>

                            </div>

                            <div class="tab-pane" id="task_tab" role="tabpanel" >
                                <p class="text-center">
                                    <img id="TaskMainLoading" class="justify-content-center" hidden src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                </p> 
                                <div id="TaskMainBeforeLoading" hidden>
                                    {{-- <p ng-show="duplicate" style="text-align:center;color: #e29e30;">
                                        <i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;Duplicate entry of roll no. (@{{ rollno }}) found in this class. Please confirm your roll no.
                                    </p> --}}
                                    <div class="row task-list">
                                        <div class="col-md-4" ng-repeat="g in generals | filter: type='ttask' | orderBy: 'created_at': true | limitTo: t_pagn_count "  >
                                            <div class="card task_card">
                                                <div class="container">
                                                    <span class="check pull-left" ng-if="g.mysubmission!=null&&g.resultPublished=='unpublished'" style="color: #2385aa" data-placement="top" title="Submitted, Not graded"><i class="fa fa-check-circle"></i></span>
                                                    <span class="check pull-left" ng-if="g.mysubmission!=null&&g.resultPublished=='published'" style="color: #0f9d58"  data-placement="top" title="Submitted and Graded"><i class="fa fa-check-circle"></i></span>
                                                    <span class="check pull-left" ng-if="g.mysubmission==null&&g.status=='closed'" style="color: red"  data-placement="top" title="Failed to Submit"><i class="fas fa-ban"></i></span>
                                                    
                                                    <div class="card-body" style="padding-bottom: 8px;">
                                                        <h4 class="title" style="margin-bottom:0px">@{{g.name}}</h4>
                                                        <p class="description edited" style="margin-top:0px"><i>(@{{ g.status }})</i></p>
                                                        <br>  
                                                        <p class="description" ng-show="g.collectiontask.description">@{{g.collectiontask.description.substr(0,30)}}...<a href="JavaScript:Void(0);" data-toggle="modal" data-target="#viewTask" ng-click="viewTask(g)">see more</a></p>
                                                        <p class="description" ng-hide="g.collectiontask.description"><br><i>(Task description not provided)<i></p>
                                                        <div class="row text-center task_file" ng-show="g.collectiontask.file">
                                                            <div class="col-md-12 " >
                                                                <a class="folder-small task-list-item filename" style="font-size:13px;margin-left:15px; margin-right:15px;"  data-placement="top" rel="tooltip" ng-click="select(g.collectiontask,4)" ng-right-click="select(g.collectiontask,4)" title="@{{g.collectiontask.file.split('/').slice(-1)[0]}}">
                                                                        
                                                                    <img ng-src="{{asset('/assets/img')}}/@{{getIcon(g.collectiontask.file.split('.').slice(-1)[0])}}" />
                                                                    @{{g.collectiontask.file.split('/').slice(-1)[0].split('.').slice(0,1)[0].substr(0,10)}}..
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <p class="description" ng-hide="g.collectiontask.file"><br><i>(Task file not provided)<i></p>
                                                        
                                                        <br>
                                                        <br>
                                                        <p ng-show="g.deadline" class="task_deadline text-center pull-down-task-deadline">
                                                            Deadline: <span>@{{g.deadline | date: 'MMM d, y'}}</span>
                                                        </p>
                                                        <div ng-switch="g.mysubmission">
                                                            <div ng-switch-when="null">
                                                                <p class="text-center task_action pull-down-task-actions">
                                                                    <button ng-if="g.status == 'open'" class="btn btn-primary" data-toggle="modal" data-target="#uploadFile" style="margin-top: 15px;" ng-click="select(g,4)">Submit</button>
                                                                </p>
                                                            </div>
                                                            <div ng-switch-default>
                                                                <p class="text-center task_action pull-down-task-actions">
                                                                    <button ng-if="g.status == 'open' && !g.mysubmission.grade" class="btn btn-warning" data-toggle="modal" data-target="#editFile" style="margin-top: 15px;" ng-click="select(g.mysubmission,5)">Edit</button>
                                                                    <button class="btn btn-default" style="margin-top: 15px;" ng-click="select(g.mysubmission,5);task_open();">View</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <p class="description task_posted_date pull-down-task-date">
                                                            <span><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                    <p class="text-center" id="ViewMoreTaskBeforeLoading">
                                        <a href="JavaScript:Void(0);" ng-hide="t_pagn_count>=tasks.length" ng-click="viewMoreTasks()">View more</a>
                                        <span ng-show="t_pagn_count>=tasks.length">No more items</span>
                                    </p> 
                                    <p class="text-center">
                                        <img id="ViewMoreTaskLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                                    </p>    
                                   
                                </div>

                                <div id="context-menutask" class=" clearfix animated fast fadeIn">
                                    <ul class="dropdown-menu dropdown-right-click" role="menu" aria-labelledby="dropdownMenu">
                                
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                                <i class="fa fa-eye"></i> Preview
                                            </a>
                                        </li>
                                
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="download()">
                                                <i class="fas fa-download"></i> Download
                                            </a>
                                        </li>
                                
                                    </ul>
                                
                                </div>

                                <div class="modal fade" id="viewTask" tabindex="-1" role="dialog" aria-labelledby="viewTaskLabel" aria-hidden="true" style="top:15%";>
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header justify-content-center" style="background:  white;">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            <h4 class="title">@{{selectedTask.name}}</h4>
                                             
                                            </div> 
                                            <div class="modal-body">
                                                <p class="description">@{{selectedTask.collectiontask.description}}</p>
                                                    
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane" id="extra_tab" role="tabpanel">
                                <p class="text-center">
                                    <img id="PostMainLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                </p> 
                                <div class="container">
                                    <div class="row post-list" id="PostMainBeforeLoading"  hidden ng-cloak>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <div class="general_extra" ng-repeat="g in generals | filter: type='extra' | limitTo: p_pagn_count" style="padding: 0rem 1rem;">
                                                <div class="container">
                                                    <div class="extra_head">
                                                        <div class="row">
                                                            <div class="col-2 pdimg">
                                                                @if($drive->owner->data()->photo)
                                                                    <img src="{{ $drive->owner->data()->photo}}" alt="{{ $drive->owner->data()->name }}"/>
                                                                @else
                                                                    <img src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $drive->owner->data()->name }}"/>
                                                                @endif

                                                            </div>
                                                            <div class="col-6 pdname">
                                                                <span>{{ $drive->owner->data()->name }}</span><br>
                                                                <span class="general_posted_date"><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <p ng-if="g.content.length<400" class="description" style="text-align:justify; font-style: normal;" ng-bind-html="g.content">
                                                    <div ng-if="g.content.length>400">
                                                        <p class="description" style="text-align:justify; font-style: normal;" ng-bind-html="g.content.substr(0,200)"></p>
                                                        <a href="javascript:void(0);" class="viewmorelink" >View more..</a>
                                                    </div>
                                                    <div ng-if="g.content.length>400" hidden>
                                                        <p class="description" style="text-align:justify; font-style: normal;" ng-bind-html="g.content"></p>
                                                        <a href="javascript:void(0);" class="viewlesslink" >..hide</a>
                                                    </div>
                                                            
                                                    </p>
                                                    <div class="row">
                                                        <div class="col-md-3 " ng-repeat="f in g.postedfiles">
                                                            <a class="folder-medium post-list-item filename" onhover="this.css('text-decoration','underline');" style="font-size:15px;margin-left:15px; margin-right:15px;" title="@{{f.file.split('/').slice(-1)[0]}}" ng-click="select(f,3)" ng-right-click="select(f,3)">
                                                                    
                                                                <img ng-src="{{asset('/assets/img')}}/@{{getIcon(f.file.split('.').slice(-1)[0])}}"/>
                                                                @{{f.file.split('/').slice(-1)[0].split('.').slice(0,1)[0].substr(0,10)}}..
                                                            </a>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <p class="text-center" id="ViewMorePostBeforeLoading">
                                                <a href="JavaScript:Void(0);" ng-hide="p_pagn_count>=posts.length" ng-click="viewMorePosts()">View more</a>
                                                <span ng-show="p_pagn_count>=posts.length">No more items</span>
                                            </p> 
                                            <p class="text-center">
                                                <img id="ViewMorePostLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                                            </p> 
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                        </div>

                                        <div class="col-lg-3 col-md-3 col-sm-12">
                                            <div class="container">
                                                <div class="card shared_materials floating_card" style="">
                                                    <div class="text-center shared_header" style="height:25%" >
                                                        
                                                        <p class="">Shared Materials</p>
                                                        <hr style="margin:0.3rem">
                                                    </div>
                                                    
                                                    <div class="container">
                                                        <div class="card-body listfiles">
                                                            <ul>
                                                                <li ng-repeat="f in postfiles|limitTo:4">
                                                                    <div class="">
                                                                        <a class="folder-small post-list-item filename" style="font-size:13px;margin-left:15px; margin-right:15px;cursor:pointer" title="@{{f.file.split('/').slice(-1)[0]}}" ng-click="select(f,3)" ng-right-click="select(f,3)">
                                                                            <img ng-src="{{asset('/assets/img')}}/@{{getIcon(f.file.split('.').slice(-1)[0])}}"/>&nbsp;
                                                                            @{{f.file.split('/').slice(-1)[0].split('.').slice(0,1)[0].substr(0,10)}}..
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                                
                                                            </ul>
                                                                
                                                            <p class="text-center" ng-show="postfiles.length>4"> <a href="javascript:void(0)" class="" data-toggle="modal" data-target="#allPostFiles">View All</a></p>
                                                        </div>
                                                    </div>

                                                    <div class="modal fade" id="allPostFiles" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header justify-content-center" style="border-radius: 4px 4px 0 0; background: none; padding-top: 24px; padding-right: 24px;padding-bottom: 0;padding-left: 24px;">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                        <i class="now-ui-icons ui-1_simple-remove"></i>
                                                                    </button>
                                                                    <h4 class="title">All Shared Files</h4>
                                                                </div> 
                                                                <div class="modal-body justify-content-center" >
                                                                    <div style="height:20px;margin-bottom:30px">
                                                                        <input type="text" style="float:right;max-width:220px" class="form-control" ng-model="postfilesearch" placeholder="Search.." ng-keyup="$event.keyCode == 13"/>
                                                                    </div>
                                                                    <ul style="height:20rem; overflow-y:auto; padding:0.5rem 0.5rem;">      
                                                                        <li ng-repeat="f in postfiles| filter:{ file : postfilesearch}">
                                                                            <div class="">
                                                                                <a class="folder-small post-list-item filename" style="font-size:13px;margin-left:15px; margin-right:15px;cursor:pointer" title="@{{f.file.split('/').slice(-1)[0]}}" ng-click="select(f,3)" ng-right-click="select(f,3)">
                                                                                    <img ng-src="{{asset('/assets/img')}}/@{{getIcon(f.file.split('.').slice(-1)[0])}}"/>&nbsp;
                                                                                    @{{f.file.split('/').slice(-1)[0].substr(0,50)}}..
                                                                                </a>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    
                                                                        
                                                                </div>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>   
                                        
                                            </div>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        <br>
                                        <br>

                                    </div>
                                </div>
                                <div id="context-menupost" class=" clearfix animated fast fadeIn">
                                    <ul class="dropdown-menu dropdown-right-click" role="menu" aria-labelledby="dropdownMenu">
                              
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                                <i class="fa fa-eye"></i> Preview
                                            </a>
                                        </li>
                                
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="download()">
                                                <i class="fas fa-download"></i> Download
                                            </a>
                                        </li>
                                
                                    </ul>
                                
                                </div>
                            </div>

                            <div class="tab-pane" id="notice_tab" role="tabpanel">
                                 <p class="text-center">
                                    <img id="NoticeMainLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                </p>
                                <div class="container" id="NoticeMainBeforeLoading" hidden style="align:left">
                                    <div class="general_items">
                                        <div ng-repeat="g in generals | limitTo: n_pagn_count">
                                          
                                            <div class="general_notice" ng-if="g.type=='notice'">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p class="text-center">
                                                            <i  class="far fa-bell general_icons"></i>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="container">
                                                            <div class="pull-right">
                                                                <span class="badge badge-primary" >Notice</span>
                                                            </div>
                                                        <h5>@{{g.title}}</h5>
                                                            <p ng-if="g.description.length<400" class="description" style="text-align:left;">@{{g.description}}
                                                            </p>
                                                            <div ng-if="g.description.length>400">
                                                                <p class="description" style="text-align:left;">@{{g.description.substr(0,200)}}</p>
                                                                <a href="javascript:void(0);" class="viewmorelink" >View more..</a>
                                                            </div>
                                                            <div ng-if="g.description.length>400" hidden>
                                                                <p class="description" style="text-align:left;">@{{g.description}}</p>
                                                                <a href="javascript:void(0);" class="viewlesslink" >..hide</a>
                                                            </div>
                                                            <p class="description general_posted_date">
                                                                <span><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="general_result" ng-if="g.type=='result'">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p class="text-center">
                                                            <i  class="fa fa-graduation-cap general_icons"></i>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="container">
                                                            <div class="pull-right">
                                                                <span class="badge badge-primary" >Result</span>
                                                            </div>
                                                        <h5>@{{g.collection.name}}</h5>
                                                            <p class="description" style="text-align:left;">@{{g.message}}
                                                            </p><p>
                                                        <a href="/class/@{{drive_id}}/@{{g.id}}/viewresult" target="_blank" class="btn btn-success btn-sm pull-right">View Result</a>
                                                            </p>
                                                            <br>   
                                                            <br>
                                                        <p class="description general_posted_date">
                                                                <span><i>@{{g.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-center" id="ViewMoreNoticeBeforeLoading">
                                        <a href="JavaScript:Void(0);" ng-hide="n_pagn_count>=notices.length+results.length" ng-click="viewMoreGenerals()">View more</a>
                                        <span ng-show="n_pagn_count>=notices.length+results.length">No more items</span>
                                    </p> 
                                    <p class="text-center">
                                        <img id="ViewMoreNoticeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($boxad)
            <div class="container_adv">                 
                <div class="banner_adv">
                        <span class="pull-right banner_inner_close" id="closead">X</span>
                        
                        <span class="pull-left banner_inner_badge">Ad:</span>
                        <div class="banner_inner_adv">
                        <a href="{{$boxad->link}}" target="_blank">{{$boxad->content}}</a>
                        </div>
                </div>
            </div>
            @endif
        </div>

        <div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="uploadFileLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center" style="background:  white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <h4 class="title">Submit Assignment</h4>
                        <br>
                    </div>
                    {{-- <p class="text-center">
                        <img id="uploadFileLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                    </p>  --}}
                    <div class="myProgress" style="padding:2rem" ng-if="uploadtsk">
                    </div>
                    <form ng-submit="uploadTask()"  method="POST">
                        <div id="uploadFileBeforeLoading">
                            <div class="modal-body">
                                <p style="text-align:  center;color: red;" id="taskuploadError"></p>

                                <label>Roll No:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="far fa-user"></i>
                                    </span>
                                    
                                    <input type="text" class="form-control" ng-model="_rollno" placeholder="Enter Your Roll No." ng-disabled="!editmode" }}>

                                    <span class="input-group-addon" ng-show="rollno">
                                        <i ng-class="{ true:'far fa-times-circle',false:'far fa-edit'}[editmode]" title="@{{ editmode ? 'cancel':'edit' }}" style="margin-left:  5px;cursor:  pointer;" ng-click="editmode=!editmode;_rollno=rollno"></i>
                                    </span>
                                </div>
                                <label>Upload Assignment</label>
                                <div class="input-group">

                                    <span class="input-group-addon">
                                        <i class="far fa-file"></i>
                                    </span>
                                    
                                    <input type="file" class="form-control" fileread="file" ng-model="file">
                                </div>
                                
                            </div>
                            <div class="modal-footer justify-content-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editFile" tabindex="-1" role="dialog" aria-labelledby="editFileLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center" style="background:  white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <h4 class="title">Edit</h4>
                        <br>
                    </div>
                    <p class="text-center" style="margin-bottom: 0rem;">
                        <img id="editFileLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                    </p>
                    <div class="myProgress" style="padding:2rem" ng-if="edittsk">
                    </div>
                    <form ng-submit="editUploadedTask()"  method="POST">
                        <div id="editFileBeforeLoading">
                            <div class="modal-body">
                            	<p class="text-center">*Re-submitting new file will replace previously submitted file.</p>
                                <p style="text-align:  center;color: red;margin-bottom: 0rem;" id="taskeditError"></p>
                                <label>Roll No:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="far fa-user"></i>
                                    </span>
                                    
                                    <input type="text" class="form-control" ng-model="_rollno" placeholder="Enter Your Roll No." ng-disabled="!editmode" }}>

                                    <span class="input-group-addon" ng-show="rollno">
                                        <i ng-class="{ true:'far fa-times-circle',false:'far fa-edit'}[editmode]" title="@{{ editmode ? 'cancel':'edit' }}" style="margin-left:  5px;cursor:  pointer;" ng-click="editmode=!editmode;_rollno=rollno"></i>
                                    </span>
                                </div>
                                <label>Upload New Assignment (Optional):</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="far fa-file"></i>
                                    </span>
                                    
                                    <input type="file" class="form-control" fileread="file" ng-model="file">
                                </div>
                                
                            </div>
                            <div class="modal-footer justify-content-center">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
       
        
        <div class="modal fade" id="viewerpopup" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center" style="background-color: #00000096;color: white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="cursor:pointer">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <button type="button" class="close" style="background-color:  transparent;right: 90px;cursor:pointer" ng-click="download()">
                            <i class="fas fa-download"></i>
                        </button>
                        <h4 class="title ng-binding" style="opacity:  0;">@{{ selectedname }}</h4>
                    </div>
                        
                    <div class="modal-body">
                        <p class="text-center">
                            <img id="BeforeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                        </p>
                        <div style="text-align: center;">
                            <h5 ng-hide="viewerror==''" style="color:white;margin-top:100px;">@{{viewerror}}</h5>
                            <iframe id="viewerimage" hidden ng-src="@{{contentimage}}" style="height: 85vh;width: 300px;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                            <iframe id="viewerdoc" hidden ng-src="@{{content}}" style="height: 85vh;width: 100%;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="downloadlist" ng-hide="downloads.length==0" ng-cloak class="alert alert-info" role="alert" style="position: fixed;right: 20px;bottom: 20px;min-width:500px;background-color: #4397b6;">
            <strong>Downloads: </strong>
            <ul style="list-style:none;margin-bottom: 0px;">
                <li ng-repeat="dl in downloads">
                    @{{ dl.name }}
                    <a ng-click="canceldownload(dl.id,dl.type)" style="color:white" href="javascript:void(0)">
                        <i class="fa fa-remove" style="margin-left: 20px;" title="Cancel"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@if(! App\Class_visited::hasVisited($drive->id,Auth::user()->id))
{{App\Class_visited::visit($drive->id,Auth::user()->id)}}

<div class="modal fade" id="firstTimeModal" tabindex="-1" role="dialog" aria-labelledby="errorDriveLabel" aria-hidden="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                @if($drive->owner->data()->photo)
                    <img class="photo-container" src="{{ $drive->owner->data()->photo}}" alt="{{ $drive->owner->data()->name }}">
                @else
                    <img class="photo-container" src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $drive->owner->data()->name }}">
                @endif
            </div>
            <div class="modal-body">
                <p class="description" style="font-style:normal !important; padding-top:1rem">Hello <b>{{Auth::user()->data()->name}}</b>, welcome to the class of <b>‘{{$drive->name}}’</b>. 
                    I am <b>{{ $drive->owner->data()->name }}</b>, your instructor for this class.
                    I will be sharing all the relevant subject matters and will give you assignments and tasks through this platform. So, check regularly and stay updated.
                    <br>Enjoy learning! </p>
                         <img class="pull-right" style="height:5rem;" src="/logo.png" alt="{{ $drive->owner->data()->name }}">
            </div>
        </div>
    </div>
</div>
@endif
    
@endsection

@section('js')

{{-- @if(! App\Class_visited::hasVisited($drive->id,Auth::user()->id)) --}}
<script>
    $(document).ready(function(){
        $('#firstTimeModal').modal('show');
    })
    </script>
{{-- @endif --}}
<script src="/js/angular-sanitize.js"></script>
<script type='text/javascript' src='/build/loading-bar.js'></script>
<script>
    var span = $('#extraFixed'),
    div = $('#PostMainBeforeLoading'),
    win = $('#extra_tab'),
    divHeight = div.height();
    $('#PostMainBeforeLoading').scroll(compute).scroll();
    $('#extra_tab').resize(compute).resize();
    
    function compute() {
    var spanHeight = span.outerHeight(),
        spanOffset = span.offset().top + spanHeight,
        divOffset = div.offset().top + divHeight;

    if (spanOffset >= divOffset) {
        
        var windowScroll = win.scrollTop() + win.height() - 50;
        if (spanOffset > windowScroll) {
        span.removeClass('bottom');
        }
    }
    }
</script>
<script>
    $('#viewerimage').on('load',function(){
         $('#BeforeLoading').attr('hidden', 'hidden');
         $(this).removeAttr('hidden');
         $(this).contents().find('img').css('max-width','300px');
    });
    
    $('#viewerdoc').on('load',function(){
         $('#BeforeLoading').attr('hidden', 'hidden');
         $(this).removeAttr('hidden');
    });
    
    $('#viewerpopup').on('hidden.bs.modal', function () {
    	$(this).find('#viewerimage')[0].contentWindow.location.reload(true);
    	$(this).find('#viewerimage').attr('src','');
    	$(this).find('#viewerdoc').attr('src','');
    });
</script>
<script>
    $(document).ready(function(){
        var eTop = $('.floating_card').offset().top; //get the offset top of the element
        $('#generaltab').trigger('click');    
        $(window).scroll(function() { //when window is scrolled
            // console.log($(window).scrollTop());
            var sc=$(window).scrollTop();
            $('.floating_card').each(function(index){
                var et=$(this).parent().offset().top;
                var eb= $(this).parent().height();
                if(sc<eb+100 && sc>=et-100)
                {
                    $(this).css('top',sc-et+100);
                }
                else if(sc<et)
                {
                    $(this).css('top',0);
                }
                
            })
        });
    });
   
    $(document).ready(function(){
        var hideAllTab=function(){
            $('[id*="_tab"]').attr('hidden',true);
        }
        $('#generaltab').click(function(){
            hideAllTab();
            $('#general_tab').removeAttr('hidden');
        });
        $('#boardtab').click(function(){
            hideAllTab();
            $('#board_tab').removeAttr('hidden');
        });
        $('#drivetab').click(function(){
            hideAllTab();
            $('#drive_tab').removeAttr('hidden');
        });
        $('#tasktab').click(function(){
            hideAllTab();
            $('#task_tab').removeAttr('hidden');
        });
        $('#extratab').click(function(){
            hideAllTab();
            $('#extra_tab').removeAttr('hidden');
        });
        $('#noticetab').click(function(){
            hideAllTab();
            $('#notice_tab').removeAttr('hidden');
        });
        $('#closead').click(function(){
            $('.container_adv').attr('hidden','true');
        })
        $('#generaltab').trigger('click');

        angular.bootstrap($('#ClassView'), ['classapp']);
    })
</script>
<script>
    $(document).on("click", ".viewmorelink", function() {
        $(this).parent().attr('hidden',true);
        $(this).parent().next().removeAttr('hidden');
    });
    $(document).on("click", ".viewlesslink", function() {
        $(this).parent().attr('hidden',true);
        $(this).parent().prev().removeAttr('hidden');
    });
</script>

<script src="{{ asset('classroom/general.js') }}"></script>
@endsection