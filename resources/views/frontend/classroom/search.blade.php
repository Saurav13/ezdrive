@extends('layouts.app')

@section('body')
    <style>
        .search-result img{
            height:130px;
            width: 130px;
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true">
            </div>
            <div class="container">
                <div class="content-center">
                    <form class="form" method="GET" action="{{ route('searchmore') }}">
                        <div class="content">
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </span>
                                <input type="text" class="form-control" name="search" placeholder="Search">
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <h2 class="title">Search Results</h2>
                <div class="team">
                    <div class="row" >
                        @foreach($results as $r)
                            <div class="col-md-3">
                                <div class="search-result">
                                    @if($r->photo)
                                    <img src="{{$r->photo}}" alt="{{ $r->name }}" class="rounded-circle img-raised">
                                    @else
                                <img src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $r->name }}" class="rounded-circle img-raised">
                                    @endif
                                    <h4 class="title"><a href="{{ route('publicprofile',$r->slug) }}" style="color:#212529">{{ $r->name }}</a></h4>
                                    <p class="category text-primary" style="font-size: 12px;margin-bottom:  5px;">{{$r->designation}}</p>
                                    <p class="category text-primary" style="font-size: 13px;"> {{$r->organization}}</p>
                                    <p class="description">{{(strlen($r->about) < 85) ? $r->about : substr($r->about,0,85).'..'}}</p>
                                    
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @if(count($results)==0)
                    	<p style="font-size:20px;text-align:center;margin:50px">No results found</p>
                    @endif
                        
                    @if($results)
                        <div style="text-align:center;display: inline-block;margin-top:25px;">
                            {!! $results->appends($_GET)->links() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection