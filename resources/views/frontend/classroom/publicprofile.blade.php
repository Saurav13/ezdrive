@extends('layouts.app')

@section('body')
<style>
    .description{
        font-weight:500;
    }
    .info {
        max-width: 360px;
        margin: 0 auto;
        padding: 70px 0 30px;
        text-align: center;
    }
    .icon.icon-primary {
        color: #f96332;
    }
    
    .info .icon {
        color: #888888;
        transition: transform .4s, box-shadow .4s;
    }
    .info.info-hover:hover .icon.icon-primary+.info-title {
        color: #f96332;
    }
    .info.info-hover:hover .icon {
        -webkit-transform: translate3d(0, -0.5rem, 0);
        -moz-transform: translate3d(0, -0.5rem, 0);
        -o-transform: translate3d(0, -0.5rem, 0);
        -ms-transform: translate3d(0, -0.5rem, 0);
        transform: translate3d(0, -0.5rem, 0);
    }
    .info.info-hover .info-title {
        transition: color .4s;
    }
    .info .info-title {
        margin: 25px 0 15px;
        padding: 0 15px;
        color: #2c2c2c;
    }
    .info p {
        color: #888888;
        padding: 0 15px;
        font-size: 1.1em;
    }
    .profile-page .page-header {
        min-height: 300px;
    }
    .nav-pills1 .nav-item .nav-link{
        padding: 10px;
        text-align: center;
        height: 100px;
        width: 100px;
        font-weight: 400;
        color: #9A9A9A;
        margin-right: 19px;
        background-color: white;
        border-radius: 10px;
    }
    .nav-pills1 .nav-item .nav-link.active, .nav-pills .nav-item .nav-link.active:focus, .nav-pills .nav-item .nav-link.active:hover {
        background-color: #2385aa;
        color: #FFFFFF;
        box-shadow: 0px 5px 35px 0px rgba(0, 0, 0, 0.3);
    }
        
 
    .folder{
        padding: 0px;
        margin:32px;
        margin-top:12px;
    }

    .folder img{
        height: 50px;
        width: 50px;
    }
    .uploadicon{
        font-size:25px; 
        margin-top:10px;
    }
            
    .uploadicon:hover{
        font-size:30px;
        margin-top:5px;
    }
    .pull-down-footer{
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
    .pull-down-info{
        position: absolute;
        bottom: 4rem;
        left: 0;
        right: 0;
    }
    .pull-down-code{
        position: absolute;
        bottom: 7.5rem;
        left: 0;
        right: 0;
    }
    .student_number{
        font-size: 11px;
        margin-right: 5px;
        margin-top: 3px;
        background: #2385aa;
        z-index: 100;
        right: -1px;
        top: -3px;
        color: white;
        position: absolute;
        font-size: 10px;
        border: 2px solid #2385aa;
        border-radius: 50%;
        padding: 0px 6px;
        font-weight: 600;
    }
</style>
<div class="modal fade" id="teacherProfile" tabindex="-1" role="dialog" aria-labelledby="myProfileLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center" style="border-radius: 4px 4px 0 0; background: none; padding-top: 24px; padding-right: 24px;padding-bottom: 0;padding-left: 24px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="z-index: 10;">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
            </div>
            <div class="modal-body justify-content-center">
                <div class="photo-container">
                    @if($user->data()->photo)
                        <img src="{{ $user->data()->photo}}" alt="{{ $user->data()->name }}">
                    @else
                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $user->data()->name }}">
                    @endif
                </div>
                
                <br>
                
                @if($user->data()->name)
                <p class="description" style="font-size:18px;">{{$user->data()->name}}</p>
                @endif
                @if($user->data()->designation)
                <p class="description" style="font-size:18px;">{{$user->data()->designation}}</p>
                @endif
                @if($user->data()->email)
                <p class="description" style="font-size:18px;"><b>Email:  </b>{{$user->data()->email}}</p>
                @endif
                @if($user->data()->phone)
                <p class="description" style="font-size:18px;"><b>Phone </b> {{$user->data()->phone}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="page-header page-header-small" style="height: 100vh; max-height: 300px;" filter-color="blue">
        <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg5.jpg');">
        </div>
        <div class="container">
            <div class="content-center">
                    
                <div class="photo-container">
                    <a style="cursor:pointer" data-toggle="modal" data-target="#teacherProfile">
                    @if($user->data()->photo)
                        <img src="{{ $user->data()->photo}}" alt="">
                    @else
                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="">
                    @endif
                    </a>
                </div>
                <a style="cursor:pointer" data-toggle="modal" data-target="#teacherProfile">
                   
                <h3 class="title">{{ $user->data()->name }}</h3>
                </a>
                <p class="category">{{$user->data()->designation}}</p>
              
                    
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            
        </div>
        


        <div class="container text-center">
            <br>
            <h5 class="category text-gray">Class</h5>
            <div class="row">
                @forelse($user->drives as $d)
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-profile"  style="height:21rem;">
                                <span rel="tooltip" data-placement="top" title="Number of Students" class="student_number pull-right">{{$d->followers()->count()}}</span>
                            
                            <div class="card-body">
                            <h6 class="category text-gray" title="{{$d->college}}">{{strlen($d->college)>32?substr($d->college,0,32).'..':$d->college}}<</h6>
                                <h4 class="card-title"><a href="{{ route('class',$d->slug) }}" style="color:#2c2c2c" title="{{$d->name}}">{{ substr($d->name,0,22) }}</a></h4>
                                <p class="card-description pull-down-code" rel="tooltip" data-placement="top" title="Class Code"  style="font-weight:600;">
                                    {{$d->code}}
                                </p>
                                <p class="card-description pull-down-info" title="{{$d->detail}}">
                                    {{substr($d->detail,0,40)}}
                                </p>
                                <div class="card-footer pull-down-footer">
                                    @if(Auth::check())
                                    	<a href="{{ route('class',$d->slug) }}" class="btn btn-primary btn-round">Open</a>
                                    	<button class="btn btn-primary btn-round" id="Drive{{ $d->id }}" drive-id="{{ $d->id }}" drive-name="{{ $d->name }}">{{ Auth::user()->followed_drives()->where('drives.id',$d->id)->exists() ? 'Leave':(Auth::user()->pending_drives()->where('drives.id',$d->id)->exists() ? 'Requested':'Join') }}</button>
                                    @else
                                    	<a href="{{ URL::to('searchbycode?code='.$d->code) }}" class="btn btn-primary btn-round">View</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-md-12 col-lg-12">
                        <h5 class="description" style="font-weight:600;">
                            {{ $user->name }} hasn't created any class yet.
                        </h5>
                    </div>
                @endforelse 
                
            </div>   
        </div>
    </div>

    <div class="modal fade modal-mini modal-primary" id="confirmAlert" tabindex="-1" role="dialog" aria-labelledby="confirmAlertLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div >
                    <div class="modal-header justify-content-center">
                       
                        
                    </div>
                    <div class="modal-body">
                        <p id="confirmText"></p>
                    </div>
                    <div class="modal-footer">
                        <button id="confirmOk" type="button" class="btn btn-link btn-neutral">OK</button>
                        <button type="button" id="confirmNo" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="errorAlert" tabindex="-1" role="dialog" aria-labelledby="errorDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Opps!</h4><br>
            </div>
            <div class="modal-body">
            <p class="description" style="color:red;" id="errorMessage"></p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        
        $("[id*='Drive']").click(function(e){

            var ele = $(this);
            
            var text = (ele.text() == 'Requested' ? 'Cancel request to join' : ele.text()) + ' ' + ele.attr('drive-name') + '?';
            $('#confirmText').html(text);
            $('#confirmAlert').modal('show');

            $('#confirmOk').unbind().click(function(){
                $('#confirmAlert').modal('hide');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::to('follow') }}/"+ele.attr('drive-id'),
                    dataType: 'json',
                    beforeSend: function(){
                        ele.attr('disabled','disabled');
                    },
                    success: function(response) {
                        ele.html(response.action);
                    },
                    error: function(){
                        $('#errorMessage').html('Something went wrong. Pleaase Try again');
                        $('#errorAlert').modal('show');
                    },
                    complete: function(){
                        ele.removeAttr('disabled');
                    }
                });
            });
        });

    });
   
</script>
@endsection