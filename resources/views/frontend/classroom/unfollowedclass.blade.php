@extends('layouts.app')

@section('body')
<link rel="stylesheet" href="{{ asset('dist/angular-filemanager.min.css') }}">
<style>
    [ng\:cloak], [ng-cloak], .ng-cloak {
        display: none !important;
    }
    .description{
        font-weight:500;
    }
    .info {
        max-width: 360px;
        margin: 0 auto;
        padding: 70px 0 30px;
        text-align: center;
    }
    .icon.icon-primary {
        color: #f96332;
    }
    
    .info .icon {
        color: #888888;
        transition: transform .4s, box-shadow .4s;
    }
    .info.info-hover:hover .icon.icon-primary+.info-title {
        color: #f96332;
    }
    .info.info-hover:hover .icon {
        -webkit-transform: translate3d(0, -0.5rem, 0);
        -moz-transform: translate3d(0, -0.5rem, 0);
        -o-transform: translate3d(0, -0.5rem, 0);
        -ms-transform: translate3d(0, -0.5rem, 0);
        transform: translate3d(0, -0.5rem, 0);
    }
    .info.info-hover .info-title {
        transition: color .4s;
    }
    .info .info-title {
        margin: 25px 0 15px;
        padding: 0 15px;
        color: #2c2c2c;
    }
    .info p {
        color: #888888;
        padding: 0 15px;
        font-size: 1.1em;
    }
    .profile-page .page-header {
        min-height: 400px;
    }
    .nav-pills1 .nav-item .nav-link{
        padding: 10px;
        text-align: center;
        height: 100px;
        width: 100px;
        font-weight: 400;
        color: #9A9A9A;
        background-color: white;
        border-radius: 10px;
    }
    .nav-pills1 .nav-item .nav-link.active, .nav-pills .nav-item .nav-link.active:focus, .nav-pills .nav-item .nav-link.active:hover {
        background-color: #2385aa;
        color: #FFFFFF;
        box-shadow: 0px 5px 35px 0px rgba(0, 0, 0, 0.3);
    }
        
 
    .folder{
        padding: 0px;
        margin:10px;
        margin-top:12px;
    }

    .folder img{
        height: 50px;
        width: 50px;
    }
    .uploadicon{
        font-size:25px; 
        margin-top:10px;
    }
            
    .uploadicon:hover{
        font-size:30px;
        margin-top:5px;
    }
</style>

<style>
    #context-menu ul, #downloadlist{
        box-shadow: 0 1px 4px rgba(0,0,0,0.6)!important;
    }

    #context-menu ul li, #downloadlist ul li{
        display:flex;
        align-items:center;
        font-size:15px;
    }
    #context-menu ul li:hover{
        background:rgba(100,100,100,0.12);
    }

    #context-menu ul li i, #downloadlist ul li i {
        width:20px; 
    }

    #viewerpopup .modal-dialog {
        min-width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #viewerpopup .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
        background-color: #00000096;
    }
    .drive_breadcrumb{
        padding-left: 2rem;
        border-bottom: 2px solid #dcd9d980;
        color: #6cbaff;
        font-weight: 600;
        font-size: 14px; 
    }
    .drive_breadcrumb_a{
        color: #6cbaff;
    }
    .drive_breadcrumb_seperator{
        font-size: 18px;
        padding-left: 5px;
        padding-right: 5px;
        color: #949fab;
    }
    .drive_breadcrumb_last{
        color: #1677ce;
    }
    @media only screen and (min-width : 320px) and (max-width : 480px) { 
        .r-social-description { display: none !important; }}
        
    
       
</style>
<div class="modal fade" id="teacherProfile" tabindex="-1" role="dialog" aria-labelledby="myProfileLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center" style="border-radius: 4px 4px 0 0; background: none; padding-top: 24px; padding-right: 24px;padding-bottom: 0;padding-left: 24px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title title-up">Profile</h4>
            </div>
            <div class="modal-body justify-content-center">
                <div class="photo-container">
                    <a style="cursor:pointer" data-toggle="modal" data-target="#teacherProfile">
                    @if($drive->owner->data()->photo)
                        <img src="{{ $drive->owner->data()->photo}}" alt="{{ $drive->owner->data()->name }}">
                    @else
                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $drive->owner->data()->name }}">
                    @endif
                    </a>
                    
                </div>
                <br>
                @if($drive->owner->data()->name)
                <p class="description" style="font-size:18px;">{{$drive->owner->data()->name}}</p>
                @endif
                @if($drive->owner->data()->designation)
                <p class="description" style="font-size:18px;">{{$drive->owner->data()->designation}}</p>
                @endif
                @if($drive->owner->data()->email)
                <p class="description" style="font-size:18px;"><b>Email:  </b>{{$drive->owner->data()->email}}</p>
                @endif
                @if($drive->owner->data()->phone)
                <p class="description" style="font-size:18px;"><b>Phone </b> {{$drive->owner->data()->phone}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="page-header page-header-small" style="height: 100vh; max-height: 300px;" filter-color="blue">
        <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg5.jpg');">
        </div>
        <div class="container" style="z-index:0">
            <div class="content-center">
                    
                <div class="photo-container">
                    @if( $drive->owner->data()->photo)
                        <img src="{{ $drive->owner->data()->photo}}" alt="{{ $drive->owner->data()->name }}">
                    @else
                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="{{ $drive->owner->data()->name }}">
                    @endif
                </div>
                <h3 class="title">{{ $drive->name }}</h3>
                <p class="category">{{ $drive->owner->data()->name }}</p>
                <div class="content">
                    <div class="social-description r-social-description">
                        <h2>{{ $drive->followers->count() }}</h2>
                        <p>Students</p>
                    </div>
                    <div class="social-description r-social-description">
                        <h2>{{ $drive->drive_contents->count() }}</h2>
                        <p>Materials</p>
                    </div>
                    <div class="social-description r-social-description">
                        <h2>{{ $drive->posts->count() }}</h2>
                        <p>Posts</p>
                    </div>
                </div>
                    
            </div>
        </div>
    </div>
    <div class="section" ng-app="ClassMaterial">
        <div class="container">
            <div class="button-container" style="z-index:0">
                <form method="POST" action="{{ route('followdrive',$drive->id) }}" style="display:inline">
                    {{ csrf_field() }}
                    @if(Auth::check())
                        <button type="submit" class="btn btn-primary btn-round btn-lg" rel="tooltip" title="{{Auth::user()->pending_drives()->where('drives.id',$drive->id)->exists() ? 'Cancel Request':'Join'}}" >{{ Auth::user()->pending_drives()->where('drives.id',$drive->id)->exists() ? 'Requested':'Join' }}</button>
                    @else
                        <button type="submit" class="btn btn-primary btn-round btn-lg" rel="tooltip" >Join</button>
                    
                    @endif
                </form>
               
            </div>
        </div>
        <br>
        <div class="container" ng-controller="DriveController" ng-init="drive_id={{$drive->id}};openRoot()">
            <h4 class="category text-gray text-center" >
                <a style="float:left" href="javascript:void(0)" ng-click="back()" ng-hide="isRoot" ng-cloak>
                    <i class="fa fa-arrow-left" title="Back" rel="tooltip" data-placement="top"></i>
                </a>Shared Materials
            </h4>
            <div class="row drive_breadcrumb">
                <div class="col-md-8" >
                    <a style="cursor:pointer" class="drive_breadcrumb_a" ng-click="openRoot()"><span>{{$drive->name}}</span></a>
                    <span class="drive_breadcrumb_seperator" ng-cloak>/</span>
                    <span ng-repeat="b in breadcrumbs.slice(0,-1)">
                        <a style="cursor:pointer" class="drive_breadcrumb_a" ng-click="openFolder(b.id)"><span ng-cloak>@{{b.name}}</span></a>
                        <span class="drive_breadcrumb_seperator">/</span>
                    </span>
                    <span class="drive_breadcrumb_last" ng-cloak>@{{breadcrumbs.slice(-1)[0].name}}</span>
                </div>
                <div class="col-md-4">
                    <input type="text" style="float:right;max-width:220px" class="form-control" ng-model="driveSearchKey" placeholder="Search.." ng-keyup="$event.keyCode == 13 && searchDrive(driveSearchKey)"/>
                </div>                                            
            </div> 
            <p class="text-center">
                <img id="DriveMainLoading" class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
            </p>    
            <div class="container"  style="padding-bottom:20px; min-height:400px; padding-right:5px; padding-left:5px;" >
                <div id="DriveMainBeforeLoading" hidden>
                    <ul class="nav nav-pills1 nav-pills-primary nav-pills-icons item-list"  id="navicons" role="tablist">
                        <li class="nav-item item-list-item" ng-repeat="c in folders">
                            <a ng-cloak class="nav-link folder" data-toggle="tab"  role="tablist" ng-click="select(c,1)" ng-dblclick="openFolder(c.id)" ng-right-click="select(c,1)" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" title="@{{c.name}}">
                                {{--  <i class="now-ui-icons files_box" style="font-size:40px" ></i>  --}}
                                <img src="/assets/img/folder.png"/><br>
                                @{{c.name}} </a>
                        </li>
                        <li class="nav-item item-list-item" ng-repeat="c in files">
                            <a ng-cloak class="nav-link folder" data-toggle="tab"  role="tablist"  ng-click="select(c,2)" ng-dblclick="openFile(c.id)" ng-right-click="select(c,2)" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" title="@{{c.name}}">
                                {{--  <i class="now-ui-icons files_box" style="font-size:40px" ></i>  --}}
                                <img src="{{asset('/assets/img')}}/@{{getIcon(c.extension)}}"/><br>
                                <span>@{{c.name}}</span></a>
                        </li>
                    </ul>   
                    <p class="description" ng-cloak ng-show="folders.length==0 && files.length==0"><br><br>No files or folders yet.<p>  
                </div>
                {{-- <div id="context-menu" class=" clearfix animated fast fadeIn">
                    <ul class="dropdown-menu dropdown-right-click" role="menu" aria-labelledby="dropdownMenu">
                
                        <li ng-show="selectType==1">
                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                <i class="fa fa-folder-open"></i> Open
                            </a>
                        </li>

                        <li ng-show="selectType==2">
                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                <i class="fa fa-eye"></i> Preview
                            </a>
                        </li>
                
                        <li>
                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="download()">
                                <i class="fa fa-cloud-download"></i> Download
                            </a>
                        </li>
                
                    </ul>
                
                </div> --}}
            </div>
        </div>

        {{-- <div class="modal fade" id="viewerpopup" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center" style="background-color: #00000096;color: white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="cursor:pointer">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <button type="button" class="close" style="background-color:  transparent;right: 90px;cursor:pointer" ng-click="download()">
                            <i class="fas fa-cloud-download"></i>
                        </button>
                    </div>
                        
                        <div class="modal-body">
                            <p class="text-center">
                                <img id="BeforeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                            </p>
                            <div style="text-align: center;">
                                <h5 ng-hide="viewerror==''" style="color:white;margin-top:100px;">@{{viewerror}}</h5>
                                <iframe id="viewerimage" hidden ng-src="@{{contentimage}}" style="height: 85vh;width: 300px;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                                <iframe id="viewerdoc" hidden ng-src="@{{content}}" style="height: 85vh;width: 100%;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                            </div>
                        </div>
                </div>
            </div>
        </div> --}}

        {{-- <div id="downloadlist" ng-hide="downloads.length==0" ng-cloak class="alert alert-info" role="alert" style="position: fixed;right: 20px;bottom: 20px;min-width:500px;background-color: #4397b6;">
            <strong>Downloads: </strong>
            <ul style="list-style:none;margin-bottom: 0px;">
                <li ng-repeat="dl in downloads">
                    @{{ dl.name }}
                    <a ng-click="canceldownload(dl.id,dl.type)" style="color:white" href="javascript:void(0)">
                        <i class="fa fa-remove" style="margin-left: 20px;" title="Cancel"></i>
                    </a>
                </li>
            </ul>
        </div> --}}
    </div>
</div>
@endsection

@section('js')
<script>
    $('#viewerimage').on('load',function(){
         $('#BeforeLoading').attr('hidden', 'hidden');
         $(this).removeAttr('hidden');
         $(this).contents().find('img').css('max-width','300px');
    });
    
    $('#viewerdoc').on('load',function(){
         $('#BeforeLoading').attr('hidden', 'hidden');
         $(this).removeAttr('hidden');
    });
</script>

<script src="{{ asset('classroom/unauth.js') }}"></script>

@endsection