@extends('layouts.app')

@section('body')
<style>
    .explore_text .title{
        font-size: 28px;
        font-weight: 400;
    }
    .explore_text .description{
        font-size: 21px;
    }
</style>
<div class="wrapper">
     <div class="main">
        <div class="section">
            <div class="container text-center">
                <div class="row justify-content-md-center">
                    <div class="col-md-12">
                        <h2 class="title">Welcome to EZU-Class</h2>
                        <img src="/assets/img/bg8.jpg" style="height:25rem; width:100%"/>
                    </div>
                </div>
                <br>
                
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <p class="title">Share materials</p>
                            <p class="description">Upload materials to your drive and share to your students. Through ezu drive sharing is easy and fun.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <img src="/assets/img/bg8.jpg" style="height:20rem;"/>
                        </div>
                    </div>
                
                </div>
                <br>
                <div class="row">
                    
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <img src="/assets/img/bg8.jpg" style="height:20rem;"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <p class="title">Share materials</p>
                            <p class="description">Upload materials to your drive and share to your students. Through ezu drive sharing is easy and fun.</p>
                        </div>
                    </div>
                
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <p class="title">Share materials</p>
                            <p class="description">Upload materials to your drive and share to your students. Through ezu drive sharing is easy and fun.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <img src="/assets/img/bg8.jpg" style="height:20rem;"/>
                        </div>
                    </div>
                
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <img src="/assets/img/bg8.jpg" style="height:20rem;"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="explore_text">
                            <p class="title">Share materials</p>
                            <p class="description">Upload materials to your drive and share to your students. Through ezu drive sharing is easy and fun.</p>
                        </div>
                    </div>
                    
                
                </div>
                <br>
                <br>
                <p class="text-center">
                    <a href="/" class="btn btn-neutral btn-lg" style="border: 1px solid #2385aa;">Get Started</a>
                </p>
            </div>
        </div>
    </div>
</div>
@stop