@extends('layouts.app')

@section('body')
<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link href="/tagit/jquery.tagit.css" rel="stylesheet" type="text/css"/>
<link href="/tagit/tagit.ui-zendesk.css" rel="stylesheet" type="text/css"/>
<link rel='stylesheet' href='/build/loading-bar.css' type='text/css' media='all' />
<style>
	#theBoard p{
	    line-height: .5em;
	    }
    div[data-placeholder]:not(:focus):not([data-div-placeholder-content]):before {
        content: attr(data-placeholder);
        float: left;
        margin-left: 2px;
        color: #b3b3b3;
    }
    #loading-bar .bar {
        position: relative;
    }
    [ng\:cloak], [ng-cloak], .ng-cloak {
        display: none !important;
    }
    .description{
        font-weight:500;
    }
    .info {
        max-width: 360px;
        margin: 0 auto;
        padding: 32px 0 30px;
        text-align: center;
    }
    .m-info {
        max-width: 360px;
        margin: 0 auto;
        padding: 30px 0 30px;
        text-align: center;
    }
    .icon.icon-primary {
        color: #f96332;
    }
    
    .info .icon {
        color: #888888;
        transition: transform .4s, box-shadow .4s;
    }
    .info.info-hover:hover .icon.icon-primary+.info-title {
        color: #f96332;
    }
    .info.info-hover:hover .icon {
        -webkit-transform: translate3d(0, -0.5rem, 0);
        -moz-transform: translate3d(0, -0.5rem, 0);
        -o-transform: translate3d(0, -0.5rem, 0);
        -ms-transform: translate3d(0, -0.5rem, 0);
        transform: translate3d(0, -0.5rem, 0);
    }
    .info.info-hover .info-title {
        transition: color .4s;
    }
    .info .info-title {
        margin: 25px 0 15px;
        padding: 0 15px;
        color: #2c2c2c;
    }
    .m-info p {
        color: #888888;
        padding: 0 15px;
        font-size: 1.1em;
    }
    .m-info .icon {
        color: #888888;
        transition: transform .4s, box-shadow .4s;
    }
    .m-info.info-hover:hover .icon.icon-primary+.info-title {
        color: #f96332;
    }
    .m-info .info-hover:hover .icon {
        -webkit-transform: translate3d(0, -0.5rem, 0);
        -moz-transform: translate3d(0, -0.5rem, 0);
        -o-transform: translate3d(0, -0.5rem, 0);
        -ms-transform: translate3d(0, -0.5rem, 0);
        transform: translate3d(0, -0.5rem, 0);
    }
    .m-info .info-hover .info-title {
        transition: color .4s;
    }
    .m-info .info-title {
        margin: 25px 0 15px;
        padding: 0 15px;
        color: #2c2c2c;
        font-size:18px;
    }
    .m-info .info-description{
        font-size:14px;
    }
    .m-info p {
        color: #888888;
        padding: 0 15px;
        font-size: 1.1em;
    }
    .profile-page .page-header {
        min-height: 290px;
    }
    .nav-pills1 .nav-item .nav-link{
        padding: 10px;
        text-align: center;
        height: 100px;
        width: 100px;
        font-weight: 400;
        color: #9A9A9A;
        margin-right: 19px;
        background-color: white;
        border-radius: 10px;
    }
    .nav-pills1 .nav-item .nav-link.active, .nav-pills .nav-item .nav-link.active:focus, .nav-pills .nav-item .nav-link.active:hover {
        background-color: #2385aa;
        color: #FFFFFF;
        box-shadow: 0px 5px 35px 0px rgba(0, 0, 0, 0.3);
    }
    
    .nav-pills1.nav-pills-primary .nav-item .nav-link .n1 .active,
    .nav-pills1.nav-pills-primary .nav-item .nav-link .n1 .active:focus,
    .nav-pills1.nav-pills-primary .nav-item .nav-link .n1 .active:hover {
        background-color:#2385aa ;
    }
   
    .folder{
        padding: 0px;
        margin:10px;
        margin-top:12px;
    }

    .folder img{
        height: 50px;
        width: 50px;
    }

    .folder1{
        padding: 0px;
        margin:10px;
        margin-top:12px;
        font-size: 13px;
    }

    .folder1 img{
        height: 50px;
        width: 50px;
    }
    .uploadicon{
        font-size:25px; 
        margin-top:10px;
    }
            
    .uploadicon:hover{
        font-size:30px;
        margin-top:5px;
    }
    .folder-small{
        padding: 0px;
        margin:32px;
        margin-top:12px;
    }

    .folder-small img{
        height: 30px;
        width: 30px;
    }
    .folder-medium{
        padding: 0px;
        margin:32px;
        margin-top:12px;
    }

    .folder-medium img{
        height: 35px;
        width: 35px;
    }

    .post::-webkit-input-placeholder {
        /* WebKit, Blink, Edge */
        font-size:20px;
    }
    .post:-moz-placeholder {
        /* Mozilla Firefox 4 to 18 */
        font-size:20px;
    }
    .post::-moz-placeholder {
        /* Mozilla Firefox 19+ */
        font-size:20px;
    }
    .post:-ms-input-placeholder {
        /* Internet Explorer 10-11 */
        font-size:20px;
    }
    .msidenavitem{

    }
    .nav-pills .nav-link.active, .show>.nav-pills .nav-link{
        color: #fff;
        background-color: #4397b6;
    }
    .manager_title{
        border-bottom: 1px solid #d6b8b8;
        padding-left: 7px;
        color: #615b5b;
        font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif;
        font-size: 1.3em;
        font-weight: 700;
        line-height: 1.4em;
    }
    .manager_newreq{
        height:15rem;
        overflow-y:auto;
        overflow-x:hidden;
    }
    .request_ul{
        list-style-type: none;
    }

    .request_li{
        margin-bottom: 1.2rem;
        margin-top: 1.2rem;
    }
    .request_img{
        height:4rem;
    }
    .request_name{
        margin-top: 0.8rem;
        font-size: 1.2rem;

    }
    .request_actions{
        margin-top: 0.6rem;
    }
    .student_img{
        height:4rem;
        margin-bottom: 1.2rem;
    }
    .student_name{
        margin-top: 1.3rem;
        font-size: 0.9rem;
    }
    .student_actions{
        margin-top: 0.6rem;
    }
    .manager_label{
        margin-bottom:0px;
        padding-left: 7px;
        color: #615b5b;
        font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif;
        font-size: 20px;
        font-weight: 700;
        line-height: 1.4em;
    }
    .card_post{
        border: 1px solid #2385aa;
        border-radius: 10px;
    }
    .notice_list_title{
        color: #2383aa;
    }
    .drive_breadcrumb{
        padding-left: 2rem;
        border-bottom: 2px solid #dcd9d980;
        color: #6cbaff;
        font-weight: 600;
        font-size: 14px;
        
    }
    .drive_breadcrumb_a{
       
        color: #6cbaff;
        
    }
    .drive_breadcrumb_seperator{
        font-size: 18px;
        padding-left: 5px;
        padding-right: 5px;
        color: #949fab;
    }
    .drive_breadcrumb_last{
        color: #1677ce;
    }
    .received_from{
        text-align: right;
        padding-right: 1rem;
        margin-bottom:0px;
        border:none;
    }
    .colc_breadcrumb{
        margin-bottom:0px;
        border:none;
    }
    .breadcrumb-border{
        border-bottom: 2px solid #dcd9d980;
    }
    .importFile_inner{
        box-shadow: none;
        border: 2px solid #2385aa;
        border-radius: 5px;
        
    }
    .importFile_inner-overflow{
        height:20rem;
        overflow-y:auto;
        overflow-x:hidden;
        margin: 0.5rem 0rem;
    }
    .importFile_inner p{
        font-weight: 100;
        margin-top: 1rem;
        font-size: 20px;
        line-height: 1rem;
    }
    .importFile_inner li{
        padding: 0.4rem 1rem;
        cursor:pointer;
    }
    .importFile_inner span{
        font-weight: 100;
        margin-top: 1rem;
        font-size: 20px;
        line-height: 1rem;
    }
    .importFile_inner input{
        height: 1rem;
        width: 1rem;
        margin-top: 1rem;
    }
    
    .importFile_inner img{
        height: 2rem;
        margin: 0.5rem 1rem;
    }
    .selectdrive{
        border-radius: 5px;
        margin-bottom: 0.5rem;
    }
    .general_request_container{
        text-align: center !important;
        /* border: 2px solid #d3d3d4; */
        margin: 1rem;
    }
    .general_request_container img{
        height: 4rem;
        margin: 1rem;
    }
    /* .general_request_container button{
        border-radius: 50%;
        height: 2.7rem;
    } */
    .container_adv{
        text-align: center !important;
        margin: 1rem 0rem;
    }
    .banner_adv{
        border: 2px solid #0039b1ad;
        margin: 0px 20%;
       
        border-radius: 6px;
    }
    .banner_inner_adv{
        padding: 1rem 0px;
    }
    .banner_inner_adv a{
        color: #888888;
        font-weight: 600;
        font-size: 16px;
    }
    .banner_inner_close{
        font-size: 11px;
        cursor: pointer;
        margin: 2px;
        margin-right: 4px;
    }
    .banner_inner_badge{
        margin: 3px;
        padding: 0px 6px;
        background: #efbf0a;
        border-radius: 4px;
        color: white;
        font-size: 10px;
    }
    .taskCard{
        border: 1px solid #2385aa;
        border-radius: 11px;
    }
    
    .floating_card_fixed{
        background-color:#ddd;
        position: fixed;
        padding: 2em;
        left: 50%;
        top: 0%;
        transform: translateX(-50%);
    }
    .notice_list{
        padding-left:10px; 
        list-style-type: none;
    }
    .notice_list li::before{
        content: '➢';
        padding-right:1rem;

    }
    .collection_leftgradebadge{
        background: red;
        z-index: 100;
        color:white;
        position: absolute;
        font-size: 10px;
        border: 2px solid red;
        border-radius: 50%;
        padding: 0px 6px;
        font-weight: 600;
    }
    .general_posted_date{
        text-align: left !important;
        margin-bottom: 0;
        font-size:12px !important;
    }
    .general_extra{
        margin-bottom:3rem;
    }
    .general_extra .container{
        padding-bottom: 1rem;
        border-bottom:2px solid #edecec;
    }
    .general_extra .general_icons{
        margin-top: 0rem;
        margin-left: 4rem;
        font-size: 48px;
        /* border: 1px solid #2385aa; */
        /* border-radius: 54px; */
        padding: 1rem;
        font-weight: 100;
    }
    .extra_head{
        text-align:left !important;
    }
    .extra_head img{
        width: 3rem;
        height: 3rem;
        border-radius: 50%;
        overflow: hidden;
        margin: 0 auto;
        box-shadow: 0px 10px 25px 0px rgba(0, 0, 0, 0.3);
    }
    .extra_head .pdimg{
        padding-left: 2.5rem;
        padding-right: 1.5rem;
    }
    .extra_head .pdname{
        padding:0px;
    }
    .notice_card{
        min-height: 0px;
        padding: 0.8rem;
        padding-bottom: 0.5rem;
        border-radius: 8px;
        border: 2px solid #a0a0a082;
    }
    .newpost_actions{
        border: 2px solid #ddd !important;
        padding: 0.5rem 0.8rem;
        border-radius: 8px;
    }
    
    .newpost_actions i{
        font-size: 20px;
        color: #bababa;
    }
    .newpost_actions:hover i{
        font-size: 20px;
        color: #2385aa;
    }
    .result_list li{
        margin:0.5rem 1rem;
    }
    .result_list a{
        font-size:17px;
    }
    .info_i{
        position: absolute;
        bottom: 2px;
        right: 6px;
        cursor: pointer;
    }
    .tgl {
    display: none;
    }
    .tgl, .tgl:after, .tgl:before, .tgl *, .tgl *:after, .tgl *:before, .tgl + .tgl-btn {
    box-sizing: border-box;
    }
    .tgl::-moz-selection, .tgl:after::-moz-selection, .tgl:before::-moz-selection, .tgl *::-moz-selection, .tgl *:after::-moz-selection, .tgl *:before::-moz-selection, .tgl + .tgl-btn::-moz-selection {
    background: none;
    }
    .tgl::selection, .tgl:after::selection, .tgl:before::selection, .tgl *::selection, .tgl *:after::selection, .tgl *:before::selection, .tgl + .tgl-btn::selection {
    background: none;
    }
    .tgl + .tgl-btn {
    outline: 0;
    display: block;
    width: 6em;
    height: 2em;
    position: relative;
    cursor: pointer;
    -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;
    }
    .tgl + .tgl-btn:after, .tgl + .tgl-btn:before {
    position: relative;
    display: block;
    content: "";
    width: 50%;
    height: 100%;
    }
    .tgl + .tgl-btn:after {
    left: 0;
    }
    .tgl + .tgl-btn:before {
    display: none;
    }
    .tgl:checked + .tgl-btn:after {
    left: 50%;
    }
    .tgl-skewed + .tgl-btn {
    overflow: hidden;
    -webkit-transform: skew(-10deg);
            transform: skew(-10deg);
    -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
    transition: all .2s ease;
    font-family: sans-serif;
    background: #888;
    }
    .tgl-skewed + .tgl-btn:after, .tgl-skewed + .tgl-btn:before {
    -webkit-transform: skew(10deg);
            transform: skew(10deg);
    display: inline-block;
    transition: all .2s ease;
    width: 100%;
    text-align: center;
    position: absolute;
    line-height: 2em;
    font-weight: bold;
    color: #fff;
    text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
    }
    .tgl-skewed + .tgl-btn:after {
    left: 100%;
    content: attr(data-tg-on);
    }
    .tgl-skewed + .tgl-btn:before {
    left: 0;
    content: attr(data-tg-off);
    }
    .tgl-skewed + .tgl-btn:active {
    background: #888;
    }
    .tgl-skewed + .tgl-btn:active:before {
    left: -10%;
    }
    .tgl-skewed:checked + .tgl-btn {
    background: #2385aa;
    }
    .tgl-skewed:checked + .tgl-btn:before {
    left: -100%;
    }
    .tgl-skewed:checked + .tgl-btn:after {
    left: 0;
    }
    .tgl-skewed:checked + .tgl-btn:active:after {
    left: 10%;
    }



</style>
<link rel="stylesheet" href="{{ asset('dist/angular-filemanager2.min.css') }}">
	
<style>
    #context-menu ul,#context-menucolct ul, #downloadlist{
        box-shadow: 0 1px 4px rgba(0,0,0,0.6)!important;
    }

    #context-menu ul li,#context-menucolc ul li, #downloadlist ul li{
        display:flex;
        align-items:center;
        font-size:15px;
    }
    #context-menu ul li:hover, #context-menucolc ul li:hover{
        background:rgba(100,100,100,0.12);
    }

    #context-menu ul li i,#context-menucolc ul li i, #downloadlist ul li i {
        width:20px; 
    }

    #context-menucolc {
        position: absolute;
        display: none;
        z-index: 9999;
    }
    #viewerpopup .modal-dialog {
        min-width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #viewerpopup .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
        background-color: #00000096;
    }
    #Collectionviewerpopup .modal-dialog {
        min-width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #Collectionviewerpopup .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
        background-color: #00000096;
    }
    .float_corner{
        position:fixed;
        bottom:1rem;
        right:1rem;
        background:transparent;
      
    }
    .gj-textbox-md{
        padding: 0.3rem 1rem;
        border-radius: 8px;
    }
    .gj-datepicker-md [role=right-icon] {
        padding: 0rem 0.3rem;
    }
    @media only screen and (min-width : 320px) and (max-width : 480px) { 
        .r-social-description { display: none !important; }}
        
    @media only screen and (min-width : 320px) and (max-width : 480px) { 
        .r-tab { display: none !important; }}
    @media only screen and (min-width : 320px) and (max-width : 480px) { 
        .r-tab-icon {
            line-height: 2.4rem !important;
            font-size: 11px !important;
        }}
       
    @media only screen and (min-width : 320px) and (max-width : 480px) { 
        .nav-pills .nav-item .nav-link {
            padding: 0 15.5px;
            text-align: center;
            height: 2.4rem;
            width: 2.4rem;
            font-weight: 400;
            color: #9A9A9A;
            margin-right: 19px;
            background-color: rgb(222,222,222,0.3);
            border-radius: 10px;
        }
        .nav-pills .nav-item i {
            display: block;
            font-size: 14px;
            line-height: 35px;
        }
    }
    @media only screen and (min-width : 481px) and (max-width : 1500px) { 
        .nav-pills .nav-item .nav-link {
            padding: 0 15.5px;
            text-align: center;
            height: 100px;
            width: 100px;
            font-weight: 400;
            color: #9A9A9A;
            margin-right: 19px;
            background-color: rgb(222,222,222,0.3);
            border-radius: 10px;
        }
        .nav-pills .nav-item i {
            display: block;
            font-size: 20px;
            line-height: 60px;
        }
    }
    @media only screen and (min-width : 320px) and (max-width : 480px) { 
        .floating_card{
        }
    }
    @media only screen and (min-width : 481px) and (max-width : 1500px) { 
        .floating_card{
            top: 0.5rem;
            right: 0.5rem;
            position: absolute;
            margin-right: 1rem;
        }
    }  
    
    
</style>

<a href="{{URL::to('/user/drivepanel/'.$drive->slug.'/studentsview')}}"  rel="tooltip" data-placement="top" title="See your class from student's perspective." target="_blank" class="btn btn-neutral float_corner" style="z-index:1050;" ><i class="fas fa-street-view" style="font-size:26px;"></i></a>

<div class="wrapper" ng-app="driveapp">
        <div ng-hide="uploadModalDisplay()" ng-cloak class="alert alert-info" role="alert" style="position: fixed;left: 20px;bottom: 20px;min-width:500px; background:white;border: 2px solid #2385aa;
        border-radius: 5px; z-index:9999;">
            <p class="text-center uploadFileLoading" hidden>
                <span>Saving File..</span><br>
                <img class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
            </p> 
            <div class="progress-container progress-primary progressBarLoading" >
                    
                <span class="progress-badge">Uploading</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="@{{progress}}" aria-valuemin="0" aria-valuemax="100" style="width: @{{progress}}%;">
                        <span class="progress-value">@{{progress}}%</span>
                    </div>
                </div>
            </div>
        </div>
    
    <div class="page-header page-header-small" style="height: 100vh; max-height: 20rem;" filter-color="blue">
        <div class="page-header-image" data-parallax="true" style="background-image: url(/bannerad_images{{'/'.$banner}});">
        </div>
        <div class="container">
            <div class="content-center">
                    
                <h3 class="title" style="margin-bottom:10px;">{{ $drive->name }}</h3>
                <span rel="tooltip" data-placement="top" title="Class Code">({{$drive->code}})</span>
                <h3 class="title" style="margin-bottom:10px;">{{Auth::user()->data()->name}}</h3>
                <div class="content">
                    <div class=" social-description r-social-description">
                        <h2 ng-cloak>@{{ totalStudents }}</h2>
                        <p>Students</p>
                    </div>
                    <div class="social-description r-social-description">
                    <h2 ng-cloak>@{{totalUngraded}}</h2>
                        <p>Ungraded files</p>
                    </div>
                    <div class="social-description r-social-description">
                    <h2 ng-cloak>@{{newRequests.length}}</h2>
                        <p>New Requests</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section"  id="classsection" style="padding:0px 0px" >
        <div class="container">
            {{-- <div class="button-container" style="margin-top:-130px;">
            
                    <a > 
                        @if(Auth::user()->data()->photo)
                            <img class="photo-container" src="{{Auth::user()->data()->photo}}" alt="">
                        @else
                            <img class="photo-container" src="{{asset('assets/img/default-avatar.png')}}" alt="">
                        @endif
                    </a>
            </div> --}}
        </div>
        <br>
        <div class="section section-pills"  style="padding-top:40px;">
            <div class="container">
                
                <div id="navigation-pills">
                    <!--
                        color-classes: "nav-pills-primary", "nav-pills-info", "nav-pills-success", "nav-pills-warning","nav-pills-danger"
                    -->
                    <ul class="nav nav-pills nav-pills-primary nav-pills-icons justify-content-center" id="classtabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab"  href="#dlink1" role="tablist" rel="tooltip" id="generaltab" title="Everything you need is right here" data-placement="top" >
                                <i class="now-ui-icons shopping_shop r-tab-icon" ></i><span class="r-tab">General</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab"  href="#dlink2" role="tablist" rel="tooltip" id="boardtab" title="Save everything you teach." data-placement="top" >
                                <i class="fas fa-chalkboard" ></i><span class="r-tab">Board</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#dlink3" id="drivetab" role="tablist" rel="tooltip" title="Upload Materials">
                                <i class="far fa-hdd"></i>
                                <span class="r-tab">Drive</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#dlink4" id="collectiontab" role="tablist" rel="tooltip" title="Assignment Collection">
                                <i class="far fa-folder-open"></i>
                                <span class="r-tab">Collection</span>
                            </a>
                        </li>
                    
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#dlink5" id="extrastab" role="tablist" rel="tooltip" title="Post or share extra materials">
                                <i class="fas fa-puzzle-piece"></i>
                                <span class="r-tab">Extras</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#dlink6"  id="managertab" role="tablist" rel="tooltip" title="Manage your digital class">
                                <i class="fas fa-cogs"></i>
                               <span class="r-tab"> Manager</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content tab-space ">
                        <div class="tab-pane active" id="dlink1" ng-controller="GeneralController" ng-init="init()">
                            <div class="card" style="padding:0rem 1rem;" >
                                <div class="general_request_container" ng-show="newRequests.length>0" ng-cloak>
                                    <p class="manager_label">New Requests</p>
                                    <div class="row">
                                        <div class="col-md-3" ng-repeat="r in newRequests | limitTo : 4">
                                            
                                            <img ng-if="!r.data.photo" src="{{asset('assets/img/default-avatar.png')}}" alt="Thumbnail Image" class="rounded-circle img-raised request_img">
                                            <img ng-if="r.data.photo" ng-src="@{{r.data.photo}}" alt="Thumbnail Image" class="rounded-circle img-raised request_img">
                                            
                                            <p >@{{r.data.name}}</p>
                                            <button id="GeneralAcceptBeforeLoading@{{ r.id }}" class="btn btn-sm btn-success" style="background-color:09ca00;" ng-click="g_accept(r.id)"  rel="tooltip"  title="Accept" data-placement="top"  ><i class="fa fa-check"  ></i></button>
                                            <img id="GeneralAcceptLoading@{{ r.id }}" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                            <button id="GeneralIgnoreBeforeLoading@{{ r.id }}" class="btn btn-sm btn-warning" style="background-color:ffa60c;" ng-click="g_ignore(r.id)"  rel="tooltip"  title="Ignore" data-placement="top" ><i class="fa fa-remove"></i></button>
                                            <img id="GeneralIgnoreLoading@{{ r.id }}" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                <hr>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="info info-hover">
                                            <div class="icon icon-warning" style="font-size:40px">
                                                <i class="fas fa-chalkboard" id="generalBoardButton" style="cursor:pointer"></i>
                                            </div>
                                            <h4 class="info-title info-description">White Board</h4>
                                            <p ng-if="NBoards==0" class="description">Save anything that you have taught today. Also share what you will teach tomorrow.</p>
                                            <p ng-cloak ng-if="NBoards!=0" class="description">You have saved teachings of @{{NBoards}} days.</p>
                                            
                                            </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="info info-hover">
                                            <div class="icon icon-success" style="font-size:40px">
                                                <i class="far fa-hdd" id="generalDriveButton" style="cursor:pointer"></i>
                                            </div>
                                            <h4 class="info-title">Share Materials</h4>
                                            <p ng-if="NDriveFiles==0" class="description info-description">Upload notes, slides or any materials to share through your drive.</p>
                                            <p ng-cloak ng-if="NDriveFiles!=0" class="description info-description"> You have shared @{{NDriveFiles}} files through your drive.</p>
                                           
                                            
                                        
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="info info-hover">
                                            <div class="icon icon-success" style="font-size:40px">
                                                <i class="far fa-folder-open" id="generalCollectionButton" style="cursor:pointer"></i>
                                            </div>
                                            <h4 class="info-title info-description">Assignment Collection</h4>
                                            <p ng-if="totalAssignments==0" class="description">Create a folder for collection of assigments, homeworks or others from your students.</p>
                                            <p ng-cloak ng-if="totalAssignments!=0" class="description">You have @{{totalUngraded}} ungraded assignments out of @{{totalSubmittedAssignments}} submitted.</p>
                                            
                                             </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="info info-hover">
                                            <div class="icon icon-warning" style="font-size:40px">
                                                <i class="fas fa-puzzle-piece" id="generalExtrasButton2" style="cursor:pointer"></i>
                                            </div>
                                            <h4 class="info-title info-description">Extras</h4>
                                            <p ng-if="NPosts==0" class="description">Post information or extra materials for your students. Also send notices or quick text.</p>
                                            <p ng-cloak ng-if="NPosts!=0" class="description">You have shared @{{NPostedFiles}} materials in @{{NPosts}} posts.</p>
                                            <p ng-cloak ng-if="NNotices!=0" class="description">You have published @{{NNotices}} notices.</p>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div class="info info-hover">
                                            <div class="icon icon-warning" style="font-size:40px">
                                                <i class="fas fa-list-ul" id="generalResultButton" style="cursor:pointer"></i>
                                            </div>
                                            <h4 class="info-title info-description">Publish Results</h4>
                                            <p ng-if="NPublishedResults==0" class="description">Grade assignments and publish results.</p>
                                            <p ng-cloak ng-if="NPublishedResults!=0" class="description">You have published @{{NPublishedResults}} results out of @{{totalAssignments}} given assignments.</p>
                                         </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="info info-hover">
                                            <div class="icon icon-warning" style="font-size:40px">
                                                <i class="fas fa-cogs" id="generalManagerButton" style="cursor:pointer"></i>
                                            </div>
                                            <h4 class="info-title info-description">Manager</h4>
                                            <p class="description">Manage your class, view students, storage space and class profile.</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="tab-pane" id="dlink2" ng-controller="BoardController" ng-init="getBoards()">
                            <div class="card">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="container" style="align:left">
                                                <div class="" style="">
                                                    <div class="container">
                                                        <br>
                                                        <p class="description"> Your White Board</p>
                                                        <div class="card-body">
                                                            <form method="POST" ng-submit="saveBoard()">
                                                                <label>Class Date:</label>
                                                                <input class="form-control" ng-model="board.date" ng-value="board.date" ng-change="changeBoard()" id="datepicker3" style="padding: 0.3 rem 1rem !important"/>
                                                                <br>
                                                                <label>Board:</label>
                                                                <input name="image" type="file" id="upload" hidden onchange="">
                                                                <div style="text-align:center">
                                                                    <img id="changeBoardLoading" hidden src="{{asset('drive/driveloading.gif')}}" style="height:3rem; width:3rem;"/>
                                                                </div>                                                                    
                                                                <div ui-tinymce data-placeholder='' id="theBoard" class="form-control" ng-model="board.content" style="min-height:150px;max-width: 100%;padding: 10px;resize: none;border: 1px solid 949494 ;border-radius: 8px;line-height:1;"></div>
                                                                <p style="margin-top:10px">
                                                                    <div id="BoardBeforeLoading">
                                                                        <button type="submit" class="btn btn-primary pull-right">Save</button> 
                                                                        <button type="button" class="btn pull-right" data-toggle="modal" style="background-color:#d42525" data-target="#removeBoard" ng-if="board.id">Reset</button> 
                                                                    </div>
                                                                    <img id="BoardLoading" hidden class="pull-right" src="{{asset('drive/driveloading.gif')}}" style="height:3rem; width:3rem;"/>
                                                                </p>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade modal-mini modal-primary" id="removeBoard" tabindex="-1" role="dialog" aria-labelledby="removeBoardLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div >
                                            <div class="modal-header justify-content-center">
                                                
                                                <div class="modal-profile" >
                                                
                                                    <i class="fa fa-trash"></i>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to remove this board?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="removeBoardLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 1rem; width:1rem;"/>
                                                <button id="removeBoardBeforeLoading" type="button" ng-click="deleteBoard()" class="btn btn-link btn-neutral">OK</button>
                                                <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade modal-mini modal-primary" id="confirmChange" tabindex="-1" role="dialog" aria-labelledby="confirmChangeLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div >
                                            <div class="modal-header justify-content-center">
                                                
                                                <div class="modal-profile" >
                                                
                                                    <i class="fa fa-edit"></i>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>You have some unsaved changes. Save them?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" ng-click="saveBoard()" class="btn btn-link btn-neutral">Save</button>
                                                <button type="button" class="btn btn-link btn-neutral" ng-click="content=board.content;changeBoard()" data-dismiss="modal">Discard</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="dlink3" ng-controller="DriveController" ng-init="openRoot()">
                            <div class="card">
                                <nav class="navbar bg-primary navbar-expand-lg" style="padding-top: 0px;padding-bottom: 0px;">
                                    <div class="container">
                                        <div class="navbar-translate" style="margin:1px; width:unset">
                                        <a class="nav-link" href="#pablo" ng-click="back()" >
                                                        <i class="fa fa-arrow-left" title="Back" rel="tooltip" data-placement="top"></i>
                                                </a>
                                        </div>
                                        <div class="navbar-translate" style="margin:1px;  width:unset">
                                            <a class="nav-link" href="#pablo" data-toggle="modal" data-target="#addFolder" >
                                                <i class="fa fa-plus" title="New Folder" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                        <div class="navbar-translate" style="margin:1px;  width:unset">
                                            <a class="nav-link" href="#pablo" data-toggle="modal" data-target="#uploadFile">
                                                <i class="fa fa-upload" title="Upload Files" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>

                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-show="visibleIcon('Preview')">
                                            <a class="nav-link" href="#download" ng-click="open()" >
                                                <i class="fa fa-eye" title="Preview" rel="tooltip" data-placement="top"></i>
                                            </a>
                                            <a target="_blank" href="#!" hidden id="driveHiddenDownload"></a>
                                        </div>

                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-show="visibleIcon('Download')">
                                            <a class="nav-link" href="#download" ng-click="download()" >
                                                <i class="fa fa-download" title="Download" rel="tooltip" data-placement="top"></i>
                                            </a>
                                            <a target="_blank" href="#!" hidden id="driveHiddenDownload"></a>
                                        </div>
                                        
                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-show="visibleIcon('Rename')">
                                            <a class="nav-link" href="#pablo" data-toggle="modal" data-target="#renameFolder" >
                                                <i class="fa fa-edit" title="Rename" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                        <div class="navbar-translate" style="margin:1px;  width:unset" ng-show="visibleIcon('Delete')">
                                            <a class="nav-link" href="#pablo" data-toggle="modal" data-target="#delete" >
                                                <i class="fa fa-trash" title="Delete" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                        <div class="collapse navbar-collapse justify-content-end" id="example-navbar-primary">
                                            <ul class="navbar-nav">
                                                {{--  <li class="nav-item active">
                                                    <a class="nav-link" href="#pablo">
                                                        <i class="now-ui-icons objects_globe"></i>
                                                        <p>Discover</p>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#pablo">
                                                        <i class="now-ui-icons users_circle-08"></i>
                                                        <p>Profile</p>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#pablo">
                                                        <i class="now-ui-icons ui-1_settings-gear-63"></i>
                                                        <p>Settings</p>
                                                    </a>
                                                </li>  --}}
                                                <li class="nav-item">
                                                    
                                                        <input type="text" class="form-control" ng-model="driveSearchKey" placeholder="Search.." style="margin-top:5px;" ng-keyup="$event.keyCode == 13 && searchDrive(driveSearchKey)"/>
                                                    
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <div class="container"  style="padding-bottom:20px; min-height:400px;">
                                    
                                    <p class="drive_breadcrumb" ng-cloak>
                                        <a href="#!" class="drive_breadcrumb_a" ng-click="openRoot()"><span>{{$drive->name}}</span></a>
                                        <span class="drive_breadcrumb_seperator">/</span>
                                        <span ng-repeat="b in breadcrumbs.slice(0,-1)">
                                            <a href="#!" class="drive_breadcrumb_a" ng-click="openFolder(b.id)"><span>@{{b.name}}</span></a>
                                            <span class="drive_breadcrumb_seperator">/</span>
                                        </span>
                                        <span class="drive_breadcrumb_last">@{{breadcrumbs.slice(-1)[0].name}}</span>
                                    </p> 
                                    <p class="text-center">
                                        <img id="DriveMainLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                    </p>    
                                    <div id="DriveMainBeforeLoading">
                                            
                                        <upload to="{{URL::to('/user/drive'.'/'.$drive->id.'/uploadfile')}}" ng-model="object.id">
                                            <ul class="nav nav-pills1 nav-pills-primary nav-pills-icons item-list"  id="driveitems" role="">
                                                <li class="nav-item " ng-repeat="c in folders">
                                                    <a class="nav-link folder item-list-item" data-toggle="tab" ng-click="select(c.id,1)" ng-dblclick="openFolder(c.id)"  ng-right-click="select(c.id,1)"  ios-dblclick="openFolder(c.id)" role="tablist" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" title="@{{c.name}}" >
                                                        {{--  <i class="now-ui-icons files_box" style="font-size:40px" ></i>  --}}
                                                        <img src="/assets/img/folder.png"/><br>
                                                        @{{c.name}} </a>
                                                </li>
                                                <li class="nav-item " ng-repeat="c in files">
                                                    <a class="nav-link folder item-list-item" data-toggle="tab"  role="tablist"  ng-click="select(c.id,2)" ng-dblclick="select(c.id,2); open()" ios-dblclick="select(c.id,2); open()" ng-right-click="select(c.id,2)" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" title="@{{c.name}}">
                                                        {{--  <i class="now-ui-icons files_box" style="font-size:40px" ></i>  --}}
                                                    <img ng-src="{{asset('/assets/img')}}/@{{getIcon(c.extension)}}"/><br>
                                                        @{{c.name}} </a>
                                                </li>
                                            </ul>   
                                            <p class="text-center">
                                                <img id="DNDLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                                            </p>
                                            <br>
                                            <br>
                                            <p class="description" style="color:#000000c7">Upload notes, slides or any other material here to share it with your class.</p>
                                            <p class="description"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#uploadFile">Upload</a></p>
                                                    
                                        </upload>
                                        <p id="upresponse" hidden> </p>
                                        
                                        <button id="upbtn" ng-click="updateResponse()"  hidden></button>
                                    </div>
                                </div>
                            </div>
                            <div id="context-menu" class=" clearfix animated fast fadeIn">
                                <ul class="dropdown-menu dropdown-right-click" role="menu" aria-labelledby="dropdownMenu">
                            
                                    <li ng-show="selectType==1">
                                        <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                            <i class="fa fa-folder-open"></i> Open
                                        </a>
                                    </li>

                                    <li ng-show="selectType==2">
                                        <a style="color:#666666" href="#!" tabindex="-1" ng-click="open()">
                                            <i class="fa fa-eye"></i> Preview
                                        </a>
                                    </li>
                                    
                                    <li ng-show="selectType==1">
                                        <a style="color:#666666" href="#!" data-toggle="modal" data-target="#renameFolder" tabindex="-1" >
                                            <i class="fa fa-edit"></i> Rename
                                        </a>
                                    </li>

                                    <li>
                                        <a style="color:#666666" href="#!" tabindex="-1" ng-click="download()">
                                            <i class="fas fa-download"></i> Download
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a style="color:#666666" href="#!" data-toggle="modal" data-target="#delete" tabindex="-1" >
                                            <i class="fa fa-trash"></i> Delete
                                        </a>
                                    </li>
                            
                                </ul>
                            
                            </div>
                            <div ng-hide="downloads.length==0" ng-cloak class="alert alert-info" role="alert" style="position: fixed;right: 20px;bottom: 20px;min-width:500px;background-color: #4397b6;">
                                <strong>Fetching files: </strong>
                                <ul style="list-style:none;margin-bottom: 0px;">
                                    <li ng-repeat="dl in downloads">
                                        @{{ dl.name }}
                                        <a ng-click="canceldownload(dl.id,dl.type)" style="color:white" href="javascript:void(0)">
                                            &nbsp; <i class="fa fa-remove" style="margin-left: 20px;" title="Cancel"></i>
                                        </a>
                                        {{-- <div class="myProgress" style="padding:2rem">
                                        </div> --}}
                                        
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="modal fade" id="viewerpopup" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center" style="background-color: #00000096;color: white;">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="cursor:pointer">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <button type="button" class="close" style="background-color:  transparent;right: 90px;cursor:pointer" ng-click="download()">
                                                <i class="fas fa-download"></i>
                                            </button>
                                            <h4 class="title">@{{ selectedItem.name }}</h4>
                                        </div>
                                            
                                            <div class="modal-body">
                                                <p class="text-center">
                                                    <img id="BeforeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                </p>
                                                <div style="text-align: center;">
                                                    <h5 ng-hide="viewerror==''" style="color:white;margin-top:100px;">@{{viewerror}}</h5>
                                                    <iframe id="viewerimage" hidden ng-src="@{{contentimage}}" style="height: 85vh;width: 300px;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                                                    <iframe id="viewerdoc" hidden ng-src="@{{content}}" style="height: 85vh;width: 100%;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                                                    
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="addFolder" tabindex="-1" role="dialog" aria-labelledby="addFolderLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Create Folder</h4>
                                        </div>
                                        
                                        <p class="text-center">
                                            <img id="addFolderLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                        </p> 
                                        <form ng-submit="addFolder(newfoldername)"  method="POST">
                                            <div id="addFolderBeforeLoading">
                                                <div class="modal-body">
                                                        {{csrf_field()}}
                                                        <label class="title" style="margin-left:15px; margin-top:0px;">Folder Name</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="far fa-folder"></i>
                                                            </span>
                                                            <input type="text" required name="name" class="form-control" ng-model="newfoldername" placeholder="Folder Name">
                                                        </div>
                                                    
                                                </div>
                                                <div class="modal-footer justify-content-center">
                                                    <button type="submit" class="btn btn-primary">Create</button>
                                                </div>
                                            </div>
                                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="uploadFileLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Upload File</h4>
                                            <br>
                                            
                                                
                                            
                                        </div>
                                        <p class="text-center uploadFileLoading" hidden>
                                            <span>Saving File..</span><br>
                                            <img class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                                        </p> 
                                        {{-- <div class="myProgress" style="padding:2rem">
                                        </div> --}}
                                        <div class="progress-container progress-primary progressBarLoading" style="padding:1rem;" hidden>
                                            
                                                <span class="progress-badge">Uploading</span>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning " style="padding:1rem; width: @{{progress}}%;" role="progressbar" aria-valuenow="@{{progress}}" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="progress-value">@{{progress}}%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <form ng-submit="uploadFile()"  method="POST">
                                            <div class="uploadFileBeforeLoading">
                                                <div class="modal-body">
                                                        {{csrf_field()}}
                                                        <label>Upload Limit: 10MB</label>
                                                <div class="input-group"  >
                                                            <span class="input-group-addon">
                                                                <i class="far fa-file"></i>
                                                            </span>
                                                            
                                                            
                                                            <input type="file" required class="form-control" fileread="file" ng-model="file">
                                                        </div>
                                                    
                                                </div>
                                                <div class="modal-footer justify-content-center">
                                                    <button type="submit" class="btn btn-primary">Upload</button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="uploadFileBeforeLoading">
                                            <h4 class="title" style="font-size: 23px;
                                            font-weight: 500;">Or</h4>
                                            <div class="modal-footer justify-content-center">
                                                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#importFile" ng-click="importInit()">Import from other class</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="importFile" tabindex="-1" role="dialog" aria-labelledby="importFileLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Import Files</h4>
                                            <br>
                                            
                                                
                                            
                                        </div>
                                        
                                        <p class="text-center">
                                            <img id="ImportLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                        </p> 
                                        <div class="container" id="ImportBeforeLoading" style="padding: 1rem 2rem;">
                                            <select id="importDriveSelect" ng-options="option.name for option in importDrives track by option.id" class="form-control selectdrive" ng-model="importSelectedDriveId" ng-change="onImportSelectChange()" >
                                    
                                                <option value="" disabled selected>Select your drive..</option>
                                            </select>
                                            
                                            <a href="#!" ng-click="importUpOneLevel()"><i class="fa fa-arrow-up"></i>Up one level</a>
                                            <span class="pull-right">@{{checkedIds.length}} files selected</span>
                                            <ul class="list-group importFile_inner importFile_inner-overflow" ng-show="importFiles.length>0||importFolders.length>0">
                                                <li class="list-group-item" ng-click="importFolderSingleClick(f.id)" ng-dblclick="importFolderDoubleClick(f.id)" ios-dblclick="importFolderDoubleClick(f.id)" id="importFolder@{{f.id}}" ng-repeat="f in importFolders">
                                                    <img src="/assets/img/folder.png"/>
                                                    <span >@{{f.name}}</span>
                                                </li>
                                                <li class="list-group-item" ng-click="importFileSingleClick(f.id)" id="importFile@{{f.id}}" ng-repeat="f in importFiles">
                                                        <img src="/assets/img/file.png"/>
                                                        <span  >@{{f.name}}</span>
                                                        <input type="checkbox" ng-checked="importIsChecked(f.id)" ng-click="importApplyCheck(f.id)" class="pull-right">
                                                </li>
                                                
                                            </ul>
                                            <ul class="list-group importFile_inner importFile_inner-overflow" ng-hide="importFiles.length>0||importFolders.length>0">
                                                    <li>No files and Folders</li> 
        
                                            </ul>
                                        </div>
                                        <div class="modal-footer justify-content-center">
                                            <button <img id="ImportSelectedFilesBeforeLoading" type="button" ng-click="importSelectedFiles()" class="btn btn-primary">Import Selected Files</button>
                                        </div>
                                        <p class="text-center">
                                            <img id="ImportSelectedFilesLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="renameFolder" tabindex="-1" role="dialog" aria-labelledby="renameFolderLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Rename Folder</h4>
                                        </div>
                                        
                                        <p class="text-center">
                                            <img id="renameFolderLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                        </p> 
                                        <form ng-submit="renameFolder(renamefoldername)"  method="POST">
                                            <div id="renameFolderBeforeLoading">
                                                <div class="modal-body">
                                                        {{csrf_field()}}
                                                        <label class="title" style="margin-left:15px; margin-top:0px;">Folder Name</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="far fa-folder"></i>
                                                            </span>
                                                            <input type="text" name="name" required class="form-control" ng-model="renamefoldername" placeholder="Folder Name">
                                                        </div>
                                                    
                                                </div>
                                                <div class="modal-footer justify-content-center">
                                                    <button type="submit" class="btn btn-primary">Rename</button>
                                                </div>
                                            </div>
                                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade modal-mini modal-primary" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        
                                    
                                        <div >
                                            <div class="modal-header justify-content-center">
                                                <div class="modal-profile" >
                                                
                                                    <i class="fa fa-trash"></i>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to delete this file/folder?<br>Deleted files cannot be recovered.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="deleteLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 3rem; width:3rem;"/>
                                                
                                                <button type="button" id="deleteBeforeLoading" ng-click="delete()" class="btn btn-link btn-neutral">Ok</button>
                                                <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="dlink4" ng-controller="CollectionController" ng-init="getCollections()">
                            <div class="card" style="min-height: 41rem;">
                                <nav class="navbar bg-primary navbar-expand-lg" style="padding-top: 0px;padding-bottom: 0px;">
                                    <div class="container">
                                        <div class="navbar-translate" style="margin:1px; width:unset">
                                            <a class="nav-link" href="#pablo" ng-click="getCollections()" >
                                                <i class="fa fa-arrow-left" title="Back" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-hide="inside" data-toggle="modal" data-target="#addCollection">
                                            <a class="nav-link" href="#pablo" >
                                                <i class="fa fa-plus" title="Create Collection" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                        
                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-show="visibleIcon('Rename')">
                                            <a class="nav-link" href="#!"  data-toggle="modal" data-target="#renameCollection" >
                                                <i class="fas fa-pencil-alt" title="Rename" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                
                                                                               
                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-show="visibleIcon('Download')">
                                            <a class="nav-link" href="#download" ng-click="download()" >
                                                <i class="fa fa-download" title="Download" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                       
                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-show="visibleIcon('Preview')">
                                            <a class="nav-link" href="#download" ng-click="open()" >
                                                <i class="fa fa-eye" title="Preview" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>

                                        <div class="navbar-translate" style="margin:1px; width:unset" ng-show="visibleIcon('Delete')">
                                            <a class="nav-link" href="#pablo" data-toggle="modal" data-target="#deleteCollection" >
                                                <i class="fa fa-trash" title="Delete" rel="tooltip" data-placement="top"></i>
                                            </a>
                                        </div>
                                        

                                        <div class="collapse navbar-collapse justify-content-end" id="example-navbar-primary">
                                            <ul class="navbar-nav">
                                                <li class="nav-item">
                                                    
                                                        
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <div class="row breadcrumb-border" ng-cloak>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <p class="drive_breadcrumb colc_breadcrumb">
                                            <a href="#!" class="drive_breadcrumb_a" ng-click="getCollections()"><span>{{$drive->name}}</span></a>
                                            <span class="drive_breadcrumb_seperator" ng-show="inside">/</span>
                                            
                                            <span class="drive_breadcrumb_last" ng-show="inside">@{{selectedCollection.name}}</span>
                                        </p>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12" ng-show="inside">
                                        <p class="drive_breadcrumb received_from">
                                        <span ng-show="files.length>0">Submitted By: </span><span>@{{selectedFile.user_name }}</span>
                                        </p>
                                    </div>
                                </div>

                                <p class="text-center">
                                    <img id="CollectionMainLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                </p> 
                                <div class="container"  style="padding-bottom:20px; min-height:400px;">
                                    <div id="CollectionMainBeforeLoading">
                                        <div class="row" ng-hide="inside">
                                            <div class="col-md-8">
                                                <ul class="nav nav-pills1 nav-pills-primary nav-pills-icons colc-list"  id="navicons" role="">
                                                    <li class="nav-item " ng-repeat="c in collections track by $index">
                                                        
                                                        <a class="nav-link folder1 colc-list-item"  data-id="@{{ c.id }}" title="@{{c.name}}" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" ng-class="{active:$index == 0}"  data-toggle="tab"  role="tablist" ng-click="select(c,1)" ng-right-click="select(c,1)" ng-dblclick="open()" ios-dblclick="open()">
                                                            {{--  <i class="now-ui-icons files_box" style="font-size:40px" ></i>  --}}
                                                            <img src="/assets/img/folder.png"/>
                                                            <span class="collection_leftgradebadge" ng-show="c.nfiles && c.nUnGraded" title="@{{c.nUnGraded}} left to grade.">@{{c.nUnGraded}}</span>
                                                            <span ng-show="c.nfiles && !c.nUnGraded"><i class="fa fa-check" style="color:green; font-size:9px;" title="all files graded"></i></span>   
                                                            <br>
                                                            @{{c.name}}
                                                        </a>
                                                    </li>
                                                        
                                                        
                                                </ul>   
                                                <br>
                                                <p class="description" style="color:#000000c7">Create collection folder to assign and collect tasks and assignments.<p>
                                                <p class="description"><a href="#!" data-toggle="modal" data-target="#addCollection" class="btn btn-primary">Create</a></p>
                                            </div>
                                            <div class="col-md-4" ng-cloak ng-show="selectedCollection.id!=0">
                                                <br>
                                                <br>
                                                <div class="card floating_card" >
                                                    <div class="container">
                                                        <h5 class="title">@{{selectedCollection.name}}</h5>
                                                            
                                                    <p class="description" style="margin-bottom: 1px;">Files Collected: @{{selectedCollection.nfiles}}</p>
                                                    <p class="description" style="margin-bottom: 1px;">Deadline: @{{selectedCollection.deadline}} &nbsp;<a href="#!" ng-hide="onDeadlineEdit" ng-click="toggleDeadlineEdit()">Edit</a><a href="#!" ng-show="onDeadlineEdit" ng-click="toggleDeadlineEdit()">Cancel</a></p>
                                                    <div ng-show="onDeadlineEdit" class="text-center">
                                                        <input id="datepicker2" ng-model="editedDeadline"/>
                                                        <input id="editDeadlineBeforeLoading" ng-if="selectedCollection.id!=0" type="button" class="btn btn-primary btn-sm" value="Set" ng-click="editDeadline()"/>
                                                    </div>
                                                    <p class="text-center">
                                                        <img id="editDeadlineLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                    </p> 
                                                    <br>
                                                    <p class="description" style="margin-bottom: 1px;"><strong>Task: </strong></p>
                                                    <p class="description" style="margin-bottom: 1px;">@{{selectedCollection.collectiontask.description.length>70?selectedCollection.collectiontask.description.substr(0,70)+'...':selectedCollection.collectiontask.description}} 
                                                        <a href="#!" data-toggle="modal" data-target="#seeMoreTask" ng-click="seeMoreTask()" ng-show="selectedCollection.collectiontask.description.length>70" >see more</a></p>
                                                        
                                                        <br>
                                                    <p class="description" style="margin-bottom: 1px;" ng-hide="selectedCollection.resultPublished=='published'">Status: 
                                                        <br>    
                                                        <input class="tgl tgl-skewed" id="cb3" type="checkbox" ng-if="selectedCollection.id!=0"  ng-checked="selectedCollection.status=='open'"/>
                                                            <label class="tgl-btn" rel="tooltip" data-placement="top" title="click to toggle" data-tg-off="CLOSED" data-tg-on="OPEN" for="cb3" style="margin-left:38%;font-size:12px;"  ng-click="closeCollection()"></label>

                                                        
                                                    </p>
                                                    <div class="text-center">
                                                        <img id="closeCollectionLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                    </div>
                                                    <p class="text-center">
                                                        <button class="btn btn-primary"  ng-if="selectedCollection.id!=0" data-toggle="modal" data-target="#publishResult"  ng-show="selectedCollection.nfiles && !selectedCollection.nUnGraded && selectedCollection.resultPublished=='unpublished'">@{{selectedCollection.resultPublished=='published'?'Re:':''}} Publish Results</button>
                                                        <a ng-if="selectedCollection.id!=0" href="/user/collection/{{$drive->id}}/@{{selectedCollection.id}}/viewresult" target="_blank" ng-show="selectedCollection.resultPublished=='published'" ng-click="downloadResult()">View result</a>
                                                    </p>
                                                    <p class="text-center">
                                                        
                                                    </p>
                                                    {{-- <p class="description" style="margin-bottom: 1px;">Mail link: @{{selectedCollection.mail_link}}</p> --}}
                                                    </div>
                                                    <div class="card-footer" ng-if="selectedCollection.id!=0">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="container text-center" ng-show="selected">
                                                                    <button class="btn btn-primary btn-icon btn-round" ng-click="download()" type="button" style="margin:5px" title="Download all files in @{{selectedCollection.name}}" data-placement="top" >
                                                                        <i class="now-ui-icons arrows-1_cloud-download-93" style="font-size:18px;"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary btn-icon btn-round" type="button" style="margin:5px" title="Edit Task" data-placement="top" data-toggle="modal" data-target="#viewTask">
                                                                        &nbsp;  <i class="fa fa-edit" style="font-size: 18px;"></i>
                                                                    </button>                                                                                
                                                                    <button class="btn btn-primary btn-icon btn-round" type="button" style="margin:5px; font-size:10px;"  title="Delete @{{selectedCollection.name}}" data-placement="top" data-toggle="modal" data-target="#deleteCollection">
                                                                        &nbsp;<i class="fa fa-trash" style="font-size:18px; "></i>
                                                                    </button>
                                                                    {{-- <button ng-show="selectedCollection.status=='open'" class="btn btn-primary btn-icon btn-round" type="button" style="margin:5px; font-size:10px;" rel="tooltip" title="Close @{{selectedCollection.name}}" data-placement="top" data-toggle="modal" data-target="#closeCollection">
                                                                        &nbsp;<i class="fa fa-stop" style="font-size:18px; "></i>
                                                                    </button>
                                                                    <button ng-hide="selectedCollection.status=='open'" class="btn btn-primary btn-icon btn-round" type="button" style="margin:5px; font-size:10px;" rel="tooltip" title="Resume @{{selectedCollection.name}}" data-placement="top" data-toggle="modal" data-target="#closeCollection">
                                                                        &nbsp;<i class="fa fa-play" style="font-size:18px; "></i>
                                                                    </button> --}}
                                                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#collectionInformation"><i class="fas fa-info pull-right info_i" ></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        <div class="row" ng-show="inside">
                                            <div class="col-md-8">
                                                    <ul class="nav nav-pills1 nav-pills-primary nav-pills-icons colc-list"  id="collectionfiles" role="">
                                                        
                                                        <li class="nav-item"  ng-repeat="f in files track by $index">
                                                            <a class="nav-link folder1 colc-list-item colc-file-list-@{{f.collection_id}}" ng-class="{active:$index == 0}" data-toggle="tab" id="cfiles@{{f.id}}" role="tablist" ng-click="select(f,2)" ng-dblclick="select(f,2);open()" ios-dblclick="select(f,2);open()" ng-right-click="select(f,2)" style="overflow:hidden;text-overflow:ellipsis;cursor:pointer" title="@{{f.name}}">
                                                                {{--  <i class="now-ui-icons files_box" style="font-size:40px" ></i>  --}}
                                                                
                                                                <img ng-src="{{asset('/assets/img')}}/@{{getIcon(f.extension)}}"/>
                                                                <span ng-show="f.grade"><i class="fa fa-check" style="color:green;font-size:10px;" title="graded(@{{f.grade}})"></i></span><br>
                                                                @{{f.name}} </a>
                                                        </li>
                                                            
                                                    </ul>   
                                                    <br><br><br><br><br><br><br><br><br>
                                            </div>
                                            <div class="col-md-4" ng-cloak ng-show="selectedFile.id!=0">
                                                <br>
                                                <br>
                                                <div class="card floating_card">
                                                    <div class="container">
                                                        <h5 class="title">@{{selectedCollection.name}}</h5>
                                                            
                                                        <p class="description" style="margin-bottom: 1px;">Submitted By: @{{selectedFile.user_name}}</p>
                                                        <p class="description" style="margin-bottom: 1px;">Roll No.: @{{selectedFile.roll_no ? selectedFile.roll_no: '-'}}</p>
                                                        <p class="description" style="margin-bottom: 1px;" >Submitted At: <text style="@{{setLateColor()}}">@{{selectedFile.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</text> </p>
                                                        <br>
                                                        <p class="description" style="margin-bottom: 1px;"><strong>Grade: @{{selectedFile.grade?selectedFile.grade:'(ungraded)'}}</strong></p>
                                                        <p class="description" style="margin-bottom: 1px;"><a ng-hide="selectedFile.grade" ng-click="onGrading=true" style="font-size: 13px;" href="#!">grade now</a></p>
                                                        <p class="description" style="margin-bottom: 1px;"><a ng-show="selectedFile.grade" ng-click="onGrading=true" style="font-size: 13px;" href="#!">edit</a></p>
                                                        
                                                        <p class="description" ng-show="onGrading" style="margin-bottom: 1px;">
                                                            <input  type="text" ng-model="gradetext" placeholder="enter grade, leave blank to ungrade!" class="form-control"/>
                                                            <button class="btn btn-primary btn-small" ng-if="selectedFile.id!=0" id="saveGradeBeforeLoading" ng-click="saveGrade();">Save</button>
                                                        </p>
                                                        <p class="text-center">
                                                            <img id="saveGradeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                        </p> 
                                                   
                                                    
                                                    </div>
                                                    <div class="card-footer" ng-if="selectedFile.id!=0">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="container text-center" ng-show="selected">
                                                                    <button class="btn btn-primary btn-icon btn-round" ng-click="download()" type="button" style="margin:5px" title="Download @{{selectedFile.name}}" data-placement="top">
                                                                        <i class="now-ui-icons arrows-1_cloud-download-93" style="font-size:18px;"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary btn-icon btn-round" type="button" style="margin:5px" title="Preview" data-placement="top" ng-click="open()">
                                                                        &nbsp;  <i class="fa fa-eye" style="font-size: 18px;"></i>
                                                                    </button>  
                                                                    
                                                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#collectionInformation"><i class="fas fa-info pull-right info_i" ></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div> 
                                        <br>
                                    </div>
                                </div>
                                <div id="context-menucolc" class=" clearfix animated fast fadeIn">
                                    <ul class="dropdown-menu dropdown-right-click" role="menu" aria-labelledby="dropdownMenu">
                                
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-hide="inside" ng-click="open()">
                                                <i class="fa fa-folder"></i> Open
                                            </a>
                                        </li>
                                
                                        
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-show="inside" ng-click="open()">
                                                <i class="fa fa-eye"></i> Preview
                                            </a>
                                        </li>
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-hide="inside" data-toggle="modal" data-target="#viewTask">
                                                <i class="fa fa-edit"></i> Edit Task
                                            </a>
                                        </li>
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-hide="inside"  data-toggle="modal" data-target="#renameCollection">
                                                <i class="fas fa-pencil-alt"></i> Rename
                                            </a>
                                        </li>
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-click="download()">
                                                <i class="fas fa-download"></i> Download
                                            </a>
                                        </li>
                                
                                        <li>
                                            <a style="color:#666666" href="#!" tabindex="-1" ng-hide="inside" data-toggle="modal" data-target="#deleteCollection">
                                                <i class="fa fa-trash"></i> Delete
                                            </a>
                                        </li>
                                
                                    </ul>
                                
                                </div>
                                <div ng-hide="downloads.length==0" ng-cloak class="alert alert-info" role="alert" style="position: fixed;right: 20px;bottom: 20px;min-width:500px;background-color: #4397b6;">
                                    <strong>Fetching files: </strong>
                                    <ul style="list-style:none;margin-bottom: 0px;">
                                        <li ng-repeat="dl in downloads">
                                            @{{ dl.name }}
                                            <a ng-click="canceldownload(dl.id,dl.type)" style="color:white" href="javascript:void(0)">
                                                &nbsp; <i class="fa fa-remove" style="margin-left: 20px;" title="Cancel"></i>
                                            </a>
                                            <div class="myProgress" style="padding:2rem">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="modal fade" id="addCollection" tabindex="-1" role="dialog" aria-labelledby="addCollectionLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header justify-content-center">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                                <h4 class="title">Create Collection Folder</h4>
                                            </div>
                                            
                                            
                                            <form ng-submit="addCollection()"  method="POST">
                                                
                                                <div class="modal-body">
                                                    {{csrf_field()}}
                                                    <label class="title" style="margin-left:15px; margin-top:0px;">Enter name of the collection</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="far fa-folder"></i>
                                                        </span>
                                                        <input required type="text" name="name" class="form-control" ng-model="newcollectionname" placeholder="eg: Assignment 1">
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="button" id="addTaskButton" ng-click="showTaskCard()" class="btn btn-neutral" style="border: 1px solid;">Add task</button>
                                                    </div>
                                                    <div id="taskCard" hidden>
                                                        <label class="title" style="margin-left:15px; margin-top:0px;">Add task</label>
                                                        
                                                        <div class="card taskCard" >
                                                            <div class="container">
                                                                <label class="title" style="margin-left:15px; margin-top:0px;">Set task description</label>
                                                                <div class="input-group">
                                                                <textarea class="form-control" name="description" ng-model="newtaskdescription" placeholder="Post task here.">
                                                                </textarea>
                                                                </div>
                                                                <label class="title" style="margin-left:15px; margin-top:0px;">Upload a task File (optional)</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="far fa-file"></i>
                                                                    </span>
                                                                    <input type="file" name="file" class="form-control" fileread="file" ng-model="file">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="button" id="addDeadlineButton" ng-click="showDeadlineCard()" class="btn btn-neutral" style="border: 1px solid;">Set deadline</button>
                                                    </div>
                                                    <div id="deadlineCard" style="padding:1rem;" hidden>
                                                        <label class="title" style="margin-left:15px; margin-top:0px;">Set Deadline</label>
                                                    
                                                        
                                                        <input id="datepicker" ng-model="newtaskdeadline" />
                                                    </div>
                                                            
                                                    
                                                    
                                                </div>
                                                <p class="text-center">
                                                    <img id="addCollectionLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 5rem; width:5rem;"/>
                                                </p> 
                                                <div class="modal-footer justify-content-center" id="addCollectionBeforeLoading">
                                                    <button type="submit" class="btn btn-primary">Create and Post</button>
                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="viewTask" tabindex="-1" role="dialog" aria-labelledby="viewTaskLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header justify-content-center">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            <h4 class="title">Task for @{{editTask.name}}</h4>
                                            </div>
                                            
                                            
                                            <form ng-submit="editTaskSubmit()"  method="POST">
                                                
                                                <div class="modal-body">
                                                    {{csrf_field()}}
                                                    
                                                    
                                                
                                                        <label class="title" style="margin-left:15px; margin-top:0px;">Task:</label>
                                                        <div class="input-group">
                                                        <textarea class="form-control" name="description" ng-model="editTask.description" placeholder="Post task detail here.">
                                                            </textarea>
                                                        </div>
                                                    <label class="title" style="margin-left:15px; margin-top:0px;">File: </label>
                                                        
                                                    <a href="#!">@{{editTask.file}}</a>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="far fa-file"></i>
                                                            </span>
                                                            <input type="file" id="editTaskFileInput" name="file1" class="form-control" fileread="file1" ng-model="file1">
                                                        </div>
                                                            
                                                            
                                                    
                                                    
                                                </div>
                                                <p class="text-center">
                                                    <img id="viewTaskLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                </p> 
                                                <div class="modal-footer justify-content-center" id="viewTaskBeforeLoading">
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="seeMoreTask" tabindex="-1" role="dialog" aria-labelledby="seeMoreTaskLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header justify-content-center">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            <h4 class="title">Task for @{{selectedCollection.name}}</h4>
                                            </div>
                                            
                                            
                                                
                                                <div class="modal-body">
                                                    {{csrf_field()}}
                                                    
                                                    
                                                
                                                        <label class="title" style="margin-left:15px; margin-top:0px;">Task:</label>
                                                        <div class="input-group">
                                                        <p class="description">@{{selectedCollection.collectiontask.description}}</p>
                                                        </div>
                                                    <label class="title" style="margin-left:15px; margin-top:0px;">File: </label>
                                                        
                                                    <a href="#!">@{{selectedCollection.collectiontask.file.split('/').slice(-1)[0]}}</a>
                                                    
                                                    &nbsp;<button type="button" data-dismiss="modal" data-toggle="modal" data-target="#viewTask" class="btn btn-primary pull-right">edit</button>
                                                
                                                            
                                                    
                                                    
                                                </div>
                                                <p class="text-center">
                                                    <img id="viewTaskLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                </p> 
                                            
                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade modal-mini modal-primary" id="deleteCollection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            
                                        
                                            <div >
                                                <div class="modal-header justify-content-center">
                                                
                                                    <div class="modal-profile" >
                                                    
                                                        <i class="fa fa-trash"></i>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to delete this collection?</p>
                                                </div>
                                                <div class="modal-footer">
                                                        <img id="deleteCollectionLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 3rem; width:3rem;"/>
                                                
                                                    <button type="button" id="deleteCollectionBeforeLoading" ng-click="deleteCollection()"class="btn btn-link btn-neutral">Yes</button>
                                                    <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade modal-mini modal-primary" id="closeCollection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            
                                        
                                            <div >
                                                <div class="modal-header justify-content-center">
                                                    <div class="text-center">
                                                        <img id="closeCollectionLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 5rem; width:5rem;"/>
                                                    </div>
                                                    <div class="modal-profile" id="closeCollectionBeforeLoading">
                                                    
                                                        <i class="fa fa-play" ng-hide="selectedCollection.status=='open'"></i>
                                                        <i class="fa fa-stop" ng-show="selectedCollection.status=='open'" ></i>
                                                    </div>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to @{{selectedCollection.status=='open'?'close':'resume'}} this collection?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" ng-click="closeCollection()"class="btn btn-link btn-neutral">Ok</button>
                                                    <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="renameCollection" tabindex="-1" role="dialog" aria-labelledby="renameCollectionLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header justify-content-center">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                                <h4 class="title">Rename Collection</h4>
                                            </div>
                                            
                                            <p class="text-center">
                                                <img id="renameCollectionLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                            </p> 
                                            <form ng-submit="renameCollection(renamecollectionname)"  method="POST">
                                                <div id="renameCollectionBeforeLoading">
                                                    <div class="modal-body">
                                                            {{csrf_field()}}
                                                            <label class="title" style="margin-left:15px; margin-top:0px;">New name</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-bookmark-o"></i>
                                                                </span>
                                                                <input type="text" name="name" class="form-control" ng-model="renamecollectionname" placeholder="Folder Name">
                                                            </div>
                                                        
                                                    </div>
                                                    <div class="modal-footer justify-content-center">
                                                        <button type="submit" class="btn btn-primary">Rename</button>
                                                    </div>
                                                </div>
                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="publishResult" tabindex="-1" role="dialog" aria-labelledby="publishResultLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header justify-content-center">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                                <h4 class="title">Publish @{{selectedCollection.name}} results</h4>
                                            </div>
                                            
                                            
                                        
                                            <form ng-submit="publishResult(resultmessage)"  method="POST">
                                                <div >
                                                    <div class="modal-body">
                                                            <p class="description">
                                                                Results will be published in class. Once published, results can be viewed by all students
                                                            </p>
                                                            <br>
                                                            {{csrf_field()}}
                                                            <label class="title" style="margin-left:15px; margin-top:0px;">Message:</label>
                                                        
                                                                <textarea  style="margin-left:1rem;" class="form-control" ng-model="resultmessage" >Results are OUT!!!</textarea>
                                                                {{-- <input type="textarea" > --}}
                                                            
                                                        
                                                    </div>
                                                    <div class="modal-footer justify-content-center">
                                                            <img id="publishResultLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                        <button type="submit" class="btn btn-primary" id="publishResultBeforeLoading">Publish</button>
                                                    </div>
                                                </div>
                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="Collectionviewerpopup" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header justify-content-center" style="background-color: #00000096;color: white;">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="cursor:pointer">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                                <button type="button" class="close" style="background-color:  transparent;right: 90px;cursor:pointer" ng-click="download()">
                                                    <i class="fas fa-download"></i>
                                                </button>
                                                <h4 class="title">@{{ selectedFile.name }}</h4>
                                            </div>
                                                
                                                <div class="modal-body">
                                                    <p class="text-center">
                                                        <img id="CollectionBeforeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                    </p>
                                                    <div style="text-align: center;">
                                                        <h5 ng-hide="viewerror==''" style="color:white;margin-top:100px;">@{{viewerror}}</h5>
                                                        <iframe id="Collectionviewerimage" hidden ng-src="@{{contentimage}}" style="height: 85vh;width: 300px;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                                                        <iframe id="Collectionviewerdoc" hidden ng-src="@{{content}}" style="height: 85vh;width: 100%;border:0;" allowfullscreen webkitallowfullscreen></iframe>
                                                        
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="collectionInformation" tabindex="-1" role="dialog" aria-labelledby="viewTaskLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        <h4 class="title">Information</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p class="description">Create collection folder to give assignments and tasks to students</p>
                                            <p class="description">Students are able to submit assignments and tasks in their respective collection folder</p>
                                            <p class="description">Assignments and tasks from students are collected in the collection folder</p>
                                            <p class="description">Collected assigments are viewable and downloadable</p>
                                            <p class="description">You can grade/assign marks to collected assignments and publish results.</p>
                                            <p class="description">Collection folders are auto-closed for collection at deadline</p>
                                            <p class="description">You can reopen/close collection folder to continue collection, as per your need</p>
                                            <p class="description">Once the result is published, the respective collection will permanently be closed for further collection from students</p>                                                                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="dlink5" ng-controller="PostController" ng-init="getPosts()">
                            <div class="card">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-9">
                                            <div class="container" style="align:left">
                                                <div class="" style="">
                                                    <div class="container">
                                                        <br>
                                                        <p class="description"> Post something or share materials</p>
                                                        <div class="card-body">
                                                            <form method="POST" ng-submit="postpost()">
                                                                    <textarea class="form-control post" required ng-model="postContent" id="posttextarea" style="min-height:100px;" placeholder="Post something, share links or upload extra materials."></textarea>
                                                                    <br>
                                                                    <div class="row">
                                                                        <div class="col-md-3" ng-repeat="f in postfile" >
                                                                            <a class="folder-small" style="font-size:13px;" title="@{{f.name}}" >
                                                                                    <img ng-src="{{asset('/assets/img')}}/@{{getIcon(f.name.split('.').slice(-1)[0])}}"/>
                                                                                @{{f.name.substr(0,10)}}
                                                                            </a>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <br>
                                                                    <input type="file" mfileread="postfile" ng-model="postfile" id="postfile" hidden multiple/>
                                                                    <p>
                                                                        <button id="PostBeforeLoading" type="submit" class="btn btn-primary pull-right">Post</button> 
                                                                        <img id="PostLoading" hidden class="pull-right" src="{{asset('drive/driveloading.gif')}}" style="height:3rem; width:3rem;"/>
                                                                        {{-- <button type="button" class="btn btn-primary" onclick="$('#postfile').trigger('click')">Upload Files</button>  --}}
                                                                        <button type="button" class="btn btn-neutral newpost_actions" rel="tooltip" onclick="$('#postfile').trigger('click')" title="Attach files" data-placement="top"><i class="fa fa-paperclip"></i></button>
                                                                    </p>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                    
                                                <p class="description"> Older posts:</p>
                                                <p class="text-center">
                                                    <img id="PostMainLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                                                </p>
                                                {{--old post <div class="card card_post" style="" ng-repeat="p in posts">
                                                    <div class="container">
                                                                
                                                        <div class="card-body">
                                                            
                                                            <p class="description" style="text-align:left; margin-left: 1rem;">@{{p.content}} 
                                                            </p>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-3" ng-repeat="f in p.postedfiles" >
                                                                    <a class="folder-small" style="font-size:13px; margin-left:15px; margin-right:15px;" title="@{{f.file.split('/').slice(-1)[0]}}">
                                                                        
                                                                            <img src="{{asset('/assets/img')}}/@{{getIcon(f.file.split('.').slice(-1)[0])}}"/>
                                                                        @{{f.file.split('/').slice(-1)[0].substr(0,10)}}
                                                                                
                                                                    </a>
                                                                </div>
                                                                
                                                            </div>
                                                        
                                                            <div class="postactions " style="margin-top: 3rem;">
                                                                    <p class="pull-left" style="margin-top: 0.5rem;">@{{postedDateFormat(p.created_at)}}</p>
                                                                <button class="btn btn-sm btn-warning pull-right" data-toggle="modal" data-target="#editPost" ng-click="editPost(p)"style="background-color:09ca00;">Edit</button>
                                                                <button class="btn btn-sm btn-danger pull-right" data-toggle="modal" data-target="#removePost" ng-click="editPost(p)" style="background-color:#c32727;">Remove</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="general_extra" ng-repeat="p in posts" style="padding: 0rem 1rem;">
                                                    <div class="container">
                                                        <div class="extra_head">
                                                            <div class="row">
                                                                <div class="col-2 pdimg">
                                                                    @if(Auth::user()->data()->photo)
                                                                        <img src="{{Auth::user()->data()->photo}}" alt="">
                                                                    @else
                                                                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="">
                                                                    @endif
                                                                </div>
                                                                <div class="col-6 pdname">
                                                                    <span>{{Auth::user()->data()->name}}</span><br>
                                                                    <span class="general_posted_date"><i>@{{p.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}}</i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        
                                                        <p ng-if="p.content.length < 400"class="description" style="text-align:justify;" ng-bind-html="p.content"></p>
                                                        
                                                        <div ng-if="p.content.length>400">
                                                            <p class="description" style="text-align:justify;" ng-bind-html="p.content.substr(0,200)"></p>
                                                            <a href="#!" class="viewmorelink" >View more</a>
                                                        </div>
                                                        <div ng-if="p.content.length>400" hidden>
                                                            <p class="description" style="text-align:justify;" ng-bind-html="p.content"></p>
                                                            <a href="#!" class="viewlesslink" >..hide</a>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-3" ng-repeat="f in p.postedfiles">
                                                                <a class="folder-medium post-list-item" style="font-size:15px;margin-left:15px; margin-right:15px;" title="@{{f.file.split('/').slice(-1)[0]}}" >
                                                                        
                                                                    <img ng-src="{{asset('/assets/img')}}/@{{getIcon(f.file.split('.').slice(-1)[0])}}"/>
                                                                    @{{f.file.split('/').slice(-1)[0].substr(0,10)}}
                                                                </a>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="postactions ">
                                                            <button class="btn btn-sm btn-warning pull-right" data-toggle="modal" data-target="#editPost" ng-click="editPost(p)"style="background-color:09ca00;">Edit</button>
                                                            <button class="btn btn-sm btn-danger pull-right" data-toggle="modal" data-target="#removePost" ng-click="editPost(p)" style="background-color:#c32727;">Remove</button>
                                                        </div>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>
                                                <p class="text-center">
                                                    <img id="viewMorePostLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                </p> 
                                                <p class="text-center" id="viewMorePostBeforeLoading">
                                                    <a href="#!" ng-click="viewMorePost()" >View more</a>
                                                </p>
                                                <p class="text-center" id="noMorePosts" hidden>
                                                    No more posts
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <br>
                                            <br>
                                            <br>
                                            <div class="card" style="">
                                                <div class="card-body notice_card" style="min-height:138px;">
                                                    <p class="description"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#postNotice" ng-click="initPostNotice()"><i class="far fa-bell"></i>&nbsp;&nbsp;Post New Notice</a></p>
                                                    
                                                        <ol class="notice_list">
                                                            <li ng-repeat="n in notices" style="margin:5px;">
                                                            <a href="#!"  data-toggle="modal" data-target="#viewNotice" ng-click="viewNotice(n)" class="notice_list_title">@{{n.title}} </a>
                                                            </li>
                                                        </ol>
                                                    <p class="text-center">
                                                        <a href="#!" id="loadNoticesBeforeLoading" style="margin:10px; color:#2385aa" ng-click="loadNotices()"> View recent notices</a>
                                                    </p>
                                                        <p class="text-center">
                                                        <img id="loadNoticesLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                    </p>
                                                    <p class="text-center" hidden id="noMoreNotice">
                                                        No more notices    
                                                    </p>
                                                </div>
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="editPost" tabindex="-1" role="dialog" aria-labelledby="editPostLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                        <h4 class="title">Edit post</h4>
                                        </div>
                                        <form ng-submit="savePost(post.content)"  method="POST">
                                            <div class="modal-body">
                                                {{csrf_field()}}
                                                <div class="input-group">
                                                    {{-- <div contenteditable="true" ng-bind-html="post.content" ng-model="post.content"></div> --}}
                                                    <textarea class="form-control" name="description" placeholder="Enter Something" ng-model="post.content">@{{ post.content | removeHTMLTags }}</textarea>
                                                </div>
                                                
                                            </div>
                                            <p class="text-center">
                                                <img id="editPostLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                            </p> 
                                            <div class="modal-footer justify-content-center" id="editPostBeforeLoading">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade modal-mini modal-primary" id="removePost" tabindex="-1" role="dialog" aria-labelledby="removePostLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div >
                                            <div class="modal-header justify-content-center">
                                                
                                                <div class="modal-profile" >
                                                
                                                    <i class="fa fa-trash"></i>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to remove this post?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <img id="removePostLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 1rem; width:1rem;"/>
                                                <button id="removePostBeforeLoading" type="button" ng-click="deletePost()" class="btn btn-link btn-neutral">Yes</button>
                                                <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="postNotice" tabindex="-1" role="dialog" aria-labelledby="postNoticeLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Post a notice</h4>
                                        </div>
                                        
                                        
                                        <form ng-submit="postNotice()"  method="POST">
                                            
                                            <div class="modal-body">
                                                {{csrf_field()}}
                                                
                                                <label class="title" style="margin-left:15px; margin-top:0px;">Title</label>
                                                <div class="input-group">
                                                <input type="text" class="form-control" ng-model="notice.title" placeholder="Notice Title"/>
                                                </div>
                                                
                                                <label class="title" style="margin-left:15px; margin-top:0px;">Body</label>
                                                <div class="input-group">
                                                <textarea class="form-control" name="description" ng-model="notice.description" placeholder="Notice Body">
                                                    </textarea>
                                                </div>
                                                        
                                                        
                                                
                                                
                                            </div>
                                            <p class="text-center">
                                                <img id="postNoticeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                            </p> 
                                            <div class="modal-footer justify-content-center" id="postNoticeBeforeLoading">
                                                <button type="submit" class="btn btn-primary">Post</button>
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="editNotice" tabindex="-1" role="dialog" aria-labelledby="editNoticeLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Edit notice</h4>
                                        </div>
                                        
                                        
                                        <form ng-submit="saveNotice()"  method="POST">
                                            
                                            <div class="modal-body">
                                                {{csrf_field()}}
                                                
                                                <label class="title" style="margin-left:15px; margin-top:0px;">Title</label>
                                                <div class="input-group">
                                                <input type="text" class="form-control" ng-model="notice.title" placeholder="Notice Title"/>
                                                </div>
                                                
                                                <label class="title" style="margin-left:15px; margin-top:0px;">Body</label>
                                                <div class="input-group">
                                                <textarea class="form-control" name="description" ng-model="notice.description" placeholder="Notice Body">
                                                    </textarea>
                                                </div>
                                                        
                                                        
                                                
                                                
                                            </div>
                                            <p class="text-center">
                                                <img id="saveNoticeLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                            </p> 
                                            <div class="modal-footer justify-content-center" id="saveNoticeBeforeLoading">
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade modal-mini modal-primary" id="viewNotice" tabindex="-1" role="dialog" aria-labelledby="viewNoticeLabel" aria-hidden="true">
                                <div class="modal-dialog" style="width:40% ;max-width:none">
                                    <div class="modal-content">
                                        <div >
                                            <div class="modal-header justify-content-center">
                                                
                                                <div class="modal-profile" >
                                                
                                                    <i class="fa fa-info"></i>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                    <p id="noticeDate" style="font-size:  12px;font-style:  italic;"></p>
                                                <p id="noticeModalBody"></p>
                                            </div>
                                            <div class="modal-footer justify-content-center">
                                                
                                                <button type="button" class="btn btn-link btn-neutral" ng-click="editNotice(notice)" data-toggle="modal" data-target="#editNotice" data-dismiss="modal">Edit</button>
                                                <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="dlink6" ng-controller="ManagerController" ng-init="init()">
                            <div class="card">
                                <div class="container" style="padding:20px;">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <h5 class="manager_label">Class Manager</h5>
                                            <br>
                                            <ul class="nav nav-pills nav-pills-primary flex-column" role="tablist">
                                                <li class="msidenavitem">
                                                    <a class="nav-link" data-toggle="tab" href="#linkm1" role="tablist">
                                                        Students
                                                    </a>
                                                </li>
                                                <li class="msidenavitem">
                                                    <a class="nav-link active show" data-toggle="tab" href="#linkm2" role="tablist">
                                                        Class Profile
                                                    </a>
                                                </li>
                                                <li class="msidenavitem">
                                                    <a class="nav-link" data-toggle="tab" href="#linkm3" role="tablist">
                                                        Space Usage
                                                    </a>
                                                </li>
                                                <li class="msidenavitem">
                                                    <a class="nav-link" data-toggle="tab" href="#linkm4" role="tablist" ng-click="loadResults()">
                                                        Published Results
                                                    </a>
                                                </li>
                                                <li class="msidenavitem">
                                                    <a class="nav-link" data-toggle="tab" href="#linkm5" role="tablist">
                                                        Info
                                                    </a>
                                                </li>
                                            </ul>
                                            <br>
                                            
                                        </div>
                                        <div class="col-md-9">
                                            <div class="tab-content">
                                                <div class="tab-pane" id="linkm1">
                                                    <p class="text-center">
                                                        <img id="StudentLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                    </p> 
                                                    <div id="StudentBeforeLoading">
                                                        <p class="manager_title">New Requests <span class="pull-right">Total requests: @{{newRequests.length}}</span></p>
                                                        <div class="manager_newreq">
                                                            <ul class="request_ul">
                                                                <li class="request_li" ng-repeat="n in newRequests">
                                                                    <div class="row">
                                                                        <div class="col-md-2">
                                                                                <img ng-if="!n.data.photo" src="{{asset('assets/img/default-avatar.png')}}" alt="Thumbnail Image" class="rounded-circle img-raised request_img">
                                                                                <img ng-if="n.data.photo" ng-src="@{{n.data.photo}}" alt="Thumbnail Image" class="rounded-circle img-raised request_img">
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <p class="request_name">@{{n.data.name}}</p>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <div class="request_actions">
                                                                                <button id="AcceptBeforeLoading@{{ n.id }}" class="btn btn-sm btn-success" style="background-color:09ca00;" ng-click="accept(n.id)">Accept</button>
                                                                                <img id="AcceptLoading@{{ n.id }}" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                                                <button id="IgnoreBeforeLoading@{{ n.id }}" class="btn btn-sm btn-warning" style="background-color:ffa60c;" ng-click="ignore(n.id)">Ignore</button>
                                                                                <img id="IgnoreLoading@{{ n.id }}" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p ng-hide="newRequests.length>0" class="description">No new requests</p>
                                                        </div>
                                                        <br>
                                                        <br>
                                                        <p class="manager_title">Students<span class="pull-right">Total students: @{{ totalStudents }}</span></p>
                                                        <div class="manager_students">
                                                            <div class="row">
                                                                <div class="col-md-6" ng-repeat="f in fewStudents | orderBy : 'data.name'">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-xs-2">
                                                                                <img ng-if="!f.data.photo" src="{{asset('assets/img/default-avatar.png')}}" alt="Thumbnail Image" class="rounded-circle img-raised student_img">
                                                                                <img ng-if="f.data.photo" ng-src="@{{f.data.photo}}" alt="Thumbnail Image" class="rounded-circle img-raised student_img">
                                                                        </div>
                                                                        <div class="col-md-5 col-xs-7">
                                                                            <p class="student_name">@{{f.data.name}}</p>
                                                                        </div>
                                                                        <div class="col-md-3 col-xs-2">
                                                                            <div class="student_actions">
                                                                                    <button ng-click="removeStudentPrompt(f.id)" data-toggle="modal" data-target="#removeStudent" class="btn btn-sm btn-danger" style="background-color:#d40c0c">X</button>
                                                                              
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <p ng-hide="fewStudents.length>0" class="description">No students currently.</p>
                                                            <p class="text-center">
                                                                <img id="AllStudentLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 3rem; width:3rem;"/>
                                                            </p> 
                                                            <p class="description"><button class="btn btn-primary" id="AllStudentBeforeLoading" ng-click="viewAllStudents()" >View All</button></p>
                                                        </div>
                                                        <br>
                                                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#invitationModal" ><i class="fas fa-envelope"></i>&nbsp;&nbsp;Invite Students</button>
                                                    </div>
                                                </div>
                                                <div class="tab-pane active show" id="linkm2">
                                                    <p class="manager_title">Details</p>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding-bottom: 1em;">  <label class="manager_label" >Name:</label></td>
                                                                        <td style="padding-bottom: 1em;"><span id="drive_name" class="description" style="font-size:20px;left: 1rem;position:  relative;">{{$drive->name}}</span><br></td>
                                                                    </tr>
                                                                        <tr>
                                                                        <td style="padding-bottom: 1em;">  <label class="manager_label">College:</label></td>
                                                                        <td style="padding-bottom: 1em;"><span id="drive_college" class="description" style="font-size:20px;left: 1rem;position:  relative;">{{$drive->college}}</span><br></td>
                                                                    </tr>
                                                                        <tr>
                                                                        <td style="padding-bottom: 1em;">  <label class="manager_label">Info:</label></td>
                                                                        <td style="padding-bottom: 1em;"><span id="drive_info" class="description" style="font-size:20px;left: 1rem;position:  relative;">{{$drive->detail}}</span><br></td>
                                                                    </tr>
                                                                        <tr>
                                                                        <td style="padding-bottom: 1em;">  <label class="manager_label">Type:</label></td>
                                                                        <td style="padding-bottom: 1em;"><span id="drive_type" class="description" style="font-size:20px;left: 1rem;position:  relative;">{{$drive->type}}</span><br></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="padding-bottom: 1em;">  <label class="manager_label">Created at:</label></td>
                                                                        <td style="padding-bottom: 1em;"><span class="description" style="font-size:20px;left: 1rem;position:  relative;">{{date('M j, Y',strtotime($drive->created_at))}}</span><br></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <button class="btn btn-primary" data-toggle="modal" data-target="#editInfo">Edit Info</button>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="container text-center">
                                                                <text class="manager_label" rel="tooltip" data-placement="top" title="Student can search your class through this.">Class Code:</text>
                                                                <span style="margin-left:1rem" rel="tooltip" data-placement="top" title="Student can search your class through this."> {{$drive->code}}</span>
                                                                <br>
                                                                <br>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#invitationModal"><i class="fas fa-envelope"></i>&nbsp;&nbsp;Invite Students</button>                                                  
                                                            </div>
                                                            <br>
                                                            <br>
                                                            {{-- <div class="container text-center">
                                                                <text class="manager_label">Drive Access: 
                                                                </text>
                                                                <br>
                                                                <br>
                                                                <p style="margin-left: 36%;">
                                                                <input class="tgl tgl-skewed" id="cb3" type="checkbox" ng-click="toggleDriveAccess()" {{$drive->drive_access=="PUBLIC"?'checked':''}}/>
                                                                <label class="tgl-btn" data-tg-off="PRIVATE" data-tg-on="PUBLIC" for="cb3"></label>
                                                                </p>
                                                                <p class="text-center">
                                                                    <img id="ToggleDriveAccessLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 1.5rem; width:1.5rem;"/>
                                                                </p> 
                                                            </div> --}}
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <p class="manager_title">Renewal</p>
                                                   <p class="description"><button class="btn btn-primary" data-toggle="modal" data-target="#resetDrive">Reset Class</button></p>

                                                    <p class="description">Resetting class will remove all the posts, collections, boards and students. However the files from the drive will remain intact.</p>
                                                    
                                                    
                                                </div>
                                                <div class="tab-pane" id="linkm3">
                                                    <p class="manager_title">Space Usage</p>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="card text-center" style="box-shadow:none; border: 2px solid #2385aa; border-radius:10px;">
                                                                <div class="container" style="padding-top: 1rem;padding-bottom:  2rem;">
                                                                    <text class="manager_label">Drive</text><br><br>
                                                                    <text class="descrption" style="font-size:20px" id="drivespace">200MB</text>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="card text-center" style="box-shadow:none; border: 2px solid #2385aa; border-radius:10px;">
                                                                <div class="container" style="padding-top: 1rem;padding-bottom:  2rem;">
                                                                    <text class="manager_label">Collection</text><br><br>
                                                                    <text class="descrption" style="font-size:20px" id="collectionspace">200MB</text>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="card text-center" style="box-shadow:none; border: 2px solid #2385aa; border-radius:10px;">
                                                                <div class="container" style="padding-top: 1rem;padding-bottom:  2rem;">
                                                                    <text class="manager_label">Posts</text><br><br>
                                                                    <text class="descrption" style="font-size:20px" id="postspace">200MB</text>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label class="manager_label" >Space used by this class: </label>
                                                    <span class="description" style="font-size:20px; margin-left:1rem" id="thistotalspace">600MB</span><br>
                                                    <br>
                                                    <label class="manager_label" >Total space used by the user: </label>
                                                    <span class="description" style="font-size:20px; margin-left:1rem" id="totalspace">300MB out of 1GB</span>
                                                    <br>
                                                    <br>
                                                    <p class="description" style="font-size:18px; margin-left:1rem">1 GB space is provided for free to each users in beta version.
                                                    More space coming soon in full version.</p>
                                                </div>
                                                <div class="tab-pane" id="linkm4">
                                                    <p class="manager_title">Published Results</p>
                                                    <p class="text-center">
                                                        <img id="ResultsLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                    </p> 
                                                    <ul class="result_list" id="ResultsBeforeLoading">
                                                    <li ng-repeat="r in results"><a target="_blank" href="/user/collection/{{$drive->id}}/@{{r.collection.id}}/viewresult">@{{r.collection.name}}<span> (@{{r.created_at.replace(' ','T') | date: 'MMM d, y h:mm a'}})</span></a></li>
                                                     </ul>
                                                    <a href="/user/manager/{{$drive->id}}/viewsummaryresult" target="_blank" class="btn btn-primary pull-right">View Result Summary</a>
                                                </div>
                                                <div class="tab-pane" id="linkm5">
                                                    <div class="row">
                                                    <div class="col-md-4 col-sm-6">
                                                            <div class="m-info info-hover">
                                                                <div class="icon icon-success" style="font-size:40px">
                                                                    <i class="fas fa-chalkboard"></i>
                                                                </div>
                                                                <h4 class="info-title">White Board</h4>
                                                                <p class="description">Save whatever you taught today or will teach tomorrow so that your students can view it anytime.</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-6">
                                                            <div class="m-info info-hover">
                                                                <div class="icon icon-success" style="font-size:40px">
                                                                    <i class="far fa-hdd"></i>
                                                                </div>
                                                                <h4 class="info-title">Upload Materials</h4>
                                                                <p class="description">Upload notes, slides or any materials to share. Your students will be able to download it anytime.</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="m-info info-hover">
                                                                <div class="icon icon-success" style="font-size:40px">
                                                                    <i class="far fa-folder-open"></i>
                                                                </div>
                                                                <h4 class="info-title">Assignment Collection</h4>
                                                                <p class="description">Create folder for collection of assigments from your students. You can view, grade and download the assignments.</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="m-info info-hover">
                                                                <div class="icon icon-warning" style="font-size:40px">
                                                                    <i class="fas fa-puzzle-piece"></i>
                                                                </div>
                                                                <h4 class="info-title">Extras</h4>
                                                                <p class="description">Post information or extra materials for your students.</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="m-info info-hover">
                                                                <div class="icon icon-warning" style="font-size:40px">
                                                                    <i class="far fa-bell"></i>
                                                                </div>
                                                                <h4 class="info-title">Notices</h4>
                                                                <p class="description">Send any notices, instruction or any quick text for your students.</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="m-info info-hover">
                                                                <div class="icon icon-warning" style="font-size:40px">
                                                                    <i class="fas fa-list-ul"></i>
                                                                </div>
                                                                <h4 class="info-title">Publish Results</h4>
                                                                <p class="description">Grade assignments and publish results.</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="m-info info-hover">
                                                                <div class="icon icon-warning" style="font-size:40px">
                                                                    <i class="fas fa-cogs"></i>
                                                                </div>
                                                                <h4 class="info-title">Manager</h4>
                                                                <p class="description">Manage your class, view students, storage space and class profile.</p>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade modal-mini modal-primary" id="removeStudent" tabindex="-1" role="dialog" aria-labelledby="removeStudentLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div >
                                            <div class="modal-header justify-content-center">
                                                
                                                <div class="modal-profile" >
                                                
                                                    <i class="fa fa-trash"></i>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>Removing Student will delete all records/files associated with this student.</p>
                                                <p>Are you sure you want to remove this student?</p>
                                            </div>
                                            <input type="hidden" ng-model="rid"/>
                                            <div class="modal-footer">
                                                <img id="RemoveStudentLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                <button id="RemoveStudentBeforeLoading" type="button" ng-click="removeStudent(rid)" class="btn btn-link btn-neutral">Yes</button>
                                                <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="viewAllStudents" tabindex="-1" role="dialog" aria-labelledby="addFolderLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">All Students</h4>
                                        </div>
                                        
                                        
                                        
                                        <ul class="request_ul" id="">
                                            <li class="request_li" ng-repeat="a in allStudents">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                            <img src="{{asset('assets/img/default-avatar.png')}}" alt="Thumbnail Image" class="rounded-circle img-raised request_img">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <p class="request_name">@{{a.name}}</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="request_actions">
                                                            <button id="RemoveFromAllStudentBeforeLoading" class="btn btn-sm btn-danger" style="background-color:#d40c0c" ng-click="removeFromAllStudent(a.id)">X</button>
                                                            <img id="RemoveFromAllStudentLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 2rem; width:2rem;"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <p ng-hide="allStudents.length>0" class="description">No students currently.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="editInfo" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Class Info</h4>
                                        </div>
                                    <form id="editDriveInfo" action="{{URL::to('/user/drivepanel'.'/'.$drive->id.'/'.'edit')}}" method="POST">
                                            
                                            <div class="modal-body">
                                                    {{csrf_field()}}
                                                    <label class="title" style="margin-left:15px; margin-top:0px;">Class Name</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fas fa-book-open"></i>
                                                        </span>
                                                    <input type="text" name="name" value="{{$drive->name}}" class="form-control" placeholder="Name of Class eg. Mathematics-I, Sociology">
                                                    </div>
                                                    <label class="title" style="margin-left:15px;  margin-top:0px;">College</label>
                                                        
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-university"></i>
                                                        </span>
                                                        <input type="text" name="college" value="{{$drive->college}}" class="form-control" placeholder="College eg. Pulchowk Campus, LACM..">
                                                    </div>
                                                    <label class="title" style="margin-left:15px;  margin-top:0px;">Info</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-info"></i>
                                                        </span>
                                                        <input type="text" name="detail" value="{{$drive->detail}}" class="form-control" placeholder="eg. 1st year, 2nd sem...etc">
                                                    </div>
                                                    <label class="title" style="margin-left:15px;  margin-top:0px;">Type</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-info"></i>
                                                        </span>
                                                        <select class="form-control" name="type" value="{{$drive->type}}">
                                                            <option class="form-control" value="Semester">Semester</option>
                                                            <option class="form-control" value="Trimester">Trimester</option>
                                                            <option class="form-control" value="Annual">Annual</option>
                                                            
                                                        </select>
                                                    </div>
                                                
                                            </div>
                                            <p class="text-center">
                                                    <img id="editDriveInfoLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                </p> 
                                            <div class="modal-footer justify-content-center">
                                                    <p class="text-center">
                                                            <img id="editDriveInfoLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                                                        </p>
                                                <button id="editDriveInfoBeforeLoading" type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="invitationModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header justify-content-center">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="now-ui-icons ui-1_simple-remove"></i>
                                            </button>
                                            <h4 class="title">Send Invitation</h4>
                                        </div>
                                        <form id="invitationForm" action="{{URL::to('/user/manager'.'/'.$drive->id.'/'.'sendInvitation')}}" method="POST">
                                            
                                            <div class="modal-body">
                                                <p style="text-align:  center;color: red;" id="inviteError" hidden>All email addresses must be a valid email.</p>
                                                <p style="text-align:  center;color: grren;" id="inviteSuccess" hidden>All Invitations Sent.</p>
                                                <label class="title" style="margin-left:15px; margin-top:0px;">Email Address(es)</label>
                                                <div class="input-group">
                                                    <ul id="inviteEmails" style="width:100%"></ul>
                                                </div>
                                                <p> Students will be sent an email with a link to your class.</p>
                                            </div> 
                                            <div class="modal-footer justify-content-center">
                                                <p class="text-center">
                                                    <img id="invitationLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 5rem; width:5rem;"/>
                                                </p>
                                                <button id="invitationBeforeLoading" type="submit" class="btn btn-primary">Send</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @if($boxad)
            <div class="container_adv">                 
                <div class="banner_adv">
                        <span class="pull-right banner_inner_close" id="closead">X</span>
                        
                        <span class="pull-left banner_inner_badge">Ad:</span>
                        <div class="banner_inner_adv">
                        <a href="{{$boxad->link}}" target="_blank">{{$boxad->content}}</a>
                        </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<div class="modal fade" id="resetDrive" tabindex="-1" role="dialog" aria-labelledby="resetDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Reset Drive?</h4><br>
            </div>
            <div class="modal-body">
                    <p class="description">Resetting class will remove all posts, collections, notices, boards and students. However the files from the drive will remain intact.</p>
                    <form action="{{URL::to('/user/drivepanel/'.$drive->id.'/'.'resetdrive')}}" method="POST">
                        {{csrf_field()}}
                        <p class="text-center"> 
                            <button type="submit" class="btn btn-danger" style="background-color:#d42525">Reset</button>
                        </p>
                    </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Drive Info</h4>
            </div>
            <form action="{{URL::to('/user/createdrive')}}" method="POST">
                
                <div class="modal-body">
                        {{csrf_field()}}
                        <label class="title" style="margin-left:15px; margin-top:0px;">Drive Name</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-bookmark-o"></i>
                            </span>
                            <input type="text" name="name" class="form-control" placeholder="Name of Drive eg. Mathematics-I, Sociology">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">College</label>
                            
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-university"></i>
                            </span>
                            <input type="text" name="college" class="form-control" placeholder="College eg. Pulchowk Campus, LACM..">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">Info</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <input type="text" name="detail" class="form-control" placeholder="eg. 1st year, 2nd sem...etc">
                        </div>
                    
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="errorDrive" tabindex="-1" role="dialog" aria-labelledby="errorDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Something went wrong</h4><br>
            </div>
            <div class="modal-body">
            <p class="description" style="color:red;" id="errorMessage"></p>
            <p class="description" style="color:red;" id="errorDetails"></p>
            </div>
        </div>
    </div>
</div>


<input type="hidden" value="{{$drive->id}}" id="drive_id"/>


@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script type='text/javascript' src='/build/loading-bar.js'></script>
<script src="/js/angular-sanitize.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="/tagit/tag-it.js"></script>
<script>
    $(document).on("click", ".viewmorelink", function() {
        $(this).parent().attr('hidden',true);
        $(this).parent().next().removeAttr('hidden');
    });
    $(document).on("click", ".viewlesslink", function() {
        $(this).parent().attr('hidden',true);
        $(this).parent().prev().removeAttr('hidden');
    });
   
</script>
<script>
    
    $(document).ready(function(){
        $('#datepicker').datepicker({  format: 'yyyy-mm-dd' });
        $('#datepicker').click(function(){
            $(this).next().trigger('click');
        });
        $('#datepicker').keypress(function(e) {
            e.preventDefault();
        });
        monkeyPatchAutocomplete();

        function monkeyPatchAutocomplete()
        {
            $.ui.autocomplete.prototype._renderItem = function(ul, item) {
                var regexp = new RegExp(this.term);
                var highlightedVal = item.label.replace(regexp, "<span style='font-weight:bold;color:#2385aa;'>" + this.term + "</span>");
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a><img class='autocompleteUserAvatar' src='" + item.icon + "' style='width: 3rem;height: 3rem;border-radius: 50%;'/>&nbsp;&nbsp;" + highlightedVal + "&nbsp;&nbsp;</a>")
                        .appendTo(ul);
            };
        }
        
		$('#inviteEmails').tagit({
            removeConfirmation: true,
            autocomplete: ({
                source: function (request, response) {
                    $.ajax({
                        url: '/user/manager/{{$drive->id}}/searchUser',
                        data: { _token: '{{ csrf_token() }}' , key: request.term },
                        dataType: 'json',
                        type: 'POST',
                        success: function (data) {
                            
                            response($.map(data, function (item) {
                                return {
                                    label: item.name + ' (' + item.email + ')',
                                    value: item.email,
                                    icon: item.photo ? item.photo : '/assets/img/default-avatar.png'
                                }
                            }));
                        }
                    })
                },
                minLength: 2
            }),
            beforeTagAdded: function(event, ui) {
                if(ui.tagLabel == '{{ Auth::user()->email }}')
                    return false;
                return true;
            }
        });

        
        
        $('#datepicker2').datepicker({  format: 'yyyy-mm-dd' });
        $('#datepicker2').click(function(){
            $(this).next().trigger('click');
        });
        $('#datepicker2').keypress(function(e) {
            e.preventDefault();
        });
        
        $('#datepicker3').datepicker({  format: 'yyyy-mm-dd' });
        $('#datepicker3').click(function(){
            $(this).next().trigger('click');
        });
        $('#datepicker3').keypress(function(e) {
            e.preventDefault();
        });

        $('#collectiontab').trigger('click');
        $('#generaltab').trigger('click');
        $('#collectiontab').trigger('click');
        $('#managertab').trigger('click');
        $('#collectiontab').trigger('click');
        $('#managertab').trigger('click');
        $('#collectiontab').trigger('click');


        $('#generaltab').trigger('click');

        $('#generalDriveButton').click(function(){
            $('#drivetab').trigger('click');
            
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#generalDriveButton2').click(function(){
            $('#drivetab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#generalCollectionButton').click(function(){
            $('#collectiontab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#gb2').click(function(){
            $('#collectiontab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#generalExtrasButton').click(function(){
            $('#extrastab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#generalExtrasButton2').click(function(){
            $('#extrastab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#generalBoardButton').click(function(){
            $('#boardtab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#generalManagerButton').click(function(){
            $('#managertab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
        $('#generalResultButton').click(function(){
            $('#collectiontab').trigger('click');
            $("html, body").animate({
                scrollTop: $('#classsection').offset().top
            });
        });
    });
    $("#editDriveInfo").submit(function(e) {

        var url = "{{URL::to('/user/drivepanel'.'/'.$drive->id.'/'.'edit')}}"; // the script where you handle the form input.
        $('#editDriveInfoBeforeLoading').attr('hidden','true');
        $('#editDriveInfoLoading').removeAttr('hidden');
        $.ajax({
            type: "POST",
            url: url,
            data: $("#editDriveInfo").serialize(), // serializes the form's elements.
            success: function(data)
            {
                $('#drive_name').html(data.name);
                $('#drive_college').html(data.college);
                $('#drive_info').html(data.detail);
                $('#drive_type').html(data.type);
                $('#editDriveInfoBeforeLoading').removeAttr('hidden');
                $('#editDriveInfoLoading').attr('hidden','true');
                $('#editInfo').modal('hide');
                // alert(data); // show response from the php script.
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $("#invitationForm").submit(function(e) {
        e.preventDefault();
        $('#inviteError').attr('hidden','true');

        var emails = $('#inviteEmails').tagit("assignedTags");
        if(emails.length == 0) return;
        var url = "{{URL::to('/user/manager'.'/'.$drive->id.'/'.'sendInvitation')}}"; // the script where you handle the form input.
        $('#invitationBeforeLoading').attr('hidden','true');
        $('#invitationLoading').removeAttr('hidden');
        $.ajax({
            type: "POST",
            url: url,
            data: {_token:'{{ csrf_token() }}',emails:emails},
            success: function(data)
            {
                $('#invitationBeforeLoading').removeAttr('hidden');
                $('#invitationLoading').attr('hidden','true');
                $('#inviteSuccess').removeAttr('hidden');
                $('#inviteEmails').tagit('removeAll');
                setTimeout(function () {
                    $('#inviteSuccess').attr('hidden','true');
                    $('#invitationModal').modal('hide');
                }, 2000);
                
            },
            error: function(xhr)
            {
                if(xhr.status == 422){
                    $('#inviteError').removeAttr('hidden');
                }
                $('#invitationBeforeLoading').removeAttr('hidden');
                $('#invitationLoading').attr('hidden','true');
            }
        });
    });
    
    var hideAllTab=function(){
        $('[id*="dlink"]').attr('hidden',true);
    }
    $('#generaltab').click(function(){
        hideAllTab();
        $('#dlink1').removeAttr('hidden');
    });
    $('#boardtab').click(function(){
        hideAllTab();
        $('#dlink2').removeAttr('hidden');
    });
    $('#drivetab').click(function(){
        hideAllTab();
        $('#dlink3').removeAttr('hidden');
    });
    $('#collectiontab').click(function(){
        hideAllTab();
        $('#dlink4').removeAttr('hidden');
    });
    $('#extrastab').click(function(){
        hideAllTab();
        $('#dlink5').removeAttr('hidden');
    });
    $('#managertab').click(function(){
        hideAllTab();
        $('#dlink6').removeAttr('hidden');
    });

   

</script>
<script>
    $('#viewerimage').on('load',function(){
        $('#BeforeLoading').attr('hidden', 'hidden');
        $(this).removeAttr('hidden');
        $(this).contents().find('img').css('max-width','300px');
    });
    
    $('#viewerdoc').on('load',function(){
        $('#BeforeLoading').attr('hidden', 'hidden');
        $(this).removeAttr('hidden');
    });

    $('#Collectionviewerimage').on('load',function(){
        $('#CollectionBeforeLoading').attr('hidden', 'hidden');
        $(this).removeAttr('hidden');
        $(this).contents().find('img').css('max-width','300px');
    });
    
    $('#Collectionviewerdoc').on('load',function(){
        $('#CollectionBeforeLoading').attr('hidden', 'hidden');
        $(this).removeAttr('hidden');
    });

     $('#viewerpopup').on('hidden.bs.modal', function () {
    	$(this).find('#viewerimage')[0].contentWindow.location.reload(true);
    	$(this).find('#viewerimage').attr('src','');
    	$(this).find('#viewerdoc').attr('src','');
    });
    
    $('#Collectionviewerpopup').on('hidden.bs.modal', function () {
    	$(this).find('#Collectionviewerimage')[0].contentWindow.location.reload(true);
    	$(this).find('#Collectionviewerimage').attr('src','');
    	$(this).find('#Collectionviewerdoc').attr('src','');
    });
    
    $('#viewerpopup').on('hidden.bs.modal', function () {
    	$(this).find('#viewerimage')[0].contentWindow.location.reload(true);
    	$(this).find('#viewerimage').attr('src','');
    	$(this).find('#viewerdoc').attr('src','');
    });
    
    $('#Collectionviewerpopup').on('hidden.bs.modal', function () {

    	$(this).find('#Collectionviewerimage')[0].contentWindow.location.reload(true);
    	$(this).find('#Collectionviewerimage').attr('src','');
    	$(this).find('#Collectionviewerdoc').attr('src','');
    });
    
</script>
<script src="{{asset('drive/drive.js')}}"></script>
<script src="{{asset('drive/board.js')}}"></script>
<script src="{{asset('drive/collection.js')}}"></script>
<script src="{{asset('drive/post.js')}}"></script>
<script src="{{asset('drive/manager.js')}}"></script>
<script src="{{asset('drive/general.js')}}"></script>
<script>
    $(document).ready(function(){
        var eTop = $('.floating_card').offset().top; //get the offset top of the element
        if($(window).width()>995){
            $(window).scroll(function() { //when window is scrolled
                var sc=$(window).scrollTop();
                $('.floating_card').each(function(index){
                    var et=$(this).parent().offset().top;
                    var eb= $(this).parent().height();
                    if(sc<eb+50 && sc>=et-100)
                    {
                        $(this).css('top',sc-et+100);
                    }
                    else if(sc<et)
                    {
                        $(this).css('top',0);
                    }
                    
                })
            });
        }
    });

</script>
@endsection