@extends('layouts.app')

@section('body')
<link rel="stylesheet" href="/autocomplete/easy-autocomplete.css">
<style>
        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }
        .card-profile, .card-testimonial {
            margin-top: 30px;
            text-align: center;
        }
        .card {
            border: 0;
            border-radius: 0.25rem;
            display: inline-block;
            position: relative;
            width: 100%;
            margin-bottom: 30px;
            box-shadow: 0px 5px 25px 0px rgba(0, 0, 0, 0.2);
        }
        .card-profile .card-body .card-avatar, .card-testimonial .card-body .card-avatar {
            margin: 0 auto 30px;
        }
        
        .card-profile .card-avatar, .card-testimonial .card-avatar {
            max-width: 130px;
            max-height: 130px;
            margin: -60px auto 0;
        }
        .card-profile .card-avatar img, .card-testimonial .card-avatar img {
            border-radius: 50% !important;
            height:130px;
            width: 130px;
        }
        .home_icon{
            margin-bottom: 0px;
            margin-top: 0px;
        }
        .home_title{
            margin-bottom: 1.7rem;
            margin-top: 0.5rem;
            font-size: 25px;
            color:#2385aa
        }
        .home_description{
            font-weight: 500;
                font-size: 18px;
                margin:1rem;
            color: #000000c7;
            max-width: 700px;
        }
        .home_search{
            width:60%;
            margin-left:20%;
            margin-right:20%;
            line-height: 2rem;
            border-radius: 10px;
            /* border: 1px solid #2385aa; */
            border-bottom: 1px solid #2385aa;
            /* align:center; */
        }
        .home_search span{
            border-radius: 10px;
            font-size: 25px;
            padding-right: 1rem !important;
            cursor:pointer;
        }
        .home_search input{
            line-height: 2rem;
            border-radius: 10px;
        }
        .home_subinfo{
            max-width: 700px;
            margin: 20px auto 75px;
            margin-bottom: 2rem;
            font-size: 1.57em;
            line-height: 1.5em;
            color: #9A9A9A;
            font-weight: 600;
            text-align:center;            
        }
        .home_searchlabel{
            max-width: 700px;
            margin: 20px auto 75px;
            margin-bottom: 1rem;
            font-size: 1.57em;
            line-height: 1.5em;
            color: #2385aa;
            font-weight: 500;
            text-align: center;
        }
        .addicon{
            border-radius: 100px;
            height: 3rem;
        }
        .pull-down-footer{
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
        .pull-down-info{
            position: absolute;
            bottom: 4rem;
            left: 0;
            right: 0;
        }
        .pull-down-code{
            position: absolute;
            bottom: 7.5rem;
            left: 0;
            right: 0;
        }
        .student_number{
            font-size: 11px;
            margin-right: 5px;
            margin-top: 3px;
            background: #2385aa;
            z-index: 100;
            right: -1px;
            top: -3px;
            color: white;
            position: absolute;
            font-size: 10px;
            border: 2px solid #2385aa;
            border-radius: 50%;
            padding: 0px 6px;
            font-weight: 600;
        }
        .hovergreen:hover{
            border-color:green !important;
            color:green !important;
        }
        .hovergreen:active{
            border-color:green !important;
            color:green !important;
        }
        .hovergreen:focus{
            border-color:green !important;
            color:green !important;
        }
        .hovergreen:target{
            border-color:green !important;
            color:green !important;
        }
        .hoverred:hover{
            border-color:red !important;
            color:red !important;
        }
        .delete_class_btn{
            color: red;
            border: none;
            background: transparent;
            position: absolute;
            bottom: 1px;
            right: 2px;
            cursor: pointer;
        }
</style>
<style>
    @media only screen and (min-width : 320px) and (max-width : 768px) {
        .nav-tabs {
            /* display: inline-block; */
            /* width: 100%; */
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
    }
</style>
<div class="wrapper">
    <div class="page-header page-header-small" style="min-height: 246px;" filter-color="blue">
        <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg5.jpg');">
        </div>
        <div class="container">
            <div class="content-center" style="padding-top: 3rem;">
                <div class="photo-container">
                        @if(Auth::user()->data()->photo)
                        <img src="{{Auth::user()->data()->photo}}" alt="">
                        @else
                        <img src="{{asset('assets/img/default-avatar.png')}}" alt="">
                        @endif
                </div>
                <br>
           
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="button-container" style="margin-top: -93px;">
                <a href="{{ env('ACCOUNT_URL') }}editprofile?redirectUrl={{ url()->current() }}" target="_blank" class="btn btn-primary btn-round btn-lg"><i class="fas fa-user-edit"></i>&nbsp;&nbsp;Edit Profile</a>
     
            </div>
            <br>
        {{-- <h3 class="title">Welcome, {{ explode(' ',Auth::user()->data()->name)[0]}}!</h3> --}}
            <div class="row"  id="initialInfo">
                <div class="col-lg-6 col-md-6 col-xs-6 text-center" style="border-right: 2px solid #9a9a9a;">
                   
                    <h3 class="title home_title">As a Teacher</h3>
                    <p class="home_description">
                            Manage your class digitally and easily.
                    </p>
                    <p class="home_description">
                        Create your class, invite students through class code or by email.
                    </p>
                        <p class="home_description">
                        Share downloadable contents and materials with your class.
                        </p>
                        <p class="home_description">
                        Give assignments and collect them digitally, grade them and publish results.

                    </p>
                    <h5 class="home_description" style="color:#2385aa"><b>Use EzuClass:</b></h5>
                    <h5 class="description" > <a href="javascript:void(0)" class="btn btn-primary" id="startAsTeacher" > <i class="fas fa-user-tie"></i>&nbsp;&nbsp;As a Teacher</a></h5>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6 text-center">
                       <h3 class="title home_title">As a Student</h3>
                    <p class="home_description">
                            Enhance your learning experience with the use of EzuClass.
                    </p>
                    <p class="home_description">
                            Join your digital class created by your teacher.
                    </p>
                    <p class="home_description">
                            Search your class by class code or teacher's name, request to join the class. 
                    </p>
                    <p class="home_description"> 
                            View and download the materials shared by your teachers and submit assignments online.
    
                    </p>
                    <h5 class="home_description" style="color:#2385aa"><b>Use EzuClass:</b></h5>
                    <h5 class="description" ><a href="javascript:void(0)" class="btn btn-primary" id="startAsStudent" > <i class="fas fa-user-graduate"></i>&nbsp;&nbsp;As a Student</a></h5>
                
                </div>
            </div>
            <div class="card" id="tabDiv" hidden>
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" id="teachertablink" href="#asTeacher" role="tab">
                            <i class="fas fa-user-tie"></i> &nbsp;As a teacher
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" id="studenttablink" href="#asStudent" role="tab">
                            <i class="fas fa-user-graduate"></i>&nbsp; As a Student
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" id="infotablink" href="#info" role="tab">
                            <i class="fas fa-info"></i>&nbsp; Info
                        </a>
                    </li>
                </ul>
                <div class="card-body">
                    <!-- Tab panes -->
                    <div class="tab-content text-center">
                        <div class="tab-pane active" id="asTeacher" role="tabpanel">
                        <h5 class="home_searchlabel" {{$drives->count()>0?'':'hidden'}}>Create and Manage your class.</h5>
                              
                            <h4  class="title text-center" {{$drives->count()>0?'':'hidden'}}>My Classes: </h4>
                            <p class="text-center">
                            <button class="btn btn-primary addicon" rel="tooltip" data-placement="top" title="Create Class" data-toggle="modal" data-target="#createClass" {{$drives->count()>0?'':'hidden'}}><i style="font-size:20px" class="fa fa-plus"></i></button>
                            </p>
                            <div class="row" id="myclasses">        
                            @forelse($drives as $d)    
                        
                            <div class="col-md-6 col-lg-3">
                                <div class="card card-profile" style="height:20rem">
                                <span rel="tooltip" data-placement="top" title="Number of Students in this Class" class="student_number">{{$d->followers()->count()}}</span>
                                    <div class="card-body">
                                    <h6 class="category text-gray" title="{{$d->college}}">{{strlen($d->college)>32?substr($d->college,0,32).'..':$d->college}}</h6>
                                    <h4 class="card-title" title="{{$d->name}}">{{strlen($d->name)>24?substr($d->name,0,20).'..':$d->name}}</h4>

                                        <p class="card-description pull-down-code" rel="tooltip" data-placement="top" title="Class Code"  style="font-weight:600;">
                                            Class Code: {{$d->code}}
                                        </p>
                                        <p class="card-description pull-down-info" title="{{ $d->detail }}">
                                            {{strlen($d->detail)>35?substr($d->detail,0,35).'..':$d->detail}}
                                        </p>
                                        <div class="card-footer pull-down-footer">
                                            <a href="{{URL::to('/user/mydrive'.'/'.$d->slug)}}" target="_blank" class="btn btn-primary btn-simple btn-round hovergreen">Manage</a>
                                            
                                            <button class="delete_class_btn" id="deleteDrive{{  $d->id }}" drive-id="{{ $d->slug }}" drive-name="{{ $d->name }}" drive-students="{{ $d->followers->count() }}" drive-assignments="{{ $d->collections->count() }}" drive-files="{{ $d->drive_contents->count() }}" drive-posts="{{ $d->posts->count() }}" rel="tooltip" data-placement="top" title="Delete Class"><i class="far fa-trash-alt"></i></button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            @empty
                            <div class="col-md-12 col-lg-12">
                                <h3 class="title">Create your first class!!!</h3>
                            
                                <h5 class="home_subinfo">Create class and add your students to share files, post and to collect assignments.</h5>
                            
                                <p class="text-center">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#createClass">Create</button>
                                </p>
                            </div>
                            @endforelse 
                        </div>
                        
                        </div>
                        <div class="tab-pane" id="asStudent" role="tabpanel">
                                <h5 class="home_searchlabel">Find and Join your class.</h5>
                                <div class="input-group home_search">
                                    
                                    <input type="text" class="form-control" id="search" placeholder="Search by teacher's name or CLASS CODE.">
                                    <span class="input-group-addon scrolldown" id="searchIcon" target="#searchResults">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div> 
                                <p class="text-center">
                                    <img id="searchLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 5rem; width:5rem;"/>
                                </p>
                                <div id="searchResults">
                    
                                </div>
                            @if(count(Auth::user()->followed_drives)>0 || count(Auth::user()->pending_drives)>0 )
            
                                <h4 class="title text-center">My Joined Classes</h4>
                            
                                
                                <div class="row">
                                    @foreach(Auth::user()->followed_drives as $d)     
                                        <div class="col-md-6 col-lg-4">
                                            <div class="card card-profile" style="height:28rem;">
                                                <div class="card-body">
                                                    <div class="card-avatar">
                                                        <a href="#pablo">
                                                            @if($d->owner->data()->photo)
                                                            <img class="img img-raised" src="{{$d->owner->data()->photo}}">
                                                            @else
                                                            <img class="img img-raised" src="{{asset('assets/img/default-avatar.png')}}">
                                                            @endif
                
                                                        </a>
                                                    </div>
                                                    <h6 class="category text-gray"><a href="{{ route('publicprofile',$d->owner->data()->slug) }}" target="_blank" style="color:#9A9A9A">{{ $d->owner->data()->name }}</a></h6>
                                                    <h4 class="card-title">{{ $d->name }}</h4>
                                                    <p class="card-description" title="{{ $d->detail }}">
                                                        {{strlen($d->detail)>35?substr($d->detail,0,35).'..':$d->detail}}
                                                    </p>
                                                    <p class="card-description pukk-down-info">
                                                            {{ $d->college }}
                                                    </p>
                                                    <div class="card-footer pull-down-footer">
                                                        <a href="{{ route('class',$d->slug) }}" target="_blank" class="btn btn-primary btn-simple btn-round hovergreen" >Open</a>
                                                        <button type="button" class="btn btn-primary btn-simple btn-round hoverred" id="btnFollowDrive{{ $d->id }}" drive-id="{{ $d->id }}" drive-name="{{ $d->name }}">Leave</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    @endforeach 
                                    @foreach(Auth::user()->pending_drives as $d)     
                                    <div class="col-md-6 col-lg-4">
                                        <div class="card card-profile" style="height:28rem">
                                            <div class="card-body">
                                                <div class="card-avatar">
                                                    <a href="#pablo">
                                                        @if($d->owner->data()->photo)
                                                        <img class="img img-raised" src="{{$d->owner->data()->photo}}">
                                                        @else
                                                        <img class="img img-raised" src="{{asset('assets/img/default-avatar.png')}}">
                                                        @endif                                        
                                                    </a>
                                                </div>
                                                <h6 class="category text-gray"><a href="{{ route('publicprofile',$d->owner->data()->slug) }}" style="color:#9A9A9A">{{ $d->owner->data()->name }}</a></h6>
                                                <h4 class="card-title">{{ $d->name }}</h4>
                                                
                                                <p class="card-description" title="{{ $d->detail }}">
                                                    {{strlen($d->detail)>35?substr($d->detail,0,35).'..':$d->detail}}
                                                </p>
                                                <p class="card-description pukk-down-info">
                                                        {{ $d->college }}
                                                </p>
                                                <div class="card-footer pull-down-footer">
                                                    <span style="cursor:text;" class="btn btn-primary btn-simple btn-round">Pending..</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    @endforeach 
                                </div>
                            @endif
                        </div>
                        <div class="tab-pane" id="info" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 text-center" style="border-right: 2px solid #9a9a9a;">
                                    
                                    <h3 class="title home_title">As a Teacher</h3>
                                    <p class="home_description">
                                            Manage your class digitally and easily.
                                    </p>
                                    <p class="home_description">
                                        Create your class, invite students through class code or by email.
                                    </p>
                                        <p class="home_description">
                                        Share downloadable contents and materials with your class.
                                        </p>
                                        <p class="home_description">
                                        Give assignments and collect them digitally, grade them and publish results.
                
                                    </p>
                                 </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 text-center">
                                        <h3 class="title home_title">As a Student</h3>
                                    <p class="home_description">
                                            Enhance your learning experience with the use of EzuClass.
                                    </p>
                                    <p class="home_description">
                                            Join your digital class created by your teacher.
                                    </p>
                                    <p class="home_description">
                                            Search your class by class code or teacher's name, request to join the class. 
                                    </p>
                                    <p class="home_description"> 
                                            View and download the materials shared by your teachers and submit assignments online.
                    
                                    </p> 
                                   
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>

</div>

<div class="modal fade modal-mini modal-primary" id="confirmAlert" tabindex="-1" role="dialog" aria-labelledby="confirmAlertLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div >
                <div class="modal-header justify-content-center">
                    
                    
                </div>
                <div class="modal-body">
                    <p id="confirmText"></p>
                </div>
                <div class="modal-footer">
                    <button id="confirmOk" type="button" class="btn btn-link btn-neutral">OK</button>
                    <button type="button" id="confirmNo" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createClass" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Class Info</h4>
            </div>
            <form id="createClassForm" action="{{URL::to('/user/createdrive')}}" method="POST">
                
                <div class="modal-body">
                        {{csrf_field()}}
                        <label class="title" style="margin-left:15px; margin-top:0px;">Faculty</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <select required class="form-control"  id="AddClassCategory">
                               <option selected disabled value="">Select Category</option>
                               @foreach($categories as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                               @endforeach
                                
                            </select>
                        </div>
                        <label class="title" style="margin-left:15px; margin-top:0px;">Sub-Faculty</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <select required class="form-control" name="faculty_id" id="AddClassFaculty">
                               <option selected disabled value="">Select Sub-Faculty</option>
                               @foreach($faculties as $f)
                                <option value="{{$f->id}}">{{$f->name}}</option>
                               @endforeach
                                
                            </select>
                        </div>
                        <label class="title" style="margin-left:15px; margin-top:0px;">Subject</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fas fa-book-open"></i>
                            </span>
                            <input required type="text" name="name" class="form-control" placeholder="eg. Mathematics-I, Sociology">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">College</label>
                            
                        <div class="input-group" id="coldiv">
                            <span class="input-group-addon">
                                <i class="fa fa-university"></i>
                            </span>
                            <input required type="text" id="colleges" name="college" class="form-control" placeholder="College eg. Pulchowk Campus, LACM.." style="border-radius: 0 30px 30px;border-bottom-left-radius: 0;width: 100%;">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">Info</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <input required type="text" name="detail" class="form-control" placeholder="eg. 1st year, 2nd sem...etc">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">Type</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <select required class="form-control" name="type">
                                <option class="form-control" value="semester">Semester</option>
                                <option class="form-control" value="trimester">Trimester</option>
                                <option class="form-control" value="annual">Annual</option>
                                
                            </select>
                        </div>
                    
                </div>
                <div class="modal-footer justify-content-center">
                               
                    <button type="submit" name="createclass" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="resetDrive" tabindex="-1" role="dialog" aria-labelledby="resetDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Delete <span id="delDriveName"></span>?</h4><br>
            </div>
            <div class="modal-body">
                    <p class="description" style="font-weight: 600;">In this class you have:</p>
                    <div class="row text-center">
                        <div class="col-md-6">
                            <table>
                                <tbody>
                                    <tr>
                                   <td style="padding: 1rem 0.5rem;">
                                        <i class="fas fa-user-graduate" style="font-size:42px;"></i>
                                   </td>
                                   <td style="padding: 1rem 0.5rem;">
                                        <br>    
                                        <p style="line-height: 0px;margin-top: 2px; margin-bottom: 9px;" id="nStudents">12</p>
                                        <p >Students</p>
                                    <td>    
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="col-md-6">
                            <table>
                                <tbody>
                                <tr>
                                    <td style="padding: 1rem 0.5rem;">
                                        <i class="fas fa-file" style="font-size:42px;"></i>
                                    </td>
                                    <td style="padding: 1rem 0.5rem;">
                                        <br>
                                        <p style="line-height: 0px;margin-top: 2px; margin-bottom: 9px;" id="nFiles">12</p>
                                        <p>Shared Files</p>
                                    <td>    
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table>
                                <tbody>
                                    <tr>
                                   <td style="padding: 1rem 0.5rem;">
                                        <i class="fas fa-puzzle-piece" style="font-size:42px;"></i>
                                   </td>
                                   <td style="padding: 1rem 0.5rem;">
                                        <br>    
                                        <p style="line-height: 0px;margin-top: 2px; margin-bottom: 9px;" id="nPosts">12</p>
                                        <p >Posts</p>
                                    <td>    
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                           <table>
                                <tbody>
                                    <tr>
                                   <td style="padding: 1rem 0.5rem;">
                                        <i class="fas fa-edit" style="font-size:42px;"></i>
                                   </td>
                                   <td style="padding: 1rem 0.5rem;">
                                        <br>    
                                        <p style="line-height: 0px;margin-top: 2px; margin-bottom: 9px;" id="nAssignments">12</p>
                                        <p >Assignments</p>
                                    <td>    
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <p class="text-center" style="font-weight:600">Are you sure you want to delete this class?</p>

                    <form action="" method="POST" id="delDriveUrl">
                        {{csrf_field()}}
                        <p class="text-center"> 
                            <img id="delSubmitLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 4rem; width:4rem;"/>
                            <button type="submit" id="delSubmit" class="btn btn-danger" style="background-color:#d42525">Delete</button>
                        </p>
                    </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="errorAlert" tabindex="-1" role="dialog" aria-labelledby="errorDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Opps!</h4><br>
            </div>
            <div class="modal-body">
            <p class="description" style="color:red;" id="errorMessage"></p>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js')
<script src="/autocomplete/jquery.easy-autocomplete.js"></script>
<script>
    $(document).ready(function(){
        $('#startAsTeacher').click(function(){
            $('#initialInfo').attr('hidden','true');
            $('#tabDiv').removeAttr('hidden');
            $('#teachertablink').trigger('click');
        });
        $('#startAsStudent').click(function(){
            $('#initialInfo').attr('hidden','true');
            $('#tabDiv').removeAttr('hidden');
            $('#studenttablink').trigger('click');
        });
        
        if(window.location.hash=='#asTeacher')
        {
            $('#startAsTeacher').trigger('click');   
        }
        else if(window.location.hash=='#asStudent')
        {
            $('#startAsStudent').trigger('click');
        }
        
        var options = {
			data:@json($colleges),
			list: {
                match: {
                    enabled: true
                }
            }
		};
        
        $("#colleges").easyAutocomplete(options);
        $('#colleges').on('focus', function() {
            $('#coldiv').addClass('input-group-focus');
        });

        $('#colleges').focusout(function() {
            $('#coldiv').removeClass('input-group-focus');
        });
        
    });
    $(document).ready(function(){
        // $('[name="createclass"]').click(function(){
        //     // $(this).attr('hidden','true');
        //     // $('[name="createclassLoading"]').removeAttr('hidden');
        // })
        var faculties=@json($faculties);
        $('#AddClassCategory').change(function(){
            $('#AddClassFaculty').empty();
            var cat_id=$('#AddClassCategory').val();
            for(var i=0 ;i<faculties.length;i++)
            {
                if(faculties[i].category_id==cat_id)
                {
                    var a='<option value="'+faculties[i].id+'">'+faculties[i].name+'</option>';
                    $('#AddClassFaculty').append(a);   
                }
            }
        })
    });
    $(document).ready(function(){

        $("[id*='btnFollowDrive']").click(function(e){

            var ele = $(this);
            
            var text = (ele.text() == 'Requested' ? 'Cancel request to join' : ele.text()) + ' ' + ele.attr('drive-name') + '?';
            $('#confirmText').html(text);
            $('#confirmAlert').modal('show');

            $('#confirmOk').unbind().click(function(){
                $('#confirmAlert').modal('hide');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::to('follow') }}/"+ele.attr('drive-id'),
                    dataType: 'json',
                    beforeSend: function(){
                        ele.attr('disabled','disabled');
                    },
                    success: function(response) {
                        ele.html(response.action);
                    },
                    error: function(){
                       	$('#errorMessage').html('Something went wrong. Pleaase Try again');
                        $('#errorAlert').modal('show');
                    },
                    complete: function(){
                        ele.removeAttr('disabled');
                    }
                });
            });
        });
        $('#searchIcon').click(function(){
            var key=$('#search').val();
            if(key.replace(' ','').length == 0) return;
            $('#searchLoading').removeAttr('hidden');
            $('#searchResults').attr('hidden','true');
            $.ajax({
                url: '/search',
                type: 'GET',
                data:{'user':key},
                success: function(data){ 
                    $('#searchLoading').attr('hidden','true');
                    $('#searchResults').removeAttr('hidden');
                    $('#searchResults').html(data);
                    $('#viewmoreuser').attr('href','/searchmore/?search='+key);
                    $('html, body').animate({
                        scrollTop: $('#searchResults').offset().top-200
                    }, 500);
                },
                error: function(data) {
                    $('#searchLoading').attr('hidden','true');
                    $('#errorMessage').html('Something went wrong. Pleaase Try again');
                    $('#errorAlert').modal('show');
                }
            });
        })
        $('#search').keypress(function(e){
            
            if(e.which == 13){//Enter key pressed
                var key=$('#search').val();
                if(key.replace(' ','').length == 0) return;
                $('#searchResults').attr('hidden','true');
                $('#searchLoading').removeAttr('hidden');
                $.ajax({
                    url: '/search',
                    type: 'GET',
                    data:{'user':key},
                    success: function(data){ 
                        $('#searchLoading').attr('hidden','true');
                        $('#searchResults').removeAttr('hidden');
                        $('#searchResults').html(data);
                        $('#viewmoreuser').attr('href','/searchmore/?search='+key);
                        $('html, body').animate({
                            scrollTop: $('#searchResults').offset().top-200
                        }, 500);
                    },
                    error: function(data) {
                        $('#searchLoading').attr('hidden','true');
                        $('#errorMessage').html('Something went wrong. Pleaase Try again');
                        $('#errorAlert').modal('show');
                    }
                });
            }
        });
        
        $("[id*='deleteDrive']").click(function(e){
            e.preventDefault();
            var form = $(this);

            $('#delDriveName').text(form.attr('drive-name'));

            $('#delDriveUrl').attr('action',"{{ URL::to('user/drive/') }}" +'/'+ form.attr('drive-id') + '/delete');
            $('#nStudents').html(form.attr('drive-students'));
            $('#nFiles').html(form.attr('drive-files'));
            $('#nAssignments').html(form.attr('drive-assignments'));
            $('#nPosts').html(form.attr('drive-posts'));
            // var text = "Deleting class will remove all drive, posts, collections and notices contents.<br>"+
            //            "No. of students : "   + form.attr('drive-students')   +"<br>"+
            //            "No. of files : "      + form.attr('drive-files')      +"<br>"+
            //            "No. of assignments : "+ form.attr('drive-assignments')+"<br>"+
            //            "No. of posts : "      + form.attr('drive-posts')      +"<br>";
            
            // $('#delDriveDes').html(text);
            $('#resetDrive').modal('show');
            
        });
        $('#delSubmit').click(function(){
            $('#delSubmit').attr('hidden','true');
            $('#delSubmitLoading').removeAttr('hidden');
        });
      
       
        
         $("#createClassForm").submit(function() {
    		$(this).submit(function() {
        		return false;
   		 });
    		return true;
	});
    });

</script>
@endsection