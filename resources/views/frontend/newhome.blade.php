@extends('layouts.app')

@section('body')

<style>
        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }
        .card-profile, .card-testimonial {
            margin-top: 30px;
            text-align: center;
        }
        .card {
            border: 0;
            border-radius: 0.25rem;
            display: inline-block;
            position: relative;
            width: 100%;
            margin-bottom: 30px;
            box-shadow: 0px 5px 25px 0px rgba(0, 0, 0, 0.2);
        }
        .card-profile .card-body .card-avatar, .card-testimonial .card-body .card-avatar {
            margin: 0 auto 30px;
        }
        now-ui-kit.css?v=1.1.0:8286
        .card-profile .card-avatar, .card-testimonial .card-avatar {
            max-width: 130px;
            max-height: 130px;
            margin: -60px auto 0;
        }
        .card-profile .card-avatar img, .card-testimonial .card-avatar img {
            border-radius: 50% !important;
            height:130px;
            width: 130px;
        }
        .home_search{
            width:60%;
            margin-left:20%;
            margin-right:20%;
            line-height: 2rem;
            border-radius: 10px;
            /* border: 1px solid #2385aa; */
            border-bottom: 1px solid #2385aa;
            /* align:center; */
        }
        .home_subinfo{
            max-width: 700px;
            margin: 20px auto 75px;
            margin-bottom: 2rem;
            font-size: 1.57em;
            line-height: 1.5em;
            color: #9A9A9A;
            font-weight: 600;
            text-align:center;            
        }
        .home_searchlabel{
            max-width: 700px;
            margin: 20px auto 75px;
            margin-bottom: 1rem;
            font-size: 1.57em;
            line-height: 1.5em;
            color: #2385aa;
            font-weight: 500;
            text-align: center;
        }
</style>
<div class="wrapper">
    <div class="page-header page-header-small" filter-color="blue">
        <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg5.jpg');">
        </div>
        <div class="container">
            <div class="content-center">
                <div class="photo-container">
                    <img src="../assets/img/ryan.jpg" alt="">
                </div>
                <br>
           
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="button-container">
                <a href="#button" class="btn btn-primary btn-round btn-lg"><i class="fa fa-settings"></i>Edit Profile</a>
                <a href="#button" c\lass="btn btn-default btn-round btn-lg btn-icon" rel="tooltip" title="Follow me on Twitter">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="#button" class="btn btn-default btn-round btn-lg btn-icon" rel="tooltip" title="Follow me on Instagram">
                    <i class="fa fa-instagram"></i>
                </a>
            </div>
            <br>
            <h3 class="title" style="margin-top:0px;">Welcome to EZ Drive</h3>
            {{-- <h5 class="home_subinfo">A Platform to share the files, collect and download assignments among teachers and students. </h5>
               <h5> --}}
                    <h5 class="home_searchlabel">Find your teacher and follow their respective drives</h5>
                    <h5>
          
                <input type="text" class="form-control home_search" placeholder="Search for your teacher.."/>
               </h5>
            @if(count(Auth::user()->followed_drives)>0)
            
                <h4 class="title text-center">My Followed Drives</h4>
                <div class="nav-align-center">
                    <ul class="nav nav-pills nav-pills-primary" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile" role="tablist">
                                <i class="now-ui-icons design_image"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home" role="tablist">
                                <i class="now-ui-icons location_world"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#messages" role="tablist">
                                <i class="now-ui-icons sport_user-run"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    
                <div class="row">
                    @foreach(Auth::user()->followed_drives as $d)     
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-profile">
                                <div class="card-body">
                                    <div class="card-avatar">
                                        <a href="#pablo">
                                            <img class="img img-raised" src="assets/img/avatar.jpg">
                                        </a>
                                    </div>
                                    <h6 class="category text-gray">{{ $d->owner->name }}</h6>
                                    <h4 class="card-title">{{ $d->name }}</h4>
                                    <p class="card-description">
                                        {{ $d->detail }}
                                    </p>
                                    <div class="card-footer">
                                        <a href="{{ route('class',$d->id) }}" class="btn btn-primary btn-simple btn-round">Open</a>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    @endforeach 
                </div>
            @endif
            <br>
            <br>
            <h5 class="home_subinfo">For the teachers, create your first drive to share files, post and collect assignments and other stuffs.</h5>
            <h5>
                <p class="text-center">
            <button class="btn btn-primary">Create</button>
                </p>
        </div>
    </div>
@endsection