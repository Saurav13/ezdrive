@extends('layouts.app')

@section('body')
    <style>
        .search-result img{
            height:130px;
            width: 130px;
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true">
            </div>
            <div class="container">
                <div class="content-center">
                    <form class="form" method="GET" action="/searchbycode">
                        <div class="content">
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </span>
                                <input type="text" class="form-control" name="code" placeholder="Search by Code">
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <div class="team">
                    <div class="row" style="display: block;margin:100px">
                        <h3 class="title"><p style="text-align:center;font-weight: 500;">No results found</p></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection