@extends('layouts.app')

@section('body')

<style>
        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }
        .card-profile, .card-testimonial {
            margin-top: 30px;
            text-align: center;
        }
        .card {
            border: 0;
            border-radius: 0.25rem;
            display: inline-block;
            position: relative;
            width: 100%;
            margin-bottom: 30px;
            box-shadow: 0px 5px 25px 0px rgba(0, 0, 0, 0.2);
        }
        .card-profile .card-body .card-avatar, .card-testimonial .card-body .card-avatar {
            margin: 0 auto 30px;
        }
        
        .card-profile .card-avatar, .card-testimonial .card-avatar {
            max-width: 130px;
            max-height: 130px;
            margin: -60px auto 0;
        }
        .card-profile .card-avatar img, .card-testimonial .card-avatar img {
            border-radius: 50% !important;
            height:130px;
            width: 130px;
        }
        .home_search{
            width:60%;
            margin-left:20%;
            margin-right:20%;
            line-height: 2rem;
            border-radius: 10px;
            /* border: 1px solid #2385aa; */
            border-bottom: 1px solid #2385aa;
            /* align:center; */
        }
        .home_search span{
            border-radius: 10px;
            font-size: 25px;
            padding-right: 1rem !important;
            cursor:pointer;
        }
        .home_search input{
            line-height: 2rem;
            border-radius: 10px;
        }
        .home_subinfo{
            max-width: 700px;
            margin: 20px auto 75px;
            margin-bottom: 2rem;
            font-size: 1.57em;
            line-height: 1.5em;
            color: #9A9A9A;
            font-weight: 600;
            text-align:center;            
        }
        .home_searchlabel{
            max-width: 700px;
            margin: 20px auto 75px;
            margin-bottom: 1rem;
            font-size: 1.57em;
            line-height: 1.5em;
            color: #2385aa;
            font-weight: 500;
            text-align: center;
        }
        .addicon{
            border-radius: 100px;
            height: 3rem;
        }
        .pull-down-footer{
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
        .pull-down-info{
            position: absolute;
            bottom: 73px;
            left: 0;
            right: 0;
        }
        .pull-down-code{
            position: absolute;
            bottom: 105px;
            left: 0;
            right: 0;
        }
        .student_number{
            font-size: 11px;
            margin-right: 5px;
            margin-top: 3px;
            color: #aa2323;
        }
</style>
<div class="wrapper">
    <div class="page-header page-header-small" filter-color="blue">
        <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg5.jpg');">
        </div>
        <div class="container">
            <div class="content-center">
                <div class="photo-container">
                    @if(Auth::user()->data()->photo)
                    <img src="{{Auth::user()->data()->photo}}" alt="">
                    @else
                    <img src="{{asset('assets/img/default-avatar.png')}}" alt="">
                    @endif
                </div>
                <br>
           
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="button-container">
                <a href="http://localhost:8000/profile?redirectUrl={{ url()->current() }}" target="_blank" class="btn btn-primary btn-round btn-lg"><i class="fa fa-settings"></i>Edit Profile</a>
                <a href="#button" class="btn btn-default btn-round btn-lg btn-icon" data-toggle="modal" data-target="#createClass" rel="tooltip" title="Add Class">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <br>
            <h3 class="title" style="margin-top:0px;">Welcome to Ezu-Class</h3>
                    <h5 class="home_searchlabel">Find your teacher and join their respective class</h5>
                    <div class="input-group home_search">
                        
                        <input type="text" class="form-control" id="search" placeholder="Search by name or enter CLASS CODE here.">
                        <span class="input-group-addon" id="searchIcon">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>                  
                            {{-- <input type="text" class="form-control home_search" id="search" placeholder="Search for your teacher.."/> --}}
               
            </h5>
            <p class="text-center">
                <img id="searchLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 5rem; width:5rem;"/>
            </p>
               <div id="searchResults">
                    
                </div>
            @if(count(Auth::user()->followed_drives)>0)
            
                <h4 class="title text-center">My Joined Class</h4>
               
                
                <div class="row">
                    @foreach(Auth::user()->followed_drives as $d)     
                        <div class="col-md-6 col-lg-4">
                            <div class="card card-profile" style="height:28rem;">
                                <div class="card-body">
                                    <div class="card-avatar">
                                        <a href="#pablo">
                                            @if($d->owner->data()->photo)
                                            <img class="img img-raised" src="{{$d->owner->data()->photo}}">
                                            @else
                                            <img class="img img-raised" src="{{asset('assets/img/default-avatar.png')}}">
                                            @endif

                                        </a>
                                    </div>
                                    <h6 class="category text-gray"><a href="{{ route('publicprofile',$d->owner->data()->slug) }}" style="color:#9A9A9A">{{ $d->owner->data()->name }}</a></h6>
                                    <h4 class="card-title">{{ $d->name }}</h4>
                                    <p class="card-description">
                                        {{ $d->detail }}
                                    </p>
                                    <p class="card-description pukk-down-info">
                                            {{ $d->college }}
                                    </p>
                                    <div class="card-footer pull-down-footer">
                                        <a href="{{ route('class',$d->slug) }}" class="btn btn-primary btn-simple btn-round">Open</a>
                                        <button type="button" class="btn btn-primary btn-simple btn-round" id="btnFollowDrive{{ $d->id }}" drive-id="{{ $d->id }}" drive-name="{{ $d->name }}">Leave</button>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    @endforeach 
                    @foreach(Auth::user()->pending_drives as $d)     
                    <div class="col-md-6 col-lg-4">
                        <div class="card card-profile" style="height:28rem">
                            <div class="card-body">
                                <div class="card-avatar">
                                    <a href="#pablo">
                                        @if($d->owner->data()->photo)
                                        <img class="img img-raised" src="{{$d->owner->data()->photo}}">
                                        @else
                                        <img class="img img-raised" src="{{asset('assets/img/default-avatar.png')}}">
                                        @endif                                        
                                    </a>
                                </div>
                                <h6 class="category text-gray"><a href="{{ route('publicprofile',$d->owner->data()->slug) }}" style="color:#9A9A9A">{{ $d->owner->data()->name }}</a></h6>
                                <h4 class="card-title">{{ $d->name }}</h4>
                                <p class="card-description">
                                        {{ $d->college }}
                                </p>
                                <p class="card-description pull-down-info">
                                    {{ $d->detail }}
                                </p>
                                <div class="card-footer pull-down-footer">
                                    <span style="cursor:text;" class="btn btn-primary btn-simple btn-round">Pending..</span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    @endforeach 
                </div>
            @endif
            <br>
            <h4  class="title text-center" {{$drives->count()>0?'':'hidden'}}>My Class: </h4>
                   <p class="text-center">
                   <button class="btn btn-primary addicon" rel="tooltip" data-placement="top" title="Create Class" data-toggle="modal" data-target="#createClass" {{$drives->count()>0?'':'hidden'}}><i style="font-size:20px" class="fa fa-plus"></i></button>
                   </p>
                   <div class="row" id="myclasses">        
                @forelse($drives as $d)    
            
                <div class="col-md-6 col-lg-3">
                    <div class="card card-profile" style="height:18rem">
                    <span rel="tooltip" data-placement="top" title="Number of Students" class="student_number pull-right">{{$d->followers()->count()>0?$d->followers()->count():''}}</span>
                        <div class="card-body">
                            <h6 class="category text-gray">{{$d->college}}</h6>
                            <h4 class="card-title">{{$d->name}}</h4>
                            <p class="card-description pull-down-code" rel="tooltip" data-placement="top" title="Class Code"  style="font-weight:600;">
                                {{$d->code}}
                            </p>
                            <p class="card-description pull-down-info">
                                {{$d->detail}}
                            </p>
                            <div class="card-footer pull-down-footer">
                                <a href="{{URL::to('/user/mydrive'.'/'.$d->id)}}" class="btn btn-primary btn-simple btn-round">Manage</a>
                            </div>
                        </div>
                    </div>
                </div> 
                @empty
                <div class="col-md-12 col-lg-12">
                    <h3 class="title">Create your first class!!!</h3>
                   
                    <h5 class="home_subinfo">Create class to share files, post and collect assignments and other stuffs.</h5>
                  
                    <p class="text-center">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#createClass">Create</button>
                    </p>
                </div>
                @endforelse 
            </div>
            
        </div>
    </div>
    <div class="modal fade modal-mini modal-primary" id="confirmAlert" tabindex="-1" role="dialog" aria-labelledby="confirmAlertLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div >
                    <div class="modal-header justify-content-center">
                       
                        
                    </div>
                    <div class="modal-body">
                        <p id="confirmText"></p>
                    </div>
                    <div class="modal-footer">
                        <button id="confirmOk" type="button" class="btn btn-link btn-neutral">OK</button>
                        <button type="button" id="confirmNo" class="btn btn-link btn-neutral" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createClass" tabindex="-1" role="dialog" aria-labelledby="createDriveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                </button>
                <h4 class="title">Class Info</h4>
            </div>
            <form action="{{URL::to('/user/createdrive')}}" method="POST">
                
                <div class="modal-body">
                        {{csrf_field()}}
                        <label class="title" style="margin-left:15px; margin-top:0px;">Faculty</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <select class="form-control"  id="AddClassCategory">
                               <option selected disabled>Select Category</option>
                               @foreach($categories as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                               @endforeach
                                
                            </select>
                        </div>
                        <label class="title" style="margin-left:15px; margin-top:0px;">Sub-Faculty</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <select class="form-control" name="faculty_id" id="AddClassFaculty">
                               <option selected disabled>Select Sub-Faculty</option>
                               @foreach($faculties as $f)
                                <option value="{{$f->id}}">{{$f->name}}</option>
                               @endforeach
                                
                            </select>
                        </div>
                        <label class="title" style="margin-left:15px; margin-top:0px;">Subject</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-bookmark-o"></i>
                            </span>
                            <input type="text" name="name" class="form-control" placeholder="eg. Mathematics-I, Sociology">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">College</label>
                            
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-university"></i>
                            </span>
                            <input type="text" name="college" class="form-control" placeholder="College eg. Pulchowk Campus, LACM..">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">Info</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <input type="text" name="detail" class="form-control" placeholder="eg. 1st year, 2nd sem...etc">
                        </div>
                        <label class="title" style="margin-left:15px;  margin-top:0px;">Type</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-info"></i>
                            </span>
                            <select class="form-control" name="type">
                                <option class="form-control" value="semester">Semester</option>
                                <option class="form-control" value="trimester">Trimester</option>
                                <option class="form-control" value="annual">Annual</option>
                                
                            </select>
                        </div>
                    
                </div>
                <div class="modal-footer justify-content-center">
                        <img name="createclassLoading" hidden class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 3rem; width:3rem;"/>
                           
                    <button type="submit" name="createclass" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){

        $("[id*='btnFollowDrive']").click(function(e){

            var ele = $(this);
            
            var text = (ele.text() == 'Requested' ? 'Cancel request to join' : ele.text()) + ' ' + ele.attr('drive-name') + '?';
            $('#confirmText').html(text);
            $('#confirmAlert').modal('show');

            $('#confirmOk').unbind().click(function(){
                $('#confirmAlert').modal('hide');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::to('follow') }}/"+ele.attr('drive-id'),
                    dataType: 'json',
                    beforeSend: function(){
                        ele.attr('disabled','disabled');
                    },
                    success: function(response) {
                        ele.html(response.action);
                    },
                    error: function(){
                        // location.reload();
                    },
                    complete: function(){
                        ele.removeAttr('disabled');
                    }
                });
            });
        });
        $('#searchIcon').click(function(){
            var key=$('#search').val();
            $('#searchLoading').removeAttr('hidden');
            $('#searchResults').attr('hidden','true');
            $.ajax({
                url: '/search',
                type: 'GET',
                data:{'user':key},
                success: function(data){ 
                    $('#searchLoading').attr('hidden','true');
                    $('#searchResults').removeAttr('hidden');
                    $('#searchResults').html(data);
                    $('#viewmoreuser').attr('href','/searchmore/?search='+key);
                },
                error: function(data) {
                    alert('woops!'); //or whatever
                }
            });
        })
        $('#search').keypress(function(e){
            
            if(e.which == 13){//Enter key pressed
                var key=$('#search').val();
                $('#searchResults').attr('hidden','true');
                $('#searchLoading').removeAttr('hidden');
                $.ajax({
                    url: '/search',
                    type: 'GET',
                    data:{'user':key},
                    success: function(data){ 
                        $('#searchLoading').attr('hidden','true');
                        $('#searchResults').removeAttr('hidden');
                        $('#searchResults').html(data);
                        $('#viewmoreuser').attr('href','/searchmore/?search='+key);
                    },
                    error: function(data) {
                        $('#searchLoading').attr('hidden','true');
                        alert('woops!'); //or whatever
                    }
                });
            }
        });

    });
   
</script>
<script>
$(document).ready(function(){
    $('[name="createclass"]').click(function(){
        $(this).attr('hidden','true');
        $('[name="createclassLoading"]').removeAttr('hidden');
    })
    var faculties=@json($faculties);
    console.log(faculties);
    $('#AddClassCategory').change(function(){
        $('#AddClassFaculty').empty();
        var cat_id=$('#AddClassCategory').val();
        for(var i=0 ;i<faculties.length;i++)
        {
            if(faculties[i].category_id==cat_id)
            {
                var a='<option value="'+faculties[i].id+'">'+faculties[i].name+'</option>';
                $('#AddClassFaculty').append(a);   
            }
        }
    })
});
</script>
@endsection