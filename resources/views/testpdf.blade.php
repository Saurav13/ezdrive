
@extends('layouts.app')

@section('body')

<link rel='stylesheet' href='/build/loading-bar.css' type='text/css' media='all' />
<style>
  #loading-bar .bar {
    position: relative;
  }
</style>

<div class="wrapper"  style="margin:100px" ng-app="driveapp" ng-controller="GeneralController">
    <input type="file" ng-model='file' />
    <button ng-click="upload()">Upload</button>
    <br><br><br>
    <div class="myProgress" ng-if="load1">
    </div>
    <br>
    <div class="myProgress" ng-if="load2">
      </div>
</div>





@endsection

@section('js')
<script type='text/javascript' src='/build/loading-bar.js'></script>
<script>
    var driveapps=angular.module('driveapp',['angular-loading-bar'])
    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
      cfpLoadingBarProvider.parentSelector = '.myProgress';
      cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Custom Loading Message...</div>';
    }]);

    driveapps.controller('GeneralController',function($scope,$http,$rootScope){
        $scope.file;
        $scope.load1 = false;
        $scope.load2=false;
        $scope.upload = function(){
            var formData = new FormData();
            formData.append("file",$scope.file);
            // $scope.load2 = true;
            $http.post('uploadUrl', formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                // uploadEventHandlers: {
                //     progress: function(e){
                //         if (e.lengthComputable) {
                //             var progressBar = (e.loaded / e.total) * 100;
                //             elem.style.width = progressBar + '%';
                //         }
                //     }
                // },
                
            }).then(function mySuccess(response) {
                
                alert('ok');
            }, function myError(response) {
               
                alert('wrong');
            });
            
        }
        
    });

   

</script>
@endsection
