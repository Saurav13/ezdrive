<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="./assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Now Ui Kit by Creative Tim</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" />
    <!-- CSS Files -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/assets/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

</head>
<style>
    .recommendations_footer{
        background: #3c3b3b;
        padding: 2rem;
        color: #f4f4f4;
    }

    .recommendations_footer h4{
        font-weight: 300;
        font-size: 20px;
        padding-left: 2rem;
    }
    .products_list_footer{
        list-style-type:none;
        padding-left:1rem;
    }
    
    .products_list_footer li::before{
        content: '➢';
        padding-right:1rem;

    }
    .products_list_footer li{
        margin-bottom: 0.5rem;
    }
    .products_list_footer a{
        font-size: 16px;
        color: #dfdfdf;
    }
    .articles_footer{
        list-style-type:none;
        padding-left:1rem;
    }
    .articles_footer li{
        margin-bottom:0.5rem;
    }
    .articles_footer a{
        color: #dfdfdf;
        font-size: 13px;
    }
    .articles_footer span{
        font-size:11px;
    }
    .articles_footer p{
        margin-bottom: 0px;
        font-size: 13px;
    }
    .articles_footer img{
        margin-top:8px;
        max-height:4rem;
    }
   
</style>
@if(Request::segment(1)=='')
<body class="index-page sidebar-collapse">
@else
<body class="profile-page sidebar-collapse">
@endif