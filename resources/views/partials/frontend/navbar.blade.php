    <!-- Navbar -->
    <style>
        .bg-primary1{
            background-color:#2385aad9;
        }
        .login_button{
            color: white;
            opacity: 0.9;
            background: #5ba0ba;
        }
        .logout{
            background: transparent;
            border: none;
            color: white;
            cursor:pointer;
        }
        .signup_social{
        border-radius: 5px;
        border: none;
        
        color: white;
        }
        .color_google{
            background: #ea4335c2;
        }
        .color_google:hover{
            background: #da1808c2;
        }
        .color_facebook{
            background: #3b5998c2;
        }
        .color_facebook:hover{
            background:#1e4494c2;
        }
        .signup_social i{
            margin-right:1rem;
        }
        .reddot{
            background: red;
            z-index: 100;
            left: -1px;
            top: -3px;
            color:white;
            position: absolute;
            font-size: 10px;
            border: 2px solid red;
            border-radius: 50%;
            padding: 0px 6px;
            font-weight: 600;
        }
        .hovergreen:hover{
            border-color:green !important;
            color:green !important;
        }
        .hovergreen:active{
            border-color:green !important;
            color:green !important;
        }
        .hovergreen:focus{
            border-color:green !important;
            color:green !important;
        }
        .hovergreen:target{
            border-color:green !important;
            color:green !important;
        }
        @media only screen and (min-width : 320px) and (max-width : 480px) { 
        .r-search-bar { display: none !important; }}
    </style>
    @if(Request::segment(1)=='')
    <nav class="navbar navbar-expand-lg bg-primary1 fixed-top navbar-transparent " color-on-scroll="400">
    @else
    <nav class="navbar navbar-expand-lg bg-primary1 fixed-top navbar-transparent " color-on-scroll="100">
    @endif
        <div class="container">
            <div class="navbar-translate" style="display:flex;">
                <a class="navbar-brand" href="/" rel="tooltip" title="Designed by Invision. Coded by Creative Tim" data-placement="bottom" target="_blank">
                    Now Ui Kit
                </a>
               
                <form class="form r-search-bar" method="GET" action="/searchmore">
                    <div class="content">
                        <div class="input-group form-group-no-border input-lg" style="margin-bottom:0px;">
                            <span class="input-group-addon" style="background: transparent;color: white;">
                                <i class="fa fa-search"></i>
                            </span>
                            <input type="text" style="background: transparent; border-radius: 8px; color:white"  class="form-control" name="search" placeholder="Search">
                        </div>
                    </div>
                </form>

                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar top-bar"></span>
                    <span class="navbar-toggler-bar middle-bar"></span>
                    <span class="navbar-toggler-bar bottom-bar"></span>
                </button>
              
                
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="">
                <ul class="navbar-nav">
                    <li class="nav-item">
                            
                    </li>
                    @if(Auth::check())
                    <li class="nav-item">
                        <div class="dropdown">
                                <span class="reddot pull-left" id="reddot" hidden></span> 
                            <a href="#!" class="nav-link dropdown-toggle" id="notification_bell" data-toggle="dropdown">
                               
                                <i class="fa fa-bell"></i>
                                
                            </a>
                            <div class="dropdown-menu">
                                <div id="notificationlist">
                                </div>
                                <a href="javascript:void(0);" class="dropdown-item" id="viewAllNotifications" style="color:#2385aa" data-toggle="modal" data-target="#allNotifications">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; View older notifications</a>
                            </div>
                        </div>
                        
                    </li>
                    @endif
                    
                    @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <i class="fa fa-home"></i>
                            <p>Home</p>
                        </a>
                    </li>
                    @endif
                    @if(Auth::check())
                        @if(Auth::user()->drives()->count()>0)
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" style="cursor:pointer;">
                                    <i class="fa fa-clone"></i>
                                    <p>My Classes</p>
                                </a>
                                <div class="dropdown-menu">
                                    @foreach(Auth::user()->drives()->get()->take(10) as $d)
                                        <a class="dropdown-item" href="{{route('mydrive',$d->slug)}}" target="_blank">{{$d->name}}</a>
                                    @endforeach
                                    <a class="dropdown-item" style="color:#2385aa" href="/home#asTeacher" onclick="location.reload()">View all..</a>
                      
                                </div>
                            </div>
                        </li>
                        @endif
                        @if(Auth::user()->followed_drives()->count()>0)
                        <li class="nav-item">
                        <div class="dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" style="cursor:pointer;">
                                    <i class="far fa-bookmark"></i>
                                    <p>Joined Classes</p>
                                </a>
                                <div class="dropdown-menu">
                                    @foreach(Auth::user()->followed_drives()->get()->take(10) as $d)
                                        <a class="dropdown-item" href="{{route('class',$d->slug)}}" target="_blank">{{$d->name}}</a>
                                    @endforeach
                                    
                                    <a class="dropdown-item" style="color:#2385aa" href="/home#asStudent" onclick="location.reload()">View all..</a>
                                </div>
                            </div>
                        </li>
                        @endif
                        
                  
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link" href="javascript:void(0);"  class="nav-link dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                @if(Auth::user()->data()->name)
                                    <p>{{explode(" ",Auth::user()->data()->name)[0]}}</p>
                                @endif
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu">
                                    
                                {{-- <a class="dropdown-item" data-toggle="modal" data-target="#myProfile" href="#"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;My Profile</a> --}}
                                <a class="dropdown-item" href="{{ env('ACCOUNT_URL') }}editprofile?redirectUrl={{ url()->current() }}" target="_blank"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;My Profile</a>
                                <a class="dropdown-item" href="/explore"><i class="far fa-compass"></i>&nbsp;&nbsp;&nbsp;Explore</a>
                                <form action="/logout" method="POST">
                                    {{csrf_field()}}
                                    <button type="submit" class="dropdown-item" href="javascript:void(0);" >
                                            <i class="fas fa-sign-out-alt"></i>
                                        &nbsp;&nbsp;Logout
                                    </button>
                                </form>
                            </div>
                        </div>
                    </li>
                    
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)" data-toggle="modal" data-target="#login">
                            <p><i class="fas fa-sign-in-alt" style="font-size:15px"></i>
                                &nbsp;&nbsp;Login</p>
                        </a>
                    </li>
                    
                    @endif
                   
                    {{-- <form action="/logout" method="POST">
                    {{csrf_field()}}
                        <input type="submit" value="logout"/>
                    </form> --}}
                    
                </ul>
            </div>
        </div>
    </nav>
    
    @if(!Auth::check())
    <div class="modal fade modal-mini modal-primary" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
        <div class="modal-dialog" style="max-width:380px">
            <div class="modal-content" style="border-radius: 20px;">
                
            
                    <div class="modal-header justify-content-center">
                        <h4 class="title" style="color:#fff">Login</h4>
                        <br>
                        
                    </div>
                    <div class="header text-center">
                        <div class="social-line">
                            <a href="/signin/facebook" class="btn btn-neutral btn-facebook btn-icon btn-round" >
                                <i class="fab fa-facebook-f" style="margin-top: 0.6rem;"></i>
                            </a>
                           
                            <a href="/signin/google" class="btn btn-neutral btn-google btn-icon btn-round">
                                <i class="fab fa-google-plus-g" style="margin-top: 0.6rem;"></i>
                            </a>
                        </div>
                    </div>
                   
                    <form class="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
    
                        <div class="modal-body">
                            <div class="input-group form-group-no-border">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_circle-08"></i>
                                </span>
                                <input type="email" class="form-control" placeholder="Your Email..." name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            <div class="input-group form-group-no-border">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons text_caps-small"></i>
                                </span>
                                <input type="password" placeholder="Password" class="form-control" name="password" required />
                            </div>
                            <a href="{{ route('password.request') }}" class="pull-right" style="color:white;cursor:pointer;font-size:11px;">forgot password?</a>
                            <input type="checkbox" class="pull-left" style="margin-top:4px; margin-right: 4px;"  name="remember" {{ old('remember') ? 'checked' : '' }}/>&nbsp;
                            <span class="pull-left" style="cursor:pointer;font-size:11px;">Remember me</span>
                            <br>
                        </div>
                        
                        <div class="modal-footer justify-content-center">
                               
                            <button type="submit" class="btn btn-neutral" style=" color: white;opacity: 0.9; background: #5ba0ba;">Login</button>
                        </div>
                    </form>
                 
                    
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="modal fade" id="allNotifications" tabindex="-1" role="dialog" aria-labelledby="myProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center" style="border-radius: 4px 4px 0 0; background: none; padding-top: 24px; padding-right: 24px;padding-bottom: 0;padding-left: 24px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                    </button>
                    <h4 class="title">All Notifications</h4>
                </div>
                <p class="text-center">
                    <img id="allNotificationsLoading" class="justify-content-center" src="{{asset('drive/driveloading.gif')}}" style="height: 10rem; width:10rem;"/>
                </p> 
                <div class="modal-body justify-content-center" >
                        <div style="height:20rem; overflow-y:auto; padding:0.5rem 0.5rem;" id="allnotificationsdiv">
                        </div>
                        
                </div>
               
            </div>
        </div>
    </div>
    <div class="modal fade" id="myProfile" tabindex="-1" role="dialog" aria-labelledby="myProfileLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                    </button>
                    <h4 class="title title-up">My Profile</h4>
                </div>
                <div class="modal-body justify-content-center">
                    {{-- <div class="photo-container">
                        <img src="../assets/img/ryan.jpg" alt="">
                    </div> --}}
                    <p class="description" style="font-size:18px;"><b>Name: </b>  Saurav Shrestha</p>
                    <p class="description" style="font-size:18px;"><b>Email:  </b>Saurav Shrestha</p>
                    <p class="description" style="font-size:18px;"><b>Date of Birth: </b> 2051/05/27</p>

                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-default">Edit Profile</button>
                    
                </div>
            </div>
        </div>
    </div>
