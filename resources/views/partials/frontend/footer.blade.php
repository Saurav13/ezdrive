
{{-- <div class="recommendations_footer">
    <div class="row">
        <div class="col-md-3">
            <h4>Our Products</h4>
            <ul class="products_list_footer">
                <li><a href="#!">Ezunotes</a></li>
                <li><a href="#!">Ezucareer</a></li>
                <li><a href="#!">Ezuhub</a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h4>Articles</h4>
            <ul class="articles_footer">
                <li>
                    <div class="row">
                        <div class="col-md-3">
                                <img src="/assets/img/bg3.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#">Education throgth the ezu hub.</a></p>
                            <p><span>18th sept 2018</span></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#">New Article on Rise</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#">Ezu Drive launched from this day.</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#">Ezu Class coming soon...</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-3">
            <h4>Events</h4>
            <ul class="articles_footer">
                <li>
                    <div class="row">
                        <div class="col-md-3">
                                <img src="/assets/img/bg3.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">Education throgth the ezu hub.</a></p>
                            <p><span>18th sept 2018</span></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">New Article on Rise</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">Ezu Drive launched from this day.</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">Ezu Class coming soon...</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-3">
            <h4>Oppertunities</h4>
            <ul class="articles_footer">
                <li>
                    <div class="row">
                        <div class="col-md-3">
                                <img src="/assets/img/bg3.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">Education throgth the ezu hub.</a></p>
                            <p><span>18th sept 2018</span></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">New Article on Rise</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">Ezu Drive launched from this day.</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
                <li>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="/assets/img/bg1.jpg" alt="img"/>
                        </div>
                        <div class="col-md-9">
                            <p><a href="#!">Ezu Class coming soon...</a></p>
                            <p><span>18th sept 2018</span></p>
                         </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div> --}}

<footer class="footer" data-background-color="black">
    <div class="container">
        <nav>
            <ul>
                <li>
                    <a href="https://www.creative-tim.com">
                        Terms and Conditions
                    </a>
                </li>
                <li>
                    <a href="http://presentation.creative-tim.com">
                        Privacy Policy
                    </a>
                </li>
                {{-- <li>
                    <a href="http://blog.creative-tim.com">
                        Teacher's Guideline
                    </a>
                </li>
                <li>
                    <a href="https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md">
                        Student's Guideline
                    </a>
                </li> --}}
                <li>
                    <a href="https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md">
                       FAQ
                    </a>
                </li>
                <li>
                    <a href="https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md">
                        Help
                    </a>
                </li>
                <li>
                    <a href="https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md">
                        Contact Us
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright">
            &copy;
            2018 , Developed by
            <a href="http://www.incubeweb.com" target="_blank" style="color:#2385aa">inCube</a>
        </div>
    </div>
</footer>
</body>
<!--   Core JS Files   -->
<script src="/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

<script src="/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="/assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="/assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
{{-- <script src="/assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script> --}}
<script src="/assets/js/plugins/moment.min.js" type="text/javascript"></script>

<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="/assets/js/now-ui-kit.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
// the body of this function is in assets/js/now-ui-kit.js
nowuiKit.initSliders();
});

function scrollToDownload() {

if ($('.section-download').length != 0) {
    $("html, body").animate({
        scrollTop: $('.section-download').offset().top
    }, 1000);
}
}
</script>
@if(Auth::check())
<script>
    $(document).ready(function(){
        $('#viewAllNotifications').click(function(){
            
            $('#allnotificationsdiv').empty();
            $('#allNotificationsLoading').removeAttr('hidden');
            $.get('/user/allnotifications', function(data) {
                
                $('#allNotificationsLoading').attr('hidden','true');
                var content='';
               
                for(var i=0;i<data.notifications.length;i++)
                {
                    var url = $(location).attr('href').split("/").splice(0, 3).join("/");
                    url+=data.notifications[i].link;
                    
                    content+= '<a class="dropdown-item" href="'+url+'"><i class="'+getIcon(data.notifications[i].i_type)+'">&nbsp;&nbsp;&nbsp;</i>'+data.notifications[i].text+'</a>';
                }
                if(data.notifications.length>0)
                {
                    $('#allnotificationsdiv').html(content);
                }
                else{
                    $('#allnotificationsdiv').html('<a class="dropdown-item" href="#">There are no notifications currently.</a>');
                }

            });
        })
        $('#notification_bell').click(function(){
            $.get('/user/getnotifications?bellClick=1', function(data) {
                $('#notificationlist').empty();
                var content='';
               
                for(var i=0;i<data.notifications.length;i++)
                {
                    var url = $(location).attr('href').split("/").splice(0, 3).join("/");
                    url+=data.notifications[i].link;
                    content+= '<a class="dropdown-item" href="'+url+'"><i class="'+getIcon(data.notifications[i].i_type)+'">&nbsp;&nbsp;&nbsp;</i>'+data.notifications[i].text+'</a>';
                }
                if(data.count>0)
                {
                    $('#reddot').removeAttr('hidden');
                    $('#reddot').html(data.count);
                    
                }
                else{
                    $('#reddot').attr('hidden','true');
                }

                
                if(data.notifications.length>0)
                {
                    $('#notificationlist').html(content);
                }
                else{
                    $('#reddot').attr('hidden','true');
                    $('#notificationlist').html('<a class="dropdown-item"  href="#">There are no notifications currently.</a>');
                }

            });
        });
       getNotifications();
        function getNotifications(){
            $.get('/user/getnotifications', function(data) {
                $('#notificationlist').empty();
                var content='';
                for(var i=0;i<data.notifications.length;i++)
                {
                    var url = $(location).attr('href').split("/").splice(0, 3).join("/");
                    url+=data.notifications[i].link;
                    content+= '<a class="dropdown-item" href="'+url+'"><i class="'+getIcon(data.notifications[i].i_type)+'">&nbsp;&nbsp;&nbsp;</i>'+data.notifications[i].text+'</a>';
                }

                if(data.count>0)
                {
                    $('#reddot').removeAttr('hidden');
                    $('#reddot').html(data.count);
                    
                }
                else{
                    $('#reddot').attr('hidden','true');
                }
                
                if(data.notifications.length>0)
                {
                    $('#notificationlist').html(content);
                }
                else{
                    $('#reddot').attr('hidden','true');
                    $('#notificationlist').html('<a class="dropdown-item" href="#">There are no notifications currently.</a>');
                }

            });
            setTimeout(getNotifications, 5000);
        }
        var getIcon=function(iclass){
            if(iclass=='drive')
                return 'fab fa-hubspot';
            else if(iclass=='notice')
                return 'fa fa-bell';
            else if(iclass=='post')
                return 'fa fa-newspaper-o';
            else if(iclass=='request')
                return 'fa fa-user';
            else if(iclass='task')
                return 'fa fa-edit';
            else if(iclass='reminder')
                return 'fa fa-clock-o';

        }
    })
</script>
@endif
@yield('js')
</html>
