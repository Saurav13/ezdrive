<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColMimetypeToFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collectioncontents', function (Blueprint $table) {
            $table->string('mime_type');
        });
        Schema::table('postedfiles', function (Blueprint $table) {
            $table->string('mime_type');
        });
        Schema::table('collectiontasks', function (Blueprint $table) {
            $table->string('mime_type')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
