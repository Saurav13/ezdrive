<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmittedbyUserIdToCollectiontasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collectioncontents', function (Blueprint $table) {
            $table->integer('submittedBy_user_id')->unsigned()->nullable();
            $table->foreign('submittedBy_user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collectioncontents', function (Blueprint $table) {
            //
        });
    }
}
