<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriveAdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drive_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banner_ads_id')->unsigned();
            $table->foreign('banner_ads_id')->references('id')->on('banner_ads')->onDelete('cascade');
            $table->foreign('id')->references('id')->on('drives')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drive_ads');
    }
}
