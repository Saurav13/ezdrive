<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrivecontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivecontents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('drive_id')->unsigned();
            $table->string('name');
            $table->text('g_path')->nullable();
            $table->string('extension');
            $table->string('size');
            $table->integer('dir_id')->unsigned()->default(0);
            $table->foreign('drive_id')->references('id')->on('drives')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivecontents');
    }
}
