<?php

namespace App\Http\Controllers\Frontend\ClassRoom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notice;
use App\Collection;
use App\Post;
use App\Result;
use App\Board;
use App\Drive;
use Carbon\Carbon;
use Auth;

class GeneralController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if(!Drive::findOrFail($request->drive_id)) abort(404);
            
            if (Auth::user()->followed_drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    
    }
    public function generalInit($drive_id)
    {
        
        $notices=Notice::where('drive_id',$drive_id)->orderBy('id','DESC')->get();
        
        $tasks = Collection::where('drive_id',$drive_id)->orderBy('id','DESC')->with('collectiontask')->get();
        $final = $tasks->map(function ($item) {
            $item['mysubmission'] = $item->collectioncontents()->where('submittedBy_user_id',Auth::user()->id)->first();
            return $item;
        });
        
        $posts=Post::where('drive_id',$drive_id)->orderBy('id','DESC')->with('postedfiles')->get();
        $collections=Collection::where('drive_id',$drive_id)->get();
        $results= collect([]) ;
        foreach ($collections as $c) {
            $rs=Result::where('collection_id',$c->id)->with('collection')->get();
            $results=$results->merge($rs);
        }
        $board = Board::where('drive_id',$drive_id)->where('date',Carbon::today())->first();
        if(!$board)
            $board = Board::where('drive_id',$drive_id)->orderBy('date','DESC')->first();
        $board_dates = Board::where('drive_id',$drive_id)->orderBy('date','DESC')->pluck('date')->toArray();
        return json_encode(['posts'=>$posts,'tasks'=>$final,'notices'=>$notices,'results'=>$results,'board'=>$board, 'board_dates'=>$board_dates]);
    }

    public function change($drive_id,Request $request)
    {
        $request->validate([
            'date' => 'required|date'
        ]);

        $board = Board::where('drive_id',$drive_id)->where('date',$request->date)->first();
        if(!$board)
            $board = new Board;
        return response()->json(['board'=>$board], 200);
    }

    public function viewResult($drive_id,$result_id)
    {
        if(Result::findOrFail($result_id)->collection->drive_id!=$drive_id)
        {
            return "Access Denied";
        }
        $resultrecords=Result::findOrFail($result_id)->resultrecords()->get(); 
        return view('templates.result')->with('resultrecords',$resultrecords);
    }
}
