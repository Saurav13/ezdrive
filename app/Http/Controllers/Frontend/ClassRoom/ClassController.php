<?php

namespace App\Http\Controllers\Frontend\ClassRoom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Drive;
use App\Drive_ad;
use App\BoxAd;
use App\BannerAd;
use App\Usernotification;
use App\Notification;
use App\Account;
use App\Faculty;
use Auth;
use Storage;

class ClassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
       
    }

    public function publicprofile($user_name){
        $user = Account::where('slug',$user_name)->first()->user();
        if(!$user) abort(404);

        
        if($user->id == Auth::user()->id)
            return redirect('home');
            
        return view('frontend.classroom.publicprofile')->with('user',$user);
    }            
   

    public function followdrive(Request $request,$drive_id){
        $action = '';

        if(Auth::user()->drives()->where('drives.id',$drive_id)->exists()){
            return redirect()->back();
        }

        $drive = Drive::findOrFail($drive_id);
        if(Auth::user()->followed_drives()->where('drives.id',$drive_id)->exists()){
            foreach($drive->collections as $c){
                $collectioncontents = $c->collectioncontents()->where('submittedBy_user_id',Auth::user()->id)->get();
                $results = $c->results;
                foreach($collectioncontents as $cc)
                {
                    Storage::disk('google')->delete($cc->g_path);
                }
                $c->collectioncontents()->where('submittedBy_user_id',Auth::user()->id)->delete();

                foreach($results as $r)
                {
                    $r->resultrecords()->where('user_id',Auth::user()->id)->delete();
                }
            }
            
            Auth::user()->followed_drives()->detach($drive_id);
            Auth::user()->save();
            $action = 'Join';
        }
        else if(Auth::user()->pending_drives()->where('drives.id',$drive_id)->exists()){
            Auth::user()->pending_drives()->detach($drive_id);
            Auth::user()->save();
            Notification::pushNotificationClass($drive_id,'cancel_request',Auth::user()->data()->name);
            $action = 'Join';
        }else{
            Auth::user()->followed_drives()->attach($drive_id);
            Auth::user()->save();
            Notification::pushNotificationClass($drive_id,'new_request',Auth::user()->data()->name);
            $action = 'Requested';
        }
        if ($request->ajax())
            return response()->json(['response'=>'success','action'=>$action], 200);
        else{
            return redirect()->route('class',$drive->slug);
        }
    }

    public function class($drive_name){
        $drive = Drive::where('slug',$drive_name)->first();

        if(!$drive) abort(404);

        if(Auth::check() && Auth::user()->drives()->where('drives.id',$drive->id)->exists()){
            return redirect()->route('mydrive', $drive->slug);         
        }
        else if(Auth::user()->followed_drives()->where('drives.id',$drive->id)->exists()){
            $banner = new BannerAd;
            $boxad='';
            $f='';
            if(Drive_ad::where('id',$drive->id)->count()>0)
            {
                $faculty=Faculty::where('id',$drive->faculty_id)->get();
                if($faculty->count()>0)
                {
                    $f=$faculty->first()->name;
                }
            
               
                $banner=Drive_ad::findOrFail($drive->id)->bannerAds;
            }
            $boxads=BoxAd::where(function($query){
                $query->where('target','=','Student')
                    ->orWhere('target','=','Both');
                    
            })->get();
            if($boxads->count()>0)
            {
                $boxad=$boxads->shuffle()->first();
            }

            $rollno = $drive->followers()->where('user_id',Auth::user()->id)->first()->pivot->rollno;
            $duplicate = ($drive->followers()->where('rollno','!=',null)->where('rollno',$rollno)->count() > 1);
            return view('frontend.classroom.followedclass')->with('drive',$drive)->with('rollno',$rollno)->with('duplicate',$duplicate)->with('banner',$banner)->with('boxad',$boxad);            
        }else{
            return view('frontend.classroom.unfollowedclass')->with('drive',$drive);            
        }
    }
    
}
