<?php

namespace App\Http\Controllers\Frontend\ClassRoom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drive;
use App\Drivecontent;
use App\Dir;
use Session;
use Storage;
use Response;
use ZipArchive;
use Auth;
use UrlSigner;
use Carbon\Carbon;
use App\Notification;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Aws\S3\S3Client;
use ZipStream;

class DriveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->followed_drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
        
    }

    public function openRoot($drive_id)
    {
        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',0)->get(); //drives root files
        $folders=Dir::where('drive_id',$drive_id)->where('p_id','0')->get(); //drives root folders
        $breadcrumbs=$this->getBreadCrumbs(0);
        
        return json_encode(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs]);
    }

    public function searchDrive($drive_id,Request $request){
        $request->validate([
            'key'=>'required'
        ]);
        Session::put('p_id'.$drive_id,0);

        $files=DriveContent::where('drive_id',$drive_id)->where('name','LIKE','%'.$request->key.'%')->get();
        $folders=Dir::where('drive_id',$drive_id)->where('name','LIKE','%'.$request->key.'%')->get();

        return json_encode(['files'=>$files,'folders'=>$folders]);
    }

    public function openFolder($drive_id,Request $request)
    {   
        $request->validate([
            'id'=>'exists:dirs,id,drive_id,'.$drive_id
        ]);
        Session::put('p_id'.$drive_id,$request->id);
        
        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$request->id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$request->id)->get();
        $breadcrumbs=$this->getBreadCrumbs($request->id);

        return json_encode(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs]);
    }

    public function getFileLink($drive_id,Request $request){
        $request->validate([
            'id'=>'exists:drivecontents,id,drive_id,'.$drive_id
        ]);
        $dirfile = Drivecontent::findOrFail($request->id);

        $url = UrlSigner::sign(url('/class/'.$drive_id.'/drive/openfile/'.$dirfile->id.'/'.$dirfile->name), Carbon::now()->addMinutes(1));
    
        return response()->json(['url'=>$url], 200);
    }

    public function downloadFile1($drive_id,Request $request){
        $request->validate([
            'id'=>'exists:drivecontents,id,drive_id,'.$drive_id
        ]);

        $dirfile = Drivecontent::findOrFail($request->id);
        try{
            $rawData = Storage::disk('google')->get($dirfile->g_path);
        }
        catch(\Exception $e){
           return $e->getMessage();
        }
            return response($rawData, 200)
                ->header('Content-Type', $dirfile->mime_type)
                ->header('Content-Disposition', "attachment; filename='$dirfile->name'");
    }

    public function zipFolder($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:dirs,id,drive_id,'.$drive_id
        ]);
        $zip = new ZipArchive;

        $dir = Dir::findOrFail($request->id);
        $zipFileName = tempnam(storage_path().'/', "zip");
        $zip->open($zipFileName, ZipArchive::CREATE);

        $root = Drive::where('id',$drive_id)->first()->drive_drive_root;
        $files=$this->getAllFiles($request->id);
 
        if(count($files) > 0){
            foreach($files as $f)
            {
                $rawData = Storage::disk('google')->get($f->g_path);
    
                $realpath=$this->getPathString($f->dir_id,$dir->p_id);
                $zip->addFromString($realpath.$f->name, $rawData);
            }
        }
        else{
            $zip->addEmptyDir($dir->name);
        }
        
        $zip->close();

        
        // $zip = new ZipStream\ZipStream($dir->name.'.zip', [
        //     'content_type' => 'application/octet-stream'
        // ]);
        // $root=Drive::where('id',$drive_id)->first()->drive_drive_root;
        // $files=$this->getAllFiles($request->id);

        // foreach ($files as $f) {
        //     $realpath=$this->getPathString($f->dir_id);
        //     $stream = Storage::disk('google')->getDriver()->readStream($f->g_path);
        //     $zip->addFileFromStream($realpath.$f->name, $stream);
        // }
        
        // $zip->finish();



        $arr = explode('\\', $zipFileName);
        $filename = end($arr);
        
        $url = url('/class/'.$drive_id.'/drive/downloadfolder/'.$dir->id.'/'.$filename);
        
        return response(['url'=>$url], 200)
        ->header('X-FileName', $dir->name.'.zip')
        ->header('X-FileId', $dir->id);  
    }

    public function getPathString($p_id,$dir_p_id)
    {
        $str='';
        while(Dir::where('id',$p_id)->first()->p_id != $dir_p_id)
        {
            $str=Dir::where('id',$p_id)->first()->name.'/'.$str;
            $p_id=Dir::where('id',$p_id)->first()->p_id;
        }

        $str=Dir::where('id',$p_id)->first()->name.'/'.$str;

        return $str;
    }

    private function getAllFiles($p_id)
    {
        $dirs=Dir::where('p_id',$p_id)->get();
        $files= collect(new Drivecontent);
        if($dirs->count()==0)
        {
            $files=$files->merge(Drivecontent::where('dir_id',$p_id)->get());
            return $files;
        }
        else
        {
            foreach($dirs as $d)
            {
               // $files=$files->merge(Drivecontent::where('dir_id',$d->id)->get());
                $files=$files->merge($this->getAllFiles($d->id));
               
            }
        }
        $files=$files->merge(Drivecontent::where('dir_id',$p_id)->get());
        return $files;
    }

    public function back($drive_id,Request $request)
    {
        $p_id=Session::get('p_id'.$drive_id);

        if($p_id!=0)
        {
            $p_id=Dir::where('id',$p_id)->first()->p_id;
            Session::put('p_id'.$drive_id,$p_id);
        }

        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();
        $breadcrumbs=$this->getBreadCrumbs($p_id);

        return json_encode(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs,'isRoot'=>($p_id==0)?true:false]);
    }

    private function getBreadCrumbs($drive_id)
    {  
        $p_id = $drive_id;
        $crumbs = array();
        while($p_id!=0)
        {
            $dir=Dir::findOrFail($p_id);
            array_push($crumbs,$dir);
            $p_id = $dir->p_id;
            
        }
        return array_reverse($crumbs);
    }

    public function downloadFolder($drive_id,$file_id,$file_name,Request $request){
        
        $dir = Dir::where('drive_id',$drive_id)->findOrFail($file_id);
        if($dir->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }
        
         $zip = new ZipStream\ZipStream($dir->name.'.zip', [
             'content_type' => 'application/octet-stream'
         ]);
         $root=Drive::where('id',$drive_id)->first()->drive_drive_root;
         $files=$this->getAllFiles($file_id);

         foreach ($files as $f) {
             $realpath=$this->getPathString($f->dir_id,$dir->p_id);
             $stream = Storage::disk('google')->readStream($f->g_path);
             $zip->addFileFromStream($realpath.$f->name, $stream);
         }
        
         $zip->finish();
    
    }

    public function downloadFile($drive_id,$file_id,$file_name,Request $request){
        
        $dirfile = Drivecontent::where('drive_id',$drive_id)->findOrFail($file_id);
        if($dirfile->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }    
        // try{
        // $rawData = Storage::disk('google')->get($dirfile->g_path);
        // }
        // catch(\Exception $e){
        //     return $e->getMessage();
        //  }
        // return response($rawData, 200)
        //     ->header('Content-Type', $dirfile->mime_type)
        //     ->header('Content-Length',$dirfile->size)
        //     ->header('Content-Disposition', "attachment; filename=$dirfile->name");

        $readStream = Storage::disk('google')->readStream($dirfile->g_path);

        return response()->stream(function () use ($readStream) {
            fpassthru($readStream);
        }, 200, [
            'Content-Type' => $dirfile->mime_type,
            'Content-Length' => $dirfile->size,
            'Content-disposition' => 'attachment; filename="'.$dirfile->name.'"',
        ]);
    
    }

    public function openFile($drive_id,$file_id,$file_name,Request $request){
        
        $dirfile = Drivecontent::where('drive_id',$drive_id)->findOrFail($file_id);
        if($dirfile->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 
        try{
        $rawData = Storage::disk('google')->get($dirfile->g_path);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        return Response::make($rawData, 200, array('content-type'=>$dirfile->mime_type,'Content-Disposition', "attachment; filename='$dirfile->name'"));
        
    }
}
