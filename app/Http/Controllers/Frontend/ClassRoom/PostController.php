<?php

namespace App\Http\Controllers\Frontend\ClassRoom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Postedfile;
use Storage;
use Response;
use Auth;
use UrlSigner;
use Carbon\Carbon;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->followed_drives()->where('confirmed',1)->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function getPosts($drive_id)
    {
        $posts=Post::where('drive_id',$drive_id)->limit(3)->orderBy('id','DESC')->with('postedfiles')->get();
        return json_encode(['posts'=>$posts]);
    }

    public function viewMorePosts($drive_id,Request $request)
    {
        $posts=Post::where('drive_id',$drive_id)->where('id','<',$request->id)->limit(3)->orderBy('id','DESC')->with('postedfiles')->get();
        
        return json_encode(['posts'=>$posts]);
    }

    public function getFileLink($drive_id,Request $request){
        $dirfile = Postedfile::findOrFail($request->id);
        if(Post::findOrFail($dirfile->post_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 

        $url = UrlSigner::sign(url('/class/'.$drive_id.'/post/openfile/'.$dirfile->id.'/'.$dirfile->file), Carbon::now()->addMinutes(1));
    
        return response()->json(['url'=>$url], 200);
    }

    public function openFile($drive_id,$file_id,$file_name,Request $request){
        $file = Postedfile::findOrFail($file_id);
        if(Post::findOrFail($file->post_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 
        try{
            $rawData = Storage::disk('google')->get($file->g_path);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        
        return Response::make($rawData, 200, array('content-type'=>$file->mime_type,'Content-Disposition', "attachment; filename='$file->file'"));
    }

    public function downloadFile($drive_id,$file_id,$file_name,Request $request){

        $file = Postedfile::findOrFail($file_id);
        if(Post::findOrFail($file->post_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 
        try{
            $rawData = Storage::disk('google')->get($file->g_path);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        return response($rawData, 200)
            ->header('Content-Type', $file->mime_type)
            ->header('Content-Length',$file->size)
            ->header('Content-Disposition', "attachment; filename=$file->file");
    
    }
}
