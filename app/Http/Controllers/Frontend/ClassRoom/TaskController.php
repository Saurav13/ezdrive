<?php

namespace App\Http\Controllers\Frontend\ClassRoom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Collection;
use App\Collectioncontent;
use App\Collectiontask;
use Carbon\Carbon;
use Auth;
use Storage;
use Response;
use UrlSigner;
use App\ResponseLayers\DrivePanel\DriveResponse;
use App\Drive;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->followed_drives()->where('confirmed',1)->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function loadTasks($drive_id)
    {
        $tasks = Collection::where('drive_id',$drive_id)->limit(3)->orderBy('id','DESC')->with('collectiontask')->get();
        $final = $tasks->map(function ($item) {
            $item['mysubmission'] = $item->collectioncontents()->where('submittedBy_user_id',Auth::user()->id)->first();
            return $item;
        });
        
        return json_encode(['tasks'=>$final]);
    }

    public function viewMoreTasks($drive_id,Request $request)
    {
        $tasks = Collection::where('drive_id',$drive_id)->where('id','<',$request->id)->limit(3)->orderBy('id','DESC')->with('collectiontask')->get();
        $final = $tasks->map(function ($item) {
            $item['mysubmission'] = $item->collectioncontents()->where('submittedBy_user_id',Auth::user()->id)->first();
            return $item;
        });
        return json_encode(['tasks'=>$final]);
    }

    public function openFile($drive_id,$file_id,$file_name,Request $request){
        $file = Collectiontask::findOrFail($file_id);
        if(Collection::findOrFail($file_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 
        $rawData = Storage::get($file->file);
        
        $arr = explode('/', $file->file);
        $filename = end($arr);
        
        return Response::make($rawData, 200, array('content-type'=>$file->mime_type,'Content-Disposition', "attachment; filename='$filename'"));
    }

    public function getFileLink($drive_id,Request $request){
        $dirfile = Collectiontask::findOrFail($request->id);
        if(Collection::findOrFail($request->id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 

        $arr = explode('/', $dirfile->file);
        $filename = end($arr);

        $url = UrlSigner::sign(url('/class/'.$drive_id.'/task/openfile/'.$dirfile->id.'/'.$filename), Carbon::now()->addMinutes(1));
    
        return response()->json(['url'=>$url], 200);
    }

    public function downloadFile($drive_id,$file_id,$file_name,Request $request){
        
        $file = Collectiontask::findOrFail($file_id);
        if(Collection::findOrFail($file_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 
        $rawData = Storage::get($file->file);
        
        $arr = explode('/', $file->file);
        $filename = end($arr);

        return response($rawData, 200)
            ->header('Content-Type', $file->mime_type)
            ->header('Content-Length',$file->size)
            ->header('Content-Disposition', "attachment; filename=$filename");
    }

    public function sendSubmission($drive_id,Request $request){

        $collection = Collection::findOrFail($request->collection_id);
        if($collection->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }

        if($collection->collectioncontents()->where('submittedBy_user_id',Auth::user()->id)->exists())
            return response()->json(['errors'=>['file'=>[0 => 'You have already submitted the assignment. You may however edit it.']]], 422);

        if($collection->status == 'closed')
            return response()->json(['errors'=>['file'=>[0 => 'The assignment deadline has already been missed.']]], 422);

        $request->validate([
            'file' => 'required|file|max:5120',
            'rollno' => 'required'
        ]);

        $file=$request->file('file');

        $drive = Drive::findOrFail($drive_id);        
        $totalspace=intval(DriveResponse::getTotalSpace($drive->owner->id));
        
        if($totalspace+intval($file->getSize())<intval($drive->max_space))
        {
            $rollno = $drive->followers()->where('user_id',Auth::user()->id)->first()->pivot->rollno;
            if($rollno != $request->rollno)
                Auth::user()->followed_drives()->updateExistingPivot($drive->id, ['rollno' => $request->rollno]);

            $mysubmission=new Collectioncontent;
            $mysubmission->name=$file->getClientOriginalName();
            $mysubmission->extension=$file->getClientOriginalExtension();
            $mysubmission->mime_type = $file->getClientMimeType();
            $mysubmission->size=$file->getSize();
            $mysubmission->collection_id=$request->collection_id;
            $mysubmission->submittedBy_user_id = Auth::user()->id;
            $mysubmission->save();

            $filename = Collection::where('id',$request->collection_id)->first()->g_path.'/'.$mysubmission->id;
            $filepath = $file->getRealPath();


            if(Storage::disk('google')->put($filename, fopen($filepath, 'r+')))
            {
                $dir = Collection::where('id',$request->collection_id)->first()->g_path;

                $files = collect(Storage::disk('google')->listContents($dir,false));
                
                $mysubmission->g_path = $files->where('filename',$mysubmission->id)->first()['basename'];
                $mysubmission->save();

                return json_encode(['mysubmission'=>$mysubmission, 'rollno' => $request->rollno,'duplicate'=>$drive->followers()->where('rollno','!=',null)->where('rollno',$request->rollno)->count() > 1],200);
            }
            else{
                $mysubmission->delete();
                return response()->json('error', 422);
            }
        }
        else{
            return response()->json(['errors'=>['file'=>[0 => 'Your assignment could not be uploaded. Please Contact Your Teacher for more Information.']]], 422);
        }
    }

    public function getSubmissionLink($drive_id,Request $request){
        $dirfile = Collectioncontent::findOrFail($request->id);
        if(Collection::findOrFail($dirfile->collection_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 

        $url = UrlSigner::sign(url('/class/'.$drive_id.'/task/opensubmission/'.$dirfile->id.'/'.$dirfile->name), Carbon::now()->addMinutes(1));
    
        return response()->json(['url'=>$url], 200);
    }

    public function openSubmission($drive_id,$file_id,$file_name,Request $request){
        $assignment = Collectioncontent::findOrFail($file_id);
        if(Collection::findOrFail($assignment->collection_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 
        try{
        $rawData = Storage::disk('google')->get($assignment->g_path);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        return Response::make($rawData, 200, array('content-type'=>$assignment->mime_type,'Content-Disposition', "attachment; filename='$assignment->name'"));
    }

    public function downloadSubmission($drive_id,$file_id,$file_name,Request $request){

        $assignment = Collectioncontent::findOrFail($file_id);
        if(Collection::findOrFail($assignment->collection_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        } 
        try{
            $rawData = Storage::disk('google')->get($assignment->g_path);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        return response($rawData, 200)
            ->header('Content-Type', $assignment->mime_type)
            ->header('Content-Length',$assignment->size)
            ->header('Content-Disposition', "attachment; filename=$assignment->name");
    }

    public function editSubmission($drive_id,Request $request){
        $request->validate([
            'file' => 'file|max:5120',
            'rollno' => 'required'
        ]);
        
        $mysubmission = Collectioncontent::findOrFail($request->submission_id);

        if(Collection::findOrFail($mysubmission->collection_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }
         
        if($mysubmission->collection->status == 'closed')
            return response()->json(['errors'=>['file'=>[0 => 'The assignment deadline has already been missed.']]], 422);
        elseif($mysubmission->grade)
            return response()->json(['errors'=>['file'=>[0 => 'Your assignment has already been graded.']]], 422);

        $drive = Drive::findOrFail($drive_id);

        if ($request->hasFile('file')) {
            $file=$request->file('file');
        
            $totalspace=intval(DriveResponse::getTotalSpace($drive->owner->id));
            
            if($totalspace+intval($file->getSize())<intval($drive->max_space))
            {
                $rollno = $drive->followers()->where('user_id',Auth::user()->id)->first()->pivot->rollno;
                if($rollno != $request->rollno)
                    Auth::user()->followed_drives()->updateExistingPivot($drive->id, ['rollno' => $request->rollno]);

                $mysubmission->name=$file->getClientOriginalName();
                $mysubmission->extension=$file->getClientOriginalExtension();
                $mysubmission->mime_type = $file->getClientMimeType();
                $mysubmission->size=$file->getSize();

                $filename = $mysubmission->collection->g_path.'/'.$mysubmission->id;
                $filepath = $file->getRealPath();
                
                if(Storage::disk('google')->put($filename, fopen($filepath, 'r+')))
                {
                    Storage::disk('google')->delete($mysubmission->g_path);

                    $dir = $mysubmission->collection->g_path;

                    $files = collect(Storage::disk('google')->listContents($dir,false));
                    
                    $mysubmission->g_path = $files->where('filename',$mysubmission->id)->first()['basename'];
                    $mysubmission->save();
                }
                else{
                    return response()->json('error', 200);
                }
            }
            else{
                return response()->json(['errors'=>['file'=>[0 => 'Your assignment could not be uploaded. Please Contact Your Teacher for more Information.']]], 422);
            }
        }else{
            $rollno = $drive->followers()->where('user_id',Auth::user()->id)->first()->pivot->rollno;
            if($rollno != $request->rollno)
                Auth::user()->followed_drives()->updateExistingPivot($drive->id, ['rollno' => $request->rollno]);
        }
        return json_encode(['mysubmission'=>$mysubmission, 'rollno' => $request->rollno,'duplicate'=>$drive->followers()->where('rollno','!=',null)->where('rollno',$request->rollno)->count() > 1],200);
    }
}
