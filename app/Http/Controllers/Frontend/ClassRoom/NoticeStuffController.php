<?php

namespace App\Http\Controllers\Frontend\ClassRoom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notice;
use Auth;

class NoticeStuffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->followed_drives()->where('confirmed',1)->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function loadNotices($drive_id)
    {
        $notices=Notice::where('drive_id',$drive_id)->limit(3)->orderBy('id','DESC')->get();
        return json_encode(['notices'=>$notices]);
    }

    public function viewMoreNotices($drive_id,Request $request)
    {
        $notices = Notice::where('drive_id',$drive_id)->where('id','<',$request->id)->limit(3)->orderBy('id','DESC')->get();
        
        return json_encode(['notices'=>$notices]);
    }
}
