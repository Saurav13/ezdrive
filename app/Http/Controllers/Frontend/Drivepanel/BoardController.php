<?php

namespace App\Http\Controllers\Frontend\Drivepanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ResponseLayers\DrivePanel\DriveResponse;
use App\Rules\NoEmptyContent;
use App\Board;
use Auth;

class BoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
         
            if (Auth::user()->drives()->where('drives.id',$request->drive_id)->count()>0){
                return $next($request);
            }
            else
                abort(403);
        });
    }

    public function change($drive_id,Request $request)
    {
        $request->validate([
            'date' => 'required|date'
        ]);

        $board = Board::where('drive_id',$drive_id)->where('date',$request->date)->first();
        if(!$board){
            $board = new Board;
            $board->date = $request->date;
            $board->content = '';
        }
        return  DriveResponse::response(['board'=>$board],$drive_id);
    }

    public function store($drive_id,Request $request)
    {
        $request->validate([
            'content' => ['required', new NoEmptyContent],
            'date' => 'required|date'
        ]);
        
        $board = Board::where('drive_id',$drive_id)->where('date',$request->date)->first();
        if(!$board){
            $board = new Board;
            $board->date = $request->date;
            $board->drive_id = $drive_id;
        }
        $board->content = $request->content;
        $board->save();

        return DriveResponse::response(['board'=>$board],$drive_id);
    }

    public function destroy($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:boards,id,drive_id,'.$drive_id
        ]);

        Board::where('id',$request->id)->delete();
        return DriveResponse::response(['board'=>new Board],$drive_id);
    }
}
