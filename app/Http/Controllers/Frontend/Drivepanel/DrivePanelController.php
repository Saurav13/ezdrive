<?php

namespace App\Http\Controllers\Frontend\Drivepanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BuisnessLogics\ClassHelper;
use App\Drive;
use App\Post;
use App\Postedfile;
use App\Collection;
use App\Notice;
use App\Category;
use App\Faculty;
use App\Drive_ad;
use App\BoxAd;
use App\Board;
use Auth;
use Storage;
use Redirect;
use Session;
use App\Notification;

class DrivePanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
       
    }
    public function index()
    {
        $drives=Drive::where('user_id',Auth::user()->id)->get();
        $categories=Category::all();
        $faculties=Faculty::all();
        return view('frontend.drivepanel',compact('drives','faculties','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd("ASD ");
        $drive_root=Auth::user()->user_drive_root;
        $collection_root=Auth::user()->user_collection_root;
        $post_root=env('POST_G_ROOT');

        $request->validate([
            'name' => 'required',
            'college' => 'required',
            'detail'=>'required',
            'type'=>'required'
        ]);

        $drive= new Drive;
        $drive->name=$request->name;
        $drive->college=$request->college;
        $drive->detail=$request->detail;
        $drive->type=$request->type;
        $drive->faculty_id=$request->faculty_id;
        $drive->user_id=Auth::user()->id;
        $drive->code=$this->generateRandomString(6);
        $drive->save();
        
        if(Storage::disk('google')->makeDirectory($drive_root.'/'.$drive->id))
        {
            $roots=collect(Storage::disk('google')->listcontents($drive_root,false));
            $drive->drive_drive_root=$roots->where('filename',$drive->id)->first()['path'];
            
        }
        else
        {
            return "Error";           
        }
        if(Storage::disk('google')->makeDirectory($collection_root.'/'.$drive->id))
        {
            $roots=collect(Storage::disk('google')->listcontents($collection_root,false));
            $drive->drive_collection_root=$roots->where('filename',$drive->id)->first()['path'];
            
        }
        else
        {
            return "Error";
        }
        if(Storage::disk('google')->makeDirectory($post_root.'/'.$drive->id))
        {
            $roots=collect(Storage::disk('google')->listcontents($post_root,false));
            $drive->drive_post_root=$roots->where('filename',$drive->id)->first()['path'];
            
        }
        else
        {
            return "Error";
        }
        
        $drive->save();
        return Redirect::to('/home#asTeacher');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    
    public function myDrive($drive_id)
    {
        $drive = Drive::where('slug',$drive_id)->first();
        if(!$drive)
            abort(404);

        if($drive->user_id==Auth::user()->id)
        {
            Session::put('drive_id'.$drive_id,$drive_id);
            Session::put('p_id'.$drive_id,0);
            $drive=Drive::where('id',$drive_id)->select('id','name','detail','college','created_at','type','code','drive_access')->with('owner')->first();
            // dd($drive);
            $banner='default.png';
            $boxad='';
            if(Drive_ad::where('id',$drive_id)->count()>0)
            {
                $banner=Drive_ad::findOrFail($drive_id)->bannerAds->image;
            }
            $f_id=$drive->faculty_id;
            
            $boxads=BoxAd::where(function($query){
                            $query->where('target','=','Teacher')
                                   ->orWhere('target','=','Both'); 
                        })->get();
            if($boxads->count()>0)
            {
                $boxad=$boxads->shuffle()->first();
            }
        //    dd($banner);
            return view('frontend.mydrive',compact('drive','banner','boxad'));    
        }
        else
            return "Access Denied";
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $drive_id
     * @return \Illuminate\Http\Response
     */
    public function show($drive_id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $drive_id
     * @return \Illuminate\Http\Response
     */
    public function edit($drive_id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $drive_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $drive_id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $drive_id
     * @return \Illuminate\Http\Response
     */
    public function deleteDrive($drive_slug)
    {
        $drive = Drive::where('slug',$drive_slug)->first();
        if(!$drive)
            abort(404);

        if($drive->user_id == Auth::user()->id)
        {
            ClassHelper::resetDrive($drive);
            
            $drive->delete();
            return redirect()->to('/home/#asTeacher');
        }
        else
            return "Access Denied";
    }

    public function editDrive($drive_id,Request $request)
    {
        $request->validate([
            'name' => 'required',
            'college' => 'required',
            'type'=>'required',
            'detail'=>'required'
        ]);
        if(Drive::findOrFail($drive_id)->user_id==Auth::user()->id)
        {
            $name=Drive::findOrFail($drive_id)->name;
            Drive::where('id',$drive_id)->update(['name'=>$request->name,'college'=>$request->college,'detail'=>$request->detail,'type'=>$request->type]);
            if($name!=$request->name)
            {
                Notification::pushNotificationClass($drive_id,'class_rename',$name);
            }
            return Drive::where('id',$drive_id)->first();
        }
        else{
            return response()->json(['message'=>'Unauthorized'],401);
        }
    }
    public  function generateRandomString($length = 6) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function resetDrive($drive_id)
    {
        $user_id=Auth::user()->id;
        $drive=Drive::where('user_id',$user_id)->where('id',$drive_id)->first();
        if($drive!=null)
        {
            $this->deletePosts($drive_id);
            $this->deleteCollections($drive_id);
            $this->deleteNotices($drive_id);
            $this->deleteStudents($drive_id);
            $this->deleteBoards($drive_id);
        }
        return Redirect::to('/user/mydrive'.'/'.$drive->slug);
    }
    private function deletePosts($id)
    {
        $posts=Post::where('drive_id',$id)->get();
        foreach ($posts as $p) {
            $this->deletePostedfiles($p->id);
        }
        Post::where('drive_id',$id)->delete();
    }

    private function deleteCollections($id)
    {
        $collections=Collection::where('drive_id',$id)->get();
        foreach($collections as $c)
        {
            Storage::disk('google')->deleteDirectory($c->g_path);
        }
        Collection::where('drive_id',$id)->delete();
        
    }

    private function deleteNotices($id)
    {
        Notice::where('drive_id',$id)->delete();
    }

    private function deletePostedfiles($id)
    {
        $postedfiles=Postedfile::where('post_id',$id)->get();
        foreach ($postedfiles as $p) {
            Storage::disk('google')->delete($p->g_path);
            
        }
    }
    private function deleteStudents($id)
    {
        Drive::findOrFail($id)->followers()->detach();
    }
    private function deleteBoards($id)
    {
        Board::where('drive_id',$id)->delete();
    }
}
