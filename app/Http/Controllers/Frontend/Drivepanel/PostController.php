<?php

namespace App\Http\Controllers\Frontend\Drivepanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Drive;
use App\Post;
use App\Postedfile;
use Storage;
use App\Notice;
use App\ResponseLayers\DrivePanel\DriveResponse;
use Auth;
use Mail;
use App\Mail\NotificationMail;
use App\Notification;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
         
            if (Auth::user()->drives()->where('drives.id',$request->drive_id)->count()>0){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    public function getPosts($drive_id)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
       
        $posts=Post::where('drive_id',$drive_id)->limit(3)->orderBy('id','DESC')->with('postedfiles')->get();
        return DriveResponse::response(['posts'=>$posts],$drive_id);

    }
    public function post($drive_id,Request $request)
    {
        // dd($this->makelinks($request->content));
        $request->validate([
            'content'=>'required',
        ]);
        $drive_id=Session::get('drive_id'.$drive_id);
        $totalspace=intval(DriveResponse::getTotalSpace(Auth::user()->id));
        $filesizes=0;
        if(count($request->files->all()))
        {
            foreach ($request->files->all()['files'] as $f) {
                if($f->getSize()>5000000)
                {
                    return response()->json(['message'=>'too large file. max size: 5 MB per each file'],489);
                }
                $filesizes+=$f->getSize();
            }
        }
        if($totalspace+intval($filesizes)<intval(Drive::findOrFail($drive_id)->max_space))
        {
            $post=new Post;
            $post->drive_id=$drive_id;

            $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
            $string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $request->content);
            
            $post->content=$string;
            $post->save();
            if(count($request->files->all()))
            {
                foreach ($request->files->all()['files'] as $f) {
                    $postedfile=new Postedfile;
                    $postedfile->post_id=$post->id;
                    $file=$f;
                    $filenameWithExt=$file->getClientOriginalName();
                    
                   

                    $postedfile->file=$filenameWithExt;
                    $postedfile->size=$file->getClientSize();
                    $postedfile->mime_type = $file->getClientMimeType();
                    $postedfile->save();

                    $filename=Drive::where('id',$drive_id)->first()->drive_post_root.'/'.$postedfile->id;
                    $filepath=$file->getRealPath();
                    
                    try{
                        if(Storage::disk('google')->put($filename, fopen($filepath, 'r+')))
                        {
                            $dir=Drive::where('id',$drive_id)->first()->drive_post_root;
        
                            $files=collect(Storage::disk('google')->listContents($dir,false));
                            
                            // dd($files->where('filename',$drivecontent->id)->first()['basename']);
                            $postedfile->g_path=$files->where('filename',$postedfile->id)->first()['basename'];
                            $postedfile->save();
                        }
                        else{
                            Postedfile::where('id',$postedfile->id)->delete();
                        }
                 
                    }
                    catch(\Exception $e){
                        Postedfile::where('id',$postedfile->id)->delete();
                        return $e->getMessage();
                    }
                   
                    $postedfile->save();
                }
            
            }

            // $drive = Drive::find($drive_id);
            // foreach($drive->followers as $f){
            //     $content = "You have a new post from ". $drive->name." Class.";
            //     Mail::to($f->email)->queue(new NotificationMail($f,'New Post Notification',$content));
            // }
        
            $posts=Post::where('drive_id',$drive_id)->orderBy('id','DESC')->limit(3)->with('postedfiles')->get();
            Notification::pushNotificationClass($drive_id,'new_post','');
            return DriveResponse::response(['posts'=>$posts],$drive_id);
        }
        else{
            return response()->json(['status'=>'not enough space'],422);
        }
    }
    public function viewMorePosts($drive_id,Request $request)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
       
        $posts=Post::where('drive_id',$drive_id)->where('id','<',$request->id)->orderBy('id','DESC')->limit(3)->with('postedfiles')->get();
        
        return DriveResponse::response(['posts'=>$posts],$drive_id);
    }
    public function updatePost($drive_id,Request $request)
    {
        $request->validate([
            'content'=>'required',
            'id'=>'exists:posts,id,drive_id,'.$drive_id
        ]);

        $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
        $string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $request->content);

        Post::where('id',$request->id)->update(['content'=>$string]);
        return json_encode(Post::where('id',$request->id)->first());
    }
    public function deletePost($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:posts,id,drive_id,'.$drive_id
        ]);
        // dd($request->id);
        $drive_id=Session::get('drive_id'.$drive_id);

        $postedfiles=Postedfile::where('post_id',$request->id)->get();
        foreach ($postedfiles as $p) {
            Storage::disk('google')->delete($p->g_path);
        }
        Post::where('id',$request->id)->delete();
        $posts=Post::where('drive_id',$drive_id)->limit(3)->orderBy('id','DESC')->with('postedfiles')->get();
        return DriveResponse::response(['posts'=>$posts],$drive_id);
    }
    public function postNotice($drive_id,Request $request)
    {
        $request->validate([
            'title'=>'required',
            'description'=>'required'
        ]);
        $drive_id=Session::get('drive_id'.$drive_id);
        $notice=new Notice;
        $notice->title=$request->title;
        $notice->description=$request->description;
        $notice->drive_id=$drive_id;
        $notice->save();

        $drive = Drive::find($drive_id);
        Notification::pushNotificationClass($drive_id,'new_notice','');
       
        
        foreach($drive->followers as $f){
            $content = "Has posted a new notice titled '". $notice->title."'.";
            Mail::to($f->email)->queue(new NotificationMail($f,$drive,'notice',$notice->updated_at,'New Notice Notification',$content));
        }

        return Driveresponse::response(['notice'=>$notice],$drive_id);
    }
    public function saveNotice($drive_id,Request $request)
    {
        $request->validate([
            'title'=>'required',
            'description'=>'required',
            'id'=>'exists:notices,id,drive_id,'.$drive_id
        ]);
        $drive_id=Session::get('drive_id'.$drive_id);
        $notice=Notice::findOrFail($request->id);
        $notice->title=$request->title;
        $notice->description=$request->description;
        $notice->drive_id=$drive_id;
        $notice->save();
        return Driveresponse::response(['notice'=>$notice],$drive_id);
    }
    public function loadNotices($drive_id,Request $request)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
        if($request->id==0)
            $notices=Notice::where('drive_id',$drive_id)->orderBy('id','DESC')->limit(3)->get();
        else
            $notices=Notice::where('drive_id',$drive_id)->where('id','<',$request->id)->orderBy('id','DESC')->limit(3)->get();
        return DriveResponse::response(['notices'=>$notices],$drive_id);
    }
    private function makeLinks($str) {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        $urls = array();
        $urlsToReplace = array();
        if(preg_match_all($reg_exUrl, $str, $urls)) {
            $numOfMatches = count($urls[0]);
            $numOfUrlsToReplace = 0;
            for($i=0; $i<$numOfMatches; $i++) {
                $alreadyAdded = false;
                $numOfUrlsToReplace = count($urlsToReplace);
                for($j=0; $j<$numOfUrlsToReplace; $j++) {
                    if($urlsToReplace[$j] == $urls[0][$i]) {
                        $alreadyAdded = true;
                    }
                }
                if(!$alreadyAdded) {
                    array_push($urlsToReplace, $urls[0][$i]);
                }
            }
            $numOfUrlsToReplace = count($urlsToReplace);
            for($i=0; $i<$numOfUrlsToReplace; $i++) {
                $str = str_replace($urlsToReplace[$i], "<a href=\"".$urlsToReplace[$i]."\">".$urlsToReplace[$i]."</a> ", $str);
            }
            return $str;
        } else {
            return $str;
        }
    }
}
