<?php

namespace App\Http\Controllers\Frontend\Drivepanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Session;
use App\Drive;
use App\Drivecontent;
use App\Dir;
use ZipArchive;
use App\ResponseLayers\DrivePanel\DriveResponse;
use Auth;
use UrlSigner;
use Carbon\Carbon;
use App\Notification;
use App\Account;
use ZipStream;
class DriveController extends Controller
{
    public function __construct()
    {
        // drive auth baki
        
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    public function openRoot($drive_id)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
        $contents=Drivecontent::where('id',$drive_id)->where('dir_id',0)->get();
        $dir=Drive::where('id',$drive_id)->first()->drive_drive_root;
        // $recursive = false; // Get subdirectories also?
        // $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',0)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id','0')->get();
        $breadcrumbs=$this->getBreadCrumbs(0);

        return DriveResponse::response(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs],$drive_id);
        
    }
    public function openFolder($drive_id,Request $request)
    {   
        Session::put('p_id'.$drive_id,$request->id);
        $drive_id=Session::get('drive_id'.$drive_id);
        
        $files=Drivecontent::where('dir_id',$request->id)->get();
        $folders=Dir::where('p_id',$request->id)->get();
        $breadcrumbs=$this->getBreadCrumbs($request->id);
        
        // dd($breadcrumbs);
        return DriveResponse::response(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs],$drive_id);
    }
    public function addFolder($drive_id,Request $request)
    {
        
        $request->validate([
            'name' => 'required'
        ]);
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);

        $newdir= new Dir;
        $newdir->name=$request->name;
        $newdir->drive_id=$drive_id;
        $newdir->p_id=$p_id;
        $newdir->save();

        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();
        
        return DriveResponse::response(['files'=>$files,'folders'=>$folders],$drive_id);

    }

    public function back($drive_id,Request $request)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);

        if($p_id!=0)
        {
            $p_id=Dir::where('id',$p_id)->first()->p_id;
            Session::put('p_id'.$drive_id,$p_id);
        }

        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();
        $breadcrumbs=$this->getBreadCrumbs($p_id);
        
        return DriveResponse::response(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs],$drive_id);
    }
    
    public function uploadFile($drive_id,Request $request)
    {
        $request->validate([
            'file' => 'required|max:16000' ,
            
        ]);
        // sleep(100);
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);
        $file=$request->file('file');
            
        $totalspace=intval(DriveResponse::getTotalSpace(Auth::user()->id));
        
        if($totalspace+intval($file->getSize())<intval(Drive::findOrFail($drive_id)->max_space))
        {
            
            // dd($file->getClientMimeType());
            $drivecontent=new Drivecontent;
            $drivecontent->name=$file->getClientOriginalName();
            $drivecontent->extension=$file->getClientOriginalExtension();
            $drivecontent->mime_type = $file->getClientMimeType();
            $drivecontent->size=$file->getSize();
            $drivecontent->drive_id=$drive_id;
            $drivecontent->dir_id=$p_id;
            
            $drivecontent->save();

            $filename=Drive::where('id',$drive_id)->first()->drive_drive_root.'/'.$drivecontent->id;
            $filepath=$file->getRealPath();

            try{
                if(Storage::disk('google')->put($filename, fopen($filepath, 'r+')))
                {
                    $dir=Drive::where('id',$drive_id)->first()->drive_drive_root;

                    $files=collect(Storage::disk('google')->listContents($dir,false));
                    
                    // dd($files->where('filename',$drivecontent->id)->first()['basename']);
                    $drivecontent->g_path=$files->where('filename',$drivecontent->id)->first()['basename'];
                    $drivecontent->save();
                }
                else{
                    Drivecontent::where('id',$drivecontent->id)->delete();
                }
         
            }
            catch(\Exception $e){
                Drivecontent::where('id',$drivecontent->id)->delete();
                return $e->getMessage();
            }
            $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
            $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();
           
            Notification::pushNotificationClass($drive_id,'drive_uploads','');

            return DriveResponse::response(['files'=>$files,'folders'=>$folders],$drive_id);
        }
        else
        {
            return response()->json(['status'=>'not enough space'],422);
        }
    }
    
    public function renameFolder($drive_id,Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);

        Dir::where('id',$request->id)->update(['name'=>$request->name]);

        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();
        
        return DriveResponse::response(['files'=>$files,'folders'=>$folders],$drive_id);

    }

    public function deleteFolder($drive_id,Request $request)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);

        try{
            $this->recursiveDelete($request->id);
        }catch (\Exception $e) {
            return $e->getMessage();
        }

        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();

        return DriveResponse::response(['files'=>$files,'folders'=>$folders],$drive_id);
    }

    public function deleteFile($drive_id,Request $request)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);

        $path=Drivecontent::where('drive_id',$drive_id)->findOrFail($request->id)->g_path;
        try{
            if(Drivecontent::where('g_path',$path)->get()->count()<2)
                Storage::disk('google')->delete($path);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        Drivecontent::where('id',$request->id)->delete();


        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();

        return DriveResponse::response(['files'=>$files,'folders'=>$folders],$drive_id);
    }

    public function downloadFolder($drive_id,$file_id,$file_name,Request $request){
        
        $dir = Dir::where('drive_id',$drive_id)->findOrFail($file_id);
        if($dir->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }
        
         $zip = new ZipStream\ZipStream($dir->name.'.zip', [
             'content_type' => 'application/octet-stream'
         ]);
         $root=Drive::where('id',$drive_id)->first()->drive_drive_root;
         $files=$this->getAllFiles($file_id);

         foreach ($files as $f) {
             $realpath=$this->getPathString($f->dir_id,$dir->p_id);
             $stream = Storage::disk('google')->readStream($f->g_path);
             $zip->addFileFromStream($realpath.$f->name, $stream);
         }
        
         $zip->finish();
    
    }
    public function downloadFile($drive_id,$file_id,$file_name,Request $request){
        
        $dirfile = Drivecontent::where('drive_id',$drive_id)->findOrFail($file_id);
        if($dirfile->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }    
        // try{
        // $rawData = Storage::disk('google')->get($dirfile->g_path);
        // }
        // catch(\Exception $e){
        //     return $e->getMessage();
        //  }
        // return response($rawData, 200)
        //     ->header('Content-Type', $dirfile->mime_type)
        //     ->header('Content-Length',$dirfile->size)
        //     ->header('Content-Disposition', "attachment; filename=$dirfile->name");

        $readStream = Storage::disk('google')->readStream($dirfile->g_path);

        return response()->stream(function () use ($readStream) {
            fpassthru($readStream);
        }, 200, [
            'Content-Type' => $dirfile->mime_type,
            'Content-Length' => $dirfile->size,
            'Content-disposition' => 'attachment; filename="'.$dirfile->name.'"',
        ]);
    
    }
    public function openFile($drive_id,Request $request){
        $dirfile = Drivecontent::where('drive_id',$drive_id)->findorFail($request->id);
        try{
            $rawData = Storage::disk('google')->get($dirfile->g_path);
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        return response($rawData, 200)
        ->header('Content-Type', $dirfile->mime_type)
        ->header('X-FileName', $dirfile->name)
        ->header('X-FileId', $dirfile->id)
        ->header('Content-Disposition', "attachment; filename='$dirfile->name'");
    }
    
    public function getFileLink($drive_id,Request $request){
        $dirfile = Drivecontent::where('drive_id',$drive_id)->findorFail($request->id);

        $url = UrlSigner::sign(url('/class/'.$drive_id.'/drive/openfile/'.$dirfile->id.'/'.$dirfile->name), Carbon::now()->addMinutes(1));
    
        return response()->json(['url'=>$url], 200);
    }

    public function searchDrive($drive_id, Request $request)
    {
        $request->validate([
            'key'=>'required'
        ]);
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);

        $files=DriveContent::where('drive_id',$drive_id)->where('name','LIKE','%'.$request->key.'%')->get();
        $folders=Dir::where('drive_id',$drive_id)->where('name','LIKE','%'.$request->key.'%')->get();

        return DriveResponse::response(['files'=>$files,'folders'=>$folders],$drive_id);
    }

    public function importInit($drive_id)
    {
        $user_id=Auth::user()->id;
        $drives=Drive::where('user_id',$user_id)->get();
        $dirStructure=array();
        $dirFiles=array();
        foreach ($drives as $d) {
            $dirs=Dir::where('drive_id',$d->id)->get();
            $files=Drivecontent::where('drive_id',$d->id)->get();
           $dirStructure= array_merge($dirStructure,$dirs->toArray());
           $dirFiles=array_merge($dirFiles,$files->toArray());
        }
        
        return DriveResponse::response(['dirStructure'=>$dirStructure,'dirFiles'=>$dirFiles,'drives'=>$drives],$drive_id);
        
    }

    public function importSelectedFiles($drive_id,Request $request)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
        $p_id=Session::get('p_id'.$drive_id);

        $fileIds=$request->selectedFileIds;
        foreach ($fileIds as $fid) {
            $oldFile=Drivecontent::findOrFail($fid);
            $newFile=new Drivecontent;

            $newFile->drive_id=$drive_id;
            $newFile->name=$oldFile->name;
            $newFile->g_path=$oldFile->g_path;
            $newFile->extension=$oldFile->extension;
            $newFile->size=$oldFile->size;
            $newFile->mime_type=$oldFile->mime_type;
            $newFile->dir_id=$p_id;

            $newFile->save();
        }
        Notification::pushNotificationClass($drive_id,'drive_uploads','');
        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();
        
        return DriveResponse::response(['files'=>$files,'folders'=>$folders],$drive_id);

    }
    private function getPathString($p_id)
    {
        $str='';
        while(Dir::where('id',$p_id)->first()->p_id!=0)
        {
            $str=Dir::where('id',$p_id)->first()->name.'/'.$str;
            $p_id=Dir::where('id',$p_id)->first()->p_id;
        }

        $str=Dir::where('id',$p_id)->first()->name.'/'.$str;

        return $str;
    }

    private function searchFiles($key)
    {

    }


    private function getAllFiles($p_id)
    {

        $dirs=Dir::where('p_id',$p_id)->get();
        $files= collect(new Drivecontent);
        if($dirs->count()==0)
        {
            $files=$files->merge(Drivecontent::where('dir_id',$p_id)->get());
            return $files;
        }
        else
        {
            foreach($dirs as $d)
            {
               // $files=$files->merge(Drivecontent::where('dir_id',$d->id)->get());
                $files=$files->merge($this->getAllFiles($d->id));
               
            }
        }
        $files=$files->merge(Drivecontent::where('dir_id',$p_id)->get());
        return $files;
    }

    private function recursiveDelete($p_id)
    {
        $dirs=Dir::where('p_id',$p_id)->get();
        if(sizeof($dirs)==0)
        {
            Dir::where('id',$p_id)->delete();
            $files=Drivecontent::where('dir_id',$p_id)->get();
            foreach ($files as $f) {
                Storage::disk('google')->delete($f->g_path);
            }
            Drivecontent::where('dir_id',$p_id)->delete();
        }
        else
        {
            foreach ($dirs as $d) {
                $this->recursiveDelete($d->id);
            }
            $files=Drivecontent::where('dir_id',$p_id)->get();
            foreach ($files as $f) {
                Storage::disk('google')->delete($f->g_path);
            }
            Drivecontent::where('dir_id',$p_id)->delete();
            Dir::where('id',$p_id)->delete();
        }

    }
    private function getBreadCrumbs($id)
    {  
        $p_id=$id;
        $crumbs=array();
        while($p_id!=0)
        {
            $dir=Dir::findOrFail($p_id);
            array_push($crumbs,$dir);
            $p_id=$dir->p_id;
            
        }
        return array_reverse($crumbs);
    }
   

}
