<?php

namespace App\Http\Controllers\Frontend\Drivepanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drive;
use App\Category;
use App\Faculty;
use App\Drive_ad;
use App\BannerAd;
use App\BoxAd;
use Auth;
use Session;

class DriveSlugController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function myDrive($drive_id)
    {
        $drive = Drive::where('slug',$drive_id)->first();

        if(!$drive)
            abort(404);

        if($drive->user_id == Auth::user()->id)
        {
            Session::put('drive_id'.$drive->id,$drive->id);
            Session::put('p_id'.$drive->id,0);
            $drive=Drive::where('slug',$drive_id)->select('id','name','detail','college','created_at','type','code','drive_access','slug')->with('owner')->first();
            $banner='default.png';
            $boxad='';
            if(Drive_ad::where('id',$drive_id)->count()>0)
            {
                $banner=Drive_ad::findOrFail($drive_id)->bannerAds->image;
            }
            $f_id=$drive->faculty_id;
            
            $boxads=BoxAd::where(function($query){
                            $query->where('target','=','Teacher')
                                   ->orWhere('target','=','Both'); 
                        })->get();
            if($boxads->count()>0)
            {
                $boxad=$boxads->shuffle()->first();
            }
            return view('frontend.mydrive',compact('drive','banner','boxad'));    
        }
        else
            return "Access Denied";
    }
    public function studentsView($drive_name)
    {
        $drive = Drive::where('slug',$drive_name)->first();
        // dd($drive);
        if(!$drive) abort(404);

        if(Auth::check() && Auth::user()->drives()->where('drives.id',$drive->id)->exists()){
            $banner = new BannerAd;
            $boxad='';
            $f='';
            if(Drive_ad::where('id',$drive->id)->count()>0)
            {
                $faculty=Faculty::where('id',$drive->faculty_id)->get();
                if($faculty->count()>0)
                {
                    $f=$faculty->first()->name;
                }
            
               
                $banner=Drive_ad::findOrFail($drive->id)->bannerAds;
            }
            $boxads=BoxAd::where(function($query){
                $query->where('target','=','Student')
                    ->orWhere('target','=','Both');
                    
            })->get();
            if($boxads->count()>0)
            {
                $boxad=$boxads->shuffle()->first();
            }

            return view('frontend.studentsview')->with('drive',$drive)->with('banner',$banner)->with('boxad',$boxad);

        }
        else{
            return view('frontend.classroom.unfollowedclass')->with('drive',$drive);            
        }
    }
}