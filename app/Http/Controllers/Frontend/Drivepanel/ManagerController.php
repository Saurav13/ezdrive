<?php

namespace App\Http\Controllers\Frontend\Drivepanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drive;
use App\ResponseLayers\DrivePanel\DriveResponse;
use DB;
use Auth;
use Mail;
use App\Mail\InvitationMail;
use App\User;
use App\Usernotification;
use App\Account;
use UrlSigner;
use Carbon\Carbon;
use App\Result;
use App\Collection;
use App\Resultrecord;
use App\Collectioncontent;
use Storage;

class ManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    public function init($drive_id)
    {
        $newRequests=Drive::findOrFail($drive_id)->unapprovedfollowers()->get();
        $fewStudents=Drive::findOrFail($drive_id)->followers()->orderBy('id','desc')->take(6)->get();
        $newRequests->map(function($r){
            $r['data']=$r->data();
        });
        $fewStudents->map(function($r){
            $r['data']=$r->data();
        });
        return Driveresponse::response(['newRequests'=>$newRequests,'fewStudents'=>$fewStudents],$drive_id);
    }
    public function accept($drive_id,Request $request)
    {
        Drive::findOrFail($drive_id)->unapprovedfollowers()->where('user_id',$request->user_id)->update(['confirmed'=>1]);
        
        $newRequests=Drive::findOrFail($drive_id)->unapprovedfollowers()->get();
        $fewStudents=Drive::findOrFail($drive_id)->followers()->orderBy('id','desc')->take(6)->get();
        $newRequests->map(function($r){
            $r['data']=$r->data();
        });
        $fewStudents->map(function($r){
            $r['data']=$r->data();
        });
        Usernotification::pushNotification($request->user_id,$drive_id,'accepted');

        return Driveresponse::response(['newRequests'=>$newRequests,'fewStudents'=>$fewStudents],$drive_id);
    }
    public function ignore($drive_id,Request $request)
    {
        // Drive::findOrFail($drive_id)->unapprovedfollowers()->where('user_id',$request->user_id)->delete();
        DB::table('drive_user')->where('drive_id',$drive_id)->where('user_id',$request->user_id)->delete();
        $newRequests=Drive::findOrFail($drive_id)->unapprovedfollowers()->get();
        $fewStudents=Drive::findOrFail($drive_id)->followers()->orderBy('id','desc')->take(6)->get();
        $newRequests->map(function($r){
            $r['data']=$r->data();
        });
        $fewStudents->map(function($r){
            $r['data']=$r->data();
        });
        return Driveresponse::response(['newRequests'=>$newRequests,'fewStudents'=>$fewStudents],$drive_id);
    }
    public function removeStudent($drive_id,Request $request)
    {
        // Drive::findOrFail($drive_id)->unapprovedfollowers()->where('user_id',$request->user_id)->delete();
        if(DB::table('drive_user')->where('drive_id',$drive_id)->where('user_id',$request->user_id)->exists())
        {
            $drive = Drive::find($drive_id);
            foreach($drive->collections as $c){
                $collectioncontents = $c->collectioncontents()->where('submittedBy_user_id',$request->user_id)->get();
                $results = $c->results;
                foreach($collectioncontents as $cc)
                {
                    Storage::disk('google')->delete($cc->g_path);
                }
                $c->collectioncontents()->where('submittedBy_user_id',$request->user_id)->delete();

                foreach($results as $r)
                {
                    $r->resultrecords()->where('user_id',$request->user_id)->delete();
                }
            }
           
            DB::table('drive_user')->where('drive_id',$drive_id)->where('user_id',$request->user_id)->delete();
            
        }

        $newRequests=Drive::findOrFail($drive_id)->unapprovedfollowers()->get();
        $fewStudents=Drive::findOrFail($drive_id)->followers()->orderBy('id','desc')->take(6)->get();
        $newRequests->map(function($r){
            $r['data']=$r->data();
        });
        $fewStudents->map(function($r){
            $r['data']=$r->data();
        });
        Usernotification::pushNotification($request->user_id,$drive_id,'removed');

        return Driveresponse::response(['newRequests'=>$newRequests,'fewStudents'=>$fewStudents],$drive_id);
    }
    public function allStudents($drive_id,Request $request)
    {
        $allStudents=Drive::findOrFail($drive_id)->followers()->get();
        $newRequests=Drive::findOrFail($drive_id)->unapprovedfollowers()->get();
        $fewStudents=Drive::findOrFail($drive_id)->followers()->orderBy('id','desc')->take(6)->get();
        $newRequests->map(function($r){
            $r['data']=$r->data();
        });
        $fewStudents->map(function($r){
            $r['data']=$r->data();
        });
        $allStudents->map(function($r){
            $r['data']=$r->data();
        });
        // foreach ($newRequests as $n) {
        //     $n->put('data',$n->data);
        // }

        return Driveresponse::response(['newRequests'=>$newRequests,'fewStudents'=>$fewStudents,'allStudents'=>$allStudents],$drive_id);
    }
    
    public function toggleDriveAccess($drive_id)
    {
        $drive=Drive::findOrFail($drive_id);
        if($drive->drive_access=='PUBLIC')
        {
            $drive->drive_access='PRIVATE';
            $drive->save();
        }
        else
        {
            $drive->drive_access='PUBLIC';
            $drive->save();
        }
    }

    public function sendInvitation($drive_id,Request $request){
        
        $request->validate([
            'emails.*' => 'required|email'
        ]);

        $drive = Drive::findOrFail($drive_id);
        $already = $drive->followers()->pluck('users.email')->toArray();

        foreach($request->emails as $email){
            if($email == Auth::user()->email || in_array($email,$already)) continue;
            
            $url = UrlSigner::sign(url('/class/'.$drive->slug.'/invitation/accept?email='.$email), Carbon::now()->addMonths(1));

            Mail::to($email)->send(new InvitationMail($drive,$url));
        }

        return response()->json('success', 200);
    }

    public function getUser($drive_id,Request $request){
        $search = $request->key;
        $results = [];

        if($search){
            $drive=Drive::findOrFail($drive_id);
            $already = $drive->followers()->pluck('users.email')->toArray();
        
            $user_emails=User::all()->pluck('email');

            $results = Account::where('email','!=',Auth::user()->email)
                        ->whereIn('email',$user_emails)
                        ->whereNotIn('email',$already)
                        ->where('email','like','%'.$search.'%')
                        ->orderBy('name')
                        ->limit(5)
                        ->select('name','email','photo')
                        ->get()->toArray();
                           
        }
        return response()->json($results, 200);
    }
    public function loadResults($drive_id,Request $request)
    {
        $c_ids=Collection::where('drive_id',$drive_id)->get()->pluck('id');
        $results=Result::whereIn('collection_id',$c_ids)->with('collection')->get();
    
        return Driveresponse::response(['results'=>$results],$drive_id);
    }
    public function viewSummaryResult($drive_id)
    {
        $drive = Drive::findOrFail($drive_id);
        $students = $drive->followers;

        $students->map(function($f) use($drive){
            $f['name']=$f->data()->name;
            $f['roll_no'] = $drive->followers()->where('user_id',$f->id)->first()->pivot->rollno;
            return $f;
        });

        $students = $students->sortBy('roll_no');
        $c_ids=Collection::where('drive_id',$drive_id)->get()->pluck('id');
        
        $results = Result::whereIn('collection_id',$c_ids)->get(); 
        
        return view('templates.summaryresult')->with('results',$results)->with('students',$students);
    }
    
}
