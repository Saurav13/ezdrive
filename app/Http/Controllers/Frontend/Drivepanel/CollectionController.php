<?php

namespace App\Http\Controllers\Frontend\Drivepanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drive;
use App\Collection;
use App\Collectioncontent;
use App\Collectiontask;
use App\User;
use Auth;
use Session;
use DateTime;
use DateInterval;
use Storage;
use App\ResponseLayers\DrivePanel\DriveResponse;
use ZipArchive;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use Guzzle\Common\Exception\MultiTransferException;
use App\GLayers\G_Collection;
use Illuminate\Http\File;
use App\CollectionMailLayers\CollectionMail;
use UrlSigner;
use Carbon\Carbon;
use App\Result;
use App\Resultrecord;
use Mail;
use App\Mail\NotificationMail;
use App\Notification;
use ZipStream;

class CollectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            
            if (Auth::user()->drives()->where('drives.id',$request->drive_id)->exists()){
                return $next($request);
            }
            else
                abort(403);
        });
    }
    public function getCollections($drive_id)
    {
        $drive_id=Session::get('drive_id'.$drive_id);
        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        return DriveResponse::response(['collections'=>$collections],$drive_id);
        
    }
    public function addCollection($drive_id,Request $request)
    {
        $mytime = Carbon::now();
        $request->validate([
            'name'=>'required',
            'deadline'=>'nullable|after: '.$mytime,
            'file'=>'file|mimes:pdf,doc,docx,rtf,png,jpeg,jpg,zip,rar,7z,docm|max:6000'
        ]);
        $drive_id=Session::get('drive_id'.$drive_id);
        
        $totalspace=intval(DriveResponse::getTotalSpace(Auth::user()->id));
         
        if($totalspace<intval(Drive::findOrFail($drive_id)->max_space))
        {

            $date = new DateTime();
            $interval = new DateInterval('P1M');
            $date->add($interval);
            
            $collection= new Collection;
            $collection->name=$request->name;
            
            if($request->deadline){       
                
                $collection->deadline= $request->deadline;
            }
            else{
                $type=Drive::findOrFail($drive_id)->type;
                // if($type=='annual')
                //     $collection->deadline=date('Y-m-d', strtotime("+12 months", strtotime($date->format('Y-m-d'))));
                // else if($type=='semester')
                //     $collection->deadline=date('Y-m-d', strtotime("+6 months", strtotime($date->format('Y-m-d'))));
                // else if($type=='trimester')
                //     $collection->deadline=date('Y-m-d', strtotime("+4 months", strtotime($date->format('Y-m-d'))));
                    
            }
            $collection->drive_id=$drive_id;
            $collection->save();

            $task=new Collectiontask;
            $task->id=$collection->id;
           
            $root=Drive::where('id',$drive_id)->first()->drive_collection_root;
            try{
                if(Storage::disk('google')->makeDirectory($root.'/'.$collection->id))
                {
                    $folders=collect(Storage::disk('google')->listContents($root,false));
                    
                    // dd($folders);
                    $path=$folders->where('filename',$collection->id)->first()['path'];
                    $collection->g_path=$path;
                    $collection->mail_link='drive+'.$collection->id.'@ezhub.com';
                    if($request->task || $request->file('file'))
                    {
                    
                        $task->description=$request->task;

                        if($request->file('file'))
                        {
                            $file=$request->file('file');
                            // dd($file);            
                            $filenameWithExt=$file->getClientOriginalName();
                            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME).'.'.$file->getCLientOriginalExtension();
                        
                            Storage::putFileAs('/public/collection_tasks'.'/'.$collection->id, new File($file->getPathName()),$file->getClientOriginalName());
                
                            $task->file='public/collection_tasks'.'/'.$collection->id.'/'.$file->getClientOriginalName();
                            $task->mime_type = $file->getClientMimeType();
                        }

                        $task->save();
                    }
                    $collection->save();

                    $drive = Drive::find($drive_id);
                    foreach($drive->followers as $f){
                        $content = "Has assigned you a new task named '". $collection->name."'.";
                        Mail::to($f->email)->queue(new NotificationMail($f,$drive,'task',$collection->updated_at,'New Task Notification',$content));
                    }
                }
                else{
                    Collection::where('id',$collection->id)->delete();
                }
            }
            catch (\Exception $e) {
                Collection::where('id',$collection->id)->delete();
                return $e->getMessage();
            }
            $task->save();

            $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
            Notification::pushNotificationClass($drive_id,'new_task',$collection->name);
            return DriveResponse::response(['collections'=>$collections],$drive_id);
        }
        else{
            return response()->json(['message'=>'not enough space'],422);
        }
    }

    public function openCollection($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $drive = Drive::findOrFail($drive_id);
        $files=Collectioncontent::where('collection_id',$request->id)->get();
        $files->map(function($f) use($drive){
            if($f->user){
                $f['user_id']=$f->user->id;
                $f['user_name']=$f->user->data()->name;
                $f['roll_no'] = $drive->followers()->where('user_id',$f->user->id)->first() ? $drive->followers()->where('user_id',$f->user->id)->first()->pivot->rollno : null;
                return $f;
            }
        });
        return DriveResponse::response(['files'=>$files],$drive_id);
    }
    public function editDeadline($drive_id,Request $request)
    {
        $mytime = Carbon::now();
        $request->validate([
            'deadline'=>'nullable|after: '.$mytime,
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $c=Collection::findOrFail($request->id);        
        $c->deadline=$request->deadline;
        if($c->status=='closed')
            Notification::pushNotificationClass($drive_id,'task_reopen',$c->name);

        else
            Notification::pushNotificationClass($drive_id,'task_edit',$c->name);

        $c->status = 'open';
        $c->save();

        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        return DriveResponse::response(['collections'=>$collections],$drive_id);

    }

    public function getSubmissionLink($drive_id,Request $request){
       
        $dirfile = Collectioncontent::findOrFail($request->id);
        if(Collection::findOrFail($dirfile->collection_id)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Unauthorized'],401);
        }
        $url = UrlSigner::sign(url('/class/'.$drive_id.'/task/opensubmission/'.$dirfile->id.'/'.$dirfile->name), Carbon::now()->addMinutes(1));
    
        return response()->json(['url'=>$url], 200);
    }

    public function deleteCollection($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $root=Collection::findOrFail($request->id)->g_path;
        try{
        Storage::disk('google')->delete($root);
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
        Collection::where('id',$request->id)->delete();

        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        return DriveResponse::response(['collections'=>$collections],$drive_id);
    }
    public function closeCollection($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        
        $c=Collection::findOrFail($request->id);
        if($c->status=='open')
        {
            $c->status='closed';
            Notification::pushNotificationClass($drive_id,'task_close',$c->name);
        }
        else{
            $c->status='open';
            Notification::pushNotificationClass($drive_id,'task_reopen',$c->name);
        }
        $c->save();
        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        
        return DriveResponse::response(['collections'=>$collections],$drive_id);
    }
    public function renameCollection($drive_id,Request $request)
    {
        $request->validate([
            'name'=>'required',
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $root=Collection::findOrFail($request->id)->g_path;
        
        Collection::where('id',$request->id)->update(['name'=>$request->name]);

        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        return DriveResponse::response(['collections'=>$collections],$drive_id);
    }
    public function publishResult($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $drive = Drive::findOrFail($drive_id);
        
        $collectioncontents=Collectioncontent::where('collection_id',$request->id)->get();

        foreach ($collectioncontents as $c) {
            if(!$c->grade)
                return response()->json(['errors'=>['files'=>[0 => 'Some file are still ungraded.']]], 422);
        }

        $result= new Result;
        $result->collection_id=$request->id;
        $result->message=$request->message;
        $result->save();

        Collection::where('id',$request->id)->update(['resultPublished'=>'published','status'=>'closed']);
        
        foreach ($collectioncontents as $c) {
            // dd($c);
            $resultrecord=new Resultrecord;
            $resultrecord->grade=$c->grade;
            $resultrecord->name=$c->user->data()->name;
            $resultrecord->result_id=$result->id;
            $resultrecord->user_id=$c->user->id;
            $resultrecord->rollno= $drive->followers()->where('user_id',$c->user->id)->first()->pivot->rollno;
            $resultrecord->save();
        }

        foreach($drive->followers as $f){
            $content = "Has published the result of task '". $result->collection->name."'.";
            Mail::to($f->email)->send(new NotificationMail($f,$drive,'result',$result->updated_at,'New Result Notification',$content));
        }

        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        Notification::pushNotificationClass($drive_id,'publish_result',Collection::findOrFail($request->id)->name);
        return DriveResponse::response(['collections'=>$collections],$drive_id);

    }
    
    public function viewResult($drive_id,$c_id)
    {
        if(Collection::findOrFail($c_id)->drive_id!=$drive_id)
        {
            return "Access Denied";
        }
        $resultrecords=Result::where('collection_id',$c_id)->first()->resultrecords()->get(); 
        // dd($resultrecords);
        return view('templates.result')->with('resultrecords',$resultrecords);
    }


    public function editTask($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id,
            'file' => 'file|mimes:pdf,doc,docx,rtf,png,jpeg,jpg,zip,rar,7z,docm|max:6000' 
        ]);
        
        // dd($request->file);
        // $request->validate(['file'=>'mimes:pdf']);
        $drive_id=Session::get('drive_id'.$drive_id);
        $task=Collectiontask::findOrFail($request->id);
        
        $task->description=$request->description;
     
        if($request->file('file'))
        {
            
            $file=$request->file('file');
            
            // dd($file);
            $filenameWithExt=$file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME).'.'.$file->getClientOriginalExtension();
            if($task->file)
                Storage::delete($task->file);
            // dd($request->file);
            // $path = $file->storeAs('/public/collection_tasks'.'/'.$request->id, $filename);
            // Storage::put('/public/collection_tasks'.'/'.$request->id,$file);
           Storage::putFileAs('/public/collection_tasks'.'/'.$request->id, new File($file->getPathName()),$file->getClientOriginalName());
            
            $task->file='public/collection_tasks'.'/'.$request->id.'/'.$file->getClientOriginalName();
            $task->mime_type = $file->getClientMimeType();
        }

        $task->save();
            
        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        Notification::pushNotificationClass($drive_id,'task_edit',$task->collection->name);
        return DriveResponse::response(['collections'=>$collections],$drive_id);
    }
    public function downloadCollection($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $files=Collectioncontent::where('collection_id',$request->id)->get();
       
        return json_encode(['files'=>$files]);
    }
    public function storeFileInSession($drive_id,Request $request)
    {
        $raw_data=Storage::disk('google')->get($request->file['g_path']);

        Session::put($drive_id.'+'.'filedata'.'+'.$request->file['id'],$raw_data);
        Session::put($drive_id.'+'.'filename'.'+'.$request->file['id'],$request->file['name']);
       
        return "Success";

    }

    public function download($drive_id,$file_id,$file_name,Request $request)
    {
        if(Collection::where('drive_id',$drive_id)->where('id',$file_id)->count()==0)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }
        $files=Collectioncontent::where('collection_id',$file_id)->get();
              
        
         $zip = new ZipStream\ZipStream($file_name, [
             'content_type' => 'application/octet-stream'
         ]);
         
         foreach ($files as $f) {
             $stream = Storage::disk('google')->readStream($f->g_path);
             $zip->addFileFromStream($f->name, $stream);
         }
        
         $zip->finish();
        // return G_Collection::collectionDownloadResponse($request->id,$files);
    }

   
    public function downloadFile($drive_id,$file_id,$file_name,Request $request)
    {
        
        $dirfile = Collectioncontent::findOrFail($file_id);
        if($dirfile->collection->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }    
    
        $readStream = Storage::disk('google')->readStream($dirfile->g_path);

        return response()->stream(function () use ($readStream) {
            fpassthru($readStream);
        }, 200, [
            'Content-Type' => $dirfile->mime_type,
            'Content-Length' => $dirfile->size,
            'Content-disposition' => 'attachment; filename="'.$dirfile->name.'"',
        ]);
    
    }
    public function saveGrade($drive_id,Request $request)
    {
        $drive=Drive::findOrFail($drive_id);
        if(Collection::findOrFail($request->cid)->drive_id!=$drive_id)
        {
            return response()->json(['message'=>'Access Denied'],401);
        }
        $c=Collectioncontent::findOrFail($request->id);
        $c->grade=$request->grade;
        $c->save();

        $collections=Collection::where('drive_id',$drive_id)->with('collectioncontents')->with('collectiontask')->get();
        $files=Collectioncontent::where('collection_id',$request->cid)->get();
        $files->map(function($f) use($drive){
            if($f->user){
                $f['user_id']=$f->user->id;
                $f['user_name']=$f->user->data()->name;
                $f['roll_no'] = $drive->followers()->where('user_id',$f->user->id)->first()->pivot->rollno;
                return $f;
            }
        });
        return DriveResponse::response(['files'=>$files,'collections'=>$collections],$drive_id);

    }


    public function zipDownload($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $count=$request->count;
        $files=Collectioncontent::where('collection_id',$request->id)->get();
        
        $zip = new ZipArchive;

        $public_dir=public_path().'\tempCollectionDownload'.'\\'.$request->id;
        // dd($public_dir);
        if(!file_exists($public_dir))
            mkdir($public_dir,0777,true);
        // dd("Asd");
        $zipFileName = 'temp'.'.zip';
        // dd($zipFileName);
        $zip->open($public_dir . '\\' . $zipFileName, ZipArchive::CREATE) ;
        // $zip->close();
        // dd($zip);
       // Add File in ZipArchive
        //  dd($zip);
        $zip->addFromString('.txt', 'hello');
            
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ); 
        foreach ($files as $f) {
            $raw_data=Session::get($drive_id.'+'.'filedata'.'+'.$f->id);
            $zip->addFromString($f->name, $raw_data);

        }
        $zip->close();
        return "success";
    }
    
    public function zipDownload2($drive_id,Request $request)
    {
        $request->validate([
            'id'=>'exists:collections,id,drive_id,'.$drive_id
        ]);
        $count=$request->count;
        $files=Collectioncontent::where('collection_id',$request->id)->get();
        
        $zip = new ZipArchive;

        $public_dir=public_path().'\tempCollectionDownload'.'\\'.$request->id;
        // dd($public_dir);
        if(!file_exists($public_dir))
            mkdir($public_dir,0777,true);
        // dd("Asd");
        $zipFileName = 'temp'.'.zip';
        // dd($zipFileName);
        $zip->open($public_dir . '\\' . $zipFileName, ZipArchive::CREATE) ;
        // $zip->close();
        // dd($zip);
       // Add File in ZipArchive
        //  dd($zip);
        $zip->addFromString('.txt', 'hello');
            
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ); 
        foreach ($files as $f) {
            try{
                $raw_data=Storage::disk('google')->get($f->g_path);
                $zip->addFromString($f->name, $raw_data);
            }
            catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        $zip->close();
        return "success";
    }
}
