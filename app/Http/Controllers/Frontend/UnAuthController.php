<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drive;
use App\Drivecontent;
use App\Dir;
use Session;

class UnAuthController extends Controller
{
    public function openRoot($drive_id)
    {
        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',0)->get(); //drives root files
        $folders=Dir::where('drive_id',$drive_id)->where('p_id','0')->get(); //drives root folders
        $breadcrumbs=$this->getBreadCrumbs(0);
        
        return json_encode(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs]);
    }

    public function searchDrive($drive_id,Request $request){
        Session::put('p_id'.$drive_id,0);

        $files=DriveContent::where('drive_id',$drive_id)->where('name','LIKE','%'.$request->key.'%')->get();
        $folders=Dir::where('drive_id',$drive_id)->where('name','LIKE','%'.$request->key.'%')->get();

        return json_encode(['files'=>$files,'folders'=>$folders]);
    }

    public function openFolder($drive_id,Request $request)
    {   
        Session::put('p_id'.$drive_id,$request->id);
        
        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$request->id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$request->id)->get();
        $breadcrumbs=$this->getBreadCrumbs($request->id);

        return json_encode(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs]);
    }

    public function back($drive_id,Request $request)
    {
        $p_id=Session::get('p_id'.$drive_id);

        if($p_id!=0)
        {
            $p_id=Dir::where('id',$p_id)->first()->p_id;
            Session::put('p_id'.$drive_id,$p_id);
        }

        $files=Drivecontent::where('drive_id',$drive_id)->where('dir_id',$p_id)->get();
        $folders=Dir::where('drive_id',$drive_id)->where('p_id',$p_id)->get();
        $breadcrumbs=$this->getBreadCrumbs($p_id);

        return json_encode(['files'=>$files,'folders'=>$folders,'breadcrumbs'=>$breadcrumbs,'isRoot'=>($p_id==0)?true:false]);
    }

    private function getBreadCrumbs($drive_id)
    {  
        $p_id = $drive_id;
        $crumbs = array();
        while($p_id!=0)
        {
            $dir=Dir::findOrFail($p_id);
            array_push($crumbs,$dir);
            $p_id = $dir->p_id;
            
        }
        return array_reverse($crumbs);
    }
}
