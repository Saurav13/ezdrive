<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drivecontent;
use App\Collectiontask;
use App\Postedfile;
use App\Collectioncontent;
use Storage;
use Response;

class FileController extends Controller
{
    public function openDriveFile($drive_id,$file_id,$file_name,Request $request){
        
        $dirfile = Drivecontent::findOrFail($file_id);
        
        $rawData = Storage::disk('google')->get($dirfile->g_path);
        
        return Response::make($rawData, 200, array('content-type'=>$dirfile->mime_type,'Content-Disposition', "attachment; filename='$dirfile->name'"));
        
    }

    public function openPostFile($drive_id,$file_id,$file_name,Request $request){
        $file = Postedfile::findOrFail($file_id);
        try{
            $rawData = Storage::disk('google')->get($file->g_path);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        return Response::make($rawData, 200, array('content-type'=>$file->mime_type,'Content-Disposition', "attachment; filename='$file->file'"));
    }

    public function openTaskFile($drive_id,$file_id,$file_name,Request $request){
        $file = Collectiontask::findOrFail($file_id);
        $rawData = Storage::get($file->file);
        
        $arr = explode('/', $file->file);
        $filename = end($arr);
        
        return Response::make($rawData, 200, array('content-type'=>$file->mime_type,'Content-Disposition', "attachment; filename='$filename'"));
    }

    public function openSubmission($drive_id,$file_id,$file_name,Request $request){
        $assignment = Collectioncontent::findOrFail($file_id);

        $rawData = Storage::disk('google')->get($assignment->g_path);
        
        return Response::make($rawData, 200, array('content-type'=>$assignment->mime_type,'Content-Disposition', "attachment; filename='$assignment->name'"));
    }
}
