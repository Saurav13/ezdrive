<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BoxAd;
use App\Faculty;
use App\College;
use Carbon\Carbon;

class BoxAdController extends Controller
{
    
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = BoxAd::orderBy('updated_at','desc')->paginate(10);
        $faculties = Faculty::orderBy('name','asc')->get(); //only if it has class
        
        $colleges = College::orderBy('name','asc')->get(); //''
        return view('admin.boxads.index')->with('ads',$ads)->with('faculties',$faculties)->with('colleges',$colleges);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'target' => 'required|in:Teacher,Student,Both',
            'faculty.*' => 'required|exists:faculties,id',
            'college.*' => 'required|exists:colleges,id',
            'year.*' => 'required|in:Current,Pass Out',
            'link' => 'required|url',
            'expiry_date' => 'required|date|after:today',
            'content' => 'required'
        ]);
        
        $ad = new BoxAd;
        $ad->target = $request->target;
        $ad->year = json_encode($request->year);
        $ad->link = $request->link;
        $ad->expiry_date = $request->expiry_date;
        $ad->content=$request->content;
        $ad->save();

        $ad->faculties()->attach($request->faculty);
        $ad->colleges()->attach($request->college);

        $request->session()->flash('success', 'Box Ad successfully added.');        
        
        return redirect()->route('boxAds.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = BoxAd::findOrFail($id);

        $faculties= Faculty::orderBy('name','asc')->get();
        $colleges = College::orderBy('name','asc')->get();

        return view('admin.boxads.edit')->with('ad',$ad)->with('faculties',$faculties)->with('colleges',$colleges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'target' => 'required|in:Teacher,Student,Both',
            'faculty.*' => 'required|exists:faculties,id',
            'college.*' => 'required|exists:colleges,id',
            'year.*' => 'required|in:Current,Pass Out',
            'link' => 'required|url',
            'expiry_date' => 'required|date|after:today',
            'content' => 'required'
        ]);
        
        $ad = BoxAd::findOrFail($id);
        $ad->target = $request->target;
        $ad->year = json_encode($request->year);
        $ad->link = $request->link;
        $ad->expiry_date = $request->expiry_date;
        $ad->content=$request->content;
        
        $ad->save();

        $ad->faculties()->sync($request->faculty);
        $ad->colleges()->sync($request->college);

        $request->session()->flash('success', 'Box Ad successfully updated.');        
        
        return redirect()->route('boxAds.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $ad = BoxAd::findOrFail($id);

        unlink(public_path('boxad_images/'.$ad->image));
        $ad->delete();
        
        $request->session()->flash('success', 'Box Ad successfully deleted.');        
        
        return redirect()->route('boxAds.index');
    }

    public function expiredAds(){
        $date = Carbon::today();  
        $ads = BoxAd::where('expiry_date','<',$date)->orderBy('updated_at','desc')->paginate(10);

        return view('admin.boxads.result')->with('ads',$ads);
    }
}
