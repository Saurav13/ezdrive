<?php 
namespace App\Http\Controllers\Admin;
use App\Faculty;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class FacultyController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    public function index()
    {
        $facultys = Faculty::orderBy('created_at', 'desc')->paginate(20);
        $categories=Category::all();
        return view('admin.faculty.list')->with('faculty', $facultys)->with('categories',$categories);
    }
    public function store(Request $request)
    {
        $faculty = new Faculty;
        $faculty->name = $request->name;
        $faculty->category_id = $request->category_id;
        $faculty->save();
        $request->session()->flash('success', 'New faculty created');
        return redirect()->route('faculty.index');
    }
    public function show($id)
    {
        $faculty = Faculty::find($id);
        if (!$faculty) {
            abort(404);
        }
        return view('admin.facultys.show')->with('faculty', $faculty);
    }
    public function update(Request $request, $id)
    {
        $faculty = Faculty::find($id);
        if (!$faculty) {
            abort(404);
        }

        $faculty->name = $request->name;
        $faculty->category_id = $request->category_id;
        $faculty->save();
        $request->session()->flash('success', 'Faculty upated');
        return redirect()->route('faculty.index');
    }
    public function destroy($id, Request $request)
    {
        $faculty = Faculty::find($id);if (!$faculty) {
            abort(404);
        }

        $faculty->delete();
        $request->session()->flash('success', 'Faculty deleted');
        return redirect()->route('faculty.index');
    }
}
