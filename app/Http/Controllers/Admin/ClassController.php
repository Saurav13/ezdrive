<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BuisnessLogics\ClassHelper;
use App\Drive;
use App\Postedfile;
use App\Notice;
use App\Faculty;
use Storage;

class ClassController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = Drive::orderBy('name')->paginate(10);
        return view('admin.classes.index')->with('classes',$classes);
    }

    public function searchByName(Request $request)
    {
        $classes = Drive::where('name','LIKE','%'.$request->key.'%')->orderBy('name')->paginate(10);
        return view('admin.classes.search')->with('classes',$classes);
    }

    public function searchByCode(Request $request)
    {
        $class = Drive::where('code',$request->key)->paginate(10);
        return view('admin.classes.search')->with('classes',$class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'space' => 'required|numeric|min:1'
        ]);

        $class = Drive::findOrFail($id);
        $class->max_space = $request->space*1024*1024*1024;
        $class->save();
        
        return response()->json('success', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $class = Drive::findOrFail($id);
        
        ClassHelper::resetDrive($class);

        $class->delete();
        $request->session()->flash('success', 'Class sucessfully deleted');
        return redirect()->route('classes.index');
    }
}
