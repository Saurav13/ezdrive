<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use VisitLog;
use Carbon\Carbon;
use App\ResponseLayers\DrivePanel\DriveResponse;
use App\User;
use App\Drive;
use App\Category;
use App\BannerAd;
use App\BoxAd;
use App\Drivecontent;
use App\Collectioncontent;
use App\Postedfile;
use File;

class AdminController extends Controller
{ 
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
        // $this->middleware(function ($request, $next) {

        //     if (Auth::user()->roles()->where('title', '=', 'Dashboard')->exists()){
        //         return $next($request);
        //     }
        //     else
        //         abort(403);
        // });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        $date = Carbon::today();  
        
        $now = Carbon::now();
        $last_year = Carbon::today()->addMonth()->subYear()->startOfDay();
        
        $data = [];

        $dummy = Carbon::today()->addMonth()->subYear()->startOfDay();

        while ($dummy <= $now) {
            $data[date("M, Y", strtotime($dummy))] = 0;
            $dummy->addMonth();
        }
        
        $all = VisitLog::all();
        $totalvisitors = $all->count();
        $todaysvisitors = $all->where('updated_at','>',$date)->count();

        foreach($all->where('updated_at','>=',$last_year)->where('updated_at','<=',$now) as $log){
            $key = date('M, Y',strtotime($log->updated_at));
            if(array_key_exists($key,$data))
                $data[$key] += 1;
        }

        $totalSpaceUsed = [];
        $totalSpaceUsed['Drive'] = Drivecontent::all()->unique('g_path')->sum('size');
        $totalSpaceUsed['CollectionContent'] = Collectioncontent::sum('size');
        
        $file_size = 0;
        foreach( File::allFiles(storage_path('app\public\collection_tasks')) as $file)
        {
            $file_size += $file->getSize();
        }

        $totalSpaceUsed['CollectionTask'] = $file_size;
        $totalSpaceUsed['Posts'] = Postedfile::sum('size');
        
        $totalSpaceAsg = Drive::sum('max_space');
        
        $user_count = User::all()->count();

        // $categories = Category::has('classes')->withCount('classes')->orderBy('classes_count', 'DESC')->take(10)->get();

        $categories = Category::limit(10)->get();
        $bannerads = BannerAd::where('expiry_date','>=',$date)->count();
        $boxads = BoxAd::where('expiry_date','>=',$date)->count();
        $banneradsTot = BannerAd::count();
        $boxadsTot = BoxAd::count();

        return view('admin.dashboard',compact('bannerads','boxads','banneradsTot','boxadsTot','totalvisitors','todaysvisitors','data','totalSpaceUsed','totalSpaceAsg','user_count','categories'));
    }

    public function test(Request $request){
        $request->session()->flash('success', "You clicked this");
        return redirect()->route('admin_dashboard');
    }
}
