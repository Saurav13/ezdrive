<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\MyBroker;
use App\User;
use Storage;
use Validator;
use Session;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request, MyBroker $myBroker)
    {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if($response == Password::INVALID_USER){
            $user = $myBroker->checkUserEmail($request->get('email'));
            
            if($user){

                $validator = Validator::make($user, [
                    'verified' => 'accepted'
                ]);
        
                if ($validator->fails()) {
                    Session::put($user['email'], $user);
                    
                    $validator->errors()->add('email', 'Your account is not verified yet. (<a href="'.route('resend_confirmation',array('email' => $user["email"] )).'" style="color:#4545c8">Resend</a> verification email?)');
                    return redirect()->to('/login')->withErrors($validator)->withInput();
                }

                $existing = User::where('email', $user['email'])->first();

                if(!$existing){
                    $existing= User::create([
                        'id' => $user['id'],
                        'email' => $user['email'],
                    ]);
            
                    if(Storage::disk('google')->makeDirectory(env('DRIVE_G_ROOT').'/'.$existing->id))
                    {
                        $roots=collect(Storage::disk('google')->listcontents(env('DRIVE_G_ROOT'),false));
                        // dd($roots->where('filename',$existing->id)->first());
                        $existing->user_drive_root=$roots->where('filename',$existing->id)->first()['path'];
                    }
                    else{
                        $existing->where('id',$existing->id)->delete();
                        abort(404);
                    }
            
                    if(Storage::disk('google')->makeDirectory(env('COLLECTION_G_ROOT').'/'.$existing->id))
                    {
                        $roots=collect(Storage::disk('google')->listcontents(env('COLLECTION_G_ROOT'),false));
                        $existing->user_collection_root=$roots->where('filename',$existing->id)->first()['path'];
                    }
                    else{
                        $existing->where('id',$existing->id)->delete();
                        abort(404);
                    }
                    $existing->save();
                }

                $response = $this->broker()->sendResetLink(
                    $request->only('email')
                );
            }
        }

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);

    }
    
}
