<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Mail\ConfirmUserEmail;
use Mail;
use App\MyBroker;
use Storage;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // $user= User::create([
            // 'id' => $data['id'],
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'slug' => $data['slug'],
        // ]);

        

        // if(Storage::disk('google')->makeDirectory(env('DRIVE_G_ROOT').'/'.$user->id))
        // {
        //     $roots=collect(Storage::disk('google')->listcontents(env('DRIVE_G_ROOT'),false));
        //     // dd($roots->where('filename',$user->id)->first());
        //     $user->user_drive_root=$roots->where('filename',$user->id)->first()['path'];
        // }
        // else{
        //     $user->where('id',$user->id)->delete();
        //     abort(404);
        // }

        // if(Storage::disk('google')->makeDirectory(env('COLLECTION_G_ROOT').'/'.$user->id))
        // {
        //     $roots=collect(Storage::disk('google')->listcontents(env('COLLECTION_G_ROOT'),false));
        //     $user->user_collection_root=$roots->where('filename',$user->id)->first()['path'];
        // }
        // else{
        //     $user->where('id',$user->id)->delete();
        //     abort(404);
        // }
        // $user->save();

        return $user;
    }

    public function register(Request $request, MyBroker $myBroker)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->messages()->messages();
            if ($request->expectsJson()) {
                return response()->json($errors, 422);
            }

            return redirect()->route('register')->withErrors($errors)->withInput();
        }

        $result = $myBroker->registerUser($request);

        if($result == -1){
            $errors = new MessageBag;
            $errors->add('email', 'Email has already been taken');
            return redirect('register')->withErrors($errors)->withInput();
        }
        else if($result){

            Mail::to($result['email'])->send(new ConfirmUserEmail($result));

            Session::put($result['email'], $result);

            $validator = Validator::make($result, [
                'verified' => 'accepted'
            ]);
    
            if ($validator->fails()) {
                $validator->errors()->add('email', 'Please Check your email to verify your account. (<a href="'.route('resend_confirmation',array('email' => $result['email'] )).'" style="color:#4545c8">Resend</a> verification email?)');
                return redirect()->route('login')->withErrors($validator)->withInput();
            }
            return redirect()->route('login')->withInput();
        }else{
            return redirect()->route('login');
        }
    }

    public function resendConfirmation(Request $request){

        $email = $request->get('email');

        $user = Session::get($email);
        // dd($user);
        if(!$user)
            return redirect()->route('login')->withInput();

        Mail::to($email)->send(new ConfirmUserEmail($user));
        Session::flash('success','Please Check your email to verify your account');
        return redirect()->route('login')->withInput();
    }
}
