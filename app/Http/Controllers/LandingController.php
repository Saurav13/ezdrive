<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Drive;
use App\User;
/*to reminder--------------------------- */
use App\Collection;
use Carbon\Carbon;
use App\Mail\ReminderMail;
use App\ResponseLayers\DrivePanel\DriveResponse;
use Mail;
/**------------------------------------ */

use Redirect;
use Auth;

class LandingController extends Controller
{
    public function searchByCode(Request $request)
    {
        $drive=Drive::where('code',$request->code)->first();
        if($drive==null){
            return view('frontend.searchcode'); 
        }
        else{
            if(Auth::check() && Auth::user()->drives()->where('drives.id',$drive->id)->exists()){
                return redirect()->route('mydrive', $drive->slug);        
            }
            else if(Auth::check() && Auth::user()->followed_drives()->where('drives.id',$drive->id)->exists()){
                return redirect()->route('class', $drive->slug);          
            }else{
                return view('frontend.classroom.unfollowedclass')->with('drive',$drive);            
            }        
        }
    }

    public function reminder(Request $request){
        dd($request->session(),$request->cookie('XSRF-TOKEN'));
        $today = Carbon::today()->format('Y-m-d');
        $tomorrow = Carbon::tomorrow()->format('Y-m-d');
        $yesterday = Carbon::yesterday()->format('Y-m-d');
        
        foreach (Drive::all() as $d) {
        
            if($d->unapprovedfollowers()->count() >= 5){
                $content = "You have ". $d->unapprovedfollowers()->count() ." new follow requests in your '".$d->name."' Class.";
                Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'New Class Follow Requests',$content,route('mydrive',$d->id)));
            }

            $totalspace = intval(DriveResponse::getTotalSpace($d->owner->id));
            if((intval($d->max_space) - $totalspace) <= 52428800){
                $content = "You have ". round((intval($d->max_space) - $totalspace)/(1024*1024))." MB space left in your '".$d->name."' Class.";
                Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'Reminder for Class Space Renewal',$content,route('mydrive',$d->id)));
            }

            foreach ($d->collections as $c) {
                if($c->deadline == $today){
                    $submitedUsers = $c->collectioncontents()->pluck('submittedBy_user_id')->toArray();
                    $content = "Deadline for Task '".$c->name."' is set to ". date('M j, Y',strtotime($c->deadline)) ." which is today.";
                    foreach($d->followers as $f){
                        if(in_array($f->id,$submitedUsers)) continue;
                        Mail::to($f->email)->send(new ReminderMail($f,'Reminder For Task Deadline',$content,route('class',$d->slug)));
                    }
                    Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'Reminder For Task Deadline',$content,route('mydrive',$d->id)));
                }
                else if($c->deadline == $tomorrow){
                    $submitedUsers = $c->collectioncontents()->pluck('submittedBy_user_id')->toArray();
                    $content = "Deadline for Task '".$c->name."' is set to ". date('M j, Y',strtotime($c->deadline)) ." which is tomorrow.";
                    foreach($d->followers as $f){
                        if(in_array($f->id,$submitedUsers)) continue;
                        Mail::to($f->email)->send(new ReminderMail($f,'Reminder For Task Deadline',$content,route('class',$d->slug)));
                    }
                    Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'Reminder For Task Deadline',$content,route('mydrive',$d->id)));
                }
                else if($c->deadline == $yesterday){
                    $c->status = 'closed';
                    $c->save();
                }
            }
        }
    }
   
}
