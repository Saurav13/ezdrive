<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Account;
use App\Drive;
use App\Category;
use App\Faculty;
use App\Notification;
use App\Usernotification;
use App\College;
use Auth;
use Carbon\Carbon;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('searchMoreUser');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Session::put('check','check');
        // dd(Session::all());
        $drives=Drive::where('user_id',Auth::user()->id)->get();
        $categories=Category::all();
        $faculties=Faculty::all();
        $colleges = College::orderBy('name','asc')->get()->pluck('name')->toArray();
        
        return view('frontend.home',compact('drives','categories','faculties','colleges'));
    }
    
    public function searchUser()
    {
        $search = \Request::get('user'); 
        $results = [];
        $count = -1;
        if($search){
            $drives=Drive::where('code',$search)->get();
            if($drives->count()<=0)
            {   
                $user_emails=User::all()->pluck('email');
                $results = Account::where('email','!=',Auth::user()->email)
                        ->whereIn('email',$user_emails)
                        ->where('name','like','%'.$search.'%')
                        ->orderBy('name','asc')
                        ->paginate(8);    
                $view=view('frontend.partials_home.search',compact('results'));
                $view->render();
                return $view;
            }
            else
            {
                $view=view('frontend.partials_home.searchbycode')->with('drive',$drives->first());
                $view->render();
                return $view;
            }
            $count = $results->total();
        }
        $view=view('frontend.partials_home.search',compact('results'));
        $view->render();
        return $view;
    }
    
    public function searchMoreUser(Request $request)
    {
        $search = $request->search; 
        $results = [];
        $count = -1;
        if($search){
           
            $drives=Drive::where('code',$search)->get();
            if($drives->count()<=0)
            {   
                $user_emails=User::all()->pluck('email');
                $results = Account::where('email','!=',Auth::check() ? Auth::user()->email:null)
                        ->whereIn('email',$user_emails)
                        ->where('name','like','%'.$search.'%')
                        ->orderBy('name','asc')
                        ->paginate(8);    
            }
            else{
                return redirect('searchbycode?code='.$search);
            }
            $count = $results->total();
        }
        return view('frontend.classroom.search',compact('results'));
    }

    public function getNotifications(Request $request)
    {
        
        
        $user_id=Auth::user()->id;
        
        $f_drive_ids=User::findOrFail($user_id)->followed_drives()->get()->pluck('id');
        $f_notifications=Notification::where('type','Class')->whereIn('drive_id',$f_drive_ids)->get();
        
        
        $drive_ids=User::findOrFail($user_id)->drives()->get()->pluck('id');
        $d_notifications=Notification::where('type','Drive')->whereIn('drive_id',$drive_ids)->get();

        
        $u_notifications=Usernotification::where('user_id',$user_id)->get();
        
        
        $notifications= collect([]);
        $last_notified_date=User::findOrFail($user_id)->last_notified_date;

        $notifications=$f_notifications->merge($d_notifications)->merge($u_notifications)->sortByDesc('created_at')->where('created_at','>',$last_notified_date)->values();
    
        $ten_notifications=$f_notifications->merge($d_notifications)->merge($u_notifications)->sortByDesc('created_at')->values()->take(10);
        
        if($request->bellClick)
        {
            $user=User::findOrFail($user_id);
            $user->last_notified_date=Carbon::now();
            $user->save();   
        }
        // dd($notifications);
        return response()->json(['count'=>$notifications->count(),'notifications'=>$ten_notifications]);
    }

    public function allNotifications(Request $request)
    {
        $user_id=Auth::user()->id;
        
        $f_drive_ids=User::findOrFail($user_id)->followed_drives()->get()->pluck('id');
        $f_notifications=Notification::where('type','Class')->whereIn('drive_id',$f_drive_ids)->get();
        
        
        $drive_ids=User::findOrFail($user_id)->drives()->get()->pluck('id');
        $d_notifications=Notification::where('type','Drive')->whereIn('drive_id',$drive_ids)->get();

        
        $u_notifications=Usernotification::where('user_id',$user_id)->get();
        
        $last_notified_date=User::findOrFail($user_id)->last_notified_date;
        
        $notifications= collect([]);
        $notifications=$f_notifications->merge($d_notifications)->merge($u_notifications)->sortByDesc('created_at')->values();
    
        $unseen_notifications=$f_notifications->merge($d_notifications)->merge($u_notifications)->sortByDesc('created_at')->where('created_at','>',$last_notified_date)->values();
        // dd($notifications);

        return response()->json(['count'=>$unseen_notifications->count(),'notifications'=>$notifications]);
    }

    public function getCollegeNames(Request $request){
        $search = \Request::get('phrase'); 
        $results = [];
        if($search){
            $results = College::where('name','like','%'.$search.'%')->get()->toArray();
        }
        
        return response()->json($results, 200);
    }

    public function acceptInvitation(Request $request,$drive_slug){
        $drive = Drive::where('slug',$drive_slug)->first();
        $user = User::where('email',$request->email)->first();
        if($drive && $user && $drive->owner->id != $user->id){
            if(in_array($user->id,$drive->unapprovedfollowers()->pluck('users.id')->toArray())){
                $drive->unapprovedfollowers()->where('user_id',$user->id)->update(['confirmed'=>1]);
            }
            elseif(!in_array($user->id,$drive->allfollowers()->pluck('users.id')->toArray())){
                $drive->followers()->attach($user->id, array('confirmed' => 1));
                $drive->save();
            }

            return redirect()->route('class', $drive->slug);
        }else{
            abort(404);
        }
    }
}
