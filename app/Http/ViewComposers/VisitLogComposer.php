<?php 

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use VisitLog;

class VisitLogComposer
{
 
 	public function compose(View $view)
 	{
 		VisitLog::save();
 	}

}