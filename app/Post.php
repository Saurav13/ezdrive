<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function postedfiles()
    {
        return $this->hasMany('App\Postedfile');
    }
}
