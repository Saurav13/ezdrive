<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Collection;
use Carbon\Carbon;
use App\Drive;
use App\Mail\ReminderMail;
use App\ResponseLayers\DrivePanel\DriveResponse;
use Mail;

class DailyNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DailyNotification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends Daily Notifications and Reminders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today()->format('Y-m-d');
        $tomorrow = Carbon::tomorrow()->format('Y-m-d');
        $yesterday = Carbon::yesterday()->format('Y-m-d');
        
        foreach (Drive::all() as $d) {
        
            if($d->unapprovedfollowers()->count() >= 5){
                $content = "You have ". $d->unapprovedfollowers()->count() ." new follow requests in your '".$d->name."' Class.";
                Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'New Class Follow Requests',$content,route('mydrive',$d->slug)));
            }

            $totalspace = intval(DriveResponse::getTotalSpace($d->owner->id));
            if((intval($d->max_space) - $totalspace) <= 52428800){
                $content = "You have ". round((intval($d->max_space) - $totalspace)/(1024*1024))." MB space left in your '".$d->name."' Class.";
                Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'Reminder for Class Space Renewal',$content,route('mydrive',$d->slug)));
            }

            foreach ($d->collections as $c) {
                if($c->deadline == $yesterday){
                    $c->status = 'closed';
                    $c->save();
                }
                else if($c->deadline == $today){
                    $submitedUsers = $c->collectioncontents()->pluck('submittedBy_user_id')->toArray();
                    $content = "Deadline for Task '".$c->name."' is set to ". date('M j, Y',strtotime($c->deadline)) ." which is today.";
                    foreach($d->followers as $f){
                        if(in_array($f->id,$submitedUsers)) continue;
                        Mail::to($f->email)->send(new ReminderMail($f,'Reminder For Task Deadline',$content,route('class',$d->slug)));
                    }
                    Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'Reminder For Task Deadline',$content,route('mydrive',$d->slug)));
                }
                else if($c->deadline == $tomorrow){
                    $submitedUsers = $c->collectioncontents()->pluck('submittedBy_user_id')->toArray();
                    $content = "Deadline for Task '".$c->name."' is set to ". date('M j, Y',strtotime($c->deadline)) ." which is tomorrow.";
                    foreach($d->followers as $f){
                        if(in_array($f->id,$submitedUsers)) continue;
                        Mail::to($f->email)->send(new ReminderMail($f,'Reminder For Task Deadline',$content,route('class',$d->slug)));
                    }
                    Mail::to($d->owner->email)->send(new ReminderMail($d->owner,'Reminder For Task Deadline',$content,route('mydrive',$d->slug)));
                }
            }
        }
    }
}
