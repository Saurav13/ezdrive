<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Class_visited extends Model
{
    protected $table='class_visited';
    public static function visit($drive_id,$user_id)
    {
        $v = new Class_visited;
        $v->drive_id=$drive_id;
        $v->user_id=$user_id;
        $v->save();
    }

    public static function hasVisited($drive_id,$user_id)
    {
        if(Class_visited::where('drive_id',$drive_id)->where('user_id',$user_id)->count()>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
