<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Drive;

class NotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user,$subject,$content,$drive,$type,$created_at;

    public function __construct(User $user, Drive $drive,$type,$created_at, $subject, $content)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->content = $content;
        $this->drive = $drive;
        $this->type = $type;
        $this->created_at = $created_at;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->view('emails.notification')->with('user',$this->user)->with('drive',$this->drive)->with('type',$this->type)->with('created_at',$this->created_at)->with('content',$this->content);
    }
}
