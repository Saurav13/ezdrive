<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxAd extends Model
{
    public function faculties(){
        return $this->belongsToMany('App\Faculty', 'box_faculty', 'box_ad_id', 'faculty_id');
    }

    public function colleges(){
        return $this->belongsToMany('App\College', 'box_college', 'box_ad_id', 'college_id');
    }
}
