<?php 

namespace App\CollectionMailLayers;

use PhpImap;
use Storage;
use App\Drive;
use App\Collectioncontent;
use App\Collection;
use Illuminate\Http\File;

class CollectionMail
{
    const cstr_mail='{mail.projectincube.com:993/imap/ssl}';
    const submit_address='assignment@projectincube.com';
    const code='iHq73f2aS2';
    const tempMailRoot='tempMailDownload';
    public static function collect($drive_id,$collection_id)
    {
        $classcode=Drive::findOrFail($drive_id)->code;

        $mails=CollectionMail::collectMails($classcode,$collection_id);

        $tempStorePath=public_path().'\\'.self::tempMailRoot.'\\'.$collection_id;

        $files=array_diff(scandir($tempStorePath), array('..', '.'));
        foreach ($files as $f) {
            unlink($tempStorePath.'\\'.$f);
 
        }

        return 'success';

       
        
        // $files=array_diff(scandir($tempStorePath), array('..', '.'));
        // // dd($files);  
        

        // foreach ($files as $f) {
        //     $file=new File($tempStorePath.'\\'.$f);
        //     // dd($file->getExtension());
        //     $mysubmission=new Collectioncontent;
        //     $mysubmission->name=$f;
        //     $mysubmission->extension=$file->getExtension();
        //     $mysubmission->mime_type = $file->getMimeType();
        //     $mysubmission->size=$file->getSize();
        //     $mysubmission->collection_id=$collection_id;
        //     $mysubmission->submittedBy_email = CollectionMail::getEmailAddress($f,$mails);
        //     $mysubmission->save();

        //     $filename = Collection::where('id',$collection_id)->first()->g_path.'/'.$mysubmission->id;
        //     $filepath = $file->getRealPath();


        //     if(Storage::disk('google')->put($filename, fopen($filepath, 'r+')))
        //     {
        //         $dir = Collection::where('id',$collection_id)->first()->g_path;

        //         $files = collect(Storage::disk('google')->listContents($dir,false));
                
        //         $mysubmission->g_path = $files->where('filename',$mysubmission->id)->first()['basename'];
        //         $mysubmission->save();

        //         unlink($tempStorePath.'\\'.$f);
        //     }
        //     else{
        //             $mysubmission->delete();
        //             // unlink($tempStorePath.'\\'.$f);            
        //     }
        // }
        

    }
    private static function collectMails($classcode,$collection_id)
    {
        $cstr=self::cstr_mail;
        $address=self::submit_address;
        $code=self::code;

        $tempStorePath=public_path().'\\'.self::tempMailRoot.'\\'.$collection_id;

        if(!file_exists($tempStorePath))
            mkdir($tempStorePath,0777,true);
        $imap=imap_open($cstr,$address, $code);
	    $list=imap_list($imap,$cstr,"*");
 				if(array_search($cstr.'INBOX'.'.'.$classcode.'_'.$collection_id, $list))
				{
					$mailbox = new PhpImap\Mailbox($cstr.'INBOX.'.$classcode.'_'.$collection_id, $address, $code,$tempStorePath);
		    		// dd($mailbox);
		    
		    		$mails = [];
		    // Read all messaged into an array:

		    		$mailsIds = $mailbox->searchMailbox('ALL');
		    		
		    		  foreach ($mailsIds as $mailId){
				        $_mail = $mailbox->getMail($mailId);
				   		
				        $mail = array(
				            'date' => $_mail->date,
				            'fromName' => $_mail->fromName,
				            'fromAddress' => $_mail->fromAddress,
				            'subject' => $_mail->subject,
				            'content' => $_mail->textPlain,
				            'attachments' => $_mail->getAttachments()
                        );
                        // dd(array_values($_mail->getAttachments())[0]->filePath);
                        $file=new File(array_values($_mail->getAttachments())[0]->filePath);

                        $mysubmission=new Collectioncontent;
                        $mysubmission->name=(array_values($_mail->getAttachments())[0]->name);
                        $mysubmission->extension=$file->getExtension();
                        $mysubmission->mime_type = $file->getMimeType();
                        $mysubmission->size=$file->getSize();
                        $mysubmission->collection_id=$collection_id;
                        $mysubmission->submittedBy_email = $mail['fromAddress'];
                        $mysubmission->save();

                        $filename = Collection::where('id',$collection_id)->first()->g_path.'/'.$mysubmission->id;
                        $filepath = $file->getRealPath();
            
            
                        if(Storage::disk('google')->put($filename, fopen($filepath, 'r+')))
                        {
                            $dir = Collection::where('id',$collection_id)->first()->g_path;
            
                            $files = collect(Storage::disk('google')->listContents($dir,false));
                            
                            $mysubmission->g_path = $files->where('filename',$mysubmission->id)->first()['basename'];
                            $mysubmission->save();
            
                            // unlink($tempStorePath.'\\'.$f);
                        }
                        else{
                                $mysubmission->delete();
                                // unlink($tempStorePath.'\\'.$f);            
                        }

                    


				        $mailbox->deleteMail($mailId);
				        $mails []= $mail;
				    	
				    }
				    // dd($mails);
		    		// dd($mailsIds);
		    // if(!$mailsIds) {
		    //     return;
		    // }

		    		foreach ($mailsIds as $mailId){
		         		$mailbox->deleteMail($mailId);
		    	}
                
            }
 		return $mails;
    }
    private static function getEmailAddress($filename,$mails)
    {
        foreach ($mails as $mail) {
            # code...
        }
    }
}
