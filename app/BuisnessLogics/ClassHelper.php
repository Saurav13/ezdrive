<?php namespace App\BuisnessLogics;

use App\Drive;
use App\Post;
use App\Postedfile;
use App\Collection;
use App\Notice;
use App\Board;
use Storage;

class ClassHelper
{

    public static function resetDrive($class)
    {
        ClassHelper::deleteDrive($class);
        ClassHelper::deleteCollections($class);
        ClassHelper::deletePosts($class);
        ClassHelper::deleteNotices($class);
        ClassHelper::deleteStudents($class);
        ClassHelper::deleteBoards($class);
    }

    private static function deleteDrive($class){
        Storage::disk('google')->deleteDirectory($class->drive_drive_root);
        Storage::disk('google')->deleteDirectory($class->drive_collection_root);
        Storage::disk('google')->deleteDirectory($class->drive_post_root);
    }

    private static function deletePosts($class)
    {
        $posts = $class->posts;

        foreach ($posts as $p) {
            ClassHelper::deletePostedfiles($p->id);
            Storage::deleteDirectory('public/posted_files'.'/'.$p->id);
        }
    }

    private static function deleteCollections($class)
    {
        $collections = $class->collections;

        foreach($collections as $c)
        {
            Storage::delete($c->collectiontask->file);
            Storage::deleteDirectory('public/collection_tasks'.'/'.$c->id);
            $c->collectiontask->delete();
        }
    }

    private static function deleteNotices($class)
    {
        foreach($class->notices as $c)
        {
            $c->delete();
        }
    }

    private static function deletePostedfiles($id)
    {
        $postedfiles = Postedfile::where('post_id',$id)->get();
        foreach ($postedfiles as $p) {
            Storage::disk('google')->delete($p->g_path);
        }
    }

    private static function deleteStudents($class)
    {
        $class->allfollowers()->detach();
        $class->save();
    }
    private static function deleteBoards($class)
    {
        Board::where('drive_id',$class->id)->delete();
    }
}