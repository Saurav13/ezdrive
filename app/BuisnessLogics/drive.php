<?php namespace App\BuisnessLogics;


class Drive
{
    private $root;
    public function __construct()
    {
        $this->root=public_path('/../resources/assets/drive');
    }
    public function root($uid)  //open root of a teacher's drive and get files and folders.
    {
        $buff =scandir($this->root.'/'.$uid);
		$files=array();
		$folders=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
				if(strpos($b,'.')!=false)
					array_push($files, $b);
				else
					array_push($folders,$b);
			}
        }
        sort($files);
        sort($folders);
        return ['files'=>$files,'folders'=>$folders];
    }
    public function openFolder($uid,$foldername)//open particular folder of a teacher's drive and get files.
    {
        $buff =scandir($this->root.'/'.$uid.'/'.$foldername);
		$files=array();
		foreach ($buff as $b) {
			if($b!='.' and $b!='..'){
                array_push($files, $b);
			}
        }
        sort($files);
        return ['files'=>$files];
    }
}