<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultrecord extends Model
{
    protected $table = 'resultrecords';

    public function result()
    {
        return $this->belongsTo('App\Result');
    }
}
