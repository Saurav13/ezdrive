<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Account extends Model
{
    protected $table='users';
    protected $connection = 'mysql2';

    protected $hidden=['password','confirmation_code','remember_token'];
    public function user()
    {
        return User::where('email',$this->email)->first();
    }
}
