<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Drive;

class Usernotification extends Model
{
    public static function pushNotification($user_id,$drive_id,$type)
    {
        $drive=Drive::findOrFail($drive_id);

        if($type=='accepted')
        {
            $n= new Usernotification;
            $n->user_id=$user_id;
            $n->i_type='request';
            $n->text='You have been accepted in '.$drive->name;
            $n->link='/class'.'/'.$drive->slug;
            $n->save();
        }
        else if($type=='removed')
        {
            $n= new Usernotification;
            $n->user_id=$user_id;
            $n->i_type='request';
            $n->text='You have been removed from'.$drive->name;
            $n->link='/class'.'/'.$drive->slug;
            $n->save();
        }

    }
}
