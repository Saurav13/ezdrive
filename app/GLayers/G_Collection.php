<?php 

namespace App\GLayers;

use App\Collection;
use App\Collectioncontent;
use Storage;
use ZipArchive;

class G_Collection
{
    public static function deleteCollectionFiles($collection_id,$files)
    {
        $collection=Collection::findOrFail($collection_id);
        $access_token=G_Collection::getGoogleAccessToken();
    
        $ch=array();
        
        foreach($files as $f)
        {
            $url='https://www.googleapis.com/drive/v3/files/'.$f->g_path.'?access_token='.$access_token.'&token_type=Bearer';
            $ch[$f->id]= curl_init();
            curl_setopt($ch[$f->id], CURLOPT_URL, $url);
            curl_setopt($ch[$f->id], CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        $mh = curl_multi_init();
        foreach($files as $f)
        {
            curl_multi_add_handle($mh, $ch[$f->id]);
        }
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);
    
        //close the handles
        foreach($files as $f)
        {
            curl_multi_remove_handle($mh, $ch[$f->id]);
        }
        curl_multi_close($mh);
        $badArray=G_Collection::getBadResponseArray($ch);
        while(sizeof($badArray)>0)
        {
            $badArray=array();
            $badArray=G_Collection::resendBadResponse($badArray);   
            
        }
        
        return "success";
        
    }

   
   
    public static function collectionDownloadResponse($collection_id,$files)
    {
        $collection=Collection::findOrFail($collection_id);
        $access_token=G_Collection::getGoogleAccessToken();
        
        $ch=array();
        $fp=array();
    
        $tempDownloadDir=public_path() . '\\tempCollectionDownload\\'.$collection_id;
        if(!file_exists($tempDownloadDir))
            mkdir($tempDownloadDir,0777,true);
        foreach($files as $f)
        {
            $ch[$f->id]= curl_init('https://www.googleapis.com/drive/v3/files/'.$f->g_path.'?alt=media&access_token='.$access_token.'&token_type=Bearer');
             $fp[$f->id] = fopen (public_path() . '\\tempCollectionDownload\\'.$collection_id.'\\'.$f->id.$f->name, 'w');
        
            curl_setopt($ch[$f->id], CURLOPT_FILE, $fp[$f->id]);
            curl_setopt($ch[$f->id], CURLOPT_FOLLOWLOCATION, true);
        }
        $mh = curl_multi_init();
        foreach($files as $f)
        {
            curl_multi_add_handle($mh, $ch[$f->id]);
        }
        // curl_multi_add_handle($mh, $ch_2);
        
        // execute all queries simultaneously, and continue when all are complete
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);
    
        
        
        //close the handles
        foreach($files as $f)
        {
            curl_multi_remove_handle($mh, $ch[$f->id]);
            fclose($fp[$f->id]);

        }
        $badArray=G_Collection::removeBadFiles($ch,$collection_id,$files);
        // curl_multi_remove_handle($mh, $ch_2);
        curl_multi_close($mh);
        
        $responses =array();
        
        foreach ($ch as $c) {
            array_push($responses,curl_getinfo($c));
        }
        while(sizeof($badArray)>0)
        {
            $badArray=G_Collection::reRequestBadResponse($badArray,$collection_id);
            
        }

        // dd($responses);
        // return "success";
        
        // G_Collection::checkForBadResponse($responses);


        $zip = new ZipArchive;

        $public_dir=public_path().'\tempCollectionDownload'.'\\'.$collection_id;
        // dd($public_dir);
        if(!file_exists($public_dir))
            mkdir($public_dir,0777,true);
        // dd("Asd");
        
        $files=array_diff(scandir($public_dir), array('..', '.'));
        
        $zipFileName = $collection->name.'.zip';
        // dd($zipFileName);
        $zip->open($public_dir . '\\' . $zipFileName, ZipArchive::CREATE) ;
        // $zip->close();
        // dd($zip);
       // Add File in ZipArchive
        //  dd($zip);
        $zip->addFromString('.txt', 'hello');
            
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ); 

        foreach ($files as $f) {
            $zip->addFile(public_path() . '/tempCollectionDownload/'.$collection_id.'/'.$f, $f);
        }
        $zip->close();
        $filetopath=$public_dir.'/'.$zipFileName;
        $file = file_get_contents($filetopath);
        unlink($filetopath);
        return response($file, 200)
        ->header('Content-Type', 'application/octet-stream')
        ->header('X-FolderName', $collection->name.'.zip')
        ->header('X-FolderId', $collection->id)
        ->header('Content-Disposition', "attachment; filename='$collection->name''");

    }
    private static function getGoogleAccessToken()
    {
        $adapter = Storage::disk('google')->getDriver()->getAdapter();
        $service = $adapter->service;
        $client = $service->getClient();
        $token = $client->getAccessToken();
        $access_token = '';
        if (is_array($token)) {
            $access_token = $token['access_token'];
        } else {
            if ($token = @json_decode($client->getAccessToken())) {
                $access_token = $token->access_token;
            }
        }
        return $access_token;
    }
    private static function checkForBadResponse($responses)
    {
        foreach ($responses as $response) {
            if($response['http_code']!=200)
                return 0;
        }
        return 1;
    }

    private static function removeBadFiles($ch,$id,$files)
    {
        $badArray=array();
        foreach ($ch as $c) {
            $response=curl_getinfo($c);
            if($response['http_code']!=200)
            {
                $f_id=array_search($c, $ch);
                $f_name=$files->where('id',$f_id)->first()->name;
                //unlink(public_path() . '\\tempCollectionDownload\\'.$id.'\\'.$f_id.$f_name);
                array_push($badArray,$f_id);
            }
        }
        return $badArray;
    }

    private static function reRequestBadResponse($badArray,$id)
    {
        $files=Collectioncontent::whereIn('id', $badArray)->get();


        $adapter = Storage::disk('google')->getDriver()->getAdapter();

        $service = $adapter->service;
        $client = $service->getClient();
        $token = $client->getAccessToken();
        $access_token = '';
        if (is_array($token)) {
            $access_token = $token['access_token'];
        } else {
            if ($token = @json_decode($client->getAccessToken())) {
                $access_token = $token->access_token;
            }
        }

        $ch=array();
        $fp=array();
    
        foreach($files as $f)
        {
            $ch[$f->id]= curl_init('https://www.googleapis.com/drive/v3/files/'.$f->g_path.'?alt=media&access_token='.$access_token.'&token_type=Bearer');
                $fp[$f->id] = fopen (public_path() . '/tempCollectionDownload/'.$id.'/'.$f->id.$f->name, 'w');
        
            curl_setopt($ch[$f->id], CURLOPT_FILE, $fp[$f->id]);
            curl_setopt($ch[$f->id], CURLOPT_FOLLOWLOCATION, true);
        }
        $mh = curl_multi_init();
        foreach($files as $f)
        {
            curl_multi_add_handle($mh, $ch[$f->id]);
        }
        // curl_multi_add_handle($mh, $ch_2);
        
        // execute all queries simultaneously, and continue when all are complete
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);
    
        
        
        //close the handles
        foreach($files as $f)
        {
            curl_multi_remove_handle($mh, $ch[$f->id]);
            fclose($fp[$f->id]);

        }
        $badArray= G_Collection::removeBadFiles($ch,$id,$files);
        
        return $badArray;
    }
    private static function getBadResponseArray($chs)
    {
        $badArray=array();
        foreach ($chs as $c) {
            $response=curl_getinfo($c);
            if($response['http_code']!=200)
            {
                array_push($badArray,$c);
            }
        }
        return $badArray;
    }
    private static function resendBadResponse($ch)
    {
        $mh = curl_multi_init();
        foreach($ch as $c)
        {
            curl_multi_add_handle($mh, $c);
        }
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);
    
        //close the handles
        foreach($ch as $c)
        {
            curl_multi_remove_handle($mh, $c);
        }
        curl_multi_close($mh);
        return G_Collection::getBadResponseArray($ch);
    }

}