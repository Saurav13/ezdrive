<?php namespace App\ResponseLayers\DrivePanel;

use App\Drivecontent;
use App\Collectioncontent;
use App\Collection;
use App\Postedfile;
use Session;
use App\Post;
use App\Drive;
use App\Notice;
use App\Result;
use App\Board;
use Auth;

class DriveResponse
{
    public static function response($data,$drive_id)
    {

        $user_id=Auth::user()->id;

        $drivespace=DriveResponse::getDriveSpace($drive_id);
        $collectionspace=DriveResponse::getCollectionSpace($drive_id);
        $postspace=DriveResponse::getPostSpace($drive_id);
        $totalspace=DriveResponse::getTotalSpace($user_id);

        $collections=Collection::where('drive_id',$drive_id)->get();
        $coll_ids=$collections->pluck('id');
        $posts=Post::where('drive_id',$drive_id)->get();
        $post_ids=$posts->pluck('id');

        $data['drivespace']=$drivespace;
        $data['collectionspace']=$collectionspace;
        $data['postspace']=$postspace;
        $data['totalspace']=$totalspace;
        $data['totalAssignments']=$collections->count();
        $data['totalSubmittedAssignments']=Collectioncontent::whereIn('collection_id',$coll_ids)->count();
        $data['NDriveFiles']=Drivecontent::where('drive_id',$drive_id)->count();
        $data['NPostedFiles']=Postedfile::whereIn('post_id',$post_ids)->count();
        $data['NPosts']=$posts->count();
        $data['NNotices']=Notice::where('drive_id',$drive_id)->count();
        $data['NBoards']=Board::where('drive_id',$drive_id)->count();
        $data['NPublishedResults']=Result::whereIn('collection_id',$coll_ids)->count();
        $data['totalStudents']= Drive::findOrFail($drive_id)->followers->count();
       
        return json_encode($data);
    }

    private static function getDriveSpace($drive_id)
    {
        return Drivecontent::where('drive_id',$drive_id)->get()->unique('g_path')->sum('size');
    }

    private static function getCollectionSpace($drive_id)
    {
        $collections=Collection::where('drive_id',$drive_id)->get();
        $space=0;
        foreach ($collections as $c) {
            $space+=$c->collectioncontents()->get()->sum('size');
        }
        return $space;
    }
    private static function getPostSpace($drive_id)
    {
        $posts=Post::where('drive_id',$drive_id)->get();
    
        $space=0;
        foreach ($posts as $c) {
            $space+=$c->postedfiles()->get()->sum('size');
        }
        return $space;
    }
    public static function getTotalSpace($user_id)
    {
        $totalDriveSpace=0;
        $totalCollectionSpace=0;
        $totalPostSpace=0;
        
        $drives=Drive::where('user_id',$user_id)->get();
        
        foreach ($drives as $d) {
            $totalDriveSpace+=DriveResponse::getDriveSpace($d->id);
            $totalCollectionSpace+=DriveResponse::getCollectionSpace($d->id);
            $totalPostSpace+=DriveResponse::getPostSpace($d->id);
        }
        return $totalDriveSpace+$totalCollectionSpace+$totalPostSpace;
    }
}