<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Drive extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['name', 'fullname']
            ]
        ];
    }

    public function getFullnameAttribute(){
        return 'by '. Account::where('email',$this->owner->email)->first()->name;
    }

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function followers(){
        return $this->belongsToMany('App\User')->wherePivot('confirmed', 1)->withPivot('rollno');
    }

    public function unapprovedfollowers(){
        return $this->belongsToMany('App\User')->wherePivot('confirmed', 0);
    }

    public function allfollowers(){
        return $this->belongsToMany('App\User');
    }

    public function collections(){
        return $this->hasMany('App\Collection');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function boards(){
        return $this->hasMany('App\Board');
    }

    public function notices(){
        return $this->hasMany('App\Notice');
    }

    public function drive_contents(){
        return $this->hasMany('App\Drivecontent');
    }
}
