<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    public function collectioncontents()
    {
        return $this->hasMany('App\Collectioncontent');
    }

    public function collectiontask()
    {
        return $this->hasOne('App\Collectiontask','id','id');
    }
   
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    public function drive(){
        return $this->belongsTo('App\Drive');
    }
}
