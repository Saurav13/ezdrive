<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collectiontask extends Model
{
    public function collection()
    {
        return $this->belongsTo('App\Collection','id','id');
    }
}
