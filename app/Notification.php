<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function drive()
    {
        return $this->belongsTo('App/Drive');
    }
   
    public static function pushNotificationClass($drive_id,$class,$cname)
    {
        $drive=Drive::findOrFail($drive_id);
        $username=$drive->owner->data()->name;
        $drivename=$drive->name;
        $driveslug=$drive->slug;
        
        if($class=='drive_uploads')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='drive';
            $n->type='Class';
            $n->text=$username.' uploaded some files in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='new_task')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='task';
            $n->type='Class';
            $n->text=$username.' assigned new task in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='new_post')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='post';
            $n->type='Class';
            $n->text=$username.' posted in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='task_close')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='task';
            $n->type='Class';
            $n->text= '"'.$cname.'" is now closed in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='task_reopen')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='task';
            $n->type='Class';
            $n->text='"'.$cname.'" re-opened in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='publish_result')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='notice';
            $n->type='Class';
            $n->text='"'.$cname.'" result published in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='new_notice')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='notice';
            $n->type='Class';
            $n->text='You have new notice in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();

        }
        else if($class=='task_edit')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='task';
            $n->type='Class';
            $n->text='"'.$cname.'" was edited in "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='class_rename')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='task';
            $n->type='Class';
            $n->text='"'.$cname.'" was renamed to "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='new_request')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='request';
            $n->type='Drive';
            $n->text=$cname.' wants to join "'.$drivename.'"';
            $n->link='/class'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='cancel_request')
        {
            Notification::where('drive_id',$drive_id)
                        ->where('i_type','request')
                        ->where('type','Drive')
                        ->where('text', $cname.' wants to join '.$drivename)
                        ->where('link','/class'.'/'.$driveslug)
                        ->delete();
        }
        else if($class=='task_deadline')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='reminder';
            $n->type='Drive';
            $n->text='"'.$cname.'" deadline has been reached in "'.$drivename.'"';
            $n->link='/user/mydrive'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='space_usage')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='reminder';
            $n->type='Drive';
            $n->text='Space almost full in "'.$drivename.'"';
            $n->link='/user/mydrive'.'/'.$driveslug;
            $n->save();
        }
        else if($class=='reset_prompt')
        {
            $n= new Notification;
            $n->drive_id=$drive_id;
            $n->i_type='reminder';
            $n->type='Drive';
            $n->text='It\'s time to reset "'.$drivename.'"';
            $n->link='/user/mydrive'.'/'.$driveslug;
            $n->save();
        }
       
    }
}
