<?php

namespace App;


use Illuminate\Support\Facades\Auth;
use Jasny\SSO\Broker;
use Jasny\SSO\Exception;
use Jasny\SSO\NotAttachedException;
use App\User;
use Illuminate\Http\Request;
use Storage;
use Validator;
use Cookie;

class MyBroker extends Broker
{
    public function __construct()
    {
        parent::__construct(env('SSO_SERVER_URL'),env('SSO_BROKER_ID'),env("SSO_BROKER_SECRET"));
        $this->attach(true);
    }

    protected function request($method, $command, $data = null)
    {
        if (!$this->isAttached()) {
            throw new NotAttachedException('No token');
        }
        $url = $this->getRequestUrl($command, !$data || $method === 'POST' ? [] : $data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json', 'Authorization: Bearer '. $this->getSessionID()]);
        
        if ($method === 'POST' && !empty($data)) {
            $post = is_string($data) ? $data : http_build_query($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_HEADER, 1);
        $response = curl_exec($ch);
        // dd($response);
        list($header, $body) = explode("\r\n\r\n", $response, 2);
        
        if($command == 'login' || $command == 'confirmUser' || $command == 'social' || $command == 'resetPassword' || $command == 'logout')
        {
            $remember_token = null;

            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
            
            $cookies = array();
            foreach($matches[1] as $item) {
                parse_str($item, $cookie);
                $cookies = array_merge($cookies, $cookie);

                if(strpos(key($cookie), 'remember_') !== false){
                    $remember_token = $cookie;
                }
            }
            if($remember_token){
                $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
                $decryptedString = $encrypter->decrypt($remember_token[key($remember_token)]);

                Cookie::queue(key($remember_token), $decryptedString,2628000);
            }
        }

        if (curl_errno($ch) != 0) {
            $message = 'Server request failed: ' . curl_error($ch);
            throw new Exception($message);
        }

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        list($contentType) = explode(';', curl_getinfo($ch, CURLINFO_CONTENT_TYPE));

        $data = json_decode($body, true);
        if ($httpCode == 403) {
            $this->clearToken();
            
            if(array_key_exists('error',$data))
                throw new NotAttachedException($data['error'] ?: $body, $httpCode);
            elseif(array_key_exists('exception',$data))
                throw new NotAttachedException($data['exception'] ?: $body, $httpCode);
        }
        if ($httpCode >= 400){
            if($command == 'register' && $httpCode == 422){
                return -1;
            }
            elseif($command == 'setEmail' && $httpCode == 422){
                return -1;
            }
            elseif($command == 'login'){
                return;
            }
            // dd($data);
            if(array_key_exists('error',$data))
                throw new NotAttachedException($data['error'] ?: $body, $httpCode);
            elseif(array_key_exists('exception',$data))
                throw new NotAttachedException($data['exception'] ?: $body, $httpCode);
            
        }
        
        return $data;
    }

    public function loginUser($username, $password){
        try{
            $result = $this->login($username, $password);
            
            $this->userinfo = $result;
            return $this->userinfo;
        }
        catch(NotAttachedException $e){
            return false;
        }
        catch(Exception $e){
            return false;
        }
        
        return true;
    }

    public function registerUser(Request $request){
        try{
            $data = $request->all();
            $result = $this->request('POST', 'register', $data);
            if($result == -1){
                return -1;
            }
            $this->userinfo = $result;
            return $this->userinfo;
        }
        catch(NotAttachedException $e){
            return false;
        }
        catch(Exception $e){
            return false;
        }
        
        return true;
    }

    public function socialLoginUrl($provider){
        try{
            $returnUrl = url('/').'/signin/'.$provider.'/callback';
            $result = $this->request('POST', 'sociallogin', compact('provider','returnUrl'));
            return $result;
        }
        catch(NotAttachedException $e){
            return false;
        }
        catch(Exception $e){
            return false;
        }
        
        return true;
    }

    public function socialLogin(Request $request,$provider){
        $code = $request->code;
        $state = $request->state;
        $returnUrl = url('/').'/signin/'.$provider.'/callback';
        $result = $this->request('POST', 'social', compact('provider','code','state','returnUrl'));
        return $result;
    }

    public function checkUserEmail($email){
        $result = $this->request('POST', 'checkUserEmail', compact('email'));
        return $result;
    }

    public function resetPassword($email,$password){
        $result = $this->request('POST', 'resetPassword', compact('email','password'));
        return $result;
    }

    public function confirmUser($token){
        $result = $this->request('POST','confirmUser',compact('token'));
        return $result;
    }

    public function setEmail($provider,$provider_id,$email){
        $result = $this->request('POST','setEmail',compact('provider','provider_id','email'));
        
        return $result;
    }

    public function checklogin1($email){
        // $email = Auth::user()->email;
        return $this->request('POST','checklogin1',compact('email'));
    }

    public function checklogin($id,$token){
        return $this->request('POST','checklogin',compact('id','token'));
    }

    public function loginCurrentUser($returnUrl = '/home'){
        
        if($user = $this->getUserInfo()){

            $validator = Validator::make($user, [
                'verified' => 'accepted'
            ]);
    
            if ($validator->fails()) {
                $validator->errors()->add('email', 'Your account is not verified yet. (<a href="'.route('resend_confirmation',array('email' => $user["email"] )).'" style="color:#4545c8">Resend</a> verification email?)');
                return redirect()->to('/login')->withErrors($validator)->withInput();
            }

            $existing = User::where('email', $user['email'])->first();
        
            if(!$existing){
                $existing= User::create([
                    'id' => $user['id'],
                    'email' => $user['email'],
                ]);
        
                if(Storage::disk('google')->makeDirectory(env('DRIVE_G_ROOT').'/'.$existing->id))
                {
                    $roots=collect(Storage::disk('google')->listcontents(env('DRIVE_G_ROOT'),false));
                    // dd($roots->where('filename',$existing->id)->first());
                    $existing->user_drive_root=$roots->where('filename',$existing->id)->first()['path'];
                }
                else{
                    $existing->where('id',$existing->id)->delete();
                    abort(404);
                }
        
                if(Storage::disk('google')->makeDirectory(env('COLLECTION_G_ROOT').'/'.$existing->id))
                {
                    $roots=collect(Storage::disk('google')->listcontents(env('COLLECTION_G_ROOT'),false));
                    $existing->user_collection_root=$roots->where('filename',$existing->id)->first()['path'];
                }
                else{
                    $existing->where('id',$existing->id)->delete();
                    abort(404);
                }
                $existing->save();
            }

            Auth::loginUsingId($existing->id);
            
            return redirect($returnUrl);
        }
    }
}