<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drive_ad extends Model
{
    protected $table='drive_ads';
    public function bannerAds()
    {
        return $this->belongsTo('App\BannerAd','banner_ads_id','id');
    }
}
