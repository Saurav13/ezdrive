<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function banner_ads(){
        return $this->hasMany('App\BannerAd');
    }
}
