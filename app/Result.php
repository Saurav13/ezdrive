<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    public function resultrecords()
    {
        return $this->hasMany('App\Resultrecord');
    }
    public function collection()
    {
        
        return $this->belongsTo('App\Collection');;
    }
}

