<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use App\Account;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function drives(){
        return $this->hasMany('App\Drive');
    }

    public function followed_drives(){
        return $this->belongsToMany('App\Drive')->wherePivot('confirmed', 1)->withPivot('rollno');
    }

    public function pending_drives(){
        return $this->belongsToMany('App\Drive')->wherePivot('confirmed', 0);
    }

    public function records(){
        return $this->hasMany('App\Resultrecord');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function data()
    {
        return Account::where('email',$this->email)->first();
    }
}
