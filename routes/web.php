<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Drive;

Route::get('/oldhome',function(){
    $drives=Drive::where('user_id',Auth::user()->id)->get();
    return view('frontend.oldhome',compact('drives'));
});

Route::get('/test',function(){
    return view('test');
});

Route::get('/reminder','LandingController@reminder');

// Route::get('/testpdf',function(){
//     // $pdf = PDF::loadView('templates.result');
//     // return $pdf->download('result.pdf');
//     $html = View::make('templates.result')->render();
//     PDF::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save('myfile.pdf');
// });


Route::get('/testresult',function(){
    return view('templates.result');
});

Route::get('/progress',function(){
    return view('testpdf');
});

Route::post('/uploadUrl',function(){
    sleep(10);
    return 'success';
});


Route::get('/teachersguideline',function(){
    return view('frontend.static.teachersguideline');
});
Route::get('/studentsguideline',function(){
    return view('frontend.static.teachersguideline');
});


Route::get('/user/getnotifications','HomeController@getNotifications');
Route::get('/user/allnotifications','HomeController@allNotifications');

Route::get('/user/classpanel','Frontend\Drivepanel\DrivePanelController@index');
Route::get('/user/drivepanel/{drive_name}/studentsview','Frontend\Drivepanel\DriveSlugController@studentsView');

Route::post('/user/createdrive','Frontend\Drivepanel\DrivePanelController@create');

Route::get('/user/mydrive/{drive_id}','Frontend\Drivepanel\DriveSlugController@myDrive')->name('mydrive');
Route::post('/user/drivepanel/{drive_id}/edit','Frontend\Drivepanel\DrivePanelController@editDrive');
Route::post('/user/drivepanel/{drive_id}/resetdrive','Frontend\Drivepanel\DrivePanelController@resetDrive');
Route::post('user/drive/{drive_slug}/delete','Frontend\Drivepanel\DrivePanelController@deleteDrive');

Route::post('/user/board/{drive_id}/change','Frontend\Drivepanel\BoardController@change');
Route::post('/user/board/{drive_id}/save','Frontend\Drivepanel\BoardController@store');
Route::post('/user/board/{drive_id}/delete','Frontend\Drivepanel\BoardController@destroy');

Route::post('/user/drive/{drive_id}/getroot','Frontend\Drivepanel\DriveController@openRoot');
Route::post('/user/drive/{drive_id}/addfolder','Frontend\Drivepanel\DriveController@addFolder');
Route::post('/user/drive/{drive_id}/openfolder','Frontend\Drivepanel\DriveController@openFolder');
Route::post('/user/drive/{drive_id}/back','Frontend\Drivepanel\DriveController@back');
Route::post('/user/drive/{drive_id}/uploadfile','Frontend\Drivepanel\DriveController@uploadFile');
Route::post('/user/drive/{drive_id}/renamefolder','Frontend\Drivepanel\DriveController@renameFolder');
Route::post('/user/drive/{drive_id}/deletefolder','Frontend\Drivepanel\DriveController@deleteFolder');
Route::post('/user/drive/{drive_id}/deletefile','Frontend\Drivepanel\DriveController@deleteFile');
Route::get('/user/drive/{drive_id}/downloadfile/{file_id}/{file_name}','Frontend\DrivePanel\DriveController@downloadFile');
Route::get('/user/drive/{drive_id}/downloadfolder/{file_id}/{file_name}','Frontend\DrivePanel\DriveController@downloadFolder');
Route::post('/user/drive/{drive_id}/openfile','Frontend\Drivepanel\DriveController@openFile');
Route::post('/user/drive/{drive_id}/searchdrive','Frontend\Drivepanel\DriveController@searchDrive');
Route::post('/user/drive/{drive_id}/importinit','Frontend\Drivepanel\DriveController@importInit');
Route::post('/user/drive/{drive_id}/importselectedfiles','Frontend\Drivepanel\DriveController@importSelectedFiles');
Route::post('/user/drive/{drive_id}/getfilelink','Frontend\Drivepanel\DriveController@getFileLink');

Route::post('/user/collection/{drive_id}/getcollections','Frontend\Drivepanel\CollectionController@getCollections');
Route::post('/user/collection/{drive_id}/addcollection','Frontend\Drivepanel\CollectionController@addCollection');
Route::post('/user/collection/{drive_id}/opencollection','Frontend\Drivepanel\CollectionController@openCollection');
Route::post('/user/collection/{drive_id}/editdeadline','Frontend\Drivepanel\CollectionController@editDeadline');
Route::post('/user/collection/{drive_id}/edittask','Frontend\Drivepanel\CollectionController@editTask');
Route::get('/user/collection/{drive_id}/downloadcollection/{file_id}/{file_name}','Frontend\DrivePanel\CollectionController@download');
Route::get('/user/collection/{drive_id}/downloadfile/{file_id}/{file_name}','Frontend\DrivePanel\CollectionController@downloadfile');
Route::post('/user/collection/{drive_id}/storefileinsession','Frontend\Drivepanel\CollectionController@storeFileInSession');
Route::post('/user/collection/{drive_id}/deletecollection','Frontend\Drivepanel\CollectionController@deleteCollection');
Route::post('/user/collection/{drive_id}/closecollection','Frontend\Drivepanel\CollectionController@closeCollection');
Route::post('/user/collection/{drive_id}/renamecollection','Frontend\Drivepanel\CollectionController@renameCollection');
Route::post('/user/collection/{drive_id}/publishresult','Frontend\Drivepanel\CollectionController@publishResult');
Route::get('/user/collection/{drive_id}/{c_id}/viewresult','Frontend\Drivepanel\CollectionController@viewResult');
Route::post('/user/collection/{drive_id}/getsubmissionlink','Frontend\Drivepanel\CollectionController@getSubmissionLink');
Route::post('/user/collection/{drive_id}/savegrade','Frontend\Drivepanel\CollectionController@saveGrade');




// Route::post('/user/collection/{drive_id}/zipdownload','Frontend\Drivepanel\CollectionController@zipDownload');
// Route::post('/user/collection/{drive_id}/download2','Frontend\Drivepanel\CollectionController@zipDownload2');
// Route::post('/user/collection/{drive_id}/downloadcurl','Frontend\Drivepanel\CollectionController@zipDownload2_curl');


Route::post('/user/post/{drive_id}/getposts','Frontend\Drivepanel\PostController@getPosts');
Route::post('/user/post/{drive_id}/post','Frontend\Drivepanel\PostController@post');
Route::post('/user/post/{drive_id}/viewmoreposts','Frontend\Drivepanel\PostController@viewMorePosts');
Route::post('/user/post/{drive_id}/postnotice','Frontend\Drivepanel\PostController@postNotice');
Route::post('/user/post/{drive_id}/savenotice','Frontend\Drivepanel\PostController@saveNotice');
Route::post('/user/post/{drive_id}/loadnotices','Frontend\Drivepanel\PostController@loadNotices');
Route::post('/user/post/{drive_id}/updatepost','Frontend\Drivepanel\PostController@updatePost');
Route::post('/user/post/{drive_id}/deletepost','Frontend\Drivepanel\PostController@deletePost');
Route::post('/user/post/{drive_id}/getfilelink','Frontend\Drivepanel\PostController@getFileLink');

Route::post('/user/manager/{drive_id}/init','Frontend\Drivepanel\ManagerController@init');
Route::post('/user/manager/{drive_id}/accept','Frontend\Drivepanel\ManagerController@accept');
Route::post('/user/manager/{drive_id}/ignore','Frontend\Drivepanel\ManagerController@ignore');
Route::post('/user/manager/{drive_id}/removestudent','Frontend\Drivepanel\ManagerController@removeStudent');
Route::post('/user/manager/{drive_id}/allstudents','Frontend\Drivepanel\ManagerController@allStudents');
Route::post('/user/manager/{drive_id}/removestudentfromall','Frontend\Drivepanel\ManagerController@removeStudentFromAll');
Route::post('/user/manager/{drive_id}/toggledriveaccess','Frontend\Drivepanel\ManagerController@toggleDriveAccess');
Route::post('/user/manager/{drive_id}/sendInvitation','Frontend\Drivepanel\ManagerController@sendInvitation');
Route::post('/user/manager/{drive_id}/searchUser','Frontend\Drivepanel\ManagerController@getUser');
Route::post('/user/manager/{drive_id}/loadresults','Frontend\Drivepanel\ManagerController@loadResults');
Route::get('/user/manager/{drive_id}/viewsummaryresult','Frontend\Drivepanel\ManagerController@viewSummaryResult');
Route::get('/class/{slug}/invitation/accept','HomeController@acceptInvitation')->middleware('signedurl')->middleware('auth');
// the above {id} is drive_id


Route::get('/', function () {
    return view('frontend.landing');
})->middleware('sso-check')->middleware('guest');

Route::get('/explore', function () {
    return view('frontend.explore');
})->middleware('sso-check');


Route::get('/searchbycode','LandingController@searchByCode')->middleware('sso-check');

Route::get('/home','HomeController@index')->name('home')->middleware('sso-check');

// Route::get('/profile',function(){
//     return view('frontend.profile');
// });

// Route::get('/class',function(){
//     return view('frontend.class');
// });

// Route::get('/allsearch','Frontend\ClassRoom\ClassController@search');

Route::get('/searchmore','HomeController@searchMoreUser')->name('searchmore');
Route::get('/search','HomeController@searchUser')->name('search');
Route::get('/getCollegeNames','HomeController@getCollegeNames');
Auth::routes();
Route::get('signin/{provider}', 'Auth\LoginController@loginProviderUrl');
Route::get('signin/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('confirm/resend','Auth\RegisterController@resendConfirmation')->name('resend_confirmation');
Route::get('confirm/user/{token}','Auth\LoginController@confirmEmail')->name('confirmEmail');
Route::post('signup/getEmail','Auth\LoginController@setEmail')->name('set_email');

Route::post('/unauth/{drive_id}/drive/getroot','Frontend\UnAuthController@openRoot');
Route::post('/unauth/{drive_id}/drive/openfolder','Frontend\UnAuthController@openFolder');
Route::post('/unauth/{drive_id}/drive/back','Frontend\UnAuthController@back');
Route::post('/unauth/{drive_id}/drive/searchdrive','Frontend\UnAuthController@searchDrive');

Route::get('/class/{drive_name}','Frontend\ClassRoom\ClassController@class')->name('class');
Route::post('/follow/{drive_id}','Frontend\ClassRoom\ClassController@followdrive')->name('followdrive');


Route::post('/class/{drive_id}/generalinit','Frontend\ClassRoom\GeneralController@generalInit');
Route::get('/class/{drive_id}/{result_id}/viewresult','Frontend\ClassRoom\GeneralController@viewResult');

Route::post('/class/{drive_id}/board/change','Frontend\ClassRoom\GeneralController@change');

Route::post('/class/{drive_id}/notice/loadnotices','Frontend\ClassRoom\NoticeStuffController@loadNotices');
Route::post('/class/{drive_id}/notice/morenotices','Frontend\ClassRoom\NoticeStuffController@viewMoreNotices');

Route::post('/class/{drive_id}/drive/getroot','Frontend\ClassRoom\DriveController@openRoot');
Route::post('/class/{drive_id}/drive/openfolder','Frontend\ClassRoom\DriveController@openFolder');
Route::post('/class/{drive_id}/drive/getfilelink','Frontend\ClassRoom\DriveController@getFileLink');
Route::post('/class/{drive_id}/drive/back','Frontend\ClassRoom\DriveController@back');
Route::post('/class/{drive_id}/drive/downloadfolder','Frontend\ClassRoom\DriveController@zipFolder');
// Route::post('/class/{drive_id}/drive/downloadfile','Frontend\ClassRoom\DriveController@downloadFile1');
Route::post('/class/{drive_id}/drive/searchdrive','Frontend\ClassRoom\DriveController@searchDrive');

Route::get('/class/{drive_id}/drive/downloadfile/{file_id}/{file_name}','Frontend\ClassRoom\DriveController@downloadFile');
Route::get('/class/{drive_id}/drive/downloadfolder/{file_id}/{file_name}','Frontend\ClassRoom\DriveController@downloadFolder');

Route::post('/class/{drive_id}/task/gettasks','Frontend\ClassRoom\TaskController@loadTasks');
Route::post('/class/{drive_id}/task/viewmoretasks','Frontend\ClassRoom\TaskController@viewMoreTasks');
Route::post('/class/{drive_id}/task/getfilelink','Frontend\ClassRoom\TaskController@getFileLink');
Route::get('/class/{drive_id}/task/downloadfile/{file_id}/{file_name}','Frontend\ClassRoom\TaskController@downloadFile');
Route::post('/class/{drive_id}/task/sendSubmission','Frontend\ClassRoom\TaskController@sendSubmission');
Route::post('/class/{drive_id}/task/getsubmissionlink','Frontend\ClassRoom\TaskController@getSubmissionLink');
Route::get('/class/{drive_id}/task/downloadsubmission/{file_id}/{file_name}','Frontend\ClassRoom\TaskController@downloadSubmission');
Route::post('/class/{drive_id}/task/editSubmission','Frontend\ClassRoom\TaskController@editSubmission');

Route::post('/class/{drive_id}/post/getposts','Frontend\ClassRoom\PostController@getPosts');
Route::post('/class/{drive_id}/post/viewmoreposts','Frontend\ClassRoom\PostController@viewMorePosts');
Route::post('/class/{drive_id}/post/getfilelink','Frontend\ClassRoom\PostController@getFileLink');
Route::get('/class/{drive_id}/post/downloadfile/{file_id}/{file_name}','Frontend\ClassRoom\PostController@downloadFile');
//test urls
// Route::get('/class/{drive_id}/openfile/{file_id}/{file_name}',function ($drive_id,$id,$name,Request $request){
//     if(Session::get($id) == 'OK')
//     {
//         $dirfile = Drivecontent::where('g_path',$id)->first();
        
//         $rawData = Storage::disk('google')->get($dirfile->g_path);
//         Session::put($id, 'NO');
//         return Response::make($rawData, 200, array('content-type'=>$dirfile->mime_type,'Content-Disposition', "attachment; filename='$dirfile->name'"));
//     }
// });

Route::get('/class/{drive_id}/downloadfile/{file_id}','Frontend\ClassRoom\DriveController@downloadFile1');

Route::get('/class/{drive_id}/drive/openfile/{file_id}/{file_name}','Frontend\FileController@openDriveFile')->middleware('signedurl');
Route::get('/class/{drive_id}/task/openfile/{file_id}/{file_name}','Frontend\FileController@openTaskFile')->middleware('signedurl');
Route::get('/class/{drive_id}/task/opensubmission/{file_id}/{file_name}','Frontend\FileController@openSubmission')->middleware('signedurl');
Route::get('/class/{drive_id}/post/openfile/{file_id}/{file_name}','Frontend\FileController@openPostFile')->middleware('signedurl');


Route::get('user/{user_name}','Frontend\ClassRoom\ClassController@publicprofile')->name('publicprofile');

Route::prefix('admin')->group(function(){
    Route::get('/login','Admin\Auth\LoginController@showLoginForm')->name('admin_login');
	Route::post('/login','Admin\Auth\LoginController@login');
	Route::post('/logout','Admin\Auth\LoginController@logout')->name('admin_logout');
	Route::post('/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	Route::post('/password/reset','Admin\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	Route::get('/password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
    
    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');

    Route::resource('categories', 'Admin\CategoryController',['except' => ['create','edit','show']]);
    Route::resource('faculty', 'Admin\FacultyController',['except' => ['create','edit','show']]);

    Route::get('/colleges/search','Admin\CollegeController@search');
    Route::resource('colleges', 'Admin\CollegeController',['except' => ['create','edit','show']]);

    Route::get('classes/searchName', 'Admin\ClassController@searchByName')->name('classes.searchByName');
    Route::get('classes/searchCode', 'Admin\ClassController@searchByCode')->name('classes.searchByCode');
    Route::resource('classes', 'Admin\ClassController',['except' => ['create','store','edit','show']]);

    Route::get('bannerAds/expired', 'Admin\BannerAdController@expiredAds')->name('bannerAds.expiredAds');
    Route::resource('bannerAds', 'Admin\BannerAdController',['except' => ['create','show']]);

    Route::get('boxAds/expired', 'Admin\BoxAdController@expiredAds')->name('boxAds.expiredAds');
    Route::resource('boxAds', 'Admin\BoxAdController',['except' => ['create','show']]);

    Route::post('albums/{id}/addImages','Admin\AlbumController@addImages')->name('albums.addImages');
    Route::delete('albums/{album_id}/deleteImage/{image_id}','Admin\AlbumController@deleteImage')->name('albums.deleteImage');
    Route::resource('albums', 'Admin\AlbumController',['except' => ['create','edit']]);

    Route::get('settings','Admin\SettingsController@index')->name('admin.settings');
    Route::post('settings/addImage','Admin\SettingsController@addImage')->name('admin.settings.addImage');
    Route::delete('settings/removeImage/{id}','Admin\SettingsController@removeImage')->name('admin.settings.removeImage');
    Route::post('settings/contactUpdate','Admin\SettingsController@contactUpdate')->name('admin.settings.contactUpdate');
    Route::post('settings/aboutUpdate','Admin\SettingsController@aboutUpdate')->name('admin.settings.aboutUpdate');
    
    Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
    Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');

    Route::get('contact-us-messages/unseen','Admin\ContactUsMessageController@unseenMsg')->name('contact-us-messages.unseen');    
    Route::get('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@reply')->name('contact-us-messages.reply');
    Route::post('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@replySend')->name('contact-us-messages.replySend');
    Route::resource('contact-us-messages', 'Admin\ContactUsMessageController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactUsMessageController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactUsMessageController@getUnseenMsg')->name('getUnseenMsg');

    Route::resource('testimonials', 'Admin\TestimonialController',['except' => ['create','show']]);

    Route::get('dashboard','Admin\AdminController@index')->name('admin_dashboard');
    Route::post('test','Admin\AdminController@test')->name('test');

    Route::get('/','Admin\AdminController@index');
});