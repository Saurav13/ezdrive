/**
 * Binds a TinyMCE widget to <textarea> elements.
 */
angular.module('ui.tinymce', [])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
    uiTinymceConfig = uiTinymceConfig || {};
    var generatedIds = 0;
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ngModel) {
            var expression, options, tinyInstance;
            // generate an ID if not present
            if (!attrs.id) {
                attrs.$set('id', 'uiTinymce' + generatedIds++);
            }
            options = {
                // Update model when calling setContent (such as from the source editor popup)
                setup: function(ed) {
                    ed.on('init', function(args) {
                        ngModel.$render();
                    });
                    // Update model on button click
                    ed.on('ExecCommand', function(e) {
                        ed.save();
                        if(elm.prop("tagName").toLowerCase() === 'textarea') {
                            ngModel.$setViewValue(elm.val());
                        } else {
                            if(elm.html() == '<p><br data-mce-bogus="1"></p>' || elm.html() == '<p><br></p>' || elm.html() == ''){
                                elm[0].dataset.placeholder = 'Save what you have taught today or what you will teach tomorrow...';
                            }
                            else{
                                elm[0].dataset.placeholder = '';
                            }
                            ngModel.$setViewValue(elm.html());
                        }
                        
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                    });
                    // Update model on keypress
                    ed.on('KeyUp', function(e) {
                        ed.save();
                        if(elm.prop("tagName").toLowerCase() === 'textarea') {
                            ngModel.$setViewValue(elm.val());
                        } else {
                            if(elm.html() == '<p><br data-mce-bogus="1"></p>' || elm.html() == '<p><br></p>' || elm.html() == ''){
                                elm[0].dataset.placeholder = 'Save what you have taught today or what you will teach tomorrow...';
                            }
                            else{
                                elm[0].dataset.placeholder = '';
                            }
                            ngModel.$setViewValue(elm.html());
                        }
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                    });
                },
                mode: 'exact',
                inline: true,
                elements: attrs.id,
				height:"250",
	           
	            placeholder:"Make a note of what you have taught today or what you will teach tomorrow",
	            plugins: [
	                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	                "searchreplace wordcount visualblocks visualchars code",
	                "insertdatetime media nonbreaking save table contextmenu directionality",
	                "emoticons template paste textcolor colorpicker textpattern"
	            ],
	            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	            toolbar2: "print preview | forecolor backcolor emoticons",
	            image_advtab: true,
	            file_picker_callback: function(callback, value, meta) {
                    if (meta.filetype == 'image') {
                        $('#upload').trigger('click');
                        $('#upload').on('change', function() {
                        var file = this.files[0];
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            callback(e.target.result, {
                            alt: ''
                            });
                        };
                        reader.readAsDataURL(file);
                        });
                    }
	            }
            };
            if (attrs.uiTinymce) {
                expression = scope.$eval(attrs.uiTinymce);
            } else {
                expression = {};
            }
            angular.extend(options, uiTinymceConfig, expression);
            setTimeout(function() {
                tinymce.init(options);
            });


            ngModel.$render = function() {
                if (!tinyInstance) {
                    tinyInstance = tinymce.get(attrs.id);
                }
                if (tinyInstance) {
                    if(ngModel.$viewValue == '<p><br data-mce-bogus="1"></p>' || ngModel.$viewValue == '<p><br></p>' || ngModel.$viewValue == ''){
                        elm[0].dataset.placeholder = 'Save what you have taught today or what you will teach tomorrow...';
                    }
                    else{
                        elm[0].dataset.placeholder = '';
                    }
                    tinyInstance.setContent(ngModel.$viewValue || '');
                }
            };
        }
    };
}]);

var driveapps=angular.module('driveapp',['ngSanitize','angular-loading-bar','ui.tinymce'])
                    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
                        cfpLoadingBarProvider.parentSelector = '.myProgress';
                        cfpLoadingBarProvider.spinnerTemplate = '<div><span><i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;Processing...</span></div>';
                    }]);

driveapps.run(function($rootScope,$http) {
    $rootScope.newRequests=[];
    $rootScope.fewStudents = [];
    $rootScope.totalUngraded='0';

    $rootScope.totalStudents='0';
    $rootScope.totalAssignments="0";
    $rootScope.totalSubmittedAssignments="0";
    $rootScope.NDriveFiles="0";
    $rootScope.NPostedFiles="0";
    $rootScope.NPosts="0";
    $rootScope.NNotices="0";
    $rootScope.NBoards="0";
    $rootScope.NPublishedResults="0";
    $rootScope.uploading=false;
    $rootScope.progress=0;
    $rootScope.uploadModalDisplay=function(){
        
        if($rootScope.uploading==true)
            if($('#uploadFile').css("display")=='block')
                return true;
            else
                return false;
        else
            return true;
    }


});

var setResponse=function($rootScope,response){
    $rootScope.totalAssignments=response.totalAssignments;
    $rootScope.totalSubmittedAssignments=response.totalSubmittedAssignments;
    $rootScope.NDriveFiles=response.NDriveFiles;
    $rootScope.NPostedFiles=response.NPostedFiles;
    $rootScope.NPosts=response.NPosts;
    $rootScope.NNotices=response.NNotices;
    $rootScope.NPublishedResults=response.NPublishedResults;
    $rootScope.totalStudents=response.totalStudents;
}

var setSpaces=function(response){
    var ds=parseInt(response.drivespace)/(1024*1024);
    var cs=parseInt(response.collectionspace)/(1024*1024);
    var ps=parseInt(response.postspace)/(1024*1024);
    var ts=parseInt(response.totalspace)/(1024*1024*1024);
    var thts=ds+cs+ps;
    $('#drivespace').html(ds.toFixed(2).toString()+" MB");
    $('#collectionspace').html(cs.toFixed(2).toString()+" MB");
    $('#postspace').html(ps.toFixed(2).toString()+" MB");
    $('#thistotalspace').html(thts.toFixed(2)+" MB");
    $('#totalspace').html(ts.toFixed(2).toString()+" out of 1GB");
}
driveapps.controller('DriveController',function($scope,$http,$sce,$timeout,$q,$rootScope){
    $scope.folders=[]
    $scope.files=[]
    $scope.selected=false;
    $scope.selectedId;
    $scope.selectType;
    $scope.downloads=[];
    $scope.selectedItem;
    $scope.breadcrumbs=[];
    $scope.importDirStructure=[];
    $scope.importDirFiles=[];
    $scope.importFolders=[];
    $scope.importFiles=[];
    $scope.importDrives=[];
    $scope.importSelectedDriveId=null;
    $scope.checkedIds=[];
    $scope.contentimage = '';
    $scope.viewerror = '';
    $scope.content = '';

    var drive_id;
    var importp_id;
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    
   
    $scope.openRoot=function(){//init function
        drive_id=$('#drive_id').val();
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/getroot"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');

            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }
    $scope.addFolder=function(name){
        
        $('#addFolderBeforeLoading').attr('hidden','true');
        $('#addFolderLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/addfolder",
            data: {'name':name}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#addFolderBeforeLoading').removeAttr('hidden');
            $('#addFolderLoading').attr('hidden','true');
                
            
            $('#addFolder').modal('hide');
        }, function myError(response) {
            $('#addFolderBeforeLoading').removeAttr('hidden');
            $('#addFolderLoading').attr('hidden','true');
            showErrors(response);
        });
    }
    $scope.openFolder=function(id){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/openfolder",
            data: {'id':id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
            // console.log($scope.contents);
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }
    $scope.open=function(){
        if($scope.selectType==1)
        {
            $('#DriveMainBeforeLoading').attr('hidden','true');
            $('#DriveMainLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/drive/"+drive_id+"/openfolder",
                data: {'id':$scope.selectedId}
            }).then(function mySuccess(response) {
                $scope.selected=false;
                $scope.isRoot = false;
                $scope.folders = response.data.folders;
                $scope.files = response.data.files;
                $scope.breadcrumbs=response.data.breadcrumbs;
                $('#DriveMainBeforeLoading').removeAttr('hidden');
                $('#DriveMainLoading').attr('hidden','true');
                // console.log($scope.contents);
            }, function myError(response) {
                showErrors(response);
                $('#DriveMainBeforeLoading').removeAttr('hidden');
                $('#DriveMainLoading').attr('hidden','true');
            });
        }
        else{
            $scope.viewerror = '';
            $('#viewerpopup').modal('show');
            $('#viewerimage').attr('hidden', 'hidden');
            $('#viewerdoc').attr('hidden', 'hidden');
            $('#BeforeLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/drive/"+drive_id+"/getfilelink",
                data: {'id':$scope.selectedId},
                responseType:'json'
            }).then(function mySuccess(response) {
                if($scope.selectedItem.mime_type.split('/')[0] == 'image' || $scope.selectedItem.mime_type.split('/')[0] == 'video'){
                    $scope.contentimage = $sce.trustAsResourceUrl(response.data.url);
                }
                else{
                    $scope.content = $sce.trustAsResourceUrl('https://docs.google.com/viewer?embedded=true&url='+encodeURIComponent(response.data.url));
                }
            }, function myError(response) {
                if(response.status == 404){
                    $scope.viewerror = "Sorry, the file you are looking for couldn't be found or has been deleted.";
                }
                else{
                    $scope.viewerror = "Sorry, but something went wrong please try again.";
                }
                $('#BeforeLoading').attr('hidden','hidden');
                
            });
        }
    }
    $scope.back=function(){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/back"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
            // console.log($scope.contents);
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }
    var uploadFileToUrl = function (file, uploadUrl, progressCB) {
        var fd = new FormData();
        fd.append('file', file);
       
        
        // $('#uploadFileLoading').removeAttr('hidden');
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined },
            uploadEventHandlers: {
                progress: progressCB
            },
            
        })
            .then(function successCallback(response) {
                console.log(response);
                $scope.folders = response.data.folders;
                $scope.files = response.data.files;
                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $rootScope.uploading=false;
                $('.uploadFileLoading').attr('hidden','true');
                $('.uploadFileBeforeLoading').removeAttr('hidden');
                $timeout(function () {
                    $('#uploadFile').modal('hide');
                }, 1000);
            },
            function errorCallback(response) {
                $rootScope.uploading=false;
                alert('something went wrong. upload size limit: 15MB');
                // showErrors(error);
                $('.uploadFileLoading').attr('hidden','true');
                $('.uploadFileBeforeLoading').removeAttr('hidden');
                
                
            });
    }
    $scope.uploadFile=function(){
        var file = $scope.file;
        if(!file) return;
        if( file.size>16000000){
            
      	    	$('#errorMessage').empty();
           	 $('#errorDetails').empty();
            	$('#errorMessage').html('File size limit exceeded');
           
        
       		//changeEvent.target.files=[]
        	$('#errorDrive').modal('show');
        	return;
        	
        }
       
        var uploadUrl ="/user/drive/"+drive_id+"/uploadfile";
        $rootScope.progress='0';
        $('.uploadFileBeforeLoading').attr('hidden','true');
        $('.progressBarLoading').removeAttr('hidden');

        $rootScope.uploading=true;
        uploadFileToUrl(file, uploadUrl,  function (e) {
            if (e.lengthComputable) {
                $rootScope.progress= parseInt((e.loaded / e.total) * 100);
                if($rootScope.progress==100)
                {
                    $('.progressBarLoading').attr('hidden','true');
                    $('.uploadFileLoading').removeAttr('hidden');
                }
            }
        });
       
    }
    $scope.renameFolder=function(name){
        
        $('#renameFolderBeforeLoading').attr('hidden','true');
        $('#renameFolderLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/renamefolder",
            data: {'name':name,'id':$scope.selectedId}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $('#renameFolderBeforeLoading').removeAttr('hidden');
            $('#renameFolderLoading').attr('hidden','true');
                
            
            $('#renameFolder').modal('hide');
        }, function myError(response) {
            showErrors(response);
            $('#renameFolderBeforeLoading').removeAttr('hidden');
            $('#renameFolderLoading').attr('hidden','true');
        
        });
    }
    $scope.importInit=function(name){
        
        $('#uploadFile').modal('hide');
        
        $('#ImportBeforeLoading').attr('hidden','true');
        $('#ImportLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/importinit"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.importFolders=[];
            $scope.importFiles=[];
            $scope.importDirStructure=response.data.dirStructure;
            $scope.importDirFiles=response.data.dirFiles;
            $scope.importDrives=response.data.drives;
            importp_id=0;
            $('#importDriveSelect').val('');
            $('#ImportBeforeLoading').removeAttr('hidden');
            $('#ImportLoading').attr('hidden','true');
            
            
        }, function myError(response) {
            $('#renameFolderBeforeLoading').removeAttr('hidden');
            $('#renameFolderLoading').attr('hidden','true');
            showErrors(response);
        });
    }
    var openImportFolder=function(d_id,p_id)
    {
        $scope.importFolders=[];
        $scope.importFiles=[];
         for(var i=0;i<$scope.importDirStructure.length;i++)
        {
            
            if($scope.importDirStructure[i].drive_id==d_id && $scope.importDirStructure[i].p_id==p_id)
            {
                $scope.importFolders.push($scope.importDirStructure[i]);
            }
        }
        for(var i=0;i<$scope.importDirFiles.length;i++)
        {
            if($scope.importDirFiles[i].drive_id==d_id && $scope.importDirFiles[i].dir_id==p_id)
            {
                $scope.importFiles.push($scope.importDirFiles[i]);
            }
        }
    }
    $scope.onImportSelectChange=function(){
        var d_id=$scope.importSelectedDriveId.id;
        importp_id=0;
        openImportFolder(d_id,importp_id);
        $scope.checkedIds=[];

    }
    var unselectAll=function(){
        for(var i=0;i<$scope.importFolders.length;i++)
        {
            var id=$scope.importFolders[i].id;
            var myEl = angular.element( document.querySelector( '#importFolder'+id ) );
            myEl.attr('class','list-group-item');
        }
        for(var i=0;i<$scope.importFiles.length;i++)
        {
            var id=$scope.importFiles[i].id;
            var myEl = angular.element( document.querySelector( '#importFile'+id ) );
            myEl.attr('class','list-group-item');
        }
    }
    $scope.importFileSingleClick=function(id){
       unselectAll();
        var myEl = angular.element( document.querySelector( '#importFile'+id ) );
        myEl.addClass('active');
    }
    $scope.importFolderSingleClick=function(id){
       unselectAll();
        var myEl = angular.element( document.querySelector( '#importFolder'+id ) );
        myEl.addClass('active'); 
    }
    $scope.importFolderDoubleClick=function(id){
        var d_id=$scope.importSelectedDriveId.id;
        importp_id=id;
        openImportFolder(d_id,importp_id);

    }
    $scope.importUpOneLevel=function(){
        var d_id=$scope.importSelectedDriveId.id;
        if(importp_id!=0)
        {
            for(var i=0;i<$scope.importDirStructure.length;i++)
            {
                if($scope.importDirStructure[i].id==importp_id)
                {
                    importp_id=$scope.importDirStructure[i].p_id;
                    break;
                }
            }
            openImportFolder(d_id,importp_id);
        }
    }
    $scope.importIsChecked=function(id){
        for(var i=0;i<$scope.checkedIds.length;i++)
        {
            if(id==$scope.checkedIds[i])
                return true;
        }
        return false;
    }
    $scope.importApplyCheck=function(id){
        if($scope.importIsChecked(id)==false)
            $scope.checkedIds.push(id);
        else{
            var index=$scope.checkedIds.indexOf(id);
            if (index > -1) {
                $scope.checkedIds.splice(index, 1);
              }
        }
    }
    $scope.importSelectedFiles=function(){
        $('#ImportSelectedFilesBeforeLoading').attr('hidden','true');
        $('#ImportSelectedFilesLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/importselectedfiles",
            data:{'selectedFileIds':$scope.checkedIds}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            
            $('#ImportSelectedFilesBeforeLoading').removeAttr('hidden');
            $('#ImportSelectedFilesLoading').attr('hidden','true');
            
            $('#importFile').modal('hide');
            
        }, function myError(response) {
            $('#ImportSelectedFilesBeforeLoading').removeAttr('hidden');
            $('#ImportSelectedFilesLoading').attr('hidden','true');
           
            showErrors(response);
        });
    }
    $scope.delete=function(){
        if($scope.selectType==1)
        {
            $('#deleteBeforeLoading').attr('hidden','true');
            $('#deleteLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/drive/"+drive_id+"/deletefolder",
                data: {'id':$scope.selectedId}
            }).then(function mySuccess(response) {
                $scope.selected=false;
                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $scope.folders = response.data.folders;
                $scope.files = response.data.files;
                $('#deleteBeforeLoading').removeAttr('hidden');
                $('#deleteLoading').attr('hidden','true');
                    
                
                $('#delete').modal('hide');
            }, function myError(response) {
                $('#deleteBeforeLoading').removeAttr('hidden');
                $('#deleteLoading').attr('hidden','true');
                showErrors(response);
            }); 
        }
        else if($scope.selectType==2)
        {
            $('#deleteBeforeLoading').attr('hidden','true');
            $('#deleteLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/drive/"+drive_id+"/deletefile",
                data: {'id':$scope.selectedId}
            }).then(function mySuccess(response) {
                $scope.selected=false;
                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $scope.folders = response.data.folders;
                $scope.files = response.data.files;
                $('#deleteBeforeLoading').removeAttr('hidden');
                $('#deleteLoading').attr('hidden','true');
                    
                
                $('#delete').modal('hide');
            }, function myError(response) {
                $('#deleteBeforeLoading').removeAttr('hidden');
                $('#deleteLoading').attr('hidden','true');
                showErrors(response);
            });
        }
    }
    $scope.download=function(){
        var url;
        var type;
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        if($scope.selectType==1)
        {
            url = "/user/drive/"+drive_id+"/downloadfolder"+'/'+$scope.selectedId+'/'+$scope.selectedItem.name+'.zip';
           
            
        }
        else if($scope.selectType==2)
        {
            url = "/user/drive/"+drive_id+"/downloadfile"+'/'+$scope.selectedId+'/'+$scope.selectedItem.name;
           
        }
        var fileURL = url;
        a.href = fileURL;
        a.target = '_blank';
        a.click();
    }
    $scope.canceldownload = function(id,typ){

        for (var i = 0;i<$scope.downloads.length;i++){
            if($scope.downloads[i].id == id && $scope.downloads[i].type == typ){
                $scope.downloads[i].canceller.resolve();
                $scope.downloads.splice(i,1);                
            }
        }
         
    }
    $scope.searchDrive=function(key){
        if(!key || key.replace(' ','').length == 0) return;
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/drive/"+drive_id+"/searchdrive",
            data: {'key':key}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
            // console.log($scope.contents);
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }
    $scope.getIcon=function(ext){
        if(ext=='docx' || ext=='rtf' || ext=='doc' || ext=='dot' || ext=='wbk'||ext=='docm'||ext=='dotx'||ext=='dotm'||ext=='docv'||ext=='txt'||ext=='odt'||ext=='tex'||ext=='wps'||ext=='wks'||ext=='wpd')
            return 'docx.png';
        else if(ext=='pdf' ||ext=='xpdf' )
            return 'pdf.png';
        else if(ext=='png' || ext=='jpg' || ext=='bmp'||ext=='ai' ||ext=='gif'||ext=='ico'||ext=='jpeg'||ext=='ps'||ext=='psd'||ext=='svg'||ext=='tif'||ext=='tiff')
            return 'img.png';
        else if(ext=='zip'|| ext=='7z'|| ext=='rar' ||ext=='arj' ||ext=='rpm' ||ext=='tar' ||ext=='pkg' ||ext=='gz'||ext=='tar.gz'||ext=='z'||ext=='deb')
            return 'zip.png';
        else if(ext=='mp4'||ext=='wmp'||ext=='flv'||ext=='mkv'||ext=='avi'||ext=='webm'||ext=='vob'||ext=='mov'||ext=='m4p'||ext=='mpg'||ext=='mpeg'||ext=='3gp')
            return 'video.png';
        else if(ext=='xls'||ext=='xlt'||ext=='xlm'||ext=='xlxs'||ext=='xlsm'||ext=='xltx'||ext=='xltm'||ext=='xlsb'||ext=='xla'||ext=='xlam'||ext=='xll'||ext=='xlw')
            return 'excel.png'
        else if(ext=='pptx'||ext=='ppt'||ext=='pot'||ext=='pps'||ext=='pptm'||ext=='potx'||ext=='potm'||ext=='ppam'||ext=='ppsx'||ext=='ppsm'||ext=='sldx'||ext=='sldm')
            return 'ppt.png'
        else
            return 'file.png';
    
    }
    $scope.select=function(id,typ){
        $scope.selected=true;
        $scope.selectedId=id;
        $scope.selectType=typ;
        if(typ==1)
        {
            for(var i=0;i<$scope.folders.length;i++)
            {   
                if(id==$scope.folders[i].id)
                    $scope.selectedItem=$scope.folders[i];
            }
        }
        else{
            for(var i=0;i<$scope.files.length;i++)
            {
                if(id==$scope.files[i].id)
                    $scope.selectedItem=$scope.files[i];
            }
        }
    }
    $scope.visibleIcon=function(icon){
        if(icon=="Delete" && $scope.selected==true)
            return true; 
        else if(icon=="Rename" && $scope.selectType==1 && $scope.selected==true)
            return true;
        else if(icon=="Download" && $scope.selected==true)
            return true;
        else if(icon=="Preview" && $scope.selectType==2 && $scope.selected==true)
            return true;
        
        else return false;
    }
    
    
    $scope.updateResponse=function(){
        var response=$('#upresponse').attr('response');
    }
   


});


driveapps.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}]);

angular.element('.item-list').on('contextmenu', '.item-list-item', function(e) {
    var menu = angular.element('#context-menu');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }
    $('.item-list-item').each(function(){
        $(this).removeClass('active');
    });

    $(this).addClass('active');

    var l = e.pageX;
    var t = e.pageY+ menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
    
});

angular.element(window.document).on('click', function() {
    angular.element('#context-menu').hide();
    angular.element('#context-menucolc').hide();
});



driveapps.directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
});


driveapps.directive('upload', ['$http', function($http) {
    return {
        restrict: 'E',
        replace: true,
        require: '?ngModel',
        template: '',
        link: function(scope, element, attrs, ngModel) {
                
                element.on('dragover', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                });
                element.on('dragenter', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                });
                element.on('drop', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (e.originalEvent.dataTransfer){
                        if (e.originalEvent.dataTransfer.files.length > 0) {
                            upload(e.originalEvent.dataTransfer.files);
                        }
                    }
                    return false;
                });

            var upload = function(files) {
                $('#DNDBeforeLoading').attr('hidden','true');
                $('#DNDLoading').removeAttr('hidden');
                
                var data = new FormData();
                data.append("file",files[0]);
               
                $http({
                    method: 'POST',
                    url: attrs.to,
                    data: data,
                    withCredentials: true,
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).then(function mySuccess(response) {
                    scope.folders = response.data.folders;
                    scope.files = response.data.files;
                    setSpaces(response.data);

                    $('#DNDBeforeLoading').removeAttr('hidden');
                    $('#DNDLoading').attr('hidden','true');
                },function myError(response) {
                    if(response.status==422)
                        alert(response.data.status);
                        
                    $('#DNDBeforeLoading').removeAttr('hidden');
                    $('#DNDLoading').attr('hidden','true');
                });
            };  
        }
    };
}]);
driveapps.directive('iosDblclick',
    function () {

        const DblClickInterval = 300; //milliseconds

        var firstClickTime;
        var waitingSecondClick = false;

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {

                    if (!waitingSecondClick) {
                        firstClickTime = (new Date()).getTime();
                        waitingSecondClick = true;

                        setTimeout(function () {
                            waitingSecondClick = false;
                        }, DblClickInterval);
                    }
                    else {
                        waitingSecondClick = false;

                        var time = (new Date()).getTime();
                        if (time - firstClickTime < DblClickInterval) {
                            scope.$apply(attrs.iosDblclick);
                        }
                    }
                });
            }
        };
    });