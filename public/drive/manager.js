

driveapps.controller('ManagerController',function($scope,$http,$rootScope){
    $scope.allStudents=[{'name':'Demo','id':'0','photo':null}];
    $scope.results=[];
    var drive_id;
    

    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    $scope.init=function(){
        drive_id=$('#drive_id').val();
        $('#StudentBeforeLoading').attr('hidden','true');
        $('#StudentLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/init"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $rootScope.newRequests = response.data.newRequests;
            $rootScope.fewStudents = response.data.fewStudents;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#StudentBeforeLoading').removeAttr('hidden');
            $('#StudentLoading').attr('hidden','true');

            
        }, function myError(response) {
            showErrors(response);
            $('#StudentBeforeLoading').removeAttr('hidden');
            $('#StudentLoading').attr('hidden','true');
        });
    }
    $scope.accept=function(u_id){
        $('#AcceptBeforeLoading'+u_id).attr('hidden','true');
        $('#AcceptLoading'+u_id).removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/accept",
            data:{'user_id':u_id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $rootScope.newRequests = response.data.newRequests;
            $rootScope.fewStudents = response.data.fewStudents;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#AcceptBeforeLoading'+u_id).removeAttr('hidden');
            $('#AcceptLoading'+u_id).attr('hidden','true');

            
        }, function myError(response) {
            showErrors(response);
            $('#AcceptBeforeLoading'+u_id).removeAttr('hidden');
            $('#AcceptLoading'+u_id).attr('hidden','true');
        });
    }
    $scope.ignore=function(u_id){
        $('#IgnoreBeforeLoading'+u_id).attr('hidden','true');
        $('#IgnoreLoading'+u_id).removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/ignore",
            data:{'user_id':u_id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $rootScope.newRequests = response.data.newRequests;
            $rootScope.fewStudents = response.data.fewStudents;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#IgnoreBeforeLoading'+u_id).removeAttr('hidden');
            $('#IgnoreLoading'+u_id).attr('hidden','true');

            
        }, function myError(response) {
            showErrors(response);
            $('#IgnoreBeforeLoading'+u_id).removeAttr('hidden');
            $('#IgnoreLoading'+u_id).attr('hidden','true');
        });
    }
    $scope.removeStudentPrompt=function(rid){
        $scope.rid=rid;
    }
    $scope.removeStudent=function(u_id){
        $('#RemoveStudentBeforeLoading').attr('hidden','true');
        $('#RemoveStudentLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/removestudent",
            data:{'user_id':u_id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $rootScope.newRequests = response.data.newRequests;
            $rootScope.fewStudents = response.data.fewStudents;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#RemoveStudentBeforeLoading').removeAttr('hidden');
            $('#RemoveStudentLoading').attr('hidden','true');
            $('#removeStudent').modal('hide');
            
        }, function myError(response) {
            showErrors(response);
            $('#RemoveStudentBeforeLoading').removeAttr('hidden');
            $('#RemoveStudentLoading').attr('hidden','true');
        });
    }
    $scope.viewAllStudents=function(){
        $('#AllStudentBeforeLoading').attr('hidden','true');
        $('#AllStudentLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/allstudents"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $rootScope.newRequests = response.data.newRequests;
            $rootScope.fewStudents = response.data.allStudents;
            // $scope.allStudents = response.data.allStudents;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#AllStudentLoading').attr('hidden','true');

            
        }, function myError(response) {
            showErrors(response);
            $('#AllStudentLoading').attr('hidden','true');
            $('#AllStudentBeforeLoading').removeAttr('hidden');
        });
    }
    
    $scope.toggleDriveAccess=function(){
        $('#ToggleDriveAccessBeforeLoading').attr('hidden','true');
        $('#ToggleDriveAccessLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/toggledriveaccess",
        }).then(function mySuccess(response) {

            $('#ToggleDriveAccessBeforeLoading').removeAttr('hidden');
            $('#ToggleDriveAccessLoading').attr('hidden','true');

            
        }, function myError(response) {
            showErrors(response);
        });
    }
    $scope.loadResults=function(){
        $('#ResultsBeforeLoading').attr('hidden','true');
        $('#ResultsLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/loadresults",
        }).then(function mySuccess(response) {

            $scope.results=response.data.results;
            $('#ResultsBeforeLoading').removeAttr('hidden');
            $('#ResultsLoading').attr('hidden','true');

            
        }, function myError(response) {
            $('#ResultsBeforeLoading').removeAttr('hidden');
            $('#ResultsLoading').attr('hidden','true');
            showErrors(response);
        });
    }
})