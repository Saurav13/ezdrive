driveapps.controller('BoardController',function($scope,$http,$filter,$rootScope){
    var drive_id;
    $scope.board;
    var d;
    $scope.content;
    var next;

    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }

    $scope.getBoards=function(){
        drive_id=$('#drive_id').val();
        $('#BoardMainBeforeLoading').attr('hidden','true');
        $('#BoardMainLoading').removeAttr('hidden');

        $http({
            method : "POST",
            url : "/user/board/"+drive_id+"/change",
            data:{'date': $filter('date')(new Date(), "yyyy-MM-dd")}
        }).then(function mySuccess(response) {
            $scope.board = response.data.board;
            $scope.board.date = $filter('date')(new Date(), "yyyy-MM-dd");
            d = $scope.board.date;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.content = $scope.board.content;
            $('#BoardMainBeforeLoading').removeAttr('hidden');
            $('#BoardMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#BoardMainBeforeLoading').removeAttr('hidden');
            $('#BoardMainLoading').attr('hidden','true');
        });
    }

    $scope.changeBoard = function(){
        if($scope.board.content != $scope.content){
            next = $scope.board.date;
            $scope.board.date = d;
            $('#confirmChange').modal('show');
            return;
        }

        $scope.board.date = next ? next : $scope.board.date;

        $('#theBoard').attr('hidden','true');
        $('#changeBoardLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/board/"+drive_id+"/change",
            data:{'date':$scope.board.date}
        }).then(function mySuccess(response) {
            $scope.board = response.data.board;
            d = $scope.board.date;
            next = null;
            $scope.content = $scope.board.content;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#theBoard').removeAttr('hidden');
            $('#changeBoardLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $scope.board.date = d;
            $('#theBoard').removeAttr('hidden');
            $('#changeBoardLoading').attr('hidden','true');
        });
    }

    $scope.saveBoard=function(){
        $('#confirmChange').modal('hide');
        $('#BoardBeforeLoading').attr('hidden','true');
        $('#BoardLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/board/"+drive_id+"/save",
            data:{'content':$scope.board.content,'date':$scope.board.date}
        }).then(function mySuccess(response) {
            $scope.board = response.data.board;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.content = $scope.board.content;
            $('#BoardBeforeLoading').removeAttr('hidden');
            $('#BoardLoading').attr('hidden','true');
            if(next)
                setTimeout(function(){ 
                    $scope.changeBoard();
                }, 500);
        }, function myError(response) {
            showErrors(response);
            $('#BoardBeforeLoading').removeAttr('hidden');
            $('#BoardLoading').attr('hidden','true');
        });
    }

    $scope.deleteBoard=function(){
        $('#removeBoardBeforeLoading').attr('hidden','true');
        $('#removeBoardLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/board/"+drive_id+"/delete",
            data:{'id':$scope.board.id}
        }).then(function mySuccess(response) {
            d = $scope.board.date;
            $scope.board = response.data.board;
            $scope.board.date = d;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.content = $scope.board.content;
            $('#removeBoardBeforeLoading').removeAttr('hidden');
            $('#removeBoardLoading').attr('hidden','true');
            $('#removeBoard').modal('hide');
        }, function myError(response) {
            showErrors(response);
            $('#removeBoardBeforeLoading').removeAttr('hidden');
            $('#removeBoardLoading').attr('hidden','true');
        });
    }
    
});