driveapps.controller('CollectionController',function($scope,$http,$sce,$timeout,$q,$rootScope){
    $scope.collections=[];
    $scope.files=[];
    $scope.selectedCollection={'id':0,'name':'Collection Example','nfiles':'..','mail_link':'demo+demo@ezhum.com','deadline':'..','status':'..'};
    $scope.selectedFile={'id':0,'created_at':'0'};
    $scope.editTask={'id':'0','name':'Demo','description':'Do your homework','file':'assign.txt'};
    $scope.downloads=[];
    $scope.selected=false;
    $scope.selectType;
    $scope.inside=false;
    $scope.contentimage = '';
    $scope.viewerror = '';
    $scope.content = '';
    $scope.onGrading=false;
    $scope.newtaskdeadline='';
    $scope.newtaskdescription='';
    $scope.onDeadlineEdit=false;
    $scope.resultmessage="Results are OUT!!!";
    
    var drive_id;
  
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    
    $scope.getCollections=function(){
        drive_id=$('#drive_id').val();
        $('#CollectionMainBeforeLoading').attr('hidden','true');
        $('#CollectionMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/collection/"+drive_id+"/getcollections"
        }).then(function mySuccess(response) {
            $scope.collections = $scope.toCollectionList(response.data.collections);
            $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
            $scope.inside=false;
            $scope.files=[];
            $scope.selected=false;
            if($scope.collections.length > 0){
                $scope.selected= true;
                $scope.selectType = 1;
                if($scope.selectedCollection.id!=0){
                    for(var i=0;i<$scope.collections.length;i++)
                    {
                        if($scope.collections[i].id==$scope.selectedCollection.id)
                        {
                            $scope.selectedCollection=$scope.collections[i];
                            
                            break;
                        }
                    }
                    $('.colc-list-item').each(function(){
                        $(this).removeClass('active');
                    });
                    $('.colc-list-item[data-id='+$scope.selectedCollection.id+']').addClass('active');
                }
                else{
                    $scope.selectedCollection = $scope.collections[0];
                    $scope.editTask.name=$scope.collections[0].name;
                    $scope.editTask.file="";

                    if($scope.collections[0].collectiontask!=null)
                    {
                        $scope.editTask.description=$scope.collections[0].collectiontask.description;

                        $scope.editTask.file=$scope.collections[0].collectiontask.file ? $scope.collections[0].collectiontask.file.split('/').slice(-1)[0] : null;
                    }
                    else{
                        $scope.editTask.description="";
                        $scope.editTask.file="" ;
                        
                    }
                    $scope.editTask.id=$scope.collections[0].id;
                    $('.colc-list-item').first().addClass('active');
                }
            }
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#CollectionMainBeforeLoading').removeAttr('hidden');
            $('#CollectionMainLoading').attr('hidden','true');
            
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }

    $scope.addCollection=function(){
        var formData = new FormData();
        if( $scope.file && $scope.file.size>6000000){
                    
                    $('#errorMessage').empty();
                $('#errorDetails').empty();
                $('#errorMessage').html('File size limit exceeded');
            
        
                //changeEvent.target.files=[]
            $('#errorDrive').modal('show');
            return;
            
        }
        if($scope.file)
            formData.append("file",$scope.file);
        formData.append("name",$scope.newcollectionname);
        formData.append("task",$scope.newtaskdescription);
        formData.append("deadline",$scope.newtaskdeadline);
        
        $('#addCollectionBeforeLoading').attr('hidden','true');
        $('#addCollectionLoading').removeAttr('hidden');
        
        $http.post("/user/collection/"+drive_id+"/addcollection", formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         }).then(function success(response){
            $scope.collections = $scope.toCollectionList(response.data.collections);
            $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            if($scope.collections.length == 1){
                $scope.selected = true;
                $scope.selectedCollection = $scope.collections[0];
                $scope.editTask.name=$scope.collections[0].name;
                $scope.editTask.file="";

                if($scope.collections[0].collectiontask!=null)
                {
                    $scope.editTask.description=$scope.collections[0].collectiontask.description;

                    $scope.editTask.file=$scope.collections[0].collectiontask.file ? $scope.collections[0].collectiontask.file.split('/').slice(-1)[0] : null;
                }
                else{
                    $scope.editTask.description="";
                    $scope.editTask.file="" ;
                    
                }
                $scope.editTask.id=$scope.collections[0].id;
            }
        
            
            $('#addCollectionBeforeLoading').removeAttr('hidden');
            $('#addCollectionLoading').attr('hidden','true');
            $('#addCollection').modal('hide');
        }, function error(response){
           
            showErrors(response);
                
            $('#addCollectionBeforeLoading').removeAttr('hidden');
            $('#addCollectionLoading').attr('hidden','true');
        });
    }

    $scope.open=function(){
        
        if($scope.selectType==1)
        {
            $('#CollectionMainBeforeLoading').attr('hidden','true');
            $('#CollectionMainLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/collection/"+drive_id+"/opencollection",
                data: {'id':$scope.selectedCollection.id}
            }).then(function mySuccess(response) {
                $scope.selected=false;
                $scope.inside=true;
                $scope.selectedFile={'id':0,'created_at':'0'};

                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $scope.files = response.data.files;
                if($scope.files.length > 0){
                    $scope.selected= true;
                    $scope.selectType = 2;
                    $scope.selectedFile = $scope.files[0];
                    $('.colc-list-item').each(function(i){
                        $(this).removeClass('active');
                    });
                    $('.colc-file-list-'+$scope.selectedCollection.id).first().addClass('active');
                }
                
                $('#CollectionMainBeforeLoading').removeAttr('hidden');
                $('#CollectionMainLoading').attr('hidden','true');
                // console.log($scope.contents);
            }, function myError(response) {
                // location.reload();
                showErrors(response);
                $('#CollectionMainBeforeLoading').removeAttr('hidden');
                $('#CollectionMainLoading').attr('hidden','true');
            });
        }
        else{
            $scope.viewerror = '';
            $('#Collectionviewerpopup').modal('show');
            $('#Collectionviewerimage').attr('hidden', 'hidden');
            $('#Collectionviewerdoc').attr('hidden', 'hidden');
            $('#CollectionBeforeLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/collection/"+drive_id+"/getsubmissionlink",
                data: {'id':$scope.selectedFile.id},
                responseType:'json'
            }).then(function mySuccess(response) {
                if($scope.selectedFile.mime_type.split('/')[0] == 'image' || $scope.selectedFile.mime_type.split('/')[0] == 'video'){
                    $scope.contentimage = $sce.trustAsResourceUrl(response.data.url);
                }
                else{
                    $scope.content = $sce.trustAsResourceUrl('https://docs.google.com/viewer?embedded=true&url='+encodeURIComponent(response.data.url));
                }
            }, function myError(response) {
                if(response.status == 404){
                    $scope.viewerror = "Sorry, the file you are looking for couldn't be found or has been deleted.";
                }
                else{
                    $scope.viewerror = "Sorry, but something went wrong please try again.";
                }
                $('#CollectionBeforeLoading').attr('hidden','hidden');
                
            });
        }
    }

    $scope.showTaskCard=function(){
        $('#taskCard').removeAttr('hidden');
        $('#addTaskButton').attr('hidden','true');
    }
    $scope.showDeadlineCard=function(){
        $('#deadlineCard').removeAttr('hidden');
        $('#addDeadlineButton').attr('hidden','true');
    }
    $scope.select=function(c,typ)
    {
        $scope.selected=true;
        $scope.selectType=typ;
        if(typ==1)
        {  
            $scope.selectedCollection=c;
            $scope.editTask.name=c.name;
            $scope.editTask.file="";

            if(c.collectiontask!=null)
            {
                $scope.editTask.description=c.collectiontask.description;

                $scope.editTask.file=c.collectiontask.file ? c.collectiontask.file.split('/').slice(-1)[0] : null;
            }
            else{
                $scope.editTask.description="";
                $scope.editTask.file="" ;
                
            }
            $scope.editTask.id=c.id;
        }
        else{
            $scope.selectedFile=c;
            $scope.onGrading=false;
        }
    }
    
    $scope.download=function()
    {
        var url;
        var type;
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";

        if($scope.inside==true)
        {
            url = "/user/collection/"+drive_id+"/downloadfile"+'/'+$scope.selectedFile.id+'/'+$scope.selectedFile.name;
        }
        else
        {
            url = "/user/collection/"+drive_id+"/downloadcollection"+'/'+$scope.selectedCollection.id+'/'+$scope.selectedCollection.name+'.zip';
        }
        var fileURL = url;
        a.href = fileURL;
        a.target = '_blank';
        a.click();
      
    }
    $scope.download2=function()
    {
        var count=0;
        // $('#CollectionMainBeforeLoading').attr('hidden','true');
        // $('#CollectionMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/collection/"+drive_id+"/downloadcollection",
            data :{'id':$scope.selectedCollection.id}
        }).then(function mySuccess(response) {
            var files=response.data.files;
            for(var i=0;i<files.length;i++)
            {
                // console.log(files[i]);
                $http({
                    method : "POST",
                    url : "/user/collection/"+drive_id+"/storefileinsession",
                    data :{'file':files[i]}
                }).then(function mySuccess(response){
                    count+=1
                    if(count==files.length)
                    {
                        $http({
                            method : "POST",
                            url : "/user/collection/"+drive_id+"/zipdownload",
                            data : {'count':files.length,'id':$scope.selectedCollection.id}
                        }).then(function mySuccess(response){
                            
                            //after download
                        });
                    }
                });
            }
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }
    $scope.toCollectionList=function(data)
    {
        var collection=[];
        for(i=0;i<data.length;i++)
        {
            var g=0;
            for(var j=0;j<data[i].collectioncontents.length;j++)
            {
                if(!data[i].collectioncontents[j].grade)
                {
                    g+=1;
                }
            }
            var temp={'id':data[i].id,'name':data[i].name,'nUnGraded':g,'nfiles':data[i].collectioncontents.length,'mail_link':data[i].mail_link,'collectiontask':data[i].collectiontask,'deadline':data[i].deadline,'status':data[i].status,'resultPublished':data[i].resultPublished}
            collection.push(temp);
        }
        return collection;
    }
    $scope.deleteCollection=function(){
        if($scope.inside==false){
            $('#deleteCollectionBeforeLoading').attr('hidden','true');
            $('#deleteCollectionLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/collection/"+drive_id+"/deletecollection",
                data: {'id':$scope.selectedCollection.id}
            }).then(function mySuccess(response) {
                $scope.selected=false;
                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $scope.collections =$scope.toCollectionList(response.data.collections);
                $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
                // $scope.selectedCollection={'id':'0','name':'Collection Example','nfiles':'..','mail_link':'demo+demo@ezhum.com','deadline':'..','status':'..'};
                if($scope.collections.length > 0){
                    $scope.selected= true;
                    $scope.selectedCollection = $scope.collections[0];
                    $('.colc-list-item').each(function(){
                        $(this).removeClass('active');
                    });
                    $('.colc-list-item').first().addClass('active');
                }
                else{
                    $scope.selectedCollection = {'id':0,'created_at':'0'};
                }

                $('#deleteCollectionBeforeLoading').removeAttr('hidden');
                $('#deleteCollectionLoading').attr('hidden','true');
                    
                
                $('#deleteCollection').modal('hide');
            }, function myError(response) {
                $('#deleteCollectionBeforeLoading').removeAttr('hidden');
                $('#deleteCollectionLoading').attr('hidden','true');
                showErrors(response);
            }); 
        }
        
    }
    $scope.closeCollection=function(){
        if($scope.inside==false){
            $('#closeCollectionBeforeLoading').attr('hidden','true');
            $('#closeCollectionLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/collection/"+drive_id+"/closecollection",
                data: {'id':$scope.selectedCollection.id}
            }).then(function mySuccess(response) {
                $scope.selected=true;
                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $scope.collections = $scope.toCollectionList(response.data.collections);
                $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
                // $scope.selectedCollection={'id':'0','name':'Collection Example','nfiles':'..','mail_link':'demo+demo@ezhum.com','deadline':'..','status':'..'};
                for(var i=0;i<$scope.collections.length;i++)
                {
                    if($scope.collections[i].id==$scope.selectedCollection.id)
                    {
                        $scope.selectedCollection=$scope.collections[i];
                        break;
                    }
                }

                $('#closeCollectionBeforeLoading').removeAttr('hidden');
                $('#closeCollectionLoading').attr('hidden','true');
                    
                $('#closeCollection').modal('hide');
            }, function myError(response) {
                $('#closeCollectionBeforeLoading').removeAttr('hidden');
                $('#closeCollectionLoading').attr('hidden','true');
                showErrors(response);
            }); 
        }
        
    }
    $scope.renameCollection=function(name){
        if($scope.inside==false){
            $('#renameCollectionBeforeLoading').attr('hidden','true');
            $('#renameCollectionLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/collection/"+drive_id+"/renamecollection",
                data: {'id':$scope.selectedCollection.id,'name':name}
            }).then(function mySuccess(response) {
                $scope.selected=true;
                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $scope.collections = $scope.toCollectionList(response.data.collections);
                $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
                // $scope.selectedCollection={'id':'0','name':'Collection Example','nfiles':'..','mail_link':'demo+demo@ezhum.com','deadline':'..','status':'..'};
                for(var i=0;i<$scope.collections.length;i++)
                {
                    if($scope.collections[i].id==$scope.selectedCollection.id)
                    {
                        $scope.selectedCollection=$scope.collections[i];
                        break;
                    }
                }
                $('#renameCollectionBeforeLoading').removeAttr('hidden');
                $('#renameCollectionLoading').attr('hidden','true');
                    
                
                $('#renameCollection').modal('hide');
            }, function myError(response) {
                $('#renameCollectionBeforeLoading').removeAttr('hidden');
                $('#renameCollectionLoading').attr('hidden','true');
                showErrors(response);
            }); 
        }
        
    }
    $scope.editTaskSubmit=function(){
        var formData = new FormData();
        if($scope.file1 && $scope.file1.size>16000000){
            
            $('#errorMessage').empty();
            $('#errorDetails').empty();
            $('#errorMessage').html('File size limit exceeded');
            $('#errorDrive').modal('show');
            return;  
        }

        if($scope.file1)
            formData.append("file",$scope.file1);
        formData.append('id',$scope.selectedCollection.id);
        if($scope.editTask.description!=='')
            formData.append('description',$scope.editTask.description);
        $('#viewTaskBeforeLoading').attr('hidden','true');
        $('#viewTaskLoading').removeAttr('hidden');
        $http.post("/user/collection/"+drive_id+"/edittask", formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         }).then(function success(response){
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.collections = $scope.toCollectionList(response.data.collections);
            $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);

            // $scope.selectedCollection={'id':'0','name':'Collection Example','nfiles':'..','mail_link':'demo+demo@ezhum.com','deadline':'..','status':'..'};
            for(var i=0;i<$scope.collections.length;i++)
            {
                if($scope.collections[i].id==$scope.selectedCollection.id)
                {
                    $scope.selectedCollection=$scope.collections[i];
                    break;
                }
            }
            $('#editTaskFileInput').val(null);
            $('#viewTaskBeforeLoading').removeAttr('hidden');
            $('#viewTaskLoading').attr('hidden','true');
            $('#viewTask').modal('hide');
        }, function error(response){
            showErrors(response);
                // location.reload();
            $('#viewTaskBeforeLoading').removeAttr('hidden');
            $('#viewTaskLoading').attr('hidden','true');
            });
    }
    $scope.saveGrade=function(){
        $('#saveGradeBeforeLoading').attr('hidden','true');
        $('#saveGradeLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/collection/"+drive_id+"/savegrade",
            data: {'grade':$scope.gradetext,'id':$scope.selectedFile.id,'cid':$scope.selectedCollection.id}
        }).then(function mySuccess(response) {
            $scope.files = response.data.files;
            $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
            setResponse($rootScope,response.data);
            for(var i=0;i<$scope.files.length;i++)
            {
                if($scope.files[i].id==$scope.selectedFile.id)
                {
                    $scope.selectedFile=$scope.files[i];
                    break;
                }
            }
            $('#saveGradeBeforeLoading').removeAttr('hidden');
            $('#saveGradeLoading').attr('hidden','true');
            $scope.onGrading=false;
            $scope.gradetext = '';
        }, function myError(response) {
            $('#saveGradeBeforeLoading').removeAttr('hidden');
            $('#saveGradeLoading').attr('hidden','true');
            showErrors(response);
        }); 
    }
    $scope.publishResult=function(message){
        
        if($scope.inside==false){
            $('#publishResultBeforeLoading').attr('hidden','true');
            $('#publishResultLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/user/collection/"+drive_id+"/publishresult",
                data: {'id':$scope.selectedCollection.id,'message':message}
            }).then(function mySuccess(response) {
                $scope.selected=true;
                setSpaces(response.data);
                setResponse($rootScope,response.data);
                $scope.collections = $scope.toCollectionList(response.data.collections);
                $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
                // $scope.selectedCollection={'id':'0','name':'Collection Example','nfiles':'..','mail_link':'demo+demo@ezhum.com','deadline':'..','status':'..'};
                for(var i=0;i<$scope.collections.length;i++)
                {
                    if($scope.collections[i].id==$scope.selectedCollection.id)
                    {
                        $scope.selectedCollection=$scope.collections[i];
                        break;
                    }
                }
                $('#publishResultBeforeLoading').removeAttr('hidden');
                $('#publishResultLoading').attr('hidden','true');
                    
                
                $('#publishResult').modal('hide');
            }, function myError(response) {
                $('#publishResultBeforeLoading').removeAttr('hidden');
                $('#publishResultLoading').attr('hidden','true');
                showErrors(response);
            }); 
        }
        
    }
    
    $scope.visibleIcon=function(icon){
        if(icon=="Delete" && $scope.selected==true && $scope.inside==false)
            return true; 
        else if(icon=="Download" && $scope.selected==true)
            return true;
        else if(icon=="Preview" && $scope.selected==true && $scope.inside==true)
            return true;
        else if(icon=="Rename" && $scope.selected==true && $scope.inside==false)
            return true;
        else return false;
    }
    $scope.getIcon=function(ext){
        if(ext=='docx' || ext=='rtf' || ext=='doc' || ext=='dot' || ext=='wbk'||ext=='docm'||ext=='dotx'||ext=='dotm'||ext=='docv'||ext=='txt'||ext=='odt'||ext=='tex'||ext=='wps'||ext=='wks'||ext=='wpd')
            return 'docx.png';
        else if(ext=='pdf' ||ext=='xpdf' )
            return 'pdf.png';
        else if(ext=='png' || ext=='jpg' || ext=='bmp'||ext=='ai' ||ext=='gif'||ext=='ico'||ext=='jpeg'||ext=='ps'||ext=='psd'||ext=='svg'||ext=='tif'||ext=='tiff')
            return 'img.png';
        else if(ext=='zip'|| ext=='7z'|| ext=='rar' ||ext=='arj' ||ext=='rpm' ||ext=='tar' ||ext=='pkg' ||ext=='gz'||ext=='tar.gz'||ext=='z'||ext=='deb')
            return 'zip.png';
        else if(ext=='mp4'||ext=='wmp'||ext=='flv'||ext=='mkv'||ext=='avi'||ext=='webm'||ext=='vob'||ext=='mov'||ext=='m4p'||ext=='mpg'||ext=='mpeg'||ext=='3gp')
            return 'video.png';
        else if(ext=='xls'||ext=='xlt'||ext=='xlm'||ext=='xlxs'||ext=='xlsm'||ext=='xltx'||ext=='xltm'||ext=='xlsb'||ext=='xla'||ext=='xlam'||ext=='xll'||ext=='xlw')
            return 'excel.png'
        else if(ext=='pptx'||ext=='ppt'||ext=='pot'||ext=='pps'||ext=='pptm'||ext=='potx'||ext=='potm'||ext=='ppam'||ext=='ppsx'||ext=='ppsm'||ext=='sldx'||ext=='sldm')
            return 'ppt.png'
        else
            return 'file.png';
    }
    $scope.setLateColor=function(){
        var submittedDate= new Date($scope.selectedFile.created_at.split(' ')[0]);

        if($scope.selectedCollection.deadline){
            var deadline= new Date($scope.selectedCollection.deadline);

            if(submittedDate>deadline)
                return 'color:red;'
            else
                return 'color:green';
        }
        else{
            return 'color:green';
        } 
    }

   $scope.toggleDeadlineEdit=function(){
       $scope.onDeadlineEdit= !$scope.onDeadlineEdit;
   }

   $scope.editDeadline=function(){
    $('#editDeadlineBeforeLoading').attr('hidden','true');
    $('#editDeadlineLoading').removeAttr('hidden');

    $http({
        method : "POST",
        url : "/user/collection/"+drive_id+"/editdeadline",
        data: {'deadline':$scope.editedDeadline,'id':$scope.selectedCollection.id}
    }).then(function mySuccess(response) {
        $scope.collections =$scope.toCollectionList(response.data.collections);
        $rootScope.totalUngraded=$scope.getTotalUngraded(response.data.collections);
        for(var i=0;i<$scope.collections.length;i++)
        {
            if($scope.collections[i].id==$scope.selectedCollection.id)
            {
                $scope.selectedCollection=$scope.collections[i];
                break;
            }
        }
        $('#editDeadlineBeforeLoading').removeAttr('hidden');
        $('#editDeadlineLoading').attr('hidden','true');
        $scope.onDeadlineEdit=false;
        $scope.editedDeadline = '';
    }, function myError(response) {
        $('#editDeadlineBeforeLoading').removeAttr('hidden');
        $('#editDeadlineLoading').attr('hidden','true');
        showErrors(response);
    }); 
   }

   $scope.getTotalUngraded=function(collections){
        var total=0;    
        for(var i=0;i<collections.length;i++){
            
            for(var j=0;j<collections[i].collectioncontents.length;j++){
                if(!collections[i].collectioncontents[j].grade){
                    total+=1;
                }
            }
        }
        return total;
   }

});


angular.element('.colc-list').on('contextmenu', '.colc-list-item', function(e) {
    
    var menu = angular.element('#context-menucolc');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }

    $('.colc-list-item').each(function(){
        $(this).removeClass('active');
    });

    $(this).addClass('active');
    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
}); 