driveapps.controller('PostController',function($scope,$http,$filter,$rootScope){
    $scope.posts=[]
    $scope.notices=[];
    $scope.postContent="";
    $scope.postfile=[];
    var drive_id;
    $scope.post;
   
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    $scope.getPosts=function(){
        drive_id=$('#drive_id').val();
        $('#PostMainBeforeLoading').attr('hidden','true');
        $('#PostMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/post/"+drive_id+"/getposts"
        }).then(function mySuccess(response) {
            $scope.posts = response.data.posts;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#PostMainBeforeLoading').removeAttr('hidden');
            $('#PostMainLoading').attr('hidden','true');
            if($scope.posts.length==0)
            {
            	$('#viewMorePostBeforeLoading').attr('hidden','true');
            	$('#noMorePosts').removeAttr('hidden');
            }
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }
    $scope.postpost=function(){
        var formData = new FormData();
        for(var i=0;i<$scope.postfile.length;i++)
        {
            formData.append("files[]",$scope.postfile[i]);
        }
        formData.append('content',$scope.postContent);
        $('#PostBeforeLoading').attr('hidden','true');
        $('#PostLoading').removeAttr('hidden');
        $http.post("/user/post/"+drive_id+"/post", formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         }).then(function success(response){
            $scope.posts = response.data.posts;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $scope.postContent='';
            $scope.postfile=[];
            $('#PostBeforeLoading').removeAttr('hidden');
            $('#PostLoading').attr('hidden','true');
            if($scope.posts.length==0)
            {
            	$('#viewMorePostBeforeLoading').attr('hidden','true');
            	$('#noMorePosts').removeAttr('hidden');
            }
            else{
                $('#viewMorePostBeforeLoading').removeAttr('hidden');
            	$('#noMorePosts').attr('hidden','true');
            }
        }, function error(response){
            showErrors(response);
                
            $('#PostBeforeLoading').removeAttr('hidden');
            $('#PostLoading').attr('hidden','true');
            });
    }
    $scope.viewMorePost=function(){
        $('#viewMorePostBeforeLoading').attr('hidden','true');
        $('#viewMorePostLoading').removeAttr('hidden');
        var id=0;
        if($scope.posts.length>0)
            id=$scope.posts.slice(-1)[0].id;
        $http({
            method : "POST",
            url : "/user/post/"+drive_id+"/viewmoreposts",
            data:{'id':id}
        }).then(function mySuccess(response) {
            if(response.data.posts.length>0)
            {
                $scope.posts=$scope.posts.concat(response.data.posts);
                $('#viewMorePostBeforeLoading').removeAttr('hidden');
                $('#viewMorePostLoading').attr('hidden','true');

            }
            else{
                $('#noMorePosts').removeAttr('hidden');
                $('#viewMorePostLoading').attr('hidden','true');
            }
            
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
            $('#viewMorePostBeforeLoading').removeAttr('hidden');
            $('#viewMorePostLoading').attr('hidden','true');
        });
    }
    var removeAtag=function(html){
        var div = document.createElement("div");
        div.innerHTML = html;
        var text = div.textContent || div.innerText || "";
        return text;
    }
    $scope.editPost=function(p){
        $scope.post=Object.assign({},p);
        $scope.post.content=removeAtag($scope.post.content);   
    }
    
    $scope.savePost=function(p)
    {
        $('#editPostBeforeLoading').attr('hidden','true');
        $('#editPostLoading').removeAttr('hidden');
        var con = $filter('removeHTMLTags')($scope.post.content);
        $http({
            method : "POST",
            url : "/user/post/"+drive_id+"/updatepost",
            data:{'content':con,'id':$scope.post.id}
        }).then(function mySuccess(response) {
            $scope.post=response.data;
            for(var i=0;i<$scope.posts.length;i++)
            {
                if($scope.posts[i].id==$scope.post.id)
                {
                    $scope.posts[i]=$scope.post;
                    break;
                }
            }
            $('#editPostBeforeLoading').removeAttr('hidden');
            $('#editPostLoading').attr('hidden','true');
            $('#editPost').modal('hide');
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }
    $scope.deletePost=function(){
        $('#removePostBeforeLoading').attr('hidden','true');
        $('#removePostLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/post/"+drive_id+"/deletepost",
            data:{'id':$scope.post.id}
        }).then(function mySuccess(response) {
            $scope.posts = response.data.posts;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#removePostBeforeLoading').removeAttr('hidden');
            $('#removePostLoading').attr('hidden','true');
            $('#removePost').modal('hide');
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }
    $scope.initPostNotice=function(){
        $scope.notice.title='';
        $scope.notice.description='';
    }
    $scope.postNotice=function(){
        $('#postNoticeBeforeLoading').attr('hidden','true');
        $('#postNoticeLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/post/"+drive_id+"/postnotice",
            data:{'title':$scope.notice.title,'description':$scope.notice.description}
        }).then(function mySuccess(response) {
            $scope.notice=response.data.notice;

            $scope.notices.unshift(Object.assign({},$scope.notice));
            $('#postNoticeBeforeLoading').removeAttr('hidden');
            $('#postNoticeLoading').attr('hidden','true');
            $('#postNotice').modal('hide');
            $scope.notice.title='';
            $scope.notice.description='';
            setResponse($rootScope,response.data);
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }
    $scope.loadNotices=function(){
        $('#loadNoticesLoading').removeAttr('hidden');
        $('#loadNoticesBeforeLoading').attr('hidden','true');
        var id=0;
        if($scope.notices.length>0)
            id=$scope.notices.slice(-1)[0].id;
        $http({
            method : "POST",
            url : "/user/post/"+drive_id+"/loadnotices",
            data:{'id':id}
        }).then(function mySuccess(response) {
            setResponse($rootScope,response.data);
            if(response.data.notices.length>0)
            {
                $scope.notices=$scope.notices.concat(response.data.notices);
                $('#loadNoticesBeforeLoading').html('view more');
                $('#loadNoticesBeforeLoading').removeAttr('hidden');
                $('#loadNoticesLoading').attr('hidden','true');
            }
            else
            {   $('#noMoreNotice').removeAttr('hidden');

                $('#loadNoticesLoading').attr('hidden','true');
            }
            
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }
    $scope.viewNotice=function(n)
    {
        $scope.notice=n;
        var date = $filter('date')(n.created_at.replace(' ','T'),'MMM d, y h:mm a');
        $('#noticeDate').html(date);
        $('#noticeModalBody').html(n.description);
    }
    $scope.saveNotice=function()
    {
        $('#saveNoticeBeforeLoading').attr('hidden','true');
        $('#saveNoticeLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/post/"+drive_id+"/savenotice",
            data:{'title':$scope.notice.title,'description':$scope.notice.description,'id':$scope.notice.id}
        }).then(function mySuccess(response) {
            $scope.notice=response.data.notice;
            
            $('#saveNoticeBeforeLoading').removeAttr('hidden');
            $('#saveNoticeLoading').attr('hidden','true');
            $('#editNotice').modal('hide');
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
            $('#saveNoticeBeforeLoading').removeAttr('hidden');
            $('#saveNoticeLoading').attr('hidden','true');
            $('#editNotice').modal('hide');
        });
    }
    $scope.getIcon=function(ext){
        if(ext=='docx' || ext=='rtf' || ext=='doc')
            return 'docx.png';
        else if(ext=='pdf')
            return 'pdf.png';
        else if(ext=='png' || ext=='jpg' || ext=='bmp')
            return 'img.png';
        else if(ext='zip'|| ext=='7z'|| ext=='rar')
            return 'zip.png';
    }
    $scope.postedDateFormat=function(date)
    {
        return $filter('date')(date.replace(' ','T'),'MMM d, y h:mm a');
    }
    
})



driveapps.directive("mfileread", [function () {
    return {
        scope: {
            mfileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                for(var i =0;i<changeEvent.target.files.length;i++ )
                {
                      if(changeEvent.target.files[i].size>6000000){
            
      	    		$('#errorMessage').empty();
           	 	$('#errorDetails').empty();
            		$('#errorMessage').html('File size limit exceeded');
           
        
       	
        		$('#errorDrive').modal('show');
        		return;
        		}
                }
                scope.$apply(function () {
                   scope.mfileread = changeEvent.target.files;
                   console.log(scope.mfileread); 
                    
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    }
}]);

driveapps.filter('removeHTMLTags', function() {
	return function(text) {
		return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
	};
});