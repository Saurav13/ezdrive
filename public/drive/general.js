driveapps.controller('GeneralController',function($scope,$http,$rootScope){
    var drive_id;
     var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
  
    $scope.init=function(){
        drive_id=$('#drive_id').val();
    }
    $scope.g_accept=function(u_id){
        $('#GeneralAcceptBeforeLoading'+u_id).attr('hidden','true');
        $('#GeneralAcceptLoading'+u_id).removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/accept",
            data:{'user_id':u_id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $rootScope.newRequests = response.data.newRequests;
            $rootScope.fewStudents = response.data.fewStudents;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#GeneralAcceptBeforeLoading'+u_id).removeAttr('hidden');
            $('#GeneralAcceptLoading'+u_id).attr('hidden','true');

            // console.log($scope.folders);
        }, function myError(response) {
        $('#GeneralAcceptBeforeLoading'+u_id).removeAttr('hidden');
            $('#GeneralAcceptLoading'+u_id).attr('hidden','true');
            showErrors(response);
        });
    }
    $scope.g_ignore=function(u_id){
        $('#GeneralIgnoreBeforeLoading'+u_id).attr('hidden','true');
        $('#GeneralIgnoreLoading'+u_id).removeAttr('hidden');
        $http({
            method : "POST",
            url : "/user/manager/"+drive_id+"/ignore",
            data:{'user_id':u_id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $rootScope.newRequests = response.data.newRequests;
            $rootScope.fewStudents = response.data.fewStudents;
            setSpaces(response.data);
            setResponse($rootScope,response.data);
            $('#GeneralIgnoreBeforeLoading'+u_id).removeAttr('hidden');
            $('#GeneralIgnoreLoading'+u_id).attr('hidden','true');

            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
            $('#GeneralIgnoreBeforeLoading'+u_id).removeAttr('hidden');
            $('#GeneralIgnoreLoading'+u_id).attr('hidden','true');
        });
    }
});