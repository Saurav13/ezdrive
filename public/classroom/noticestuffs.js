c.controller('GeneralController',function($scope,$rootScope ,$http,$filter){

    $scope.notices=[];
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    $scope.loadGeneral=function(){
            
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/notice/loadnotices"
        }).then(function mySuccess(response) {
            $scope.notices = response.data.notices;
            $('#loadGeneralBeforeLoading').removeAttr('hidden');
            $('#loadGeneralLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
        });
    }

    $scope.loadmoreNotices=function(){
        $('#loadNoticesLoading').removeAttr('hidden');
        $('#loadNoticesBeforeLoading').attr('hidden','true');

        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/notice/morenotices",
            data:{'id':$scope.notices.slice(-1)[0].id}
        }).then(function mySuccess(response) {
            if(response.data.notices.length > 0)
            {
                $scope.notices=$scope.notices.concat(response.data.notices);
                $('#loadNoticesBeforeLoading').removeAttr('hidden');
                $('#loadNoticesLoading').attr('hidden','true');
            }
            else
            {   
                $('#noMoreNotice').removeAttr('hidden');
                $('#loadNoticesLoading').attr('hidden','true');
            }
            
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }

    $scope.viewNotice=function(n)
    {
        var date = $filter('date')(n.created_at.replace(' ','T'),'MMM d, y h:mm a');
        $('#noticeDate').html(date);
        $('#noticeModalBody').html(n.description);
    }
});