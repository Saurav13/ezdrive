var c = angular.module('ClassMaterial',[]);

c.controller('DriveController',function($scope,$http,$sce,$q,$scope){
    $scope.drive_id;
    $scope.selected=false;
    $scope.selectType;
    $scope.entity = {};
    $scope.folders=[]
    $scope.files=[]
    $scope.isRoot = true;
    $scope.breadcrumbs=[];
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }

    $scope.getIcon=function(ext){
        if(ext=='docx' || ext=='rtf' || ext=='doc' || ext=='dot' || ext=='wbk'||ext=='docm'||ext=='dotx'||ext=='dotm'||ext=='docv'||ext=='txt'||ext=='odt'||ext=='tex'||ext=='wps'||ext=='wks'||ext=='wpd')
            return 'docx.png';
        else if(ext=='pdf' ||ext=='xpdf' )
            return 'pdf.png';
        else if(ext=='png' || ext=='jpg' || ext=='bmp'||ext=='ai' ||ext=='gif'||ext=='ico'||ext=='jpeg'||ext=='ps'||ext=='psd'||ext=='svg'||ext=='tif'||ext=='tiff')
            return 'img.png';
        else if(ext=='zip'|| ext=='7z'|| ext=='rar' ||ext=='arj' ||ext=='rpm' ||ext=='tar' ||ext=='pkg' ||ext=='gz'||ext=='tar.gz'||ext=='z'||ext=='deb')
            return 'zip.png';
        else if(ext=='mp4'||ext=='wmp'||ext=='flv'||ext=='mkv'||ext=='avi'||ext=='webm'||ext=='vob'||ext=='mov'||ext=='m4p'||ext=='mpg'||ext=='mpeg'||ext=='3gp')
            return 'video.png';
        else if(ext=='xls'||ext=='xlt'||ext=='xlm'||ext=='xlxs'||ext=='xlsm'||ext=='xltx'||ext=='xltm'||ext=='xlsb'||ext=='xla'||ext=='xlam'||ext=='xll'||ext=='xlw')
            return 'excel.png'
        else if(ext=='pptx'||ext=='ppt'||ext=='pot'||ext=='pps'||ext=='pptm'||ext=='potx'||ext=='potm'||ext=='ppam'||ext=='ppsx'||ext=='ppsm'||ext=='sldx'||ext=='sldm')
            return 'ppt.png'
        else
            return 'file.png';
    }

    $scope.select=function(obj,typ){
        $scope.selected=true;
        $scope.entity=obj;
        $scope.selectType=typ;
    }

    $scope.openRoot=function(){//init function
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/unauth/"+$scope.drive_id+"/drive/getroot"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }

    $scope.searchDrive=function(key){
        
        if(!key || key.replace(' ','').length == 0) return;
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/unauth/"+$scope.drive_id+"/drive/searchdrive",
            data: {'key':key}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.isRoot = false;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
            
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }

    $scope.openFolder=function(id){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/unauth/"+$scope.drive_id+"/drive/openfolder",
            data: {'id':id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.isRoot = false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }

    $scope.open=function(){
        if($scope.selectType==1)
        {
            $('#DriveMainBeforeLoading').attr('hidden','true');
            $('#DriveMainLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/unauth/"+$scope.drive_id+"/drive/openfolder",
                data: {'id':$scope.entity.id}
            }).then(function mySuccess(response) {
                $scope.selected=false;
                $scope.isRoot = false;
                $scope.folders = response.data.folders;
                $scope.files = response.data.files;
                $scope.breadcrumbs=response.data.breadcrumbs;
                $('#DriveMainBeforeLoading').removeAttr('hidden');
                $('#DriveMainLoading').attr('hidden','true');
                // console.log($scope.contents);
            }, function myError(response) {
                showErrors(response);
                $('#DriveMainBeforeLoading').removeAttr('hidden');
                $('#DriveMainLoading').attr('hidden','true');
            });
        }
        else{
            $scope.openFile($scope.entity.id);
        }
    }

    $scope.back=function(){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/unauth/"+$scope.drive_id+"/drive/back"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.isRoot = response.data.isRoot;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }
});