var classapp=angular.module('classapp',['ngSanitize','angular-loading-bar'])
.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '.myProgress';
    cfpLoadingBarProvider.spinnerTemplate = '<div><span><i class="fa fa-spinner fa-spin"></i>&nbsp;&nbsp;<span id="lloader">Uploading</span>...</span></div>';
}]);
var showSuccess=function(message){
    $('#successMessage').html(message);
    $('#showSuccess').modal('show');
}

classapp.controller('GeneralController',function($scope,$location,$http,$sce,$q,$filter,$timeout){
    $scope.generals=[];
    $scope.tasks=[];
    $scope.notices=[];
    $scope.results=[];
    $scope.posts=[];
    $scope.board;
    $scope.board_dates = [];
    $scope.postfiles = [];
    $scope.selectedTask;
    $scope.g_pagn_length=10;
    $scope.t_pagn_length=6;
    $scope.p_pagn_length=10;
    $scope.n_pagn_length=10;
    $scope.g_pagn_count=10;
    $scope.t_pagn_count=9;
    $scope.p_pagn_count=10;
    $scope.n_pagn_count=10;
    $scope.rollno;
    $scope.editmode;
    $scope.duplicate;
    $scope.uploadtsk = false;
    $scope.edittsk = false;
    
    var addType=function(data,type){
        for(var i=0;i<data.length;i++){
            data[i]['type']=type;
            if(type=='extra'){
                $scope.postfiles = $scope.postfiles.concat(data[i].postedfiles);
            }
        }
        return data;
    }

    var sortAndGetGenerals=function(){
        $scope.tasks=addType($scope.tasks,'ttask');
        $scope.notices=addType($scope.notices,'notice');
        $scope.posts=addType($scope.posts,'extra');
        $scope.results=addType($scope.results,'result');

        
        $scope.generals=$scope.generals.concat($scope.tasks);
        $scope.generals=$scope.generals.concat($scope.notices);
        $scope.generals=$scope.generals.concat($scope.posts);
        $scope.generals=$scope.generals.concat($scope.results);
       
        
        $scope.generals=$filter('orderBy')($scope.generals,'created_at',true);
        
    }
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    $scope.init=function(){
        $('#BoardMainBeforeLoading').attr('hidden','true');
        $('#BoardMainLoading').removeAttr('hidden');
        $('#TaskMainBeforeLoading').attr('hidden','true');
        $('#TaskMainLoading').removeAttr('hidden');
        $('#NoticeMainBeforeLoading').attr('hidden','true');
        $('#NoticeMainLoading').removeAttr('hidden');
        $('#PostMainBeforeLoading').attr('hidden','true');
        $('#PostMainLoading').removeAttr('hidden');
        
        $scope.editmode = $scope.rollno ? false:true;
        $scope._rollno = $scope.rollno;

        $http({
            method : "POST",
            url : "/class/"+$scope.drive_id+"/generalinit"
        }).then(function mySuccess(response) {
            $scope.board_dates = response.data.board_dates;
            $scope.board = response.data.board;
            $scope.tasks=response.data.tasks;
            $scope.posts=response.data.posts;
            $scope.results=response.data.results;
            $scope.notices=response.data.notices;
            sortAndGetGenerals();
            $('#BoardMainBeforeLoading').removeAttr('hidden');
            $('#BoardMainLoading').attr('hidden','true');
            $('#GeneralMainBeforeLoading').removeAttr('hidden');
            $('#GeneralMainLoading').attr('hidden','true');
            $('#TaskMainBeforeLoading').removeAttr('hidden');
            $('#TaskMainLoading').attr('hidden','true');
            $('#NoticeMainBeforeLoading').removeAttr('hidden');
            $('#NoticeMainLoading').attr('hidden','true');
            $('#PostMainBeforeLoading').removeAttr('hidden');
            $('#PostMainLoading').attr('hidden','true');
        }, function myError(response) {
            $('#BoardMainBeforeLoading').removeAttr('hidden');
            $('#BoardMainLoading').attr('hidden','true');
            $('#GeneralMainBeforeLoading').removeAttr('hidden');
            $('#GeneralMainLoading').attr('hidden','true');
            $('#TaskMainBeforeLoading').removeAttr('hidden');
            $('#TaskMainLoading').attr('hidden','true');
            $('#NoticeMainBeforeLoading').removeAttr('hidden');
            $('#NoticeMainLoading').attr('hidden','true');
            $('#PostMainBeforeLoading').removeAttr('hidden');
            $('#PostMainLoading').attr('hidden','true');
            showErrors(response);
        });

        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$scope.drive_id+"/drive/getroot"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });    
    }

    $scope.viewMoreGenerals=function(){
        $('#ViewMoreGeneralBeforeLoading').attr('hidden','true');
        $('#ViewMoreGeneralLoading').removeAttr('hidden');
        $timeout(function() {  
            $('#ViewMoreGeneralBeforeLoading').removeAttr('hidden');
            $('#ViewMoreGeneralLoading').attr('hidden','true');
            $scope.g_pagn_count+=$scope.g_pagn_length;
        }, 500);   
    }

    $scope.viewMoreNotices=function(){
        $('#ViewMoreNoticeBeforeLoading').attr('hidden','true');
        $('#ViewMoreNoticeLoading').removeAttr('hidden');
        $timeout(function() {  
            $('#ViewMoreNoticeBeforeLoading').removeAttr('hidden');
            $('#ViewMoreNoticeLoading').attr('hidden','true');
            $scope.n_pagn_count+=$scope.n_pagn_length;
        }, 500);  
    }

    $scope.viewMoreTasks=function(){
        $('#ViewMoreTaskBeforeLoading').attr('hidden','true');
        $('#ViewMoreTaskLoading').removeAttr('hidden');
        $timeout(function() {  
            $('#ViewMoreTaskBeforeLoading').removeAttr('hidden');
            $('#ViewMoreTaskLoading').attr('hidden','true');
            $scope.t_pagn_count+=$scope.t_pagn_length;
        }, 500);
            
        
    }

    $scope.viewMorePosts=function(){
        $('#ViewMorePostBeforeLoading').attr('hidden','true');
        $('#ViewMorePostLoading').removeAttr('hidden');
        $timeout(function() {  
            $('#ViewMorePostBeforeLoading').removeAttr('hidden');
            $('#ViewMorePostLoading').attr('hidden','true');
            $scope.p_pagn_count+=$scope.p_pagn_length;
        }, 500);
            
        
    }

    $scope.getIcon=function(ext){
        if(ext=='docx' || ext=='rtf' || ext=='doc' || ext=='dot' || ext=='wbk'||ext=='docm'||ext=='dotx'||ext=='dotm'||ext=='docv'||ext=='txt'||ext=='odt'||ext=='tex'||ext=='wps'||ext=='wks'||ext=='wpd')
            return 'docx.png';
        else if(ext=='pdf' ||ext=='xpdf' )
            return 'pdf.png';
        else if(ext=='png' || ext=='jpg' || ext=='bmp'||ext=='ai' ||ext=='gif'||ext=='ico'||ext=='jpeg'||ext=='ps'||ext=='psd'||ext=='svg'||ext=='tif'||ext=='tiff')
            return 'img.png';
        else if(ext=='zip'|| ext=='7z'|| ext=='rar' ||ext=='arj' ||ext=='rpm' ||ext=='tar' ||ext=='pkg' ||ext=='gz'||ext=='tar.gz'||ext=='z'||ext=='deb')
            return 'zip.png';
        else if(ext=='mp4'||ext=='wmp'||ext=='flv'||ext=='mkv'||ext=='avi'||ext=='webm'||ext=='vob'||ext=='mov'||ext=='m4p'||ext=='mpg'||ext=='mpeg'||ext=='3gp')
            return 'video.png';
        else if(ext=='xls'||ext=='xlt'||ext=='xlm'||ext=='xlxs'||ext=='xlsm'||ext=='xltx'||ext=='xltm'||ext=='xlsb'||ext=='xla'||ext=='xlam'||ext=='xll'||ext=='xlw')
            return 'excel.png'
        else if(ext=='pptx'||ext=='ppt'||ext=='pot'||ext=='pps'||ext=='pptm'||ext=='potx'||ext=='potm'||ext=='ppam'||ext=='ppsx'||ext=='ppsm'||ext=='sldx'||ext=='sldm')
            return 'ppt.png'
        else
            return 'file.png';
    }

    $scope.viewTask=function(t)
    {
        $scope.selectedTask=t;
    }

    $scope.changeBoard = function(){
        $('#theBoard').attr('hidden','true');
        $('#changeBoardLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$scope.drive_id+"/board/change",
            data:{'date':$scope.board.date}
        }).then(function mySuccess(response) {
            $scope.board = response.data.board;
            $('#theBoard').removeAttr('hidden');
            $('#changeBoardLoading').attr('hidden','true');
        }, function myError(response) {
            $scope.board.content = '';
            showErrors(response);
            $('#theBoard').removeAttr('hidden');
            $('#changeBoardLoading').attr('hidden','true');
        });
    }

    //Drive Controller//
    $scope.folders=[]
    $scope.files=[]
    $scope.isRoot = true;
    $scope.breadcrumbs=[];

    $scope.drive_openRoot=function(){//init function
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$scope.drive_id+"/drive/getroot"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }

    $scope.drive_searchDrive=function(key){
        
        if(!key || key.replace(' ','').length == 0) return;
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$scope.drive_id+"/drive/searchdrive",
            data: {'key':key}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.isRoot = false;
            
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
            
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }

    $scope.drive_openFolder=function(id){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$scope.drive_id+"/drive/openfolder",
            data: {'id':id}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.isRoot = false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }

    $scope.open=function(){
        if($scope.selectType==1)
        {
            $('#DriveMainBeforeLoading').attr('hidden','true');
            $('#DriveMainLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/class/"+$scope.drive_id+"/drive/openfolder",
                data: {'id':$scope.entity.id}
            }).then(function mySuccess(response) {
                $scope.selected=false;
                $scope.isRoot = false;
                $scope.folders = response.data.folders;
                $scope.files = response.data.files;
                $scope.breadcrumbs=response.data.breadcrumbs;
                $('#DriveMainBeforeLoading').removeAttr('hidden');
                $('#DriveMainLoading').attr('hidden','true');
                // console.log($scope.contents);
            }, function myError(response) {
                showErrors(response);
                $('#DriveMainBeforeLoading').removeAttr('hidden');
                $('#DriveMainLoading').attr('hidden','true');
            });
        }
        else{
            $scope.openFile($scope.entity.id);
        }
    }

    $scope.drive_back=function(){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$scope.drive_id+"/drive/back"
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.isRoot = response.data.isRoot;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            showErrors(response);
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        });
    }
    
    //--Drive Controller end//


    //from scope//
    $scope.drive_id;
    $scope.selected=false;
    $scope.selectType;
    $scope.content;
    $scope.entity = {};
    $scope.downloads = [];
    $scope.contentimage;
    $scope.viewerror = '';

    $scope.select=function(obj,typ){
        if(typ == 4 || typ == 5){
            $scope._rollno = $scope.rollno;
            $('#taskuploadError').html('');
            $('#taskeditError').html('');
        }

        $scope.selected=true;
        $scope.entity=obj;
        $scope.selectType=typ;
    }

    $scope.openFile=function(id){
        var url;
        
        $scope.viewerror = '';
        $('#viewerpopup').modal('show');
        $('#viewerimage').attr('hidden', 'hidden');
        $('#viewerdoc').attr('hidden', 'hidden');
        $('#BeforeLoading').removeAttr('hidden');

        if($scope.selectType==2)
        {
            $scope.selectedname = $scope.entity.name;
            url = "/class/"+$scope.drive_id+"/drive/getfilelink";
        }
        else if($scope.selectType==3)
        {
            $scope.selectedname = $scope.entity.file;
            url = "/class/"+$scope.drive_id+"/post/getfilelink";
        }
        else if($scope.selectType==4)
        {
            $scope.selectedname = $scope.entity.file.split('/').slice(-1)[0];
            url = "/class/"+$scope.drive_id+"/task/getfilelink";
        }
        else if($scope.selectType==5)
        {
            $scope.selectedname = $scope.entity.name;
            url = "/class/"+$scope.drive_id+"/task/getsubmissionlink";
        }

        $http({
            method : "POST",
            url : url,
            data: {'id':$scope.entity.id},
            responseType:'json'
        }).then(function mySuccess(response) {
            if($scope.entity.mime_type.split('/')[0] == 'image' || $scope.entity.mime_type.split('/')[0] == 'video'){
                $scope.contentimage = $sce.trustAsResourceUrl(response.data.url);
            }
            else{
                $scope.content = $sce.trustAsResourceUrl('https://docs.google.com/viewer?embedded=true&url='+encodeURIComponent(response.data.url));
            }
        }, function myError(response){
           
            if(response.status == 404){
                $scope.viewerror = "Sorry, the file you are looking for couldn't be found or has been deleted.";
            }
            else{
                $scope.viewerror = "Sorry, but something went wrong please try again.";
            }
            $('#BeforeLoading').attr('hidden','hidden');
                
        });
    }

    $scope.download=function(){
        var url;
        var type;
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        
        if($scope.selectType==1)
        {   
            //var canceller = $q.defer();

            //$scope.downloads.push({id:$scope.entity.id,type:$scope.selectType,name:$scope.entity.name+'.zip',canceller:canceller});


           url = "/class/"+$scope.drive_id+"/drive/downloadfolder/"+$scope.entity.id+'/'+$scope.entity.name+'.zip';
             
            // $http({
            //     method : "POST",
            //     url : "/class/"+$scope.drive_id+"/drive/downloadfolder",
            //     data: {'id':$scope.entity.id},
            //     responseType:'json',
            //     timeout: canceller.promise
            // }).then(function mySuccess(response) {

                
            //     for (var i = 0;i<$scope.downloads.length;i++){
            //         if($scope.downloads[i].id == $scope.entity.id && $scope.downloads[i].type == 1)
            //             $scope.downloads.splice(i,1);
            //     }

            // }, function myError(response){
            //     if(response.xhrStatus == 'error')
            //         $scope.downloads = [];
            //     showErrors(response);
                
            // });
        }
        else if($scope.selectType==2)
        {
            url = "/class/"+$scope.drive_id+"/drive/downloadfile/"+$scope.entity.id+'/'+$scope.entity.name;
        }
        else if($scope.selectType==3)
        {
            url = "/class/"+$scope.drive_id+"/post/downloadfile/"+$scope.entity.id+'/'+$scope.entity.file;
        }
        else if($scope.selectType==4)
        {
            url = "/class/"+$scope.drive_id+"/task/downloadfile/"+$scope.entity.id+'/'+$scope.entity.file.split('/').slice(-1)[0];
        }
        else if($scope.selectType==5)
        {
            url = "/class/"+$scope.drive_id+"/task/downloadsubmission/"+$scope.entity.id+'/'+$scope.entity.name;
        }
        var fileURL = url;
        a.href = fileURL;
        a.target = '_blank';
        a.click();
    }

    $scope.canceldownload = function(id,typ){

        for (var i = 0;i<$scope.downloads.length;i++){
            if($scope.downloads[i].id == id && $scope.downloads[i].type == typ){
                $scope.downloads[i].canceller.resolve();
                $scope.downloads.splice(i,1);                
            }
        }
         
    }
    //..from scope end//



    //from tasks contorller//
    $scope.file;
    


    $scope.task_open=function(){   
        $scope.openFile($scope.entity.id);
    }
    
    $scope.uploadTask = function(){
        if(!$scope.file) return;
        $('#taskuploadError').html('');
        if($scope.file.size > 5242880){
            $('#taskuploadError').html('The file must not me larger than 5MB.');
            return;
        }
        var formData = new FormData();
        formData.append("file",$scope.file);
        formData.append('collection_id',$scope.entity.id);
        
        if($scope._rollno)
            formData.append('rollno',$scope._rollno);
        else{
            $('#taskuploadError').html('The rollno field is required.');
            return;
        }

        $('#uploadFileBeforeLoading').attr('hidden','true');
        $('#uploadFileLoading').removeAttr('hidden');
        
        $scope.uploadtsk = true;
        $http.post("/class/"+$scope.drive_id+"/task/sendSubmission", formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         }).then(function success(response){
            for(var i=0;i<$scope.tasks.length;i++){
                if($scope.tasks[i].id == formData.get('collection_id')){
                    $scope.tasks[i].mysubmission = response.data.mysubmission;
                }
            }
            $scope.rollno = response.data.rollno;
            $scope.duplicate = response.data.duplicate;
                
            $scope.editmode = false;
            $timeout(function () {
                $scope.uploadtsk = false;
                $('#uploadFileBeforeLoading').removeAttr('hidden');
                $('#uploadFileLoading').attr('hidden','true');
               
                $('#uploadFile').modal('hide');
                showSuccess('Assignment Submitted Successfully!!');
            }, 1000);
        }, function error(xhr){
            if(xhr.status == 422){
                $.each( xhr.data.errors, function( key, value ) {
                    $('#taskuploadError').html(value[0]);
                    return false;
                });
            }   
            else
                $('#taskuploadError').html('Something went wrong. Please try again.');

            $scope.uploadtsk = false;
            $('#uploadFileBeforeLoading').removeAttr('hidden');
            $('#uploadFileLoading').attr('hidden','true');
        });
    }

    $scope.editUploadedTask = function(){
        
        var formData = new FormData();
        if($scope._rollno)
            formData.append('rollno',$scope._rollno);
        else{
            $('#taskeditError').html('The rollno field is required.');
            return;
        }
        
        if($scope.file){
            if($scope.file.size > 5242880){
                $('#taskeditError').html('The file must not me larger than 5MB.');
                return;
            }
            $scope.edittsk = true;
            formData.append("file",$scope.file);
        }
        else
        	$('#editFileLoading').removeAttr('hidden');

        formData.append('submission_id',$scope.entity.id);
        
        $('#editFileBeforeLoading').attr('hidden','true');
        
        $('#taskeditError').html('');
        
        
        $http.post("/class/"+$scope.drive_id+"/task/editSubmission", formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         }).then(function success(response){
            for(var i=0;i<$scope.tasks.length;i++){
                if($scope.tasks[i].mysubmission && $scope.tasks[i].mysubmission.id == formData.get('submission_id')){
                    $scope.tasks[i].mysubmission = response.data.mysubmission;
                }
            }
            $scope.rollno = response.data.rollno;
            $scope.editmode = false;
            $scope.duplicate = response.data.duplicate;
            $timeout(function () {
                $scope.edittsk = false;
                $('#editFileBeforeLoading').removeAttr('hidden');
                $('#editFileLoading').attr('hidden','true');
                // $scope.file = null;

                $('#editFile').modal('hide');
                showSuccess('Assignment Submitted Successfully!!');

            }, 1000);
            
            
        }, function error(xhr){
            if(xhr.status == 422){
                $.each( xhr.data.errors, function( key, value ) {
                    $('#taskeditError').html(value[0]);
                    return false;
                });
            }   
            else
                $('#taskeditError').html('Something went wrong. Please try again.');
            $scope.edittsk = false;
            $('#editFileBeforeLoading').removeAttr('hidden');
            $('#editFileLoading').attr('hidden','true');
        });
    }
    //--from task controller end//

});






angular.element('.item-list').on('contextmenu', '.item-list-item', function(e) {
    var menu = angular.element('#context-menu');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }
    $('.item-list-item').each(function(){
        $(this).removeClass('active');
    });

    $(this).addClass('active');

    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
    
});

angular.element(window.document).on('click', function() {
    angular.element('#context-menu').hide();
    angular.element('#context-menupost').hide();
    angular.element('#context-menutask').hide();
    angular.element('#context-general').hide();
});

angular.element('.task-list').on('contextmenu', '.task-list-item', function(e) {
    
    var menu = angular.element('#context-menutask');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }

    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
});

angular.element('.post-list').on('contextmenu', '.post-list-item', function(e) {
    
    var menu = angular.element('#context-menupost');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }

    var l = e.pageX;
    var t = e.pageY + menu.height();

    diff1 = Math.abs(e.pageY-$(this).offset().top);
    diff2 = Math.abs(t-$(this).offset().top);

    if(diff1 < diff2)
        t = e.pageY;

    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
});

angular.element('.general_items').on('contextmenu', '.general-list-item', function(e) {
    
    var menu = angular.element('#context-general');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }
    
    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
});

angular.element('.task-list').on('click', '.task-list-item', function(e) {
    
    var menu = angular.element('#context-menutask');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }

    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.stopImmediatePropagation();
    e.preventDefault();
});

angular.element('.post-list').on('click', '.post-list-item', function(e) {
    
    var menu = angular.element('#context-menupost');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }
    
    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    diff1 = Math.abs(e.pageY-$(this).offset().top);
    diff2 = Math.abs(t-$(this).offset().top);

    if(diff1 < diff2)
        t = e.pageY;
        
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.stopImmediatePropagation();
    e.preventDefault();
});

angular.element('.general_items').on('click', '.general-list-item', function(e) {
    var menu = angular.element('#context-general');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }
    
    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.stopImmediatePropagation();
    e.preventDefault();
});

classapp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    $('#taskuploadError').html('');
                    $('#taskeditError').html('');
                    
                });
            });
        }
    }
}]);

classapp.directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
});

classapp.directive('iosDblclick',
    function () {

        const DblClickInterval = 300; //milliseconds

        var firstClickTime;
        var waitingSecondClick = false;

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {

                    if (!waitingSecondClick) {
                        firstClickTime = (new Date()).getTime();
                        waitingSecondClick = true;

                        setTimeout(function () {
                            waitingSecondClick = false;
                        }, DblClickInterval);
                    }
                    else {
                        waitingSecondClick = false;

                        var time = (new Date()).getTime();
                        if (time - firstClickTime < DblClickInterval) {
                            scope.$apply(attrs.iosDblclick);
                        }
                    }
                });
            }
        };
    }
);