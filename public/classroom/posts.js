c.controller('PostController',function($scope,$http,$sce,$rootScope){

    $scope.posts=[];
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    $scope.getPosts=function(){
        $('#PostMainBeforeLoading').attr('hidden','true');
        $('#PostMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/post/getposts"
        }).then(function mySuccess(response) {
            $scope.posts = response.data.posts;
            $('#PostMainBeforeLoading').removeAttr('hidden');
            $('#PostMainLoading').attr('hidden','true');

        }, function myError(response) {
            showErrors(response);
        });
    }

    $scope.viewMorePost=function(){
        $('#viewMorePostBeforeLoading').attr('hidden','true');
        $('#viewMorePostLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/post/viewmoreposts",
            data:{'id':$scope.posts.slice(-1)[0].id}
        }).then(function mySuccess(response) {
            if(response.data.posts.length>0)
            {
                $scope.posts=$scope.posts.concat(response.data.posts);
                $('#viewMorePostBeforeLoading').removeAttr('hidden');
                $('#viewMorePostLoading').attr('hidden','true');

            }
            else{
                $('#noMorePosts').removeAttr('hidden');
                $('#viewMorePostLoading').attr('hidden','true');
            }
            
            // console.log($scope.folders);
        }, function myError(response) {

            showErrors(response);
            $('#viewMorePostBeforeLoading').removeAttr('hidden');
            $('#viewMorePostLoading').attr('hidden','true');
        });
    }

    $scope.allNotices=function(){
        $('#allNoticesLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/post/allnotices"
        }).then(function mySuccess(response) {
            $scope.notices=response.data.notices;
            $('#allNoticesLoading').attr('hidden','true');
            // console.log($scope.folders);
        }, function myError(response) {
            showErrors(response);
        });
    }

    $scope.open=function(){   
        $rootScope.openFile($rootScope.entity.id);
    }
});

angular.element('.post-list').on('contextmenu', '.post-list-item', function(e) {
    
    var menu = angular.element('#context-menupost');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }
    
    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
});