var c = angular.module('ClassMaterial',[]);



c.run(function($rootScope,$location,$http,$sce,$q) {
    $rootScope.drive_id;
    $rootScope.selected=false;
    $rootScope.selectType;
    $rootScope.content;
    $rootScope.entity = {};
    $rootScope.downloads = [];
    $rootScope.contentimage;
    $rootScope.viewerror = '';
    
    $rootScope.getIcon=function(ext){
        if(ext=='docx' || ext=='rtf' || ext=='doc' || ext=='dot' || ext=='wbk'||ext=='docm'||ext=='dotx'||ext=='dotm'||ext=='docv'||ext=='txt'||ext=='odt'||ext=='tex'||ext=='wps'||ext=='wks'||ext=='wpd')
            return 'docx.png';
        else if(ext=='pdf' ||ext=='xpdf' )
            return 'pdf.png';
        else if(ext=='png' || ext=='jpg' || ext=='bmp'||ext=='ai' ||ext=='gif'||ext=='ico'||ext=='jpeg'||ext=='ps'||ext=='psd'||ext=='svg'||ext=='tif'||ext=='tiff')
            return 'img.png';
        else if(ext=='zip'|| ext=='7z'|| ext=='rar' ||ext=='arj' ||ext=='rpm' ||ext=='tar' ||ext=='pkg' ||ext=='gz'||ext=='tar.gz'||ext=='z'||ext=='deb')
            return 'zip.png';
        else if(ext=='mp4'||ext=='wmp'||ext=='flv'||ext=='mkv'||ext=='avi'||ext=='webm'||ext=='vob'||ext=='mov'||ext=='m4p'||ext=='mpg'||ext=='mpeg'||ext=='3gp')
            return 'video.png';
        else if(ext=='xls'||ext=='xlt'||ext=='xlm'||ext=='xlxs'||ext=='xlsm'||ext=='xltx'||ext=='xltm'||ext=='xlsb'||ext=='xla'||ext=='xlam'||ext=='xll'||ext=='xlw')
            return 'excel.png'
        else if(ext=='pptx'||ext=='ppt'||ext=='pot'||ext=='pps'||ext=='pptm'||ext=='potx'||ext=='potm'||ext=='ppam'||ext=='ppsx'||ext=='ppsm'||ext=='sldx'||ext=='sldm')
            return 'ppt.png'
        else
            return 'file.png';
    }

    $rootScope.select=function(obj,typ){
        $rootScope.selected=true;
        $rootScope.entity=obj;
        $rootScope.selectType=typ;
    }

    $rootScope.openFile=function(id){
        var url;
        
        $rootScope.viewerror = '';
        $('#viewerpopup').modal('show');
        $('#viewerimage').attr('hidden', 'hidden');
        $('#viewerdoc').attr('hidden', 'hidden');
        $('#BeforeLoading').removeAttr('hidden');

        if($rootScope.selectType==2)
        {
            url = "/class/"+$rootScope.drive_id+"/drive/getfilelink";
        }
        else if($rootScope.selectType==3)
        {
            url = "/class/"+$rootScope.drive_id+"/post/getfilelink";
        }
        else if($rootScope.selectType==4)
        {
            url = "/class/"+$rootScope.drive_id+"/task/getfilelink";
        }
        else if($rootScope.selectType==5)
        {
            url = "/class/"+$rootScope.drive_id+"/task/getsubmissionlink";
        }

        $http({
            method : "POST",
            url : url,
            data: {'id':$rootScope.entity.id},
            responseType:'json'
        }).then(function mySuccess(response) {
            if($rootScope.entity.mime_type.split('/')[0] == 'image' || $rootScope.entity.mime_type.split('/')[0] == 'video'){
                $rootScope.contentimage = $sce.trustAsResourceUrl(response.data.url);
            }
            else{
                $rootScope.content = $sce.trustAsResourceUrl('https://docs.google.com/viewer?embedded=true&url='+encodeURIComponent(response.data.url));
            }
        }, function myError(response){
           
            if(response.status == 404){
                $rootScope.viewerror = "Sorry, the file you are looking for couldn't be found or has been deleted.";
            }
            else{
                $rootScope.viewerror = "Sorry, but something went wrong please try again.";
            }
            $('#BeforeLoading').attr('hidden','hidden');
                
        });
    }

    $rootScope.download=function(){
        var url;
        var type;
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        
        if($rootScope.selectType==1)
        {   
            var canceller = $q.defer();

            $rootScope.downloads.push({id:$rootScope.entity.id,type:$rootScope.selectType,name:$rootScope.entity.name+'.zip',canceller:canceller});

            $http({
                method : "POST",
                url : "/class/"+$rootScope.drive_id+"/drive/downloadfolder",
                data: {'id':$rootScope.entity.id},
                responseType:'json',
                timeout: canceller.promise
            }).then(function mySuccess(response) {
    
                var fileId = response.headers('X-FileId');
                var fileName = response.headers('X-FileName');
                var fileURL = response.data.url;
                a.href = fileURL;
                a.download = fileName;
                a.click();
                for (var i = 0;i<$rootScope.downloads.length;i++){
                    if($rootScope.downloads[i].id == fileId && $rootScope.downloads[i].type == 1)
                        $rootScope.downloads.splice(i,1);
                }
    
            }, function myError(response){
                if(response.xhrStatus == 'error')
                    $rootScope.downloads = [];
                
            });
            return;
        }
        else if($rootScope.selectType==2)
        {
            url = "/class/"+$rootScope.drive_id+"/drive/downloadfile/"+$rootScope.entity.id+'/'+$rootScope.entity.name;
        }
        else if($rootScope.selectType==3)
        {
            url = "/class/"+$rootScope.drive_id+"/post/downloadfile/"+$rootScope.entity.id+'/'+$rootScope.entity.file;
        }
        else if($rootScope.selectType==4)
        {
            url = "/class/"+$rootScope.drive_id+"/task/downloadfile/"+$rootScope.entity.id+'/'+$rootScope.entity.file.split('/').slice(-1)[0];
        }
        else if($rootScope.selectType==5)
        {
            url = "/class/"+$rootScope.drive_id+"/task/downloadsubmission/"+$rootScope.entity.id+'/'+$rootScope.entity.name;
        }
        var fileURL = $location.protocol() + "://"+location.host+url;
        a.href = fileURL;
        
        a.click();
    }

    $rootScope.canceldownload = function(id,typ){

        for (var i = 0;i<$rootScope.downloads.length;i++){
            if($rootScope.downloads[i].id == id && $rootScope.downloads[i].type == typ){
                $rootScope.downloads[i].canceller.resolve();
                $rootScope.downloads.splice(i,1);                
            }
        }
         
    }
});

c.controller('DriveController',function($scope,$http,$sce,$q,$rootScope){
    $scope.folders=[]
    $scope.files=[]
    $scope.isRoot = true;
    $scope.breadcrumbs=[];

    $scope.openRoot=function(){//init function
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/drive/getroot"
        }).then(function mySuccess(response) {
            $rootScope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
           // location.reload();
        });
    }

    $scope.searchDrive=function(key){
        
        if(key.replace(' ','').length == 0) return;
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/drive/searchdrive",
            data: {'key':key}
        }).then(function mySuccess(response) {
            $scope.selected=false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.isRoot = false;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
            
        }, function myError(response) {
//location.reload();
        });
    }

    $scope.openFolder=function(id){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/drive/openfolder",
            data: {'id':id}
        }).then(function mySuccess(response) {
            $rootScope.selected=false;
            $scope.isRoot = false;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            //location.reload();
        });
    }

    $scope.open=function(){
        if($rootScope.selectType==1)
        {
            $('#DriveMainBeforeLoading').attr('hidden','true');
            $('#DriveMainLoading').removeAttr('hidden');
            $http({
                method : "POST",
                url : "/class/"+$rootScope.drive_id+"/drive/openfolder",
                data: {'id':$rootScope.entity.id}
            }).then(function mySuccess(response) {
                $rootScope.selected=false;
                $scope.isRoot = false;
                $scope.folders = response.data.folders;
                $scope.files = response.data.files;
                $scope.breadcrumbs=response.data.breadcrumbs;
                $('#DriveMainBeforeLoading').removeAttr('hidden');
                $('#DriveMainLoading').attr('hidden','true');
                // console.log($scope.contents);
            }, function myError(response) {
              //  location.reload();
            });
        }
        else{
            $rootScope.openFile($rootScope.entity.id);
        }
    }

    $scope.back=function(){
        $('#DriveMainBeforeLoading').attr('hidden','true');
        $('#DriveMainLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/drive/back"
        }).then(function mySuccess(response) {
            $rootScope.selected=false;
            $scope.isRoot = response.data.isRoot;
            $scope.folders = response.data.folders;
            $scope.files = response.data.files;
            $scope.breadcrumbs=response.data.breadcrumbs;
            $('#DriveMainBeforeLoading').removeAttr('hidden');
            $('#DriveMainLoading').attr('hidden','true');
        }, function myError(response) {
            alert('wrong');
        });
    }
});


angular.element('.item-list').on('contextmenu', '.item-list-item', function(e) {
    var menu = angular.element('#context-menu');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }
    $('.item-list-item').each(function(){
        $(this).find('a').removeClass('active');
    });

    $(this).find('a').addClass('active');

    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
    
});

angular.element(window.document).on('click', function() {
    angular.element('#context-menu').hide();
    angular.element('#context-menupost').hide();
    angular.element('#context-menutask').hide();
});



c.directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
});