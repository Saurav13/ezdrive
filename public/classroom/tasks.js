c.controller('TaskController',function($scope,$http,$sce,$rootScope,$filter){

    $scope.tasks = [];
    $scope.file;
    $scope.today;
    var showErrors=function(response){
        $('#errorMessage').empty();
        $('#errorDetails').empty();
        $('#errorMessage').html(response.data.message)
        $.each(response.data.errors, function(index, value) {
            $('#errorDetails').append(value+'<br>');
        
        }); 
        $('#errorDrive').modal('show');
    }
    $scope.getTasks=function(){
        $scope.today = $filter('date')(new Date(), 'yyyy-MM-dd');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/task/gettasks"
        }).then(function mySuccess(response) {
            $scope.tasks = response.data.tasks;
            $('#TaskMainBeforeLoading').removeAttr('hidden');
            $('#TaskMainLoading').attr('hidden','true');

        }, function myError(response) {
            showErrors(response);
        });
    }

    $scope.viewMoreTasks=function(){
        $('#viewMoreTaskBeforeLoading').attr('hidden','true');
        $('#viewMoreTaskLoading').removeAttr('hidden');
        $http({
            method : "POST",
            url : "/class/"+$rootScope.drive_id+"/task/viewmoretasks",
            data:{'id':$scope.tasks.slice(-1)[0].id}
        }).then(function mySuccess(response) {
            if(response.data.tasks.length>0)
            {
                $scope.tasks=$scope.tasks.concat(response.data.tasks);
                $('#viewMoreTaskBeforeLoading').removeAttr('hidden');
                $('#viewMoreTaskLoading').attr('hidden','true');
            }
            else{
                $('#noMoreTasks').removeAttr('hidden');
                $('#viewMoreTaskLoading').attr('hidden','true');
            }
            
        }, function myError(response) {

            showErrors(response);
            $('#viewMoreTaskBeforeLoading').removeAttr('hidden');
            $('#viewMoreTaskLoading').attr('hidden','true');
        });
    }

    $scope.open=function(){   
        $rootScope.openFile($rootScope.entity.id);
    }
    
    $scope.uploadTask = function(){
        if(!$scope.file) return;
        var formData = new FormData();
        formData.append("file",$scope.file);
        formData.append('collection_id',$rootScope.entity.id);
        $('#uploadFileBeforeLoading').attr('hidden','true');
        $('#uploadFileLoading').removeAttr('hidden');
        $http.post("/class/"+$rootScope.drive_id+"/task/sendSubmission", formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         }).then(function success(response){
            if(response.data != 'late'){
                for(var i=0;i<$scope.tasks.length;i++){
                    if($scope.tasks[i].id == formData.get('collection_id')){
                        $scope.tasks[i].mysubmission = response.data;
                    }
                }
            }
            $('#uploadFileBeforeLoading').removeAttr('hidden');
            $('#uploadFileLoading').attr('hidden','true');
            $('#uploadFile').modal('hide');
        }, function error(response){
            $('#uploadFileBeforeLoading').removeAttr('hidden');
            $('#uploadFileLoading').attr('hidden','true');
        });
    }

    $scope.editUploadedTask = function(){
        if(!$scope.file) return;
        var formData = new FormData();
        formData.append("file",$scope.file);
        formData.append('submission_id',$rootScope.entity.id);
        $('#editFileBeforeLoading').attr('hidden','true');
        $('#editFileLoading').removeAttr('hidden');
        $http.post("/class/"+$rootScope.drive_id+"/task/editSubmission", formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
         }).then(function success(response){
            if(response.data != 'late'){
                for(var i=0;i<$scope.tasks.length;i++){
                    if($scope.tasks[i].mysubmission && $scope.tasks[i].mysubmission.id == formData.get('submission_id')){
                        $scope.tasks[i].mysubmission = response.data;
                    }
                }
            }
            $('#editFileBeforeLoading').removeAttr('hidden');
            $('#editFileLoading').attr('hidden','true');
            $('#editFile').modal('hide');
            
        }, function error(response){
            $('#editFileBeforeLoading').removeAttr('hidden');
            $('#editFileLoading').attr('hidden','true');
        });
    }
});

angular.element('.task-list').on('contextmenu', '.task-list-item', function(e) {
    
    var menu = angular.element('#context-menutask');
    
    
    if (e.pageX >= window.innerWidth - menu.width()) {
        e.pageX -= menu.width();
    }
    if (e.pageY >= window.innerHeight - menu.height()) {
        e.pageY -= menu.height();
    }

    var l = e.pageX;
    var t = e.pageY + menu.height();
    
    menu.hide().css({
        left: l,
        top: t
    }).appendTo('body').show();
    e.preventDefault();
});

c.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                });
            });
        }
    }
}]);